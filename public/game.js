// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Where all the in-game components fit together.
 * The in-game components include the world, the player,
 * the controls and the chatbox among other things.
 * The login screen does not belong here.
 * The Game object is created on login.
 */

"use strict";

class Game{
    constructor(
        cli,
        guiterminal,
        socket,
        keys,
        settings,
        layout,
        commands
    ){
        this.socket = socket;
        this.cli = cli;
        this.settings = settings;
        this.layout = layout;
        this.player = new Player(undefined, this.settings);

        this.map = new Map();

        this.guiterminal = guiterminal;

        this.hotbarInvComponent = new HotbarInventoryComponent(this.player);
        this.invComponent = new MainInventoryComponent(this.player, this.hotbarInvComponent);
        this.chestComponent = new ChestInventoryComponent(this.player);
        this.craftComponent = new CraftInventoryComponent(this.player);
        this.deleteComponent = new DeleteInventoryComponent(this.player);

        this.hotbarComponent = new HotbarComponent(
            this.invComponent, this.player);

        this.helpComponent = new HelpComponent(this.player);

        this.friendlistComponent = new FriendlistComponent(this.player, this.socket);

        this.notify = new Notify();

        this.notifyComponent = new NotifyComponent(this.notify);

        this.aaterminalComponent = new AATerminalComponent(
            this.player,
            this.map,
            0,
        );

        this.aafileComponent = new AAFileComponent(
            this.player,
            this.map,
            0,
        );

        this.components = [
            this.hotbarInvComponent,
            this.invComponent,
            this.chestComponent,
            this.craftComponent,
            this.deleteComponent,
            this.hotbarComponent,
            this.helpComponent,
            this.friendlistComponent,
            this.notifyComponent,
            this.aaterminalComponent,
            this.aafileComponent,
        ];

        this.syncher = new Syncher(
            this.map,
            this.player,
            this.socket,
            this.cli,
            this.notify,
            commands.commandDatabase,
        );

        this.gui = new Gui(
            guiterminal,
            this.map,
            this.player,
            this.components,
            this.hotbarComponent,
            this.notifyComponent,
            this.settings,
            this.notify,
        );

        this.guiAnimation = new GuiAnimation(
            guiterminal,
            this.map,
            this.player
        );
        this.guiCredits = new GuiCredits(guiterminal);
        this.commands = commands;
        this.active = false;
        this.inChatbox = false;
        this.keys = keys; // holds current states of keyboard
        this.pendingKeyCallback = undefined; // used for key recording

        this.player.mousex = 0;
        this.player.mousey = 0;

        this.temporarilyDisableEating = 0;

        this.preparedInventoryActions = [];

        this.animationRunning = false;

        this.creditsIndex = -1;
        this.creditsIndexMod = 0;

        this.promptBlockx = 0;
        this.promptBlocky = 0;
        this.placedText = '';
        this.interactedTextx = 0;
        this.interactedTexty = 0;
        this.interactedText = '';
        this.hasCancellableCliPrompt = false;

        this.itemToPickNextTick = null;

        // socket event handlers
        this.socket.on('t', this.socketTHandler = data=>this.onSocketTerrainUpdate(data, socket));
        this.socket.on('chat', this.socketChatHandler = data=>this.onSocketChat(data, socket));
        this.socket.on('typing', this.socketTypingHandler = data=>this.onSocketTyping(data, socket));
        this.socket.on('forgrepl', this.socketForgHandler = data=>this.onSocketForg(data, socket));

        // resize event handlers in terminal
        this.guiterminal.addResizeEventListener((w,h)=>{
            for (const component of this.components) {
                component.resizeTerminal(w, h);
            }
            this.redisplay()
        });

        // mouse event handlers
        window.addEventListener('mousemove', this.mouseMoveHandler = e=>this.handleMouseMove(e), false);
        window.addEventListener('wheel', this.wheelHandler = e=>this.handleWheel(e), false);

        // keyboard has no event handlers here. It is polled by the game loop.

        // chatbox starts waiting for something to be typed
        // (new thread is created here)
        this.chatboxLoop();
        this.blurChatbox();

        this.audio_pop = new Audio('audio/pop2.ogg');
        this.popTimeout = false;

        this.lastHorizontalMove = 0;

        this.previousFoodState = 1;

        this.cli.typingIndicatorCallback = (a) => this.updateTypingIndicator(a);

    }

    kill(){
        // important: turn off all the sockets because this game object is redundant!
        this.socket.off('t', this.socketTHandler);
        this.socket.off('chat', this.socketChatHandler);
        this.socket.off('typing', this.socketTypingHandler);
        this.socket.off('forgrepl', this.socketForgHandler);
        // also window events
        window.removeEventListener('mousemove', this.mouseMoveHandler);
        window.removeEventListener('mousedown', this.mouseDownHandler);
        window.removeEventListener('mouseup', this.mouseUpHandler);
        window.removeEventListener('wheel', this.wheelHandler);
    }

    onSocketTerrainUpdate(data){
        this.syncher.serverEvent(data);
    }

    onSocketChat(data){
        if(this.active){
            if (data.timestamp === true) {
                this.printWithTimestamp(data.formatted);
            }
            else {
                // without timestamp
                this.cli.println(data.formatted);
            }
            if (data.notify === true && this.settings.s['twirt']) {
                this.playPop();
            }
            if (data.sender) {
                // initialize empty if not exist
                if (!this.player.messageList[data.sender]) {
                    this.player.messageList[data.sender] = [];
                }
                this.player.messageList[data.sender].push({
                    message: data.raw,
                    color: data.color,
                    timeout: 100,
                });
            }
        }
    }

    onSocketTyping(data) {
        if (!this.active) return;
        const name = data.name;
        const type = data.type;
        const content = data.content;
        if (name in this.player.typingList) {
            // extend typing animation
            this.player.typingList[name].type = type;
            this.player.typingList[name].duration = 50;
            this.player.typingList[name].content = content;
            return;
        } else {
            // create new typing animation
            this.player.typingList[name] = {
                type: type,
                duration: 50,
                animationFrame: 0,
                content: undefined,
            }
        }
    }

    onSocketForg(data){
        if(this.pendingForgCallback){
            // resolve the event with received data
            this.pendingForgCallback(data);
        }
    }

    printWithTimestamp(message, color) {
        const date = new Date();
        const hour = date.getHours();
        let min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;
        const timestamp = hour + ':' + min + ' ';
        message = '\\[#444\\]'+timestamp+'\\[/\\]' + message;
        this.cli.println(message);
    }

    playPop() {
        if (this.popTimeout === true) {
            // was already a pop a second ago.
            return;
        }
        this.popTimeout = true;
        if (this.audio_pop.paused) {
            this.audio_pop.play();
        }
        else {
            this.audio_pop.currentTime = 0;
        }
        setTimeout(() => this.popTimeout = false, 1000);
    }

    // grabbing key for !map
    recordKey(){
        return new Promise((resolve, reject)=>{
            // prepare the callback for this event:
            this.pendingKeyCallback = key=>{
                this.pendingKeyCallback = undefined;
                return resolve(key);
            }
        });
    }

    updateTypingIndicator(typed) {
        if (this.settings.s['typingnotifications'] === false) return;
        if (typed.length < 1) {
            this.socket.emit('typing', {type: 'stop'});
            return;
        }
        if (this.hasCancellableCliPrompt) {
            this.socket.emit('typing', {type: 'stop'});
            return;
        }
        // TODO: this should probably be filtered a bit, so we don't include the command (for example /local) as part of the message preview
        const content = this.settings.s['revealbeforesending'] ? this.cli.editable : undefined;
        // regexes are similar to cli.js
        // but not exact, because sometimes the command gets colored
        // but we don't want to send a typing notification yet
        if (/^\/(set)?global |^\/(set)?public /.test(typed)) {
            this.socket.emit('typing', {type: 'global', content: content});
            return;
        }
        if (/^\/(set)?local |^\/l /.test(typed)) {
            this.socket.emit('typing', {type: 'local', content: content});
            return;
        }
        if (/^\/w(hisper)? /.test(typed)) {
            // parsing arguments (copied from clientcommands.js)
            let args = typed.split(/ +(?=(?:(?:[^"]*"){2})*[^"]*$)/g);
            args = args.map(a=>a.replace(/"/g,''));
            args = args.filter(a => a.length > 0);
            if (args.length < 3) {
                // No recepient or message. No indicator
                this.socket.emit('typing', {type: 'stop'});
                return;
            }
            this.socket.emit('typing', {type: 'whisper',
                recipient: args[1], content: content});
            return;
        }
        if (/^\/rainbow(me)? /.test(typed)) {
            this.socket.emit('typing', {type: 'rainbow', content: content});
            return;
        }
        if (/^\/me /.test(typed)) {
            this.socket.emit('typing', {type: 'global', content: content});
            return;
        }
        if (typed[0] === '/') {
            // it is an unrelated command. No indication
            return;
        }
        switch (this.player.chatScope) {
            case 'global':
                if (/^L /.test(typed)) {
                    this.socket.emit('typing', {type: 'local', content: content});
                    return
                }
                this.socket.emit('typing', {type: 'global', content: content});
                return;
            case 'local':
                this.socket.emit('typing', {type: 'local', content: content});
                return;
            case 'whisper':
                this.socket.emit('typing', {type: 'whisper',
                    recipient: this.player.whisperTarget, content: content});
                return;
        }
    }

    async chatboxLoop(){
        let exitloop = false;
        while(!exitloop){
            const initialPromptType = this.choosePromptStyle();
            const typed = await this.cli.promptCommand(
                initialPromptType, false, 10000, ['#444'])
                .catch(error => exitloop = true);
            if (exitloop) continue;
            this.keys.clearFresh();
            if(/^\//.test(typed)){
                // is a command (client or server)
                await this.commands.executeCommand(typed);
            }
            else if (typed.length > 0) {
                // is chat
                const message = this.adjustMessageToScope(typed);
                this.socket.emit('chat', {message: message});
            }
            // stop focusing the cli
            this.blurChatbox();
        }
    }

    choosePromptStyle() {
        const promptChar = this.settings.s['revealbeforesending'] ? '!' : '>';
        switch (this.player.chatScope) {
            case 'global':
                return promptChar + ' ';
            case 'local':
                return '(L)' + promptChar + ' ';
            case 'whisper':
                return '(-> '+this.player.whisperTarget+')' + promptChar + ' ';
        }
        return '?' + promptChar + ' ';
    }

    adjustMessageToScope(typed) {
        switch (this.player.chatScope) {
            case 'global':
                return typed;
            case 'local':
                return 'L ' + typed;
            case 'whisper':
                return '/whisper '+this.player.whisperTarget+' '+typed;
        }
        return typed;
    }

    focus(){
        // window focused
        if(this.inChatbox){
            this.cli.focus();
        }
        else{
            this.gui.focus();
        }
    }

    blur(){
        // window unfocused
        this.gui.blur();
        this.cli.blur();
    }

    focusChatbox() {
        this.blurAAterminal();
        this.inChatbox = true;
        this.cli.focus();
        this.cli.unpause();
        this.cli.scrollToEnd();
    }

    blurChatbox() {
        this.inChatbox = false;
        this.cli.blur();
        this.cli.pause();
        if (this.hasCancellableCliPrompt) {
            cli.println('cancelled.');
            this.cli.abort();
            this.chatboxLoop();
            this.hasCancellableCliPrompt = false;
        }
    }

    cycleAAterminalFocus() {
        this.player.focusedAAterminal++;
        if (this.player.focusedAAterminal >=
            this.player.openedAAterminals.length) {
            this.player.focusedAAterminal = -1;
        }
    }

    focusAAterminal(n) {
        this.blurChatbox();
        this.player.focusedAAterminal = n;
    }

    blurAAterminal() {
        this.player.focusedAAterminal = -1;
    }

    handleKeyDown(keycode, key){

        // top priority to complete ignorance
        // (when game is not active)
        if(!this.active){
            return false;
        }

        // next by priority is key recording.
        // if recording keys, don't do anything else.
        if(this.pendingKeyCallback){
            // resolve the callback with the key that was just pressed
            this.pendingKeyCallback(keycode);
            return true;
        }

        // after that, priority goes to chatbox
        if(this.inChatbox){
            if (this.layout.mappings[keycode] === 'escape') {
                this.blurChatbox();
            }
            if (keycode === 17) return false;  // hotfix to allow chatbox to remember the state of ctrl (for ctrl+v)
            return true;
        }

        if (this.player.uiState === 'friendlist'
            && this.layout.mappings[keycode] === 'escape') {
            this.player.uiState = 'normal';
            return true;
        }

        // is help visible?
        if (this.player.uiState === 'help') {
            // every key closes help
            this.player.uiState = 'normal';
            // skip everything else (skip default function of key)
            return true;
        }

        if (this.player.focusedAAterminal > -1) {
            // some mapped keys still apply
            const mapping = this.layout.mappings[keycode];
            if (mapping === 'escape') {
                this.blurAAterminal();
                return true;
            }
            if (mapping === 'inspect') {
                this.cycleAAterminalFocus();
                return true;
            }
            // handle key typed into aaterminal
            const typedCharacters = AATERMINAL_TYPED_CHARACTERS[key];
            if (typedCharacters === undefined) return false;
            const wx = this.player.openedAAterminals[
                this.player.focusedAAterminal].x;
            const wy = this.player.openedAAterminals[
                this.player.focusedAAterminal].y;
            this.socket.emit('aatype', {
                x: wx,
                y: wy,
                text: typedCharacters,
            });
            return true;
        }

        // these bindings work the same whether or not inventory is open:
        switch (this.layout.mappings[keycode]) {

            case 'friendlist':
                // toggle friend list
                if (this.player.uiState === 'friendlist') {
                    this.player.uiState = 'normal';
                } else {
                    this.player.uiState = 'friendlist';
                }
                return true;

            case 'help':
                this.player.uiState = 'help';
                return true;

            case 'chat':
                this.focusChatbox();
                return true;

            case 'command':
                this.focusChatbox();
                this.cli.clearCmd();
                this.cli.handleKey('/');
                return true;

            case 'inspect':
                this.cycleAAterminalFocus();
                return false;

            case 'hotbarnext':
                this.hotbarComponent.cursorUp();
                if (this.player.uiState === 'overview') {
                    this.player.uiState = 'normal';
                }
                this.redisplay();
                return true;

            case 'hotbarprev':
                this.hotbarComponent.cursorDown();
                if (this.player.uiState === 'overview') {
                    this.player.uiState = 'normal';
                }
                this.redisplay();
                return true;

            case 'hotkey0': this.hotkeyPressed(9); return true;
            case 'hotkey1': this.hotkeyPressed(0); return true;
            case 'hotkey2': this.hotkeyPressed(1); return true;
            case 'hotkey3': this.hotkeyPressed(2); return true;
            case 'hotkey4': this.hotkeyPressed(3); return true;
            case 'hotkey5': this.hotkeyPressed(4); return true;
            case 'hotkey6': this.hotkeyPressed(5); return true;
            case 'hotkey7': this.hotkeyPressed(6); return true;
            case 'hotkey8': this.hotkeyPressed(7); return true;
            case 'hotkey9': this.hotkeyPressed(8); return true;
        }

        // is inventory visible?
        if (this.player.uiState === 'inventory'
            || this.player.uiState === 'chest') {
            // these keys get special function in inventory
            switch (this.layout.mappings[keycode]) {
                case 'up': case 'cursorup':
                    this.player.usingmousecontrols = false;
                    this.player.mouserecentlymoved = false;
                    this.player.invCursorUp();
                    this.redisplay();
                    break;
                case 'down': case 'cursordown':
                    this.player.usingmousecontrols = false;
                    this.player.mouserecentlymoved = false;
                    this.player.invCursorDown();
                    this.redisplay();
                    break;
                case 'right': case 'cursorright':
                    this.player.usingmousecontrols = false;
                    this.player.mouserecentlymoved = false;
                    this.player.invCursorRight();
                    this.redisplay();
                    break;
                case 'left': case 'cursorleft':
                    this.player.usingmousecontrols = false;
                    this.player.mouserecentlymoved = false;
                    this.player.invCursorLeft();
                    this.redisplay();
                    break;
                case 'place': {
                        this.player.usingmousecontrols = false;
                        this.player.mouserecentlymoved = false;
                        const id = this.player.hoverSlotId;
                        if (id === null) break;  // no hovered item
                        const shift = this.keys.getFullKeyStates()[16];
                        const rmb = true;
                        const lmb = !rmb;
                        this.handleInventorySlotClicked(id, shift, lmb, rmb);
                    }
                    break;
                case 'jump': {
                        this.player.usingmousecontrols = false;
                        this.player.mouserecentlymoved = false;
                        const id = this.player.hoverSlotId;
                        if (id === null) break;  // no hovered item
                        const shift = this.keys.getFullKeyStates()[16];
                        const rmb = false;
                        const lmb = !rmb;
                        this.handleInventorySlotClicked(id, shift, lmb, rmb);
                    }
                    break;
                case 'inventory': case 'escape':
                    // keys that exit inventory
                    this.closeInventory();
                    this.redisplay();
                    break;
            }
            if (key === 'PageUp') {
                for (let i = 0; i < 5; i++) {
                    this.player.invCursorUpNoWrap();
                }
            }
            if (key === 'PageDown') {
                for (let i = 0; i < 5; i++) {
                    this.player.invCursorDownNoWrap();
                }
            }
            if (key === 'Home') {
                this.player.invCursorHome();
            }
            if (key === 'End') {
                this.player.invCursorEnd();
            }
            return false;
        }

        switch (this.layout.mappings[keycode]) {

            case 'togglemap':
                if (this.player.uiState === 'overview') {
                    this.player.uiState = 'normal';
                } else {
                    this.player.uiState = 'overview';
                }
                return true;

            case 'togglecameralock':
                this.player.cameralocked = !this.player.cameralocked;
                if (this.player.cameralocked) {
                    this.player.camerax = this.player.x;
                    this.player.cameray = this.player.y;
                    this.cli.println('\\[#444\\]camera locked');
                } else {
                    this.cli.println('\\[#444\\]camera on player');
                }
                return true;

            case 'zoomout':
                if (this.player.uiState === 'overview') {
                    this.gui.overviewzoom *= 2;
                    if (this.gui.overviewzoom > 16) {
                        this.gui.overviewzoom = 16;
                    }
                }
                else {
                    // also enters overview,
                    // but always at lowest level
                    this.player.uiState = 'overview';
                    this.gui.overviewzoom = 2;
                    this.redisplay();
                }
                return true;

            case 'zoomin':
                if (this.player.uiState === 'overview') {
                    this.gui.overviewzoom /= 2;
                    if (this.gui.overviewzoom < 2) {
                        this.gui.overviewzoom = 2;
                        // zooming brings out out of the overview
                        this.player.uiState = 'normal';
                    }
                }
                return true;

            case 'inventory':
                // open inventory
                this.player.uiState = 'inventory';
                return true;

            case 'escape':
                this.player.uiState = 'normal';
                return true;

        }
        return false;
    }

    closeInventory() {
        this.player.uiState = 'normal';
        this.player.draggedAmount = 0;
        this.player.dragFromId = null;
    }

    hotkeyPressed(nkey) {
        this.hotbarComponent.cursorSet(nkey);
        if (this.player.uiState === 'overview') {
            this.player.uiState = 'normal';
        }
        this.redisplay();
    }

    handleKeyUp(keycode){
    }

    prefillChatbox(text) {
        this.cli.setEditable(text);
        this.focusChatbox();
    }

    gameTick(){
        const fullKeyStates = this.keys.getFullKeyStates();
        const fullMbStates = this.keys.getFullMbStates();
        const fullFreshKeys = this.keys.freshKeys;
        fullKeyStates[2001] = fullMbStates[1];
        fullKeyStates[2002] = fullMbStates[2];
        fullKeyStates[2003] = fullMbStates[3];
        fullFreshKeys[2001] = this.keys.freshMbs[1];
        fullFreshKeys[2002] = this.keys.freshMbs[2];
        fullFreshKeys[2003] = this.keys.freshMbs[3];

        if(!this.active){
            return;
        }

        const tick = {};

        this.player.inspectKeyPressed = false;

        if (this.itemToPickNextTick !== null) {
            this.hotbarComponent.selectItemByCode(this.itemToPickNextTick);
            this.itemToPickNextTick = null;
        }

        // pass prepared inventory actions
        if (this.preparedInventoryActions.length > 0) {
            tick.inv = this.preparedInventoryActions;
            this.preparedInventoryActions = [];
        }

        if(!this.inChatbox) {

            // iterate over defined keys
            for (const i in fullKeyStates) {

                // skip if fake property
                if (!fullKeyStates.hasOwnProperty(i)) continue;

                // skip if not pressed
                if (!fullKeyStates[i]) continue;

                // make tab list visible (before checking if inventory open)
                if (this.layout.mappings[i] === 'inspect') {
                    this.player.inspectKeyPressed = true;
                }

                // is any inventory UI visible?
                if (this.player.uiState === 'inventory'
                    || this.player.uiState === 'chest') {
                    continue;
                }
                switch (this.layout.mappings[i]) {

                    case 'down':
                        tick.d = 1;
                        break;

                    case 'left':
                        tick.l = 1;
                        this.lastHorizontalMove = -1;
                        break;

                    case 'right':
                        tick.r = 1;
                        this.lastHorizontalMove = 1;
                        break;

                    case 'jump': case 'up':
                        tick.u = 1;
                        tick.j = 1;
                        break;

                    case 'sprint':
                        tick.s = 2;
                        if (this.player.gamemode === 2) {
                            tick.s = this.settings.eqs['spectatorspeed'];
                        }
                        break;

                    case 'cursorup':
                        this.player.cursorUp();
                        this.player.usingmousecontrols = false;
                        this.player.mouserecentlymoved = false;
                        break;

                    case 'cursordown':
                        this.player.cursorDown();
                        this.player.usingmousecontrols = false;
                        this.player.mouserecentlymoved = false;
                        break;

                    case 'cursorleft':
                        this.player.cursorLeft();
                        this.player.usingmousecontrols = false;
                        this.player.mouserecentlymoved = false;
                        break;

                    case 'cursorright':
                        this.player.cursorRight();
                        this.player.usingmousecontrols = false;
                        this.player.mouserecentlymoved = false;
                        break;

                    case 'dig':
                        if (this.maybeCollectDetector()) break;
                        if (this.maybeCollectSSPOOKY()) break;
                        this.diggment(tick);
                        break;

                    case 'place':
                        if (this.hotbarComponent.getSelected() !== null) {
                            this.placement(tick);
                        }
                        if (fullFreshKeys[i]) {
                            if (this.maybeCollectDetector()) break;
                            if (this.maybeCollectSSPOOKY()) break;
                            // trigger forg without awaiting
                            // (can't slow down this function)
                            this.maybeUseForg();
                            // interactment
                            tick.I = {
                                x: this.player.cursorx,
                                y: this.player.cursory,
                                s: this.hotbarComponent.cursor,
                            };
                        }
                        break;

                    case 'eat':
                        tick.e = this.hotbarComponent.cursor;
                        break;

                    case 'pick':
                        const x = this.player.x + this.player.mousex;
                        const y = this.player.y + this.player.mousey;
                        const block = this.map.getBlock(x, y);
                        const item = Inventory.block2item(block);
                        if (item === ' ') {
                            // Attempt to select nothing
                            // (only works if there is an empty slot)
                            this.hotbarComponent.selectItemByCode(null)
                        } else {
                            if (!this.hotbarComponent.selectItemByCode(item)) {
                                tick.M = {
                                    i: item,
                                    s: this.hotbarComponent.cursor,
                                };
                                this.itemToPickNextTick = item;
                            }
                        }
                        break;

                }
            }
        }
        // check if prompt for ? or sign finished:
        if (this.placedText !== '') {
            tick.P = {
                x: this.promptBlockx,
                y: this.promptBlocky,
                s: -1,
                placedtext: this.placedText,
            }
            this.placedText = '';
        }
        // check if prompt for forg finished:
        if (this.interactedText !== '') {
            tick.I = {
                x: this.interactedTextx - this.player.x,
                y: this.interactedTexty - this.player.y,
                s: this.hotbarComponent.cursor,
                interactedtext: this.interactedText,
            }
            this.interactedText= '';
        }
        // check if on ice:
        if ((this.map.getBlock(this.player.x, this.player.y) === 'Wi'
            || this.map.getBlock(this.player.x, this.player.y-1) === 'Wl'
            || this.map.getBlock(this.player.x, this.player.y-1) === 'Wc')
            && this.player.gamemode === 0
        ) {
            if (this.lastHorizontalMove === 1 && tick.l !== 1) {
                tick.r = 1;
            }
            else if (this.lastHorizontalMove === -1 && tick.r !== 1) {
                tick.l = 1;
            }
        }

        if (this.player.warpWarmup === 20) {
            tick.W = 1;
            this.player.warpCooldown = this.player.TOTALWARPCOOLDOWN;
        }

        if (this.player.usingmousecontrols) {
            this.player.snapCursorToMouse();
        }

        this.syncher.executeTick(tick);

        if (this.player.usingmousecontrols) {
            this.player.snapCursorToMouse();
        }

        this.keys.clearFresh();

        if (this.player.warpCooldown > 0 &&
            this.map.isBlockLoaded(this.player.x, this.player.y)) {
            this.player.warpCooldown--;
        }

        if (this.player.warpCooldown === 0
            && this.map.getBlock(this.player.x, this.player.y-1) === 'WW'
            && this.map.getBlock(this.player.x, this.player.y+5) === 'R1') {
            if (this.player.warpWarmup < 20) {
                this.player.warpWarmup++;
            }
        } else {
            this.player.warpWarmup = 0;
        }

        this.redisplay();

        // TODO: this paragraph is all temporary and bad code.
        if (this.player.gamemode === 0
            && this.map.getBlock(this.player.x, this.player.y) === 'WO') {
            if (!this.player.onTentacle) {
                this.cli.println('\\[#aa3\\]Tentacle grabs you!');
                this.player.onTentacle = true;
            }
        }
        else {
            this.player.onTentacle = false;
        }

        if (this.previousFoodState > 0
            && this.player.food === 0) {
            this.cli.println('\\[#aa3\\]no energy. \\[#ff6\\]C\\[#aa3\\] to eat');
            this.notify.send('out of energy')
        }
        this.previousFoodState = this.player.food;

    }

    maybeCollectDetector() {
        // check if player's cursor is on a detector
        const wx = this.player.x + this.player.cursorx;
        const wy = this.player.y + this.player.cursory;
        for (const detector of this.player.visibleDetectors) {
            if (wx !== detector.x || wy !== detector.y) continue;
            // the click was on a detector
            const follow = this.player.inventory.hasItem('Wo')
                && this.hotbarComponent.getSelected() === 'Wo';
            this.socket.emit('iclickeddetector', {
                x: wx,
                y: wy,
                follow: follow,
            });
            return true;
        }
        return false;
    }

    maybeCollectSSPOOKY() {
        // need to be holding detector
        if (!this.player.inventory.hasItem('E')) return false;
        if (this.hotbarComponent.getSelected() !== 'E') return false;
        // check if player's cursor is on a ghost
        const wx = this.player.x + this.player.cursorx;
        const wy = this.player.y + this.player.cursory;
        for (const ghost of this.player.visibleGhosts) {
            if (wx !== ghost.x || wy !== ghost.y) continue;
            this.socket.emit('iclickedsspooky', {
                x: wx,
                y: wy,
            });
            return true;
        }
        return false;
    }

    async maybeUseForg() {
        // check if player's cursor is on forg
        const wx = this.player.x + this.player.cursorx;
        const wy = this.player.y + this.player.cursory;
        if (this.map.getBlock(wx, wy) !== 'WF') return false;
        const slot_index = this.hotbarComponent.cursor;
        const item = this.player.inventory.getSlotAt(slot_index);
        if (item === null) {
            this.notify.send('no item selected');
            return false;
        }
        const item_code = item.code;
        if (item_code[0] !== 'G') {
            this.notify.send('item not repairable');
            return false;
        }
        // must not have max durability
        const max = Inventory.item_data_templates[item_code].d;
        if (item.data.d === max) {
            this.notify.send('item already full');
            return false;
        }
        if (item.data.d >= max) {
            this.notify.send('you silly');
            return false;
        }
        // need to abort whatever's going on in the chatbox
        this.cli.abort();
        // now focus it
        this.focusChatbox();
        this.hasCancellableCliPrompt = true;
        // repeat the prompt for word
        while (true) {
            const word = await this.cli.prompt('type four char word: ', false, 4);
            if (word === undefined) break;
            if (word.length < 1) break;
            if (!/^[a-z][a-z][a-z][a-z]$/.test(word)) continue;
            // send query to server to check this word
            this.socket.emit('forgtest', {
                x: wx,
                y: wy,
                word: word,
            });

            const resp = await new Promise((resolve, reject)=>{
                // prepare the callback for this event:
                this.pendingForgCallback = resp=>{
                    this.pendingForgCallback = undefined;
                    return resolve(resp);
                }
            });
            if (resp.x !== wx) return false;
            if (resp.y !== wy) return false;
            if (resp.word !== word) return false;
            if (!resp.free) {
                this.cli.println('they '+resp.fail+' this word');
                continue;
            }

            // by setting interactedText, we trigger event next tick
            this.interactedText = word;
            this.interactedTextx = wx;
            this.interactedTexty = wy;
            this.hasCancellableCliPrompt = false;
            this.chatboxLoop();
            this.blurChatbox();
            return true;
        }
    }

    redisplay(){
        if (this.player.usingmousecontrols) {
            this.player.snapCursorToMouse();
        }

        if (this.creditsIndex > -1) {
            if (Math.floor(this.creditsIndex/20)%3 === 0) {
                this.creditsIndexMod += 2;
            }
            const successDisplay = this.guiCredits.display(this.creditsIndexMod);
            this.creditsIndex++;
            if (!successDisplay) {
                this.creditsIndex = -1;
                this.creditsIndexMod = 0;
            }
        }
        else if (this.player.animation) {
            // skip regular displaying
            if (!this.animationRunning) {
                // start the animation since it's not running already.
                this.animationRunning = true;
                this.guiAnimation.ghostAnimation(this.player.prevx, this.player.prevy, this.player.x, this.player.y).then(result => {
                    this.player.animation = false;
                    this.animationRunning = false;
                });
            }
        }
        else {
            this.gui.display();
        }
    }

    handleMouseMove(e){
        const cxy = this.guiterminal.pixelToChar(e.clientX, e.clientY);
        this.player.mousex = cxy.x;
        this.player.mousey = cxy.y;
        this.player.snapCursorToMouse();

        const xy = this.guiterminal.pixelToChar(e.clientX, e.clientY, false);
        this.player.isCurrentlyHovering = false;
        for (const component of this.components) {
            if (!component.isInventory()) continue;
            const id = component.getSlotIdAtScreenCoordinates(xy.x, xy.y);
            if (id === null) continue;  // no item hovered (in this ui comp)
            this.player.hoverSlotId = id;
            this.player.isCurrentlyHovering = true;
        }
    }

    handleMouseDown(e){
        let keycode = 2001;
        if (e.buttons > 1) {
            keycode = 2002;
        }
        if (e.buttons > 3) {
            keycode = 2003;
        }

        // if recording keys, don't do anything else.
        if(this.pendingKeyCallback){
            // resolve the callback with the key that was just pressed
            this.pendingKeyCallback(keycode);
            return true;
        }

        // check if click is in chat box
        const xy = this.guiterminal.pixelToChar(
            e.clientX, e.clientY, false);
        if (xy.x < 0 && !this.inChatbox) {
            this.focusChatbox();
            return true;
        }
        // or out of chat box
        if (xy.x >= 0 && this.inChatbox) {
            this.blurChatbox();
            return false;
        }

        const x = xy.x;
        const y = xy.y;

        if (this.hotbarComponent.isClickWithin(x, y)) {
            this.hotbarComponent.handleClick(x, y)
            // An ugly hack to prevent regular function on this click:
            this.keys.mbStates = [];
            this.keys.freshMbs = [];
            this.keys.freshMbList = [];
            return true;
        }

        if (this.friendlistComponent.handleClick(x, y)) {
            return true;
        }

        // TODO: THIS CODE IS VERY DISORGANIZED!!!
        if (this.aaterminalComponent.isClickWithin(x, y)) {
            if (this.aaterminalComponent.handleClick(x, y)) {
                this.blurAAterminal();
            } else {
                this.focusAAterminal(0);
            }
            return true;
        } else {
            // player wants to unfocus aaterminal
            this.blurAAterminal();
        }

        for (const component of this.components) {
            if (!component.isInventory()) continue;
            const id = component.getSlotIdAtScreenCoordinates(x, y);
            if (id === null) continue;  // no item clicked (in this ui comp)
            const shift = this.keys.getFullKeyStates()[16];
            const rmb = e.buttons > 1;
            const lmb = !rmb;
            this.handleInventorySlotClicked(id, shift, lmb, rmb);
            return true;
        }

        return false;
    }

    handleInventorySlotClicked(id, shift, lmb, rmb) {
        // Already dragging some item with mouse?
        const alreadyDragging = this.player.dragFromId !== null
        // Which item was clicked just now?
        const clickedItem = this.player.getItemBySlotId(id);
        // left click on any slot while dragging item:
        if (alreadyDragging) {
            const idSplit = id.split('_');
            if (idSplit[0] === 'recipe'
                && id === this.player.dragFromId) {
                // recipe clicked multiple times.
                // Instead of dropping, pick up more.
                const recipeIndex = parseInt(idSplit[1]);
                if (this.player.inventory.howManyRecipesCanExpend(
                    recipeIndex) > this.player.draggedAmount) {
                    this.player.draggedAmount++;
                }
                return;
            }
            if (id === this.player.dragFromId) {
                // stop dragging
                this.player.draggedAmount = 0;
                this.player.dragFromId = null;
                return;
            }
            // drop item(s)
            let droppedAmount = lmb ? this.player.draggedAmount : 1;
            const maxPossibleMove = this.howManyCanTransfer(this.player.dragFromId, id);
            if (droppedAmount > maxPossibleMove) {
                droppedAmount = maxPossibleMove;
            }
            if (droppedAmount < 1) {
                // Impossible to drop these items here.
                // Check if it's possible to swap these slots:
                // If dragging 0, not possible to swap
                if (this.player.draggedAmount < 1) return;
                // TODO: add condition: If dragging less than whole slot, not possible to swap
                // TODO: add condition: If source and target are same item type and neither data, not possible to swap
                // swap slots
                this.preparedInventoryActions.push({
                    s: this.player.dragFromId,
                    d: id,
                    a: 'swap',
                });
                // stop dragging when slots are swapped.
                // TODO: maybe it would be better to immediately begin dragging the same slot, after swapping
                this.player.draggedAmount = 0;
                this.player.dragFromId = null;
                return;
            }
            this.preparedInventoryActions.push({
                s: this.player.dragFromId,
                d: id,
                a: droppedAmount,
            });
            this.player.draggedAmount -= droppedAmount;
            if (this.player.draggedAmount === 0) {
                this.player.dragFromId = null;
            }
            return;
        } else {  // Not already dragging. Just clicking on a slot
            if (clickedItem === null) return;  // no item clicked
            const idSplit = id.split('_');
            if (shift) {
                // was shift-click on item or recipe
                let amount;
                let destination;
                if (idSplit[0] === 'recipe') {
                    // shift-click on recipe crafts as many as possible
                    destination = 'default';
                    amount = this.player.inventory.howManyCanCraft(parseInt(idSplit[1]));
                } else if (idSplit[0] === 'chest') {
                    // shift-click in chest sends whole slot to inventory
                    destination = 'inv_normal';
                    amount = this.howManyCanTransfer(id, destination);
                } else if (idSplit[0] === 'inv') {
                    // shift-click in inventory moves slot left or right
                    if (idSplit[1] < this.player.inventory.numNormalSlots) {
                        // shift-click in main inv sends slot to right side
                        if (this.player.uiState === 'chest') {
                            // Chest is open. Sending into chest
                            destination = 'chest_any';
                        } else {
                            // Chest not open. Sending to crafting
                            destination = 'inv_crafting';
                        }
                    } else {
                        // shift-click in crafting sends slot to main inv
                        destination = 'inv_normal';
                    }
                    // whichever destination was picked,
                    // check how many can be transfered there
                    amount = this.howManyCanTransfer(id, destination);
                } else {
                    // unknown idSplit. No action
                    return;
                }
                // abort if zero items moved
                if (amount === 0) return;
                this.preparedInventoryActions.push({
                    s: id,
                    d: destination,
                    a: amount,
                });
                return;
            }
            // was not shift-click
            if (idSplit[0] === 'recipe') {
                // was normal click on recipe
                const recipeIndex = parseInt(idSplit[1]);
                if (this.player.inventory.howManyRecipesCanExpend(
                    recipeIndex) < 1) return;
            }
            // was normal click on non-recipe slot. Start dragging
            this.player.dragFromId = id;
            this.player.draggedAmount = rmb
                ? Math.ceil(clickedItem.amount/2) : clickedItem.amount;
            return;
        }
    }

    howManyCanTransfer(id_from, id_to) {
        const from = this.parseSlotId(id_from);
        if (from === null) return 0;
        if (id_to === 'inv_normal') {
            // shift-clicking to send items to non-reserved inventory
            return from.inventory.howManyItemsCanMoveTo(
                this.player.inventory, from.index);
        }
        if (id_to === 'inv_crafting') {
            // shift-clicking to send items to crafting slots
            return from.inventory.howManyItemsCanMoveToCraftingSlots(
                this.player.inventory, from.index);
        }
        if (id_to === 'chest_any') {
            // shift-clicking to send items to chest
            if (this.player.uiState !== 'chest') return 0;
            const chest = this.player.chest.inventory;
            return from.inventory.howManyItemsCanMoveTo(
                this.player.chest.inventory, from.index);
        }
        // Transfering to not default slot. Parse it:
        const to = this.parseSlotId(id_to);
        if (to === null) return 0;
        if (to.name === 'recipe') return 0;
        if (from.name === 'recipe') {
            return this.player.inventory.howManyCanCraftIntoSlot(
                from.index, to.index);
        }
        // Normal transfer, slot to slot
        return from.inventory.howManyItemsCanMoveToSpecificSlot(
            to.inventory, from.index, to.index);
    }

    parseSlotId(id) {
        const idSplit = id.split('_');
        if (idSplit[0] === 'inv') {
            return {
                name: 'inventory',
                inventory: this.player.inventory,
                index: parseInt(idSplit[1]),
            }
        }
        if (idSplit[0] === 'chest') {
            return {
                name: 'chest',
                inventory: this.player.chest.inventory,
                index: parseInt(idSplit[1]),
            }
        }
        if (idSplit[0] === 'recipe') {
            return {
                name: 'recipe',
                inventory: null,
                index: parseInt(idSplit[1]),
            }
        }
        return null;
    }

    handleMouseUp(e){
        // let keycode = 2001;
        // if (e.buttons > 1) {
        //     keycode = 2002;
        // }
        // if (e.buttons > 3) {
        //     keycode = 2003;
        // }
        return false;
    }

    handleWheel(e){
        const xy = this.guiterminal.pixelToChar(
            e.clientX, e.clientY, false);
        if (xy.x < 0) {
            // when mouse over chatbox, scroll chatbox
            if(e.deltaY > 0){
                this.cli.bigterminal.scrollDown(3);
            }
            else {
                this.cli.bigterminal.scrollUp(3);
            }
            return;
        }

        if (this.player.gamemode === 2 && this.keys.getFullKeyStates()[16] === true) {
            // spectator mode + holding shift + scrolling
            // adjust spectator speed
            const spectatorSpeed = this.settings.eqs['spectatorspeed'];
            const multiplier = e.deltaY > 0 ? 0.7 : 1.25;
            let newSpectatorSpeed = spectatorSpeed * multiplier;
            newSpectatorSpeed = Math.round(newSpectatorSpeed);
            if (newSpectatorSpeed < 2) newSpectatorSpeed = 2;
            if (newSpectatorSpeed > 256) newSpectatorSpeed = 256;
            if (newSpectatorSpeed === spectatorSpeed) return;
            this.commands.executeCommand('/set ss='+newSpectatorSpeed);
            return;
        }

        if (this.player.uiState === 'normal') {
            // when inventory is closed, scroll hotbar by default
            if (e.deltaY > 0) {
                this.hotbarComponent.cursorDown();
            } else {
                this.hotbarComponent.cursorUp();
            }
            this.redisplay();
            return;
        }

        for (const component of this.components) {
            if (!component.isScrollable()) continue;
            if (!component.isShown()) continue;
            if (!component.isClickWithin(xy.x, xy.y)) continue;
            if (e.deltaY > 0) {
                component.scrollDown();
            } else {
                component.scrollUp();
            }
            this.redisplay();
            return;
        }
    }

    diggment(tick) {
        if (tick.D) {
            // skip because this is already in the tick
            return;
        }
        tick.D = {
            x: this.player.cursorx,
            y: this.player.cursory,
            s: this.hotbarComponent.cursor,
        };
    }

    placement(tick) {
        if (tick.P) {
            // skip because this is already in the tick
            return;
        }
        const slot_index = this.hotbarComponent.cursor;
        tick.P = {
            x: this.player.cursorx,
            y: this.player.cursory,
            s: slot_index,
        }
        const item = this.player.inventory.getSlotAt(slot_index);
        if (item === null) return;
        const item_code = item.code;
        // when placing ?, prompt for block:
        if (item_code === '?'
            && this.player.inventory.hasItem('?')) {
            const x = this.player.x + this.player.cursorx;
            const y = this.player.y + this.player.cursory;
            const space = this.map.getBlock(x, y)
            if(space === ' ' || space === 'w') {
                // need to abort whatever's going on in the chatbox
                this.cli.abort();
                // now focus it
                this.focusChatbox();
                this.promptBlockx = tick.P.x;
                this.promptBlocky = tick.P.y;
                this.hasCancellableCliPrompt = true;
                // display the prompt for block
                this.cli.prompt('pick block: ', false, 1).then(a => {
                    if (a.length > 0) {
                        this.placedText = a[0];
                    }
                    // start the chatbox loop again now
                    this.hasCancellableCliPrompt = false;
                    this.chatboxLoop();
                    this.blurChatbox();
                });
            }
        }
        // when placing sign, prompt for content:
        if (item_code === 'si'
            && this.player.inventory.hasItem('si')) {
            const x = this.player.x + this.player.cursorx;
            const y = this.player.y + this.player.cursory;
            const space = this.map.getBlock(x, y)
            if (space === ' '
                || space === 'w'
                || space === '~'
                || space === 'sn'
                || space[0] === '>') {
                // need to abort whatever's going on in the chatbox
                this.cli.abort();
                // now focus it
                this.focusChatbox();
                this.promptBlockx = tick.P.x;
                this.promptBlocky = tick.P.y;
                this.hasCancellableCliPrompt = true;
                // display the prompt for block
                this.cli.prompt('sign: ', false, 100).then(a => {
                    if (a.length > 0) {
                        this.placedText = a;
                    }
                    // start the chatbox loop again now
                    this.hasCancellableCliPrompt = false;
                    this.chatboxLoop();
                    this.blurChatbox();
                });
            }
        }
        // when placing custom warp, prompt for code:
        if (item_code === 'WV'
            && this.player.inventory.hasItem('WV')) {
            this.promptBlockx = tick.P.x;
            this.promptBlocky = tick.P.y;
            // need to abort whatever's going on in the chatbox
            this.cli.abort();
            this.cli.println('creating warp at '
                + (this.promptBlockx+this.player.x) + ', '
                + (this.promptBlocky+this.player.y) + '...')
            // now focus it
            this.focusChatbox();
            this.hasCancellableCliPrompt = true;
            // display the prompt for block
            this.cli.prompt('choose warp code: ', false, 100).then(code => {
                this.cli.prompt('who can use? owner/friends/all/cancel: ', true, 100).then(s => {
                    const scope = s[0].toLowerCase();
                    const scopeOk = ['o', 'f', 'a'].includes(scope);
                    if (code.length > 0 && scopeOk) {
                        this.placedText = 'WV' + scope + code;
                    } else {
                        this.cli.println('cancelled.');
                    }
                    // start the chatbox loop again now
                    this.hasCancellableCliPrompt = false;
                    this.chatboxLoop();
                    this.blurChatbox();
                });
            });
            // but don't actually try to place the WV block
            delete tick.P;
        }
    }

}
