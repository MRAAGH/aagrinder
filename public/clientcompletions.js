// Copyright (C) 2023-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * The client-side command completion for all commands and arguments
 */

"use strict";

class Completions {

    // completions class has same access as commands class
    constructor(commands) {
        this.commands = commands;
        this.commandDatabase = commands.commandDatabase;
    }

    static addQuotesIfNeeded(text) {
        if (text.indexOf(' ') === -1) return text;
        return '"' + text + '"';
    }

    // TODO: make case-insensitive
    static completeArgByStringArray(arg, options) {
        const matchingOptions = options.filter(
            option => option.substring(0, arg.length) === arg);
        if (matchingOptions.length === 0) {
            // No matches. Nothing to suggest
            return {text: Completions.addQuotesIfNeeded(arg),
                options: []};
        }
        if (matchingOptions.length === 1) {
            // One match. Autocomplete it
            return {text: Completions.addQuotesIfNeeded(
                matchingOptions[0]) + ' ',
                options: matchingOptions};
        }
        // Multiple matches!
        // Need to find the longest common substring
        const firstMatch = matchingOptions[0];
        // Start common substring at length of incomplete arg
        let commonSubstringLength = arg.length;
        // Try to extend common substring to length of first match
        extend_common_substring:
        while (commonSubstringLength < firstMatch.length) {
            // Abort if any match would be lost at this character
            for (const option of matchingOptions) {
                if (option[commonSubstringLength]
                    !== firstMatch[commonSubstringLength])
                    break extend_common_substring;
            }
            // Extend common substring
            commonSubstringLength++;
        }
        const commonSubstring = matchingOptions[0]
            .substring(0, commonSubstringLength);
        return {text: Completions.addQuotesIfNeeded(commonSubstring),
            options: matchingOptions};
    }

    complete(typed) {
        // We do not offer completion for strings not beginning with /
        if (typed.length < 1) return {text: typed, options: []};
        if (typed[0] !== '/') return {text: typed, options: []};

        const quotedArgs = splitByQuotes(typed);
        const args = quotedArgs.map(a=>a.replace(/"/g,''));

        // Abort if somehow no arguments found
        if (args.length < 0) return {text: typed, options: []};

        const availableCommands = (this.commands.inGame)
            ? this.commandDatabase.getCommandNames()
            : this.commands.commands.filter(c => c.isLoginScreen && !c.isHidden).map(c => c.name).sort();

        // Is there only one argument (the command name)?
        if (args.length === 1) {
            // Currently completing the command name.
            const incompleteCommand = args[0];

            return Completions.completeArgByStringArray(
                incompleteCommand, availableCommands);
        }

        // There are multiple arguments.
        // Not completing command name, but one of its arguments.
        const nonresolvedCommand = args[0];
        const command = this.commandDatabase.resolveAlias(nonresolvedCommand);

        // Abort if command not available
        if (command === null || availableCommands.indexOf(command) === -1)
            return {text: typed, options: []};

        // Abort if no known signatures for this command:
        const availableSignatures = this.commandDatabase.getSignatures(command);
        if (!availableSignatures) return {text: typed, options: []};
        const incompleteArgIndex = args.length - 1;
        const incompleteArg = args[incompleteArgIndex];
        const filteredSignatures = availableSignatures.filter(
            signature => {
                // Discard signature if too many arguments
                if (incompleteArgIndex >= signature.length + 1)
                    return false;
                // Check if any argument fails to match signature
                for (let i = 1; i < args.length-1; i++) {
                    const arg = args[i];
                    const type = signature[i-1].type;
                    // Discard signature if argument not matching type
                    if (!this.isArgumentOfType(arg, type)){ 
                        return false;
                    }
                }
                // Check if last (currently typed) arg fails to match
                const incomArgType = signature[incompleteArgIndex-1].type;
                // Discard signature if argument not matching type
                if (!this.isIncompleteArgumentOfType(
                    incompleteArg, incomArgType)) return false;
                return true;
            });
        // Abort if no matching signatures
        if (filteredSignatures.length < 1)
            return {text: typed, options: []};
        const pickedSignature = filteredSignatures[0];
        const incomArgType = pickedSignature[incompleteArgIndex-1].type;
        const result = this.completeArgumentOfType(
            incompleteArg, incomArgType);
        // Reconstruct command without last argument:
        quotedArgs.pop();
        const reconstructedCommand = quotedArgs.join(' ')
            + ' ' + result.text;
        return {text: reconstructedCommand, options: result.options};
    }

    isArgumentOfType(arg, type) {
        // No need to have any different logic from incomplete test
        // (for now)
        return this.isIncompleteArgumentOfType(arg, type);
    }

    isIncompleteArgumentOfType(arg, type) {
        // Empty string matches any type
        if (arg.length < 1) return true;
        switch (type) {
            case 'onlineplayer': case 'existingplayeraccount':
                // Does not need to be a valid player name.
                // Just needs the correct first character.
                return /^[A-Za-z]/.test(arg);
            case 'xcoord': case 'ycoord':
                return /^[0-9-\\~]/.test(arg);
            case 'integer':
                return /^[0-9-]+$/.test(arg);
        }
        return true;
    }

    completeArgumentOfType(arg, type) {
        switch (type) {
            case 'keyboardlayoutname':
                return Completions.completeArgByStringArray(
                    arg, Object.keys(LAYOUTS));
            case 'mappablefunction':
                return Completions.completeArgByStringArray(
                    arg, ACTIONS);
            case 'onlineplayer': case 'existingplayeraccount':
                // Names of other players, known client-side
                const playerNames = Object.keys(
                        this.commands.game.player.positionList);
                // Add my player name as first option
                playerNames.unshift(this.commands.game.player.name);
                return Completions.completeArgByStringArray(
                    arg, playerNames);
            case 'friendoperation':
                return Completions.completeArgByStringArray(
                    arg, ['add', 'accept', 'remove']);
            case 'xcoord':
                return {text: this.commands.game.player.x + ' ',
                    options: []};
            case 'ycoord':
                return {text: this.commands.game.player.y + ' ',
                    options: []};
            case 'integer':
                return {text: arg, options: ['number']};
            case 'gamemodenumber':
                return Completions.completeArgByStringArray(
                    arg, ['0', '1', '2']);
            case 'itemname':
                const allItemNames = Inventory.item_definitions.map(i => i.name);
                return Completions.completeArgByStringArray(
                    arg, allItemNames);
            case 'biomename':
                const allBiomeNames = Object.keys(BIOMES).map(
                    b => BIOMES[b].name);
                return Completions.completeArgByStringArray(
                    arg, allBiomeNames);
            case 'structure':
                const allStructures = [
                    'fort', 'warp',
                    'treasure', 'goldenchest',
                    'badtreasure', 'cavechest',
                ];
                return Completions.completeArgByStringArray(
                    arg, allStructures);
            case 'summonablecreaturetype':
                return Completions.completeArgByStringArray(
                    arg, ['ghost', 'monster', 'vortex']);
            case 'setting':
                const availableSettings = [];
                for (const s of this.commands.settings.SETTING_LIST) {
                    availableSettings.push(s.names[0]);
                    availableSettings.push('no' + s.names[0]);
                }
                for (const s of this.commands.settings.EQ_SETTING_LIST) {
                    availableSettings.push(s.names[0] + '=');
                }
                const result = Completions.completeArgByStringArray(
                    arg, availableSettings);
                // Make sure to display eq settings correctly
                // without a space at the end, because there's
                // not supposed to be a space after the = sign
                if (result.text.length > 2
                    && result.text[result.text.length - 2] === '='
                    && result.text[result.text.length - 1] === ' ') {
                    // Remove the space at the end
                    result.text = result.text.slice(0, -1);
                    // Describe settable range in the suggestion box:
                    const eqsettingname = result.text.slice(0, -1);
                    const eqsetting = this.commands.settings.EQ_SETTING_LIST
                        .filter(s=>s.names[0]===eqsettingname)[0];
                    const description = eqsetting.names[0]
                        +'=['+eqsetting.min+'-'+eqsetting.max+']';
                    result.options = [description];
                }
                return result;
            case 'boolsetting':
                const allBoolSettings =
                    this.commands.settings.SETTING_LIST.map(s => s.names[0]);
                return Completions.completeArgByStringArray(
                    arg, allBoolSettings);
            case 'command':
                if (arg.length > 0 && arg[0] !== '/') arg = '/'+arg;
                return Completions.completeArgByStringArray(
                    arg, this.commandDatabase.getCommandNames());
            case 'enabledisable':
                return Completions.completeArgByStringArray(
                    arg, ['enable', 'disable']);
            case 'incompletable':
                return {text: arg, options: []};
            default:
                return {text: 'COMPLETION_ERROR_'+type, options: []};
        }
    }

}
