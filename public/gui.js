// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


// Wrapper for terminal
// Displays the game view.
// Displays the section of the world that is visible
// and overlays GUI elements such as coordinates, hotbar, inventory, cursor

// Plain CPU rendering. Nothing fancy.

"use strict";

const GUI_FRAME_COLOR = '#aaaaaa';
const GUI_SLIGHT_COLOR = '#666666';
const GUI_TEXT_COLOR = '#aaaaaa';
// const CURSOR_COLOR = '#209090';
const CURSOR_COLOR = '#7f7f7f';

const VORTEX_EFFECT_RANGE = 8;
const MONSTER_RENDER_DISTANCE = 12;

const TYPING_COLORS = {
    'global': 'ccc',
    'local': 'cc0',
    'whisper': '3f3',
};

function makeSmokeColors() {
    const SMOKE_COLORS = [];
    const skyColor = BIOMES['C'].sky;
    const sandColor = BIOME_SPRITES['S']['C'].color;
    const N = 6;
    for (let i = 0; i < N; i++) {
        const shade = (1 - i / N) * 0.7;
        const color = ADD_COLORS_HEX(skyColor, sandColor, shade);
        SMOKE_COLORS.push(color);
    }
    return SMOKE_COLORS;
}
const SMOKE_COLORS = makeSmokeColors();
const SMOKE_TRAIL = 2;

const RAININESSES = [0,0,0,0,1,1,1,2,2,2,3,3,4,5,10,11,12,13,14,15,16,17,18,60];

function raininessInChunk(chunkx, chunky) {
    const which_raininess = ((chunkx*
        4250351207
    )%
        51283
        +(chunky*
            4190530093
        )%
        49811
    ) % RAININESSES.length;
    return RAININESSES[Math.abs(which_raininess)];
}

class Gui {
    constructor(terminal, map, player, components,
        hotbarComponent, notifyComponent, settings){
        this.terminal = terminal;
        this.map = map;
        this.player = player;
        this.hotbarComponent = hotbarComponent;
        this.notifyComponent = notifyComponent;
        this.components = components;
        this.settings = settings;
        this.time = 0;
        this.particles = [];
        this.bubbles = [];
        this.smokes = [];
        this.prevvortex = false;
        this.vortexwx = 0;
        this.vortexwy = 0;
        this.vortexs = 0;
        this.transitioningFromBiome = 'u';
        this.curBgColor = "#000000";
        this.curBgAsciiColor = "#000000";
        this.overviewshown = false;
        this.overviewzoom = 2;
    }

    static drawFrame(buffer, frame, frameX, frameY){
        for(const i in frame){
            const decoded = AASTRING_DECODE(frame[i], GUI_FRAME_COLOR)[0];
            for (let j = 0; j < decoded.raw.length; j++) {
                const char = decoded.raw[j];
                if (char === '@') continue;
                const color = decoded.color[j];
                const modifiers = decoded.modifiers[j];
                const x = parseInt(j)+frameX;
                const y = parseInt(i)+frameY;
                if (buffer[y] === undefined) continue;
                const sprite = {char: char, color: color};
                if (modifiers & 1) {  // blink
                    // ??0 -> blinkspeed = undefined (=0)
                    // 001 -> blinkspeed = 1
                    // 011 -> blinkspeed = 2
                    // 101 -> blinkspeed = 3
                    // 111 -> blinkspeed = 4
                    sprite.blinkspeed = (modifiers & 6) / 2 + 1;
                }
                if (modifiers & 8) {
                    sprite.invert = true;
                }
                buffer[y][x] = sprite;
            }
        }
    }

    static drawFormatted(buffer, lines, frameX, frameY) {
        for (const i in lines) {
            const line = lines[i];
            for (const j in line.color) {
                const char = line.raw[j];
                const color = line.color[j];
                const x = parseInt(j)+frameX;
                const y = parseInt(i)+frameY;
                if (buffer[y] === undefined) continue;
                const sprite = {char: char, color: color};
                buffer[y][x] = sprite;
            }
        }
    }

    static drawSpritesRelative(buffer, frameX, frameY, sprites) {
        for (const sp of sprites) {
            const y = frameY+sp.dy;
            if (y < 0 || y >= buffer.length) continue;
            buffer[y][frameX+sp.dx] = sp.sprite;
        }
    }

    static makeSpookySprite(block, biome, curbg, dx, dy) {
        const rawSprite = BIOME_SPRITES[block][biome];
        let shade = 1 - (dx*dx+4*dy*dy)*0.0007;
        if (shade < 0) shade = 0;
        return {
            char: rawSprite.char,
            color: ADD_COLORS_HEX(curbg, rawSprite.color, shade),
        };
    }

    static transitionColorHEX(current, target) {
        const rgbcur = HEX2RGB(current);
        const rgbnew = HEX2RGB(target);
        const diff = [
            rgbcur[0] - rgbnew[0],
            rgbcur[1] - rgbnew[1],
            rgbcur[2] - rgbnew[2],
        ];
        // epic trick: add 90% of current color to biome color
        // instead of adding 10% of biome color to current color
        // and this way we avoid issues with rounding
        return RGB2HEX(ADD_COLORS_RGB(rgbnew, diff, 0.9));
    }

    static getDotsAnimationFrame(type, animationFrame) {
        if (type === 'rainbow') {
            return '\\['+getRainbowColorAt(10,(10-animationFrame)%10)+'\\].'
                +'\\['+getRainbowColorAt(10,(11-animationFrame)%10)+'\\].'
                +'\\['+getRainbowColorAt(10,(12-animationFrame)%10)+'\\].';
        }
        const color = TYPING_COLORS[type];
        let dots = '.  ';
        if (animationFrame > 2) dots = '.. ';
        if (animationFrame > 5) dots = '...';
        return '\\[#'+color+'\\]'+dots;
    }

    drawPlayer(sx, sy, color, buffer) {
        const w = this.terminal.width;
        const h = this.terminal.height;
        if (sx<0 || sx>=w || sy<0 || sy>=h) return false;
        const prevBlock = buffer[h - sy - 1][sx];
        const playerSprite = {char:'P', color:'#'+color};
        if (prevBlock !== undefined
            && prevBlock.char === 'P'
            && prevBlock.secondchar !== undefined) {
            playerSprite.secondchar = prevBlock.secondchar;
        }
        else if (prevBlock !== undefined
            && (prevBlock.char === '.' || prevBlock.char === '▁')) {
            playerSprite.secondchar = {
                char: prevBlock.char,
                color: prevBlock.color,
                dx: 0, dy: 0, scale: 1};
            if (prevBlock.dy !== undefined) {
                playerSprite.secondchar.dy = -prevBlock.dy;
            }
        }
        buffer[h - sy - 1][sx] = playerSprite;
    }

    drawOverlayCharacter(sx, sy, char, color) {
        if (sx < -1) return;
        this.terminal.displayCharacter(sx, sy, char, color);
    }

    visibleBiomeAt(x, y) {
        if (this.map.biomeAt(x, y+1) === 'u') return 'u';
        if (this.map.biomeAt(x, y-1) === 'u') return 'u';
        if (this.map.biomeAt(x+1, y) === 'u') return 'u';
        if (this.map.biomeAt(x-1, y) === 'u') return 'u';
        return this.map.biomeAt(x, y);
    }

    display(){
        this.time++;
        this.notifyComponent.notify.tick();

        // vortex interpolation
        if (this.player.vortex) {
            if (this.prevvortex) {
                this.vortexs = 1 - (1 - this.vortexs)*0.98;
                const d = 1 - 1 / this.player.vortexanimationdeadline;
                this.vortexwx = this.player.vortexwx - (this.player.vortexwx - this.vortexwx)*d;
                this.vortexwy = this.player.vortexwy - (this.player.vortexwy - this.vortexwy)*d;
                if (this.player.vortexanimationdeadline > 2) {
                    this.player.vortexanimationdeadline--;
                }
            } else {
                this.vortexwx = this.player.vortexwx;
                this.vortexwy = this.player.vortexwy;
            }
        } else {
            this.vortexs = 0;
        }
        this.prevvortex = this.player.vortex;

        if (this.player.monsteranimationdeadline > 0) {
            this.player.monsteranimationdeadline--;
        }

        if (this.player.uiState === 'overview') {
            this.displayOverview();
            return;
        }

        const currentBiome = this.visibleBiomeAt(
            this.player.x, this.player.y);
        const newBgColor = BIOMES[currentBiome].sky;
        const newBgAsciiColor = BIOMES[currentBiome].parallax;

        this.curBgColor = Gui.transitionColorHEX(this.curBgColor, newBgColor);

        // Compare string array references to find out if
        // previous and next biome have different backgrounds
        let isBackgroundTransition =
            PARALLAX_BGS[currentBiome] !== PARALLAX_BGS[this.transitioningFromBiome];

        if (isBackgroundTransition) {
            // Biome transition in progress!
            // Or is it?
            if (this.curBgColor === newBgColor && this.curBgAsciiColor === newBgColor) {
                // Actually the transition is already finished now!
                this.transitioningFromBiome = currentBiome;
                isBackgroundTransition = false;
            }
        }

        let pxbg;

        if (isBackgroundTransition) {
            // Biome transition in progress!
            pxbg = PARALLAX_BGS[this.transitioningFromBiome];
            // The previous parallax color transitions into new background color
            this.curBgAsciiColor = Gui.transitionColorHEX(this.curBgAsciiColor, newBgColor);
        } else {
            pxbg = PARALLAX_BGS[currentBiome];
            this.curBgAsciiColor = Gui.transitionColorHEX(this.curBgAsciiColor, newBgAsciiColor);
        }

        if (!this.settings.s['???']) {
            this.terminal.clearScreen();
            this.terminal.setBackground(this.curBgColor);
        }

        const w = this.terminal.width;
        const h = this.terminal.height;
        this.player.attachCameraIfOutsideScreen(w, h);
        const left = this.player.getViewX() - Math.floor(w / 2);
        const top = this.player.getViewY() - Math.ceil(h / 2) + 1;
        const buffer = [];

        // render the terrain / world / map
        for(let y = 0; y < h; y++){
            buffer[h - y - 1] = [];
            for(let x = 0; x < w; x++){
                const worldx = left + x;
                const worldy = top + y;

                if (this.settings.s['chunkbordervisible'] &&
                    (worldx % 256 === 0 || worldy % 256 === 0)) {
                    buffer[h - y - 1][x] = {char: '▖', color: '#f00'};
                    continue;
                }

                const biome = this.map.blendedBiomeAt(worldx, worldy);
                const block = this.map.getBlock(worldx, worldy);

                // priority to # because this is not a normal sprite
                if (block[0] === '#') {
                    const spookySprite  = Gui.makeSpookySprite(
                        block, biome, this.curBgColor, x-w/2, y-h/2);
                    if (spookySprite.color !== this.curBgColor) {
                        buffer[h - y - 1][x] = spookySprite;
                    }
                }
                // WW is animated when on cooldown
                else if (block === 'WW' && this.player.warpCooldown > 0) {
                    const shade = Math.cos((
                        Math.log(this.player.TOTALWARPCOOLDOWN-this.player.warpCooldown,2)
                        -Math.log(this.player.TOTALWARPCOOLDOWN,2)
                    )*8)*-0.5+0.5;
                    const color = ADD_COLORS_HEX(
                        BIOME_SPRITES['WW']['A'].color,
                        '#9c001b',
                        shade);
                    buffer[h - y - 1][x] = {
                        char: 'W',
                        color: color,
                    };
                }
                else if (block === 'sn') {  // sometimes snow is swaying
                    const blockBelow = this.map.getBlock(worldx, worldy-1);
                    const biomeSpritesUnder = BIOME_SPRITES[blockBelow];
                    let snowType = 'sn'
                    if (biomeSpritesUnder) {
                        const spriteUnder = biomeSpritesUnder[biome];
                        const isSwaying = spriteUnder !== undefined && spriteUnder.sway;
                        snowType = isSwaying ? 'snsway' : 'sn';
                    }
                    const sprite = BIOME_SPRITES[snowType][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === '|/') {
                    const type = this.map.getBlock(worldx-1, worldy) === 'WO'
                        && this.map.getBlock(worldx+1, worldy) === 'WO'
                        ? '|/a' : '|/';
                    const sprite = BIOME_SPRITES[type][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === 'WO') {
                    const type = this.map.getBlock(worldx-1, worldy) === '|/'
                        && this.map.getBlock(worldx-2, worldy) === 'WO'
                        || this.map.getBlock(worldx+1, worldy) === '|/'
                        && this.map.getBlock(worldx+2, worldy) === 'WO'
                        ? 'WON' : 'WO';
                    const sprite = BIOME_SPRITES[type][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === 'O1' || block === 'O2') {
                    const blockLeft = this.map.getBlock(worldx-1, worldy);
                    const blockRight = this.map.getBlock(worldx+1, worldy);
                    const type = (blockLeft === '?F'
                            || blockLeft === '?f'
                            || blockLeft === 'WF')
                        && (blockRight === 'WX'
                            || blockRight === '?x'
                            || blockRight === '?X'
                            || blockRight[0] === 'X')
                        ? 'OF' : block;
                    const sprite = BIOME_SPRITES[type][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block[0] === '=') {
                    const trimBlock = block.substr(0,3);
                    let sprite = BIOME_SPRITES[trimBlock][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block[0] === ':') {
                    const trimBlock = block.substr(0,2);
                    let sprite = BIOME_SPRITES[trimBlock][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === 'sm'
                    && this.player.focusedAAterminal !== -1
                    && this.player.openedAAterminals[
                        this.player.focusedAAterminal].x === worldx
                    && this.player.openedAAterminals[
                        this.player.focusedAAterminal].y === worldy) {
                    const spritecode = this.time % 10 < 5 ? 'smm' : 'sMM';
                    const sprite = BIOME_SPRITES[spritecode][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if (block === 'sm' && Math.floor(this.time*0.2
                    +(0.01*(worldx+59.1)*(2*worldx+7.1)*worldx+0.2*worldy)%10
                        * (Math.min(0.0000001*(Math.pow(worldx-11*4*101,4)+Math.pow(worldy-4*1111,4)),1))
                    +(0.2*(Math.abs(worldx-3*1481-1)+Math.abs(worldy-4416)*0.1*(worldy+28.1)))%10
                        * (1-Math.min(0.0000001*(Math.pow(worldx-22*202,4)+Math.pow(worldy-404*11,4)),1))
                    +10)%10 > 0) { // arbitrary numbers for random-looking animation
                    let sprite = BIOME_SPRITES['sM'][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if(BIOME_SPRITES[block]){ // fails for air
                    if (block === 'B' || block === 'd') {
                        // miscolored B and d
                        // openssl prime -generate -bits 16

                        const which_B = ((worldx*
                            4250351207
                        )%
                            51283
                            +(worldy*
                                4190530093
                            )%
                            49811
                        )%8;

                        buffer[h - y - 1][x] = BIOME_SPRITES[block+Math.abs(which_B)][biome];
                    }
                    else {
                        let sprite = BIOME_SPRITES[block][biome];
                        if (sprite.rainbow) {
                            const worldx = x + left;
                            const which_rainbow = (worldx%8+8)%8;
                            sprite = BIOME_SPRITES[block+which_rainbow][biome];
                        }
                        buffer[h - y - 1][x] = sprite;
                    }
                }

                // something in the air
                if (buffer[h - y - 1][x] === undefined) {
                    const bgx = Math.floor(left/4) + x;
                    const bgy = Math.floor(top/4) + y;
                    const bgoffx = 0.375-((left%4+4)%4)/4;
                    const bgoffy = -0.375+((top%4+4)%4)/4;
                    // const bgoffy = 0;
                    const char = pxbg[
                        pxbg.length-1-(bgy%pxbg.length+pxbg.length)%pxbg.length
                    ][
                        (bgx%pxbg[0].length+pxbg[0].length)%pxbg[0].length
                    ];
                    buffer[h - y - 1][x] = {
                        char: char,
                        color: this.curBgAsciiColor,
                        offset: {x: bgoffx, y: bgoffy},
                        isparallax: true,
                    }
                }
            }
        }

        // try spawning new particles
        for (let i = 0; i < 100; i++){
            // TODO: instead of checking 100 random blocks,
            // use warp tile entity information
            const sx = Math.floor(Math.random()*(w+20)-10);
            const sy = Math.floor(Math.random()*(h+20)-10);
            const worldx = left + sx;
            const worldy = top + sy;
            const block = this.map.getBlock(worldx, worldy);
            if (block === 'WW') {
                const rubyblock = this.map.getBlock(worldx, worldy+6);
                if (rubyblock === 'R1') {
                    this.particles.push({
                        type: 'channeling',
                        wx: worldx,
                        wy: worldy+6,
                        dirx: Math.random()-0.5,
                        diry: -0.31,
                        rot: 0,
                        color: BIOME_SPRITES['R1'][currentBiome].color,
                        char: '*',
                        age: 0,
                        lifetime: 20,
                    });
                }
            }
            if (block === 'WC') {
                for (let i = 0; i < 3; i++) {
                    const angle = Math.random()*6.28;
                    this.particles.push({
                        type: 'collect',
                        wx: worldx,
                        wy: worldy,
                        dirx: Math.sin(angle),
                        diry: Math.cos(angle),
                        rot: Math.random()>0.5?1:-1,
                        color: BIOME_SPRITES['WC'][currentBiome].color,
                        char: '#',
                        age: 0,
                        lifetime: 20,
                    });
                }
            }
            else if (block === 'WT' || block === '~') {
                if (block === 'WT' && i >= 10) continue;
                const blockbelow = this.map.getBlock(worldx, worldy-1);
                if (blockbelow !== ' ') continue;
                const biome = this.map.biomeAt(worldx, worldy);
                let color = '#5555aa';
                if (block === '~') {
                    const block2below = this.map.getBlock(worldx, worldy-2);
                    if (block2below !== ' ') continue;
                    if (biome === 'R') {
                        // random color
                        color = RAINBOW[Math.floor(Math.random()*8)];
                        color = ADD_COLORS_HEX('#000', color, 0.7);
                    }
                    else if (biome !== 's') continue;
                    // weird logic to decide which areas are rainy:
                    const chunkx = worldx >> 8;
                    const chunky = worldy >> 8;
                    const raininess = raininessInChunk(chunkx, chunky);
                    if (i > raininess) continue;
                } else {
                    if (biome !== 'c') continue;
                }
                // will spawn water droplet
                // but need to measure fall distance first
                let j;
                for (j = 1; j < 15; j++) {
                    const blockThere = this.map.getBlock(worldx, worldy-j);
                    if (blockThere !== ' ' && blockThere !== '~') break;
                }
                // ok spawn
                this.particles.push({
                    type: 'droplet',
                    wx: worldx,
                    wy: worldy,
                    dirx: 0,
                    diry: -1,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                    color: color,
                    char: '.',
                    age: 0,
                    lifetime: j,
                });
            }
            // the remaining iterations (above 10)
            // stop here, only checking warp and rain
            if (i >= 10) continue;
            if (block[0] === 'A'
                || block[0] === 'a'
                || (block[0] === 'W' && block[1] === 'a')) {
                const leaftype = block[block.length-1];
                // no particles from bushes or palms
                if (leaftype === 'b' || leaftype === 'r'
                    || leaftype === 'u' || leaftype === 'm') continue;
                const biome = this.map.blendedBiomeAt(worldx, worldy);
                this.particles.push({
                    type: 'leaf',
                    wx: worldx,
                    wy: worldy,
                    dirx: -0.2,
                    diry: 0,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                    color: getSafeBiomeSprite(block, biome).color,
                    char: 'A',
                    age: 0,
                    lifetime: 50,
                });
            }
            else if (block[0] === '-' || block[0] === '*') {  // firefly
                // abort firefly if too many fireflies
                if (this.particles.filter(p=>p.type==='firefly').length > 3) continue;
                // abort firefly if wrong biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'a'
                    && biome !== 'A'
                    && biome !== 'p'
                    && biome !== 'P'
                    && biome !== 'm'
                    && biome !== 'f') continue;
                // abort firefly if no dirt below
                const someBlockBelow = this.map.getBlock(
                    worldx+Math.floor(Math.random()*7-3),
                    worldy+Math.floor(Math.random()*7-3)-5);
                if (someBlockBelow !== 'd') continue;
                // abort firefly if no space
                const blockAboveL = this.map.getBlock(worldx-1, worldy+2);
                if (blockAboveL !== ' ' && blockAboveL[0] !== '-') continue;
                const blockAboveR = this.map.getBlock(worldx+1, worldy+2);
                if (blockAboveR !== ' ' && blockAboveR[0] !== '-') continue;
                // abort firefly if no leaf above (A*, a* or Wa*)
                let yes_firefly = false;
                for (let l = 0; l < 5; l++) {
                    const someBlockAbove = this.map.getBlock(
                        worldx+Math.floor(Math.random()*9-4),
                        worldy+Math.floor(Math.random()*7-3)+6);
                    if (someBlockAbove[0] === 'A'
                        || someBlockAbove[0] === 'a'
                        || someBlockAbove[0] === 'W'
                            && someBlockAbove[1] === 'a') {
                        yes_firefly = true;
                        break;
                    }
                }
                if (!yes_firefly) continue;

                this.particles.push({
                    type: 'firefly',
                    wx: worldx,
                    wy: worldy+2,
                    dirx: 0,
                    diry: 0,
                    rot: 0,
                    color: '#fcc63c',
                    char: '.',
                    age: 0,
                    lifetime: 500,
                });
            }
            else if (block[0] === '=' && block[2] !== '0') {
                // wire particle
                this.particles.push({
                    type: 'emit',
                    wx: worldx,
                    wy: worldy,
                    dirx: (Math.random()-0.5)*0.3,
                    diry: (Math.random()-0.5)*0.2,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                    color: '#c077a4',
                    char: block[3],
                    age: 0,
                    lifetime: 20,
                });
            }
            if (this.player.vortex) {
                // hint particle
                for (const hint of this.player.vortexhints) {
                    const fraction = Math.random();
                    let hintwx = this.vortexwx + fraction * (hint.x - this.vortexwx);
                    let hintwy = this.vortexwy + fraction * (hint.y - this.vortexwy);
                    const displacement = fraction*(1-fraction);
                    hintwx += Math.sin(hintwy/20)*40*displacement;
                    hintwy += Math.sin(hintwx/20)*40*displacement;
                    if (Math.pow(hintwx-this.vortexwx,2)+Math.pow(hintwy-this.vortexwy,2) < 50) continue;

                    const color = ['#ffffff', '#ff1010'] [Math.floor(Math.random()*1.08)];
                    this.particles.push({
                        type: 'emit',
                        wx: hintwx,
                        wy: hintwy,
                        dirx: (Math.random()-0.5)*0.08,
                        diry: (Math.random()-0.5)*0.05,
                        rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                        color: color,
                        char: '¤',
                        age: 0,
                        lifetime: 20,
                    });
                }
            }
            // stop here, to make smoke more rare
            if (i >= 9) continue;
            if (block === 'S') {  // smoke in tentacle biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'C' && biome !== 'W') continue;
                if (this.map.getBlock(worldx, worldy+1) !== ' ') continue;
                if (this.map.getBlock(worldx+1, worldy+2) !== ' ') continue;
                this.smokes.push({
                    wx: worldx,
                    wy: worldy,
                    age: 0,
                    kill: false,
                });
            }
            // stop here, to make bubble more rare
            if (i >= 6) continue;
            if (block === 'w') {  // bubble in ocean biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'o') continue;
                if (this.map.getBlock(worldx, worldy+1) !== 'w') continue;
                if (this.map.getBlock(worldx, worldy-1) === 'w'
                    && sy > 0) continue;
                this.bubbles.push({
                    wx: worldx,
                    wy: worldy,
                    kill: false,
                });
            }
            // stop here, to make snowflake more rare
            if (i >= 1) continue;
            if (block === ' ') {  // snowflake in snow biome
                const biome = this.map.biomeAt(worldx, worldy);
                if (biome !== 'g' && Math.random() > 0.3) continue;
                if (biome !== 'i'
                    && biome !== 'g'
                    && biome !== 'I'
                    && biome !== 'T') continue;
                if (this.map.getBlock(worldx-3, worldy-2) !== ' ') continue;
                if (this.map.getBlock(worldx-6, worldy-4) !== ' ') continue;
                this.particles.push({
                    type: 'snowflake',
                    wx: worldx,
                    wy: worldy,
                    dirx: biome === 'g' ? 1.5+Math.random()*0.3 : 0.2,
                    diry: 0,
                    rot: (0.8+Math.random()*0.2)*(Math.random()>0.5?1:-1),
                    color: biome === 'g' ? '#aabbff' : '#ffffff',
                    char: '*',
                    age: 0,
                    lifetime: 50,
                });
            }
        }

        if (this.player.warpWarmup > 0) {
            // additional ruby particle every frame
            this.particles.push({
                type: 'channeling',
                wx: this.player.x,
                wy: this.player.y+5-this.player.warpWarmup*0.1,
                dirx: Math.random()-0.5,
                diry: -0.31,
                rot: 0,
                color: BIOME_SPRITES['R1'][currentBiome].color,
                char: '*',
                age: 0,
                lifetime: 20,
            });
        }

        // render animated blocks
        for (const ani of this.player.blockanimations) {
            // skip if delayed
            // (note: this code works fine if ani.d is not defined)
            if (ani.d > 0) continue;
            const x = ani.x;
            const y = ani.y;
            if (x >= left && x < left+w && y >= top && y < top+h) {
                // animated block is within screen! display!
                const biome = this.map.blendedBiomeAt(x, y);
                if (ani.b[0] === '#') {
                    buffer[h - (y-top) -1][x-left] = Gui.makeSpookySprite(
                        ani.b, biome, this.curBgColor, x-left-w/2, y-top-h/2);
                    continue;
                }
                if (ani.b === ' ') {
                    buffer[h - (y-top) -1][x-left] = undefined;
                    continue;
                }
                buffer[h - (y-top) -1][x-left] = getSafeBiomeSprite(ani.b, biome);
            }
        }
        this.player.blockanimations = this.player.blockanimations.map(
            ani=>{ani.a--; ani.d--; return ani;}).filter(ani=>ani.a>0);

        // render and update bubble particles
        for (const bubble of this.bubbles) {
            const sx = bubble.wx - left;
            const sy = bubble.wy - top;
            if (sx < 0 || sx >= w || sy < 0 || sy >= h) {
                // bubble left the screen
                bubble.kill = true;
                continue;
            }
            // display bubble
            buffer[h-sy-1][sx] = getSafeBiomeSprite('bubble', 'o');
            // bubble rises up
            bubble.wy += 2;
            if (this.map.getBlock(bubble.wx, bubble.wy-1) !== 'w'
                || this.map.getBlock(bubble.wx, bubble.wy) !== 'w') {
                bubble.kill = true;
            }
        }
        this.bubbles = this.bubbles.filter(b=>!b.kill);

        // render and update smoke particles
        for (const smoke of this.smokes) {
            // display multiple sprites for smoke particle
            for (let i = 0; i < SMOKE_TRAIL; i++) {
                const age = smoke.age + i;
                if (age >= SMOKE_COLORS.length) {
                    // reached the end of the color list
                    if (i === 0) smoke.kill = true;
                    break;
                }
                const wx = smoke.wx + (age % 2 === 0 ? 1 : 0);
                const wy = smoke.wy + age;
                const sx = wx - left;
                const sy = wy - top;
                if (this.map.getBlock(wx, wy) !== ' ') {
                    // smoke hit a ceiling
                    if (i === 0) {
                        // the core of the smoke hit the ceiling
                        if (smoke.age === 0) {
                            // nevermind, it is the origin point
                            continue;
                        } else {
                            smoke.kill = true;
                            break;
                        }
                    } else {
                        // some other part of the smoke hit ceiling
                        break;
                    }
                }
                if (sx < 0 || sx >= w || sy < 0 || sy >= h) {
                    // beyond screen
                    continue;
                }
                const sprite = {
                    char: age % 2 === 0 ? ')' : '(',
                    color: SMOKE_COLORS[age],
                };
                buffer[h-sy-1][sx] = sprite;

            }
            if (this.time % 2 === 0) smoke.age++;
        }
        this.smokes = this.smokes.filter(s=>!s.kill);

        // render sea monsters
        for (const monster of this.player.monsters) {
            const monster_animation = MONSTER_ART[monster.type];
            let frame_numbers = monster_animation.animations[monster.eye];
            if (frame_numbers === undefined) {
                // there is no specific animation for this eye. Use default
                frame_numbers = monster_animation.animations['default'];
            }
            const frame_number = frame_numbers[Math.floor(this.time)%frame_numbers.length];
            const monster_frame = monster_animation.frames[frame_number];
            const monsterInterpolatedx = monster.prevx === undefined ? monster.x : (Math.round(monster.x + (monster.prevx-monster.x)*this.player.monsteranimationdeadline/10));
            const monsterInterpolatedy = monster.prevy === undefined ? monster.y : (Math.round(monster.y + (monster.prevy-monster.y)*this.player.monsteranimationdeadline/10));
            // half-height and half-width, used as offset for centering
            const mhh = Math.floor(monster_frame.length/2);
            const mww = Math.floor(monster_frame[0].length/2);
            for (let my = 0; my < monster_frame.length; my++) {
                for (let mx = 0; mx < monster_frame[my].length; mx++) {
                    const mxCentered = monster.facing === 'r' ? (mx - mww) : (mww - mx);
                    const myCentered = my - mhh;
                    const sx = monsterInterpolatedx + mxCentered - left;
                    const sy = h - (monsterInterpolatedy-myCentered-top) -1;
                    // skip displaying if not within screen
                    if (sx < 0 || sx >= w || sy < 0 || sy >= h) continue;
                    const dx = sx - w/2;
                    const dy = sy - h/2;
                    // skip displaying if not within monster render distance
                    if (dx*dx + dy*dy*2 > MONSTER_RENDER_DISTANCE*MONSTER_RENDER_DISTANCE) continue;
                    let char = monster_frame[my][mx];
                    // skip displaying if transparent
                    if (char === ' ') continue;
                    // @ means non-transparent space
                    if (char === '@') char = ' ';
                    // replace eye with correct eye
                    if (char === 'o') char = monster.eye;
                    // mirroring
                    if (monster.facing === 'l' && char === '/') char = '\\';
                    else if (monster.facing === 'l' && char === '\\') char = '/';
                    else if (monster.facing === 'l' && char === '(') char = ')';
                    else if (monster.facing === 'l' && char === ')') char = '(';
                    buffer[sy][sx] = {char:char, color:'#333377'};
                }
            }
        }

        // render detectors
        for (const detector of this.player.visibleDetectors) {
            if (this.time % 4 < 1) break;  // blink
            const sx = detector.x - left;
            const sy = h - (detector.y-top) -1;
            // which one to display?
            const biomeHere = this.map.biomeAt(detector.x, detector.y);
            const item_code = biomeHere === 'o' ? 'Eo' : 'E';
            const follow = this.player.inventory.hasItem('Wo')
                && this.hotbarComponent.getSelected() === 'Wo';
            if (item_code === 'E' && follow) {
                // skip other decorations if at edge of screen
                if (sx < 1 || sx >= w-1 || sy < 1 || sy >= h-1) continue;
                const color = BIOME_SPRITES['E']['a'].color;
                buffer[sy][sx] = {char:'O', color:color};
                buffer[sy+1][sx+1] = {char:'\\', color:color};
                buffer[sy-1][sx+1] = {char:'/', color:color};
                buffer[sy+1][sx-1] = {char:'/', color:color};
                buffer[sy-1][sx-1] = {char:'\\', color:color};
            } else {
                // skip displaying if not within screen
                if (sx < 0 || sx >= w || sy < 0 || sy >= h) continue;
                buffer[sy][sx] = BIOME_SPRITES[item_code]['a'];
            }
        }

        // warp warmup animation
        if (this.player.warpWarmup > 0) {
            const sx = this.player.x - left;
            const sy = this.player.y - top + 5;
            if (sx>=0 && sx<w && sy>=0 && sy<h) {
                buffer[h - sy - 1][sx] = {char:' '};
            }
        }

        // render ghosts
        for (const g of this.player.visibleGhosts) {
            if (!this.player.inventory.hasItem('E')) break;
            if (this.hotbarComponent.getSelected() !== 'E') break;
            const sx = g.x - left;
            const sy = g.y - top;
            if (sx<0 || sx>=w || sy<0 || sy>=h) break;
            const sprite = Gui.makeSpookySprite(
                'Ws', currentBiome, this.curBgColor, sx-w/2, sy-h/2);
            buffer[h - sy - 1][sx] = sprite;
        }

        const mousescreenx = this.player.mousex + Math.floor(w/2);
        const mousescreeny = this.player.mousey + Math.ceil(h/2)-1;

        // get world pos of highlight
        // highlight might be by mouse or by keyboard-controlled cursor
        let highlightscreenx;
        let highlightscreeny;
        let highlightworldx;
        let highlightworldy;
        if (this.player.mouserecentlymoved) {
            highlightscreenx = mousescreenx;
            highlightscreeny = mousescreeny;
            highlightworldx = left + highlightscreenx;
            highlightworldy = top + highlightscreeny;
        } else {
            highlightworldx = this.player.x + this.player.cursorx;
            highlightworldy = this.player.y + this.player.cursory;
            highlightscreenx = highlightworldx - left;
            highlightscreeny = highlightworldy - top;
        }
        const highlightblock = this.map.getBlock(highlightworldx, highlightworldy);

        // display block name
        if (this.player.inspectKeyPressed) {
            const block = this.map.getBlock(
                highlightworldx, highlightworldy);
            if (block !== ' ') {
                const item = Inventory.block2item(block);
                const item_name = Inventory.item_names[item];
                const frame = [ '\\[#ffffdd\\]' + block
                    + '\\[#ddddbb\\]/'
                    + '\\[#ffffdd\\]' + item
                    + '\\[#ddddbb\\]/'
                    + '\\[#ffffdd\\]' + item_name ];
                if (item[0] === 'G') {
                    const tileEntity = this.map.getTileEntityAt(highlightworldx, highlightworldy);
                    if (tileEntity.length > 0) {
                        const durability = tileEntity[0].d.d;
                        const max_durability = Inventory.item_data_templates[item].d;
                        frame.push('\\[#ffffdd\\]' + durability
                            + '\\[#ddddbb\\]/'
                            + '\\[#ffffdd\\]' + max_durability);
                    }
                }
                Gui.drawFrame(buffer, frame, highlightscreenx+1, h-1-(highlightscreeny+1));
            }
        }

        // display detector instruction
        for (const detector of this.player.visibleDetectors) {
            if (detector.x === highlightworldx && detector.y === highlightworldy) {
                const biomeHere = this.map.biomeAt(detector.x, detector.y);
                const item_code = biomeHere === 'o' ? 'Eo' : 'E';
                const follow = this.player.inventory.hasItem('Wo')
                    && this.hotbarComponent.getSelected() === 'Wo';
                let text = 'click to ';
                if (item_code === 'E' && follow) {
                    for (let i = 0; i < 7; i++) {
                        const charCode = 161+Math.floor(500*Math.random());
                        text += String.fromCharCode(charCode);
                    }
                } else {
                    text += 'collect';
                }
                text += '!';
                const frame = [text];
                Gui.drawFrame(buffer, frame, highlightscreenx+1, h-1-(highlightscreeny+1));
            }
        }

        // display other player overhead stuff
        // (only for players with known position)
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            const screenx = p.x-left+1;
            const screeny = h-1-(p.y-top+1);
            let overheadOffset = 0;
            // display typing animation if typing:
            if (key in this.player.typingList
                && this.player.typingList[key].type !== 'stop') {
                const ani = this.player.typingList[key];
                const content = ani.content ? ani.content : '';
                const dots = Gui.getDotsAnimationFrame(
                    ani.type, ani.animationFrame);
                const fullText = content + dots;
                const color = TYPING_COLORS[ani.type];
                const lines = AASTRING_DECODE_WITH_WRAPPING(
                    fullText, 20, color ? '#'+color : '#ccc');
                const displayHeight = lines.length;
                Gui.drawFormatted(buffer, lines, screenx, screeny-displayHeight+1);
                overheadOffset += displayHeight;
            }
            // display recent messages:
            if (key in this.player.messageList) {
                for (let i = this.player.messageList[key].length-1; i >= 0; i--) {
                    const message = this.player.messageList[key][i];
                    const fadedMessage = message.timeout < 10
                        ? AASTRING_FADE_TO_COLOR(message.message, this.curBgColor, 1-message.timeout/10, message.color)
                        : message.message;
                    const lines = AASTRING_DECODE_WITH_WRAPPING(
                        fadedMessage, 20, message.color);
                    const displayHeight = lines.length;
                    Gui.drawFormatted(buffer, lines, screenx, screeny-overheadOffset-displayHeight+1);
                    overheadOffset += displayHeight;
                }
            }
            // display player name if hovered or key pressed:
            if (p.x === highlightworldx && p.y === highlightworldy || this.player.inspectKeyPressed) {
                const frame = ['\\[#'+p.c+'\\]'+key];
                Gui.drawFrame(buffer, frame, screenx, screeny-overheadOffset);
            }
        }

        // typing animation tick
        for (const key in this.player.typingList) {
            const ani = this.player.typingList[key];
            if (ani.type === 'stop') {
                // reset the animation
                ani.animationFrame = 0;
                continue;
            }
            ani.animationFrame++;
            ani.animationFrame %= 9;
            ani.duration--;
            if (ani.duration < 1) ani.type = 'stop';
        }

        // overhead message animation tick
        for (const key in this.player.messageList) {
            for (const message of this.player.messageList[key]) {
                message.timeout--;
            }
            this.player.messageList[key] = this.player.messageList[key]
                .filter(message => message.timeout > 0);
        }

        // render other players
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            const sx = p.x - left;
            const sy = p.y - top;
            this.drawPlayer(sx, sy, p.c, buffer);
        }

        // render my player
        {
            const sx = this.player.x - left;
            const sy = this.player.y - top;
            this.drawPlayer(sx, sy, this.player.color, buffer);
        }

        // display warp target
        if (highlightblock === 'WW') {
            const tileEntity = this.map.getTileEntityAt(highlightworldx, highlightworldy);
            if (tileEntity.length > 0){
                const warpx = tileEntity[0].d.tx;
                const warpy = tileEntity[0].d.ty;
                let label = '\\[#404040\\]Warp to \\[/\\]' + warpx + ', ' + warpy
                if (this.map.getBlock(highlightworldx, highlightworldy+6) !== 'R1') {
                    label += '\\[#9f5f7f\\] (unpowered)';
                }
                const frame = [label];
                Gui.drawFrame(buffer, frame, highlightscreenx+1, h-1-(highlightscreeny+1));
            }
        }

        // display "four forg"
        if (highlightblock === 'WF') {
            const frame = ['\\[#eeeeee\\]four forg'];
            Gui.drawFrame(buffer, frame, highlightscreenx+1, h-1-(highlightscreeny+1));
        }

        // display sign text
        if (highlightblock === 'si') {
            const tileEntity = this.map.getTileEntityAt(highlightworldx, highlightworldy);
            if (tileEntity.length > 0){
                const lines = AASTRING_DECODE_WITH_WRAPPING(
                    tileEntity[0].d.t, 20, '#ccc');
                Gui.drawFormatted(buffer, lines, highlightscreenx+1, h-1-(highlightscreeny+1));
            }
        }

        // render coordinates
        if (this.settings.s['coordinates'] && w > 11 && h > 8) {
            // render coordinates frame
            const textX = this.player.x.toString();
            const textY = this.player.y.toString();
            const textB = BIOMES[currentBiome].short;
            const width = Math.max(textX.length, textY.length, 6);
            const border = new Array(width+4).join('─');
            const frame =
                [
                    border+'┐',
                    'x: ' + (textX+'                                              ').substring(0, width) + '│',
                    'y: ' + (textY+'                                              ').substring(0, width) + '│',
                    (textB+'                                              ').substring(0, width+3) + '│',
                    border+'┘',
                ]
            Gui.drawFrame(buffer, frame, 0, 0);
        }

        // render all shown UI components
        for (const component of this.components) {
            if (!component.isShown()) continue;
            const x = mousescreenx;
            const y = h-1-mousescreeny;
            component.drawOnBuffer(buffer, this.time, x, y);
            if (!component.isClickWithin(x, y)) continue;
            const tooltip = component.getTooltipAt(x, y);
            if (tooltip.length > 0) {
                const frame = [tooltip];
                Gui.drawFrame(buffer, frame, x+1, y-1);
            }
        }

        // display recipe when hovered
        if (this.player.hoverSlotId !== null
            && this.player.hoverSlotId.split('_')[0] === 'recipe') {
            const recipeIndex = parseInt(this.player.hoverSlotId.split('_')[1]);
            const recipe = Inventory.recipes[recipeIndex];
            const need = recipe.need;
            const text = 'need: '+need.map(n => {
                const item_code = n.item;
                // TODO: instead of calculating block and sprite here, use the function which is currently static in InventoryComponent
                const block = Inventory.item2blockPlain(item_code);
                const sprite = BIOME_SPRITES_NO_EFFECT[block]['A'];
                const howManyHave = this.player.inventory.howManyHaveAsCraftingIngredient(item_code);
                const yesHaveEnough = howManyHave >= n.amount;
                let formattedNumber = n.amount;
                if (!yesHaveEnough) {
                    formattedNumber = '\\[#733\\]'+formattedNumber+'\\[/\\]';
                }
                return formattedNumber+'\\['+sprite.color+'\\]'+sprite.char
                +'\\[/\\]'
            }).join(', ')+' ';
            let hoverPosition = this.player.getSlotScreenPosById(
                this.player.hoverSlotId);
            if (hoverPosition !== null) {
                Gui.drawFrame(buffer, [text], hoverPosition.x+16, hoverPosition.y);
            }
            // hoverPosition is null because the hovered slot is
            // not displayed (happens during resizing)
            // so do not display the recipe
        }

        // display dragged item under mouse
        if (this.player.draggedAmount > 0 && this.player.dragFromId !== null) {
            const item = this.player.getItemBySlotId(this.player.dragFromId);
            const block = Inventory.item2blockPlain(item.code);
            const sprite = BIOME_SPRITES_NO_EFFECT[block]['A'];
            const number = this.player.draggedAmount * (item.displayMultiplier ? item.displayMultiplier : 1);
            const text = '\\['+sprite.color+'\\]'+sprite.char
                +'\\[#ffffff\\] '+number;
            let position;
            if (this.player.usingmousecontrols) {
                // in mouse controls, follow mouse
                position = {
                    x: mousescreenx,
                    y: -this.player.mousey + Math.floor(h/2),
                };
            } else {
                // in keyboard controls, follow keyboard hover
                const hoverPosition = this.player.getSlotScreenPosById(
                    this.player.hoverSlotId);
                if (hoverPosition === null) {
                    // for some reason, the cursor is not on screen.
                    // so just display this in the corner.
                    position = {
                        x: 0,
                        y: 0,
                    };
                } else {
                    position = {
                        x: hoverPosition.x-5,
                        y: hoverPosition.y,
                    };
                }
            }
            Gui.drawFrame(buffer, [text], position.x, position.y);
        }

        // display all of this on screen
        // first display only non-vortex parallax
        for (let y = 0; y < h; y++) {
            for (let x = 0; x < w; x++) {
                if (this.player.vortex) continue;
                if (buffer[y][x] === undefined) continue;
                if (!buffer[y][x].isparallax) continue;
                if (buffer[y][x].char === ' ') continue;
                let posx = x;
                let posy = y;
                if (buffer[y][x].offset !== undefined) {
                    posx += buffer[y][x].offset.x;
                    posy += buffer[y][x].offset.y;
                }
                this.terminal.displayCharacter(
                    posx,
                    posy,
                    buffer[y][x].char,
                    buffer[y][x].color);
            }
        }

        // now display everything except non-vortex parallax
        for (let y = 0; y < h; y++) {
            for (let x = 0; x < w; x++) {
                if (buffer[y][x] === undefined) continue;
                if (buffer[y][x].isparallax
                    && !this.player.vortex) continue;
                if (buffer[y][x].isparallax) {
                    if (buffer[y][x].char === ' '
                        && !this.player.vortex) continue;
                }
                let posx = x;
                let posy = y;
                // display secondchar (only if there is no vortex)
                if (typeof buffer[y][x].secondchar !== 'undefined'
                    && !this.player.vortex) {
                    const secondchar = buffer[y][x].secondchar;
                    this.terminal.displayCharacterScaled(
                        posx + secondchar.dx,
                        posy + secondchar.dy,
                        secondchar.scale,
                        secondchar.char,
                        secondchar.color);
                }
                if (buffer[y][x].sway) {
                    // animated leaves
                    posx += Math.sin(x/10+y/10+this.time*0.03+1)*0.15
                        +Math.sin(x/3+y/3+this.time*0.1+2)*0.05;
                    posy += Math.sin(x/10+this.time*0.03+2)*0.08
                        +Math.sin(x/3+this.time*0.1+3)*0.02;
                }
                if (buffer[y][x].flow) {
                    // animated water
                    posx += Math.sin(x*x-y+top+this.time*1.2+2)*0.02;
                }
                if (buffer[y][x].blinkspeed !== undefined) {
                    let shade = 0;
                    const baseColor = '#000000';
                    const addColor = buffer[y][x].color;
                    switch (buffer[y][x].blinkspeed) {
                        case 1:
                            // blink slowly
                            shade = Math.cos(this.time*0.314)*0.4+0.6;
                            break;
                        case 2:
                            // blink faster
                            shade = Math.cos(this.time*0.628)*0.4+0.6;
                            break;
                        case 3:
                            // blink very fast
                            shade = Math.cos(this.time*1.256)*0.4+0.6;
                            break;
                        case 4:
                            // blink super fast
                            shade = this.time % 2 ? 1 : 0.5
                            break;
                    }
                    const newColor = ADD_COLORS_HEX(baseColor, addColor, shade);
                    buffer[y][x] = {
                        char: buffer[y][x].char,
                        color: newColor,
                        invert: buffer[y][x].invert,
                    };
                }
                if (buffer[y][x].misplace !== undefined) {
                    // misplaced t
                    const wx = x + left;
                    const wy = top + h - y;

                    const which_t = ((wx*
                        4250351207
                    )%
                        51283
                        +(wy*
                            4190530093
                        )%
                        49811
                    )%8;

                    posx += (Math.abs(which_t) - 4)/32
                        * buffer[y][x].misplace;
                }
                if (buffer[y][x].offset !== undefined) {
                    posx += buffer[y][x].offset.x;
                    posy += buffer[y][x].offset.y;
                }
                if (typeof buffer[y][x].dy !== 'undefined') {
                    posy -= buffer[y][x].dy;
                }
                if ((buffer[y][x].isbiomesprite || buffer[y][x].isparallax)
                    && this.player.vortex) {
                    const vortexx = this.vortexwx-left+0.4;
                    const vortexy = -this.vortexwy+top+h-1+0.45;
                    const diffx = posx-vortexx;
                    const diffy = posy-vortexy;
                    const euclidean = Math.pow(diffx*this.terminal.characterAspectRatio(),2)+Math.pow(diffy,2);
                    const sqeffectrange = VORTEX_EFFECT_RANGE*VORTEX_EFFECT_RANGE;
                    let edgeinterpolation = (sqeffectrange-euclidean) / sqeffectrange;
                    if (edgeinterpolation < 0) {
                        // no effect (to save cpu processing)
                        if (typeof buffer[y][x].secondchar !== 'undefined') {
                            // display secondchar (no distortion)
                            const secondchar = buffer[y][x].secondchar;
                            this.terminal.displayCharacterScaled(
                                posx + secondchar.dx,
                                posy + secondchar.dy,
                                secondchar.scale,
                                secondchar.char,
                                secondchar.color);
                        }
                        this.terminal.displayCharacter(
                            posx,
                            posy,
                            buffer[y][x].char,
                            buffer[y][x].color);
                    } else {
                        edgeinterpolation = Math.pow(edgeinterpolation, 2);
                        const intensity = Math.pow(euclidean + 0.2, -0.7)*this.vortexs;
                        let angle = intensity;
                        const rotSpeed = 1/euclidean;
                        const threshold1 = 1-0.7*this.vortexs;
                        const threshold2 = 1-0.8*this.vortexs;
                        if (intensity > threshold1) {
                            angle += 0.5*this.time*rotSpeed;
                            if (buffer[y][x].char === ' ') {
                                const spookySprite  = Gui.makeSpookySprite(
                                    '#s', currentBiome, this.curBgColor, 0, 0);
                                buffer[y][x] = spookySprite;
                            }
                        }
                        else if (intensity > threshold2) angle -= this.time*rotSpeed;
                        const displacement = (10/(Math.pow(euclidean, -0.3)+1)-2)*this.vortexs;
                        if (typeof buffer[y][x].secondchar !== 'undefined') {
                            // display secondchar with same distortion
                            const secondchar = buffer[y][x].secondchar;
                            this.terminal.displayCharacterOffsetPolar(
                                posx+secondchar.dx+displacement*diffx/euclidean*edgeinterpolation,
                                posy+secondchar.dy+displacement*diffy/euclidean*edgeinterpolation,
                                angle*edgeinterpolation,
                                vortexx+0.5,
                                vortexy+0.5,
                                1,
                                secondchar.char,
                                secondchar.color);
                        }
                        this.terminal.displayCharacterOffsetPolar(
                            posx+displacement*diffx/euclidean*edgeinterpolation,
                            posy+displacement*diffy/euclidean*edgeinterpolation,
                            angle*edgeinterpolation,
                            vortexx+0.5,
                            vortexy+0.5,
                            1,
                            buffer[y][x].char,
                            buffer[y][x].color);
                    }
                } else if (buffer[y][x].invert) {
                    if (buffer[y][x].char === '▇') {
                        // inverting this character would make it invisible.
                        // instead, change it to ▒
                        this.terminal.displayCharacter(
                            posx,
                            posy,
                            '▒',
                            buffer[y][x].color);
                    } else {
                        // invert character
                        this.terminal.displayCharacter(
                            posx,
                            posy,
                            '▇',
                            buffer[y][x].color);
                        this.terminal.displayCharacter(
                            posx,
                            posy,
                            buffer[y][x].char,
                            '#000');
                    }
                } else {
                    this.terminal.displayCharacter(
                        posx,
                        posy,
                        buffer[y][x].char,
                        buffer[y][x].color);
                }
            }
        }

        // display cursor overlayed
        if (this.player.uiState === 'normal') {
            // No inventories or overlays are being shown.
            // ░ █ ▇
            const x = Math.floor(w / 2) + this.player.getViewCursorX();
            const y = h - Math.ceil(h / 2) - this.player.getViewCursorY();
            let char = '▇';
            if (this.player.digtime > 0) char = '▓';
            if (this.player.digtime > this.player.digduration*0.35) char = '▒';
            if (this.player.digtime > this.player.digduration*0.7) char = '░';
            if (!buffer[y]) {
                // cursor is in an invalid spot
            }
            else if (!buffer[y][x]) {
                // cursor in air
                this.terminal.displayCharacter(x, y, char,'#333');
            }
            else if (buffer[y][x].isparallax) {
                // cursor on background
                // display same as on air
                this.terminal.displayCharacter(x, y, char,'#333');
            }
            else if (buffer[y][x].char === '▀') {
                // cursor on sign is completely different
                char = '▀';
                let color = '#aaa';
                if (this.player.digtime > 0) {char = '░'; color = this.curBgColor}
                if (this.player.digtime > this.player.digduration*0.35) {char = '▒'; color = this.curBgColor}
                if (this.player.digtime > this.player.digduration*0.7) {char = '▓'; color = this.curBgColor}
                this.terminal.displayCharacter(x, y, char, color);
            }
            else if (buffer[y][x].char === '▇') {
                // cursor on frame
                this.terminal.displayCharacter(x, y, char, '#aaa');
            }
            else {
                // cursor on something else.
                // TODO: Most code here is duplicated from the earlier character displaying and should be generalized.
                this.terminal.displayCharacter(x, y, char,
                    buffer[y][x].color);
                let posx = x;
                let posy = y;
                if (typeof buffer[y][x].secondchar !== 'undefined') {
                    const secondchar = buffer[y][x].secondchar;
                    this.terminal.displayCharacterScaled(
                        posx + secondchar.dx,
                        posy + secondchar.dy,
                        secondchar.scale,
                        secondchar.char,
                        secondchar.color);
                }
                if (buffer[y][x].sway) {
                    // animated leaves
                    posx += Math.sin(x/10+y/10+this.time*0.03+1)*0.15
                        +Math.sin(x/3+y/3+this.time*0.1+2)*0.05;
                    posy += Math.sin(x/10+this.time*0.03+2)*0.08
                        +Math.sin(x/3+this.time*0.1+3)*0.02;
                }
                if (buffer[y][x].misplace !== undefined) {
                    // misplaced t
                    const wx = x + left;
                    const wy = top + h - y;

                    const which_t = ((wx*
                        4250351207
                    )%
                        51283
                        +(wy*
                            4190530093
                        )%
                        49811
                    )%8;

                    posx += (Math.abs(which_t) - 4)/32
                        * buffer[y][x].misplace;
                }
                if (typeof buffer[y][x].dy !== 'undefined') {
                    posy -= buffer[y][x].dy;
                }
                this.terminal.displayCharacter(
                    posx,
                    posy,
                    buffer[y][x].char,
                    '#000');
            }
        }
        // particles
        for (const particle of this.particles) {
            const dirx = particle.dirx;
            const diry = particle.diry;
            const age = particle.age;
            const lifetime = particle.lifetime;
            let dx = age * dirx;
            let dy = age * diry;
            let rot = particle.age*-0.1*particle.rot;
            let alpha = 1;
            switch (particle.type) {
                case 'leaf':
                    dy = -age*0.05 - Math.sin(age*0.2)*0.2;
                    alpha = (1-age/lifetime)*0.8;
                    break;
                case 'droplet':
                    alpha = 1.2-age/15;
                    break;
                case 'snowflake':
                    dx = age*-dirx+lifetime/2*dirx;
                    dy = -age*0.05 - Math.sin(age*0.2)*0.2;
                    alpha = (1 - Math.cos(age/lifetime*6.28))*0.2;
                    break;
                case 'firefly':
                    dx = Math.sin(age*0.3) + Math.sin(age*0.04)*2;
                    dy = -Math.sin(age*0.2) + Math.sin(age*0.05);
                    alpha = (1 - Math.pow(Math.cos(age/lifetime*6.28)/2+0.5,2))*0.5;
                    break;
                case 'emit':
                    alpha = (1-age/lifetime)*0.7;
                    break;
                case 'channeling':
                    dx = (-age+age*age*0.05)*dirx*0.5;
                    alpha = (1-age/50)*0.8;
                    break;
                case 'collect':
                    const dist = 2-2*age*age/lifetime/lifetime;
                    dx = dirx*dist;
                    dy = diry*dist;
                    alpha = Math.min(age/8, 0.6);
                    break;
            }
            const sx = particle.wx - left + dx;
            const sy = particle.wy - top + dy;
            this.terminal.displayCharacterRotated(
                sx,
                h-sy-1,
                rot,
                alpha,
                particle.char,
                particle.color,
            );
        }
        if (this.player.warpWarmup > 0) {
            const a = this.player.warpWarmup;
            const b = 0.005 * a * a + (a > 15 ? (a-15)*0.7 : 0);
            const wx = this.player.x;
            const wy = this.player.y + 5 - b;
            this.terminal.displayCharacter(
                wx - left,
                h - (wy-top) - 1,
                'R',
                BIOME_SPRITES['R1'][currentBiome].color
            );
        }
        // particle death
        this.particles = this.particles.map(
            l=>{l.age++; return l;}).filter(l=>l.age<l.lifetime);

        // display vortex face
        if (this.player.vortex) {
            // const vx = this.player.vortexwx-left+0.4;
            // const vy = -this.player.vortexwy+top+h-1+0.45
            const vx = this.vortexwx-left+0.4;
            const vy = -this.vortexwy+top+h-1+0.45
                +Math.sin(this.time*0.3+2)*0.08;
            const vc = '#bbbbbb';
            if (this.player.vortexface === 'sleep') {
                this.drawOverlayCharacter(vx-3, vy, '_', vc);
                this.drawOverlayCharacter(vx-2, vy, '_', vc);
                this.drawOverlayCharacter(vx+2, vy, '_', vc);
                this.drawOverlayCharacter(vx+3, vy, '_', vc);
            } else if (this.player.vortexface === 'awake') {
                this.drawOverlayCharacter(vx-3, vy-1, '_', vc);
                this.drawOverlayCharacter(vx-2, vy-1, '_', vc);
                this.drawOverlayCharacter(vx+2, vy-1, '_', vc);
                this.drawOverlayCharacter(vx+3, vy-1, '_', vc);
                this.drawOverlayCharacter(vx-2, vy, '#', vc);
                this.drawOverlayCharacter(vx+2, vy, '#', vc);
            } else if (this.player.vortexface === 'angrysleep') {
                this.drawOverlayCharacter(vx-2, vy, '\\', vc);
                this.drawOverlayCharacter(vx+2, vy, '/', vc);
            } else if (this.player.vortexface === 'lookplayer') {
                this.drawOverlayCharacter(vx-3, vy-1, '_', vc);
                this.drawOverlayCharacter(vx-2, vy-1, '_', vc);
                this.drawOverlayCharacter(vx+2, vy-1, '_', vc);
                this.drawOverlayCharacter(vx+3, vy-1, '_', vc);
            } else if (this.player.vortexface === 'angryplayer') {
                this.drawOverlayCharacter(vx-2, vy-1, '\\', vc);
                this.drawOverlayCharacter(vx+2, vy-1, '/', vc);
            } else if (this.player.vortexface === 'afraid') {
                this.drawOverlayCharacter(vx-2, vy-1, '/', vc);
                this.drawOverlayCharacter(vx+2, vy-1, '\\', vc);
                this.drawOverlayCharacter(vx-2, vy, '#', vc);
                this.drawOverlayCharacter(vx+2, vy, '#', vc);
            } else if (this.player.vortexface === 'uwu') {
                this.drawOverlayCharacter(vx-2, vy, 'u', vc);
                this.drawOverlayCharacter(vx+2, vy, 'u', vc);
                this.drawOverlayCharacter(vx, vy+1, 'w', vc);
            }
            if (this.player.vortexface === 'angryplayer'
                || this.player.vortexface === 'lookplayer') {
                let targetx = this.player.x;
                for (const key of Object.keys(this.player.positionList)) {
                    if (key === this.player.vortextarget) {
                        const p = this.player.positionList[key];
                        targetx = p.x;
                        break;
                    }
                }
                if (targetx > this.vortexwx+4) {
                    this.drawOverlayCharacter(vx-2, vy, '#', vc);
                    this.drawOverlayCharacter(vx+3, vy, '#', vc);
                } else if (targetx < this.vortexwx-4) {
                    this.drawOverlayCharacter(vx-3, vy, '#', vc);
                    this.drawOverlayCharacter(vx+2, vy, '#', vc);
                } else {
                    this.drawOverlayCharacter(vx-2, vy, '#', vc);
                    this.drawOverlayCharacter(vx+2, vy, '#', vc);
                }
            }
        }

    }

    // TODO: combine this with display()
    displayOverview(){
        const scale = this.overviewzoom;
        const currentBiome = this.map.biomeAt(
            this.player.x, this.player.y);
        const newBgColor = BIOMES[currentBiome].sky;

        this.terminal.clearScreen();
        this.terminal.setBackground(newBgColor);

        const w = this.terminal.width;
        const h = this.terminal.height;
        const ww = Math.floor(w / 2);
        const hh = Math.floor(h / 2);
        // adjust player coords to be divisible by scale
        this.player.attachCameraIfOutsideScreen(w*scale, h*scale);
        const px = Math.floor(this.player.getViewX() / scale) * scale;
        const py = Math.floor(this.player.getViewY() / scale) * scale;

        const borderShade = Math.cos(this.time/3)*0.5+0.5;
        const borderColor = ADD_COLORS_HEX(
            '#666666', '#aaaaaa', borderShade*borderShade);

        const buffer = [];

        // render the terrain / world / map
        for(let y = 0; y < h; y++){
            buffer[h - y - 1] = [];
            for(let x = 0; x < w; x++){
                if (y < 2 || y >= h-2 || x < 2 || x >= w-2) {
                    let char = null;
                    if (y === 1) {
                        if (x === 1) char = '╚';
                        else if (x === w-2) char = '╝';
                        else if (x > 1 && x < w-2) char = '═';
                    } else if (y === h-2) {
                        if (x === 1) char = '╔';
                        else if (x === w-2) char = '╗';
                        else if (x > 1 && x < w-2) char = '═';
                    } else if (y > 1 && y < h-2) {
                        if (x === 1 || x === w-2) char = '║';
                    }
                    if (!char) {
                        const index = x+h-1-y;
                        const word = this.player.vortex ? ' MAP IS ###   ' : ' MAP IS OPEN   CLOSE MAP WITH M   ZOOM MAP WITH + -   MAP   MAP   MAP   YES THIS IS THE MAP   MAP GIVES YOU A MAP OVERVIEW OF THE WORLD   MAP   MAP   MAP IS NOT SHOWING EVERY BLOCK   DO NOT GET CONFUSED BY MAP   MAP   MAP   MAP   WOW YOUR SCREEN IS LARGE IF YOU ARE ABLE TO READ THIS TEXT BUT ANYWAY   MAP   MAP   MAP   MAP   MAP   MAP  ';
                        char = word[index%word.length];
                    }
                    if (char) {
                        buffer[h - y - 1][x] = {char: char,
                            color: borderColor };
                    }
                    continue;
                }

                if (this.player.vortex) continue;

                const dxmid = (w/2-x)*scale;
                const dymid = (h/2-y)*scale;
                if (dxmid*dxmid+dymid*dymid*2 > 200000) continue;

                // find world coords of block to display
                // but the coordinates have to be divisible by scale
                const worldx = px - Math.floor(w/2)*scale + x*scale;
                const worldy = py - Math.floor(h/2)*scale + y*scale;

                if (this.settings.s['chunkbordervisible'] &&
                    (worldx % 256 === 0 || worldy % 256 === 0)) {
                    buffer[h - y - 1][x] = {char: '▖', color: '#f00'};
                    continue;
                }

                const block = this.map.getBlock(worldx, worldy);
                const biome = this.map.biomeAt(worldx, worldy);

                if (block[0] === '#') { // hidden in overview
                    continue;
                }
                else if (block === 'B' || block === 'd') {
                    const wx = x + Math.floor(px/scale);
                    const wy = y + Math.floor(py/scale);
                    const which_B = ((wx*
                        4250351207
                    )%
                        51283
                        +(wy*
                            4190530093
                        )%
                        49811
                    )%8;

                    buffer[h - y - 1][x] = BIOME_SPRITES[block+Math.abs(which_B)][biome];
                }
                else if (block[0] === '=') {
                    const trimBlock = block.substr(0,3);
                    let sprite = BIOME_SPRITES[trimBlock][biome];
                    buffer[h - y - 1][x] = sprite;
                }
                else if(BIOME_SPRITES[block]){ // fails for air
                    let sprite = BIOME_SPRITES[block][biome];
                    if (sprite.rainbow) {
                        const which_rainbow = (worldx%8+8)%8;
                        sprite = BIOME_SPRITES[block+which_rainbow][biome];
                    }
                    buffer[h - y - 1][x] = sprite;
                }
            }
        }

        // render other players
        for (const key of Object.keys(this.player.positionList)) {
            const p = this.player.positionList[key];
            const sx = Math.floor((p.x - px) / scale) + ww;
            const sy = Math.floor((p.y - py) / scale) + hh;
            if (sx>=0 && sx<w && sy>=0 && sy<h) {
                buffer[h - sy - 1][sx] = {char:'P', color:'#'+p.c};
                // display other player name
                if (this.player.inspectKeyPressed) {
                    const frame = ['\\[#'+p.c+'\\]'+key];
                    Gui.drawFrame(buffer, frame, sx+1, h-sy-1-1);
                }
            }
        }

        // render my player (same as above code)
        {
            const sx = Math.floor((this.player.x - px) / scale) + ww;
            const sy = Math.floor((this.player.y - py) / scale) + hh;
            if (sx>=0 && sx<w && sy>=0 && sy<h) {
                buffer[h - sy - 1][sx] = {char:'P', color:'#'+this.player.color};
            }
        }

        if (!this.player.vortex) {
            // TODO: this is mostly duplicated code from other places in this file
            const mousescreenx = this.player.mousex + Math.floor(w/2);
            const mousescreeny = this.player.mousey + Math.ceil(h/2)-1;
            const highlightscreenx = mousescreenx;
            const highlightscreeny = mousescreeny;
            const highlightworldx = px - Math.floor(w/2)*scale + highlightscreenx*scale;
            const highlightworldy = py - Math.floor(h/2)*scale + highlightscreeny*scale;
            if (this.player.mouserecentlymoved) {
                const biome_code = this.map.biomeAt(
                    highlightworldx, highlightworldy);
                const biome_name = BIOMES[biome_code].short;
                const frame = ['\\[#cccccc\\]' + biome_name ];
                Gui.drawFrame(buffer, frame, highlightscreenx+1, h-1-(highlightscreeny+1));
            }

        } else {
            const vdx = this.vortexwx - this.player.x;
            const vdy = this.vortexwy - this.player.y;
            const signdx = vdx < -70 ? -1 : vdx > 70 ? 1 : 0;
            const signdy = vdy < -30 ? -1 : vdy > 30 ? 1 : 0;
            let frame = [
                "",
            ];
            if (signdx < 0 && signdy < 0) {
                frame = [
                    "             ",
                    "             ",
                    "     ,;,     ",
                    " , ,;;#;:    ",
                    " :;;;;;'     ",
                    " :;;;;       ",
                    " ''''''      ",
                ];
            } else if (signdx < 0 && signdy > 0) {
                frame = [
                    " ,,,,,,      ",
                    " ;;;;;       ",
                    " ;:;;;;,     ",
                    " ' ':;#;:    ",
                    "     ':'     ",
                    "             ",
                    "             ",
                ];
            } else if (signdx > 0 && signdy > 0) {
                frame = [
                    "      ,,,,,, ",
                    "       ;;;;; ",
                    "     ,;;;;:; ",
                    "    :;#;:' ' ",
                    "     ':'     ",
                    "             ",
                    "             ",
                ];
            } else if (signdx > 0 && signdy < 0) {
                frame = [
                    "             ",
                    "             ",
                    "     ,;,     ",
                    "    :;#;;, , ",
                    "     ':;;;;; ",
                    "       ;;;;; ",
                    "      '''''' ",
                ];
            } else if (signdx > 0) {
                frame = [
                    "             ",
                    "         ,   ",
                    "     ,,,,;;, ",
                    "     ;#;;;;;:",
                    "     '''';:' ",
                    "         '   ",
                    "             ",
                ];
            } else if (signdx < 0) {
                frame = [
                    "             ",
                    "   ,         ",
                    " ,;;,,,,     ",
                    ":;;;;;#;     ",
                    " ':;''''     ",
                    "   '         ",
                    "             ",
                ];
            } else if (signdy > 0) {
                frame = [
                    "     ,;,     ",
                    "   ,;;;;;,   ",
                    "     ;;;     ",
                    "     ;#;     ",
                    "     '''     ",
                    "             ",
                    "             ",
                ];
            } else if (signdy < 0) {
                frame = [
                    "             ",
                    "             ",
                    "     ,,,     ",
                    "     ;#;     ",
                    "     ;;;     ",
                    "   ':;;;:'   ",
                    "     ':'     ",
                ];
            }

            Gui.drawFrame(buffer, frame, ww+signdx*Math.floor(ww/2)-6, hh-signdy*Math.floor(hh/2)-3);
        }

        // display all of this on screen

        for(let y = 0; y < h; y++){
            for(let x = 0; x < w; x++){
                if(buffer[y][x]){
                    this.terminal.displayCharacter(
                        x,
                        y,
                        buffer[y][x].char,
                        buffer[y][x].color);
                }
            }
        }
    }

    focus(){
        // nothing to do, the gui is just a display and does not require focus for anything
    }

    blur(){
        // nothing to do, the gui is just a display and does not require focus for anything
    }
}
