// Copyright (C) 2018-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * An "abstract" base class for all UI components.
 */

"use strict";

class UiComponent {
    constructor(){
        this.w = 0;
        this.h = 0;
    }

    isScrollable() {
        return false;
    }

    getScrollPercentage() {
        const fraction = this.scrollPos / this.getMaxScrollPos();
        return Math.floor(fraction*100).toString();
    }

    isInventory() {
        return false;
    }

    resizeTerminal(w, h) {
        this.w = w;
        this.h = h;
    }

    // with 0,0 at start of canvas
    isClickWithin(x, y) {
        return false;
    }

    // click is in character coordinates
    handleClick(x, y) {
        return false;
    }

    isShown() {
        return true;
    }

    getFrame() {
        const frame = ['@'];
        return frame;
    }

    getFramePos() {
        return {x: 0, y: 0};
    }

    screenToLocal(x, y) {
        const framePos = this.getFramePos();
        const xlocal = x - framePos.x;
        const ylocal = y - framePos.y;
        return {x: xlocal, y: ylocal};
    }

    localToScreen(x, y) {
        const framePos = this.getFramePos();
        const xlocal = x + framePos.x;
        const ylocal = y + framePos.y;
        return {x: xlocal, y: ylocal};
    }

    drawOnBuffer(buffer, time, hoverx, hovery) {
        const frame = this.getFrame(time, hoverx, hovery);
        const framePos = this.getFramePos();

        Gui.drawFrame(buffer, frame,
            framePos.x, framePos.y, GUI_FRAME_COLOR);
    }

    getTooltipAt(x, y) {
        return '';
    }

}

class ScrollableComponent extends UiComponent {
    constructor() {
        super();
        this.scrollPos = 0;
    }

    isScrollable() {
        return true;
    }

    getContentHeight() {
        throw "ScrollableComponent getContentHeight called!"
    }

    getNumVisualizedSlots() {
        return this.getFrameHeight() - 2;
    }

    getMaxScrollPos() {
        let maxScrollPos = this.getContentHeight() - this.getNumVisualizedSlots();
        // Never allow scrolling above the top item in inventory:
        if (maxScrollPos < 0) maxScrollPos = 0;
        return maxScrollPos;
    }

    scrollUp() {
        this.scrollPos--;
        this.limitScrollPos();
    }

    scrollDown() {
        this.scrollPos++;
        this.limitScrollPos();
    }

    limitScrollPos() {
        if (this.scrollPos < 0) {
            this.scrollPos = 0;
        }
        const maxScrollPos = this.getMaxScrollPos();
        if (this.scrollPos > maxScrollPos) {
            this.scrollPos = maxScrollPos;
        }
    }

}
