// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class Music{
    constructor(cli, settings){
        this.cli = cli;
        this.settings = settings;
        this.songList = [];
        this.prevsong = -1;
        this.fading = false;
        this.fadeStepSize = 0;
        this.volumeFadeLevel = 1;
        this.loopingms = 0;
        this.loopiterator = 0;
        this.delayms = 0;
        this.delayaction = null;
    }

    tick() {  // should be called every 100ms
        if (this.delayms > 0) {
            this.delayms -= 100;
        }
        if (this.delayms <= 0 && this.delayaction !== null) {
            // handle this action at end of delay.
            if (this.delayaction.type === 'playonce') {
                this.playMusicOnce();
            }
            else if (this.delayaction.type === 'startloop') {
                this.startMusicLoop(this.delayaction.interval);
            }
            this.delayaction = null;
        }
        if (this.delayms <= 0 && this.loopingms > 0) {
            this.loopiterator += 100;
            if (this.loopiterator >= this.loopingms) {
                this.nextSong();
                this.fading = false;
                this.loopiterator = 0;
            }
        }

        if (this.fading) {
            if (this.volumeFadeLevel < 0.01) {
                if (this.isPlaying()) {
                    this.audio_mus.pause();
                }
                this.fading = false;
                return;
            }
            if (this.audio_mus) {
                this.audio_mus.volume = this.volumeFadeLevel * 0.01 * this.settings.eqs['volume'];
                this.volumeFadeLevel -= this.fadeStepSize;
            }
        }
    }

    resetVolume() {
        if (this.fading) {
            this.fading = false;
        }
        this.audio_mus.volume = 0.01 * this.settings.eqs['volume'];
    }

    setSongList(songList) {
        this.songList = songList;
    }

    startMusicLoop(intervalSeconds) {
        this.stopMusic();
        this.nextSong();
        this.loopingms = intervalSeconds*1000;
        this.loopiterator = 0;
    }

    startMusicLoopAfterSeconds(s, intervalSeconds) {
        this.delayms = s*1000;
        this.delayaction = {type: 'startloop', interval: intervalSeconds};
    }

    playMusicOnce() {
        this.stopMusic();

        this.audio_mus = new Audio(this.songList[0].file);
        this.audio_mus.volume = 0.01 * this.settings.eqs['volume'];
        this.audio_mus.play();
    }

    playMusicOnceAfterSeconds(s) {
        this.delayms = s*1000;
        this.delayaction = {type: 'playonce'};
    }

    stopMusic() {
        this.fading = false;
        this.volumeFadeLevel = 1;
        this.loopingms = 0;
        this.loopiterator = 0;
        this.delayms = 0;
        this.delayaction = null;
        if (this.audio_mus) {
            this.audio_mus.pause();
        }
        this.prevsong = -1;
    }

    isPaused() {
        if (!this.audio_mus) return false;
        return this.audio_mus.paused;
    }

    isPlaying() {
        if (!this.audio_mus) return false;
        return !this.audio_mus.paused;
    }

    fadeOut(seconds) {
        if (!this.fading) {
            // only reset volume level if not currently fading already
            this.volumeFadeLevel = 1;
        }
        this.fading = true;
        this.fadeStepSize = 0.1/seconds;
    }

    nextSong() {
        if (this.isPlaying()) return;
        let chosenSong = 0;
        if (this.songList.length > 1) {
            // choose randomly (must be different from previous song)
            do {
                chosenSong = Math.floor(Math.random()*this.songList.length);
            } while (chosenSong === this.prevsong);
        }
        this.prevsong = chosenSong;
        this.audio_mus = new Audio(this.songList[chosenSong].file);
        if (this.settings.s.listmusic) {
            this.printNowPlaying();
        }
        this.audio_mus.volume = 0.01 * this.settings.eqs['volume'];
        this.audio_mus.play();
    }

    printNowPlaying() {
        if (this.prevsong > -1) {
            this.cli.println('*Now playing: '+this.songList[this.prevsong].title+' by '+this.songList[this.prevsong].artist+'*');
        } else {
            this.cli.println('*Not currently playing music.');
        }
    }

}
