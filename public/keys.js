// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Remembers the states of keys
 * so they can be polled at every game tick.
 * 
 * This gets around the issue of the browser sandbox
 * not allowing direct polling of keys.
 */

"use strict";

class Keys{
    constructor(){
        this.keyStates = [];
        this.freshKeys = [];
        this.freshKeyList = [];
        this.mbStates = [];
        this.freshMbs = [];
        this.freshMbList = [];
    }

    handleKeyDown(keyCode){
        if (typeof(printkeys) !== 'undefined' && printkeys === true) {
            console.log(keyCode);
        }
        if (!this.keyStates[keyCode]) {
            this.keyStates[keyCode] = true;
            this.freshKeys[keyCode] = true;
            this.freshKeyList.push(keyCode);
        }
    }

    handleKeyUp(keyCode){
        this.keyStates[keyCode] = false;
    }

    handleMbDown(mbCode){
        if (!this.mbStates[mbCode]) {
            this.mbStates[mbCode] = true;
            this.freshMbs[mbCode] = true;
            this.freshMbList.push(mbCode);
        }
    }

    handleMbUp(mbCode){
        this.mbStates[mbCode] = false;
    }


    updateMbState(e) {
        switch(e.buttons) {
            case 0:
                this.handleMbUp(1);
                this.handleMbUp(2);
                this.handleMbUp(3);
                break;
            case 1:
                this.handleMbDown(1);
                this.handleMbUp(2);
                this.handleMbUp(3);
                break;
            case 2:
                this.handleMbUp(1);
                this.handleMbDown(2);
                this.handleMbUp(3);
                break;
            case 3:
                this.handleMbDown(1);
                this.handleMbDown(2);
                this.handleMbUp(3);
                break;
            case 4:
                this.handleMbUp(1);
                this.handleMbUp(2);
                this.handleMbDown(3);
                break;
            case 5:
                this.handleMbDown(1);
                this.handleMbUp(2);
                this.handleMbDown(3);
                break;
            case 6:
                this.handleMbUp(1);
                this.handleMbDown(2);
                this.handleMbDown(3);
                break;
            case 7:
                this.handleMbDown(1);
                this.handleMbDown(2);
                this.handleMbDown(3);
                break;
        }
    }


    clearFresh(){
        this.freshKeys = [];
        this.freshKeyList = [];
        this.freshMbs = [];
        this.freshMbList = [];
    }

    getFullKeyStates(){
        const fullKeyStates = this.keyStates.slice();
        for(const k of this.freshKeyList){
            fullKeyStates[k] = true;
        }
        return fullKeyStates;
    }

    getFullMbStates(){
        const fullMbStates = this.mbStates.slice();
        for(const k of this.freshMbList){
            fullMbStates[k] = true;
        }
        return fullMbStates;
    }

    clearAll() {
        this.clearFresh();
        this.keyStates = [];
        this.mbStates = [];
    }

}
