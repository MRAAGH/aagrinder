// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class Notify {
    static MESSAGE_LIFETIME = 50;  // constant

    constructor() {
        this.messages = [];
    }

    send(text) {
        for (const message of this.messages) {
            if (message.rawtext === text) {
                // found this message already in the list
                message.repeat++;
                message.text = '(' + message.repeat + 'x) ' + message.rawtext;
                message.age = 0;
                return;
            }
        }
        const newMessage = {
            rawtext: text,
            text: text,
            repeat: 1,
            age: 0,
        };
        this.messages.push(newMessage);
    }

    sendAmount(prefix, amount, suffix) {
        for (const message of this.messages) {
            if (message.prefix === prefix && message.suffix === suffix) {
                // found this message already in the list
                message.amount += amount;
                message.text = prefix + message.amount + suffix;
                message.age = 0;
                return;
            }
        }
        // did not find this message in the list. Make new:
        const newMessage = {
            prefix: prefix,
            suffix: suffix,
            amount: amount,
            text: prefix + amount + suffix,
            age: 0,
        };
        this.messages.push(newMessage);
    }

    tick() {
        this.messages = this.messages.map(m => {m.age++; return m;})
            .filter(m=>m.age<Notify.MESSAGE_LIFETIME);
    }

}
