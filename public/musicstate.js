// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

class MusicState {
    constructor(login, cli, settings) {
        this.login = login;
        this.settings = settings;
        this.music = new Music(cli, settings);
        this.state = 'disabled';

        this.songLists = {
            loginscreen: [
                {
                    file: 'audio/music/ga04_Haydn_Seek.ogg',
                    title: 'Haydn Seek',
                    artist: 'Garrett Aldridge',
                },
            ],
            normal: [
                {
                    file: 'audio/music/ga01_O_er_Yonder.ogg',
                    title: 'O\'er Yonder',
                    artist: 'Garrett Aldridge',
                },
                {
                    file: 'audio/music/ga02_I_ll_Go_Where_You_Go.ogg',
                    title: 'I\'ll Go Where You Go',
                    artist: 'Garrett Aldridge',
                },
                {
                    file: 'audio/music/ga05_Era_On_Repeat.ogg',
                    title: 'Era On Repeat',
                    artist: 'Garrett Aldridge',
                },
                {
                    file: 'audio/music/sa01_A_Magic_World.ogg',
                    title: 'A Magic World',
                    artist: '[BC]afGun',
                },
                {
                    file: 'audio/music/sa02_Probe_Away.ogg',
                    title: 'Probe Away',
                    artist: '[BC]afGun',
                },
                {
                    file: 'audio/music/sa03_Lunar_Eclipse.ogg',
                    title: 'Lunar Eclipse',
                    artist: '[BC]afGun',
                },
            ],
            creepy: [
                {
                    file: 'audio/music/ga03_Followed.ogg',
                    title: 'Followed',
                    artist: 'Garrett Aldridge',
                },
            ],
        }

    }

    tick() {
        this.updateState();
        this.music.tick();
    }

    updateState() {
        const correctState = this.chooseCorrectState();
        if (this.state === correctState) return;
        this.setState(correctState);
    }

    forceUpdateState() {
        const correctState = this.chooseCorrectState();
        this.setState(correctState);
    }

    chooseCorrectState() {
        if (!this.settings.s.music) return 'disabled';
        if (!this.login.socket.connected) return 'disabled';
        if (!this.login.inGame) return 'loginscreen';
        if (this.login.game.player.vortex) return 'creepy';
        return 'normal';
    }

    setState(newState) {
        this.transition(this.state, newState);
        this.state = newState;
    }

    transition(fromState, toState) {
        if (toState === 'disabled') {
            this.music.stopMusic();
            return;
        }
        if (fromState === 'disabled') {
            this.music.stopMusic();
            this.music.setSongList(this.songLists[toState]);
            this.playWithCorrectInterval(toState);
            return;
        }
        if (fromState === toState) {
            // transitioning from state to itself.
            // just restarts the music.
            this.music.stopMusic();
            this.playWithCorrectInterval(toState);
            return;
        }
        if (toState === 'loginscreen') {
            // normal -> loginscreen
            // creepy -> loginscreen
            this.smoothlyTransition(toState, 5);
            return;
        }
        if (toState === 'creepy') {
            // loginscreen -> creepy
            // normal -> creepy
            this.smoothlyTransition(toState, 2);
            return;
        }
        if (fromState === 'loginscreen') {
            // loginscreen -> normal
            this.music.fadeOut(10);
            this.music.setSongList(this.songLists[toState]);
            this.music.startMusicLoopAfterSeconds(60*5, 10*60);
        }
        if (fromState === 'creepy') {
            // creepy -> normal
            this.music.fadeOut(10);
            this.music.setSongList(this.songLists[toState]);
            this.music.startMusicLoopAfterSeconds(60*10, 10*60);
        }
    }

    smoothlyTransition(toState, seconds) {
        if (this.music.isPlaying()) {
            this.music.fadeOut(seconds-0.5);
            this.music.setSongList(this.songLists[toState]);
            this.playWithCorrectIntervalAfterSeconds(toState, seconds);
        } else {
            this.music.stopMusic();
            this.music.setSongList(this.songLists[toState]);
            this.playWithCorrectInterval(toState);
        }
    }

    playWithCorrectInterval(state) {
        switch (state) {
            case 'loginscreen':
                this.music.playMusicOnce();
                return;
            case 'normal':
                this.music.startMusicLoop(10*60);
                return;
            case 'creepy':
                this.music.startMusicLoopAfterSeconds(2, 3);
                return;
        }
    }

    playWithCorrectIntervalAfterSeconds(state, seconds) {
        switch (state) {
            case 'loginscreen':
                this.music.playMusicOnceAfterSeconds(seconds);
                return;
            case 'normal':
                this.music.startMusicLoopAfterSeconds(seconds, 10*60);
                return;
            case 'creepy':
                this.music.startMusicLoopAfterSeconds(seconds, 3);
                return;
        }
    }

    isPaused() {
        return this.music.isPaused();
    }

    isPlaying() {
        return this.music.isPlaying();
    }

    resetVolume() {
        this.music.resetVolume();
    }

    printNowPlaying() {
        this.music.printNowPlaying();
    }

}
