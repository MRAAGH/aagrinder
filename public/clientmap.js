// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */



/*
 * Map is the class holding the world content.
 * This content is synchronized with the state on the server.
 * 
 * It also acts as an abstraction over chunks,
 * so that other classes do not need to worry about whether chunks exist or don't exist.
 * If other classes try to acces terrain that does not exist,
 * they will get air blocks (' ').
 * Note that the behavior is different than on the server,
 * as on the server the missing chunk would immediately get generated.
 */

"use strict";

class Map {
    constructor() {
        this.chunks = {};
    }

    getChunk(chunkx, chunky){
        if(!this.chunks[chunky]){
            return undefined;
        }
        if(!this.chunks[chunky][chunkx]){
            return undefined;
        }
        return this.chunks[chunky][chunkx];
    }

    isBlockLoaded(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        if(!this.chunks[chunky]){
            return false;
        }
        if(!this.chunks[chunky][chunkx]){
            return false;
        }
        return true;
    }

    getBlock(x, y){
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = (x%256+256)%256;
        const subchunky = (y%256+256)%256;

        if(!this.chunks[chunky]){
            return ' ';
        }
        if(!this.chunks[chunky][chunkx]){
            return ' ';
        }
        const chunk = this.chunks[chunky][chunkx];
        const block = chunk.terrain[subchunky][subchunkx];
        return block;
    }

    setBlock(x, y, block){
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = (x%256+256)%256;
        const subchunky = (y%256+256)%256;
        if(!this.chunks[chunky]){
            return;
        }
        if(!this.chunks[chunky][chunkx]){
            return;
        }
        const chunk = this.chunks[chunky][chunkx];
        chunk.terrain[subchunky][subchunkx] = block;
    }

    biomeAt(x, y){
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = ((x%256+256)%256)>>2;
        const subchunky = ((y%256+256)%256)>>3;

        if(!this.chunks[chunky]){
            return '?';
        }
        if(!this.chunks[chunky][chunkx]){
            return '?';
        }
        const chunk = this.chunks[chunky][chunkx];
        const biome = chunk.biomes[64*subchunky+subchunkx];
        if (biome === undefined) {
            console.log(chunk.biomes);
        }
        return biome;
    }

    blendedBiomeAt(x, y) {
        const dxdyBlend = [
            {dx: 0, dy: 0},
            {dx: 1, dy: 1},
            {dx: 1, dy: -1},
            {dx: -1, dy: 1},
            {dx: -1, dy: -1},
            {dx: 2, dy: 2},
            {dx: 2, dy: -2},
            {dx: -2, dy: 2},
            {dx: -2, dy: -2},
        ];
        const biomeCounts = {};
        for (const dxdy of dxdyBlend) {
            const biomeThere = this.biomeAt(x+dxdy.dx, y+dxdy.dy);
            if (biomeCounts[biomeThere] === undefined) {
                biomeCounts[biomeThere] = 0;
            } else {
                biomeCounts[biomeThere]++;
            }
        }
        const foundBiomes = Object.keys(biomeCounts);
        let mostCommonBiome = foundBiomes[0];
        for (const biome of foundBiomes) {
            if (biomeCounts[biome] > biomeCounts[mostCommonBiome]) {
                mostCommonBiome = biome;
            }
        }
        return mostCommonBiome;
    }

    // Determine who (if any) owns a claim at a specific position
    getClaim(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = (x%256+256)%256;
        const subchunky = (y%256+256)%256;

        if(!this.chunks[chunky]){
            return null;
        }
        if(!this.chunks[chunky][chunkx]){
            return null;
        }
        const chunk = this.chunks[chunky][chunkx];
        for (const claim of chunk.claims) {
            if (claim.x <= x && x <= claim.x2
                && claim.y <= y && y <= claim.y2) {
                return claim;
            }
        }
        return null;
    }

    getTileEntityAt(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        if (!chunk) {
            return [];
        }
        return chunk.tileEntities.filter(a=>a.x===x&&a.y===y);
    }

    createTileEntity(data) {
        const chunkx = data.x >> 8;
        const chunky = data.y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        if (!chunk) return;
        chunk.tileEntities.push(data);
    }

    deleteTileEntityAt(x, y) {
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.getChunk(chunkx, chunky);
        if (!chunk) return;
        chunk.tileEntities = chunk.tileEntities
            .filter(a=>a.x!==x||a.y!==y);
    }


    loadChunk(chunkx, chunky, terrain, biomes, tileEntities, claims){
        if(!this.chunks[chunky]){
            this.chunks[chunky] = {};
        }
        if(!this.chunks[chunky][chunkx]){
            this.chunks[chunky][chunkx] = new Chunk();
        }
        const res = this.chunks[chunky][chunkx].setCompressed(terrain);
        this.chunks[chunky][chunkx].biomes = biomes;
        this.chunks[chunky][chunkx].tileEntities = tileEntities;
        this.chunks[chunky][chunkx].claims = claims;
        return res;
    }

    unloadChunk(chunkx, chunky) {
        if (this.chunks[chunky]) {
            if (this.chunks[chunky][chunkx]) {
                delete this.chunks[chunky][chunkx];
            }
            if (Object.keys(this.chunks[chunky]).length === 0) {
                delete this.chunks[chunky];
            }
        }
    }

}
