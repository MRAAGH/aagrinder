// Copyright (C) 2025 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

const MONSTER_ART = [

    {  // type 0
        animations: {
            default: [0],
        },
        frames: [
            [
                '\\/     ',
                '|\\_|\\__',
                '\\_((_o/',
                '   /   ',
            ],
        ],
    },

    {  // type 1
        animations: {
            default: [0],
        },
        frames: [
            [
                '      .     ',
                '|\\_,..)\\___',
                '|@@@@@@(((o/',
                '|/\'\'--|/--"',
            ],
        ],
    },

    {  // type 2
        animations: {
            default: [0,0,0,0,1,1,1,1],
            o: [2,3],
            '^': [2,2,3,3],
        },
        frames: [
            [
                '     __',
                '    /o/',
                '  \\/(/ ',
                '  /(/ ',
                ' /_/| ',
                '//',
                '/@',
            ],
            [
                '     __',
                '    /o/',
                '  \\/(/ ',
                '  /(/ ',
                ' /_/| ',
                '@/',
                '@\\',
            ],
            [
                '     __',
                '    /o/',
                '  \\/(_\\',
                '  /(/ ',
                ' /_/| ',
                '//',
                '/@',
            ],
            [
                '     __',
                '    /o/',
                '  _/(_\\',
                '  /(/ ',
                ' /_/\\ ',
                '@/',
                '@\\',
            ],
        ],
    },


    {  // type 3
        animations: {
            default: [0,0,0,1,1,1,2,2,2,3,3,3],
            o: [0,1,2,3],
        },
        frames: [
            [
                '@@     ',
                '@_',
                '(@)',
                ' )(@(o)',
                ' @@\\_)',
                ' @@@@',
            ],
            [
                '@_     ',
                '(@)',
                '@@(',
                ' @@)(o)',
                ' @@(_)',
                ' @@@@',
            ],
            [
                '@_     ',
                '@@)',
                '@@(',
                ' @@)(o)',
                ' @(__)',
                ' @@@@',
            ],
            [
                '@@     ',
                '@@(',
                '@@)',
                ' (@@(o)',
                ' @)@@)',
                ' @(_(',
            ],
        ],
    },

];
