// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

class GuiCredits {
    constructor(terminal){
        this.terminal = terminal;
    }

    display(creditsIndex) {
        this.terminal.clearScreen();

        const w = this.terminal.width;
        const h = this.terminal.height;

        for (let screenrow = 0; screenrow < h; screenrow++) {
            const lineInCredits = screenrow-h+1+creditsIndex;
            if (lineInCredits < 0 || lineInCredits >= CREDITS.length) {
                // not a line of credits
                continue;
            }
            for (let i = 0; i < CREDITS[lineInCredits].length; i++) {
                const left = Math.floor(w/2-CREDITS[lineInCredits].length/2);
                this.terminal.displayCharacter(left+i, screenrow, CREDITS[lineInCredits][i], '#777');
            }
        }

        return creditsIndex - h < CREDITS.length;

    }
}
