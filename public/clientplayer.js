// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * The Player class is the Model of the MVC structure.
 * This class holds a lot of client-side state and an interface to it,
 * allowing it to be accessible from everywhere client-side
 */

"use strict";

const CURSOR_TOLERANCE_BEYOND_REACH_X = 11;
const CURSOR_TOLERANCE_BEYOND_REACH_Y = 7;
const CURSOR_TOLERANCE_BEYOND_INVENTORY_X = 6;
const PLAYER_MAXFOOD = 1000;
const INV_LEN = Inventory.PLAYER_INVENTORY_SLOT_COUNT;
const LEFT_INV_LEN = INV_LEN-6;
const MAX_OPENED_AATERMINALS = 1;

class Player {
    constructor(color, settings){
        this.settings = settings;
        this.uiState = 'normal';
        this.chatScope = 'global';
        this.whisperTarget = '';
        this.openedAAterminals = [];
        this.focusedAAterminal = -1;
        this.aafileSlot = -1;
        this.mousex = 0;
        this.mousey = 0;
        this.usingmousecontrols = false;
        this.mouserecentlymoved = false;
        this.x = 0;
        this.y = 0;
        this.cursorx = 0;
        this.cursory = 0;
        this.reach = 0;
        this.cameralocked = false;
        this.camerax = 0;
        this.cameray = 0;
        this.jump = 0;
        this.food = PLAYER_MAXFOOD;
        this.maxjump = 0;
        this.gamemode = 0;
        this.digx = 0;
        this.digy = 0;
        this.digtime = 0;
        this.digduration = 0;
        this.name = '';
        this.color = color;
        this.inventory = new Inventory(Inventory.PLAYER_INVENTORY_SLOT_COUNT, true);
        this.chest = {x:0, y:0, inventory:new Inventory()};
        this.positionList = {};
        this.typingList = {};
        this.messageList = {};
        this.animation = false;
        this.blockanimations = [];
        this.vortex = false;
        this.vortexwx = 0;
        this.vortexwy = 0;
        this.vortexface = '';
        this.vortextarget = '';
        this.vortexhints = [];
        this.vortexanimationdeadline = 0;
        this.monsters = [];
        this.monsteranimationdeadline = 0;
        this.friends = {friend:[], pending:[], requests:[]};
        this.TOTALWARPCOOLDOWN = 100;
        this.warpCooldown = 0;
        this.warpWarmup = 0;
        this.detectorBlinkSpeed = 0;
        this.seatectorBlinkSpeed = 'o';
        this.followBlinkSpeed = 0;
        this.visibleDetectors = [];
        this.visibleGhosts = [];

        // Inventory slot information
        this.slotHandlers = {};
        this.draggedAmount = 0;
        this.dragFromId = null;
        this.hoverSlotId = null;
        this.isCurrentlyHovering = false;
        this.lastHoveredLeft = null;
        this.lastHoveredRight = null;
    }

    cursorUp(){
        if(this.cursory < this.reach){
            this.cursory++;
        }
    }

    cursorDown(){
        if(this.cursory > -this.reach){
            this.cursory--;
        }
    }

    cursorRight(){
        if(this.cursorx < this.reach){
            this.cursorx++;
        }
    }

    cursorLeft(){
        if(this.cursorx > -this.reach){
            this.cursorx--;
        }
    }

    invCursorUp() {
        if (this.uiState === 'normal') return;
        const id = this.hoverSlotId;
        if (id === null) {
            this.hoverSlotId = 'inv_0';
            return;
        }
        const idSplit = id.split('_');
        const idNum = parseInt(idSplit[1]);
        const isLeft = idSplit[0] === 'inv' && idNum < LEFT_INV_LEN;
        if (isLeft) {  // somewhere in left inv
            // new selected is previous left slot
            this.hoverSlotId = 'inv_'
                + ((idNum - 1 + LEFT_INV_LEN) % LEFT_INV_LEN);
        } else if (idSplit[0] === 'inv' && idNum === INV_LEN - 1) {  // deletion slot
            if (this.uiState === 'chest') {
                // move from deletion to last chest slot
                const chestLen = this.chest.inventory.getNumExistingSlots();
                this.hoverSlotId = 'chest_' + (chestLen - 1);
                return;
            }
            const craftableRecipes = this.inventory.getCraftableRecipes();
            if (craftableRecipes.length > 0) {
                // next selected is last recipe
                this.hoverSlotId = 'recipe_' + (craftableRecipes[craftableRecipes.length - 1]);
            } else {
                // there are no recipes to select
                // next selected is the last craft slot
                this.hoverSlotId = 'inv_' + (INV_LEN - 2);
            }
        } else if (idSplit[0] === 'inv') {  // craft ingredient slot
            // next selected is previous craft slot
            this.hoverSlotId = 'inv_'
                + ((idNum - LEFT_INV_LEN - 1 + 6) % 6 + LEFT_INV_LEN);
        } else if (idSplit[0] === 'recipe') {  // currently in a recipe
            const craftableRecipes = this.inventory.getCraftableRecipes();
            const currentRecIndex = craftableRecipes.indexOf(idNum);
            if (currentRecIndex < 1) {  // first recipe or hidden recipe
                // select last craft
                this.hoverSlotId = 'inv_' + (INV_LEN - 2);
            } else {  // non-first recipe
                // select recipe before it
                this.hoverSlotId = 'recipe_' + (craftableRecipes[currentRecIndex - 1]);
            }
        } else if (idSplit[0] === 'chest') {  // in chest
            // currently somewhere in chest
            if (idNum === 0) {
                // select the deletion slot
                this.hoverSlotId = 'inv_' + (INV_LEN - 1);
            } else {
                // new selected is previous chest slot
                const chestLen = this.chest.inventory.getNumExistingSlots();
                this.hoverSlotId = 'chest_' + (idNum - 1);
            }
        }
        this.ensureSlotVisibleById(this.hoverSlotId);
        if (isLeft) {
            this.lastHoveredLeft = this.hoverSlotId;
        } else {
            this.lastHoveredRight = this.hoverSlotId;
        }
    }

    invCursorDown() {
        if (this.uiState === 'normal') return;
        const id = this.hoverSlotId;
        if (id === null) {
            this.hoverSlotId = 'inv_0';
            return;
        }
        const idSplit = id.split('_');
        const idNum = parseInt(idSplit[1]);
        const isLeft = idSplit[0] === 'inv' && idNum < LEFT_INV_LEN;
        if (isLeft) {  // somewhere in left inv
            // new selected is next left slot
            this.hoverSlotId = 'inv_' + ((idNum + 1) % LEFT_INV_LEN);
        } else if (idSplit[0] === 'inv' && idNum === INV_LEN - 2) {  // last craft ingredient
            const craftableRecipes = this.inventory.getCraftableRecipes();
            if (craftableRecipes.length > 0) {
                // next selected is the first recipe
                this.hoverSlotId = 'recipe_' + (craftableRecipes[0]);
            } else {
                // there are no recipes to select
                // next selected is the deletion slot
                this.hoverSlotId = 'inv_' + (INV_LEN - 1);
            }
        } else if (idSplit[0] === 'inv') {  // non-last craft, or delete
            if (this.uiState === 'chest') {
                // move from deletion to first chest slot
                this.hoverSlotId = 'chest_0';
                return;
            }
            // next selected is previous craft slot
            this.hoverSlotId = 'inv_' + ((idNum - LEFT_INV_LEN + 1) % 6 + LEFT_INV_LEN);
        } else if (idSplit[0] === 'recipe') {  // recipe
            // currently in a recipe
            const craftableRecipes = this.inventory.getCraftableRecipes();
            const currentRecIndex = craftableRecipes.indexOf(idNum);
            if (currentRecIndex === craftableRecipes.length - 1 
                || currentRecIndex === -1) {  // last recipe or hidden recipe
                // select deletion slot
                this.hoverSlotId = 'inv_' + (INV_LEN - 1);
            } else {  // non-last recipe
                // select recipe after it
                this.hoverSlotId = 'recipe_' + (craftableRecipes[currentRecIndex + 1]);
            }
        } else if (idSplit[0] === 'chest') {
            // currently somewhere in chest
            const chestLen = this.chest.inventory.getNumExistingSlots();
            if (idNum === chestLen-1) {
                // select the deletion slot
                this.hoverSlotId = 'inv_' + (INV_LEN - 1);
            } else {
                // new selected is previous chest slot
                this.hoverSlotId = 'chest_' + (idNum + 1);
            }
        }
        this.ensureSlotVisibleById(this.hoverSlotId);
        if (isLeft) {
            this.lastHoveredLeft = this.hoverSlotId;
        } else {
            this.lastHoveredRight = this.hoverSlotId;
        }
    }

    invCursorUpNoWrap() {
        const id = this.hoverSlotId;
        // abort if cursor would wrap
        if (id === 'inv_0') return;
        if (id === 'inv_' + LEFT_INV_LEN) return;
        if (id === 'chest_0') return;
        // ok, cursor up is allowed
        this.invCursorUp();
    }

    invCursorDownNoWrap() {
        const id = this.hoverSlotId;
        // abort if cursor would wrap
        if (id === 'inv_' + (LEFT_INV_LEN-1)) return;
        if (id === 'inv_' + (INV_LEN-1)) return;
        // ok, cursor down is allowed
        this.invCursorDown();
        if (id === 'chest_0') {
            // oops! we wrapped in a chest. Undo
            this.invCursorUp();
        };
    }

    invCursorRight() {
        if (this.uiState === 'normal') return;
        // new selected is last selected slot in right inv
        this.hoverSlotId = this.lastHoveredRight;
        // but verify that the slot still exists:
        if (this.getSlotScreenPosById(this.hoverSlotId) === null) {
            // slot does not exist any more!
            this.hoverSlotId = null;
        }
        if (this.hoverSlotId !== null) {
            const idSplit = this.hoverSlotId.split('_');
            if (this.uiState === 'chest' && idSplit[0] !== 'chest') {
                // can't hover this slot because it is not shown
                this.hoverSlotId = null;
            }
            if (this.uiState === 'inventory'
                && idSplit[0] !== 'inv' && idSplit[0] !== 'recipe') {
                // can't hover this slot because it is not shown
                this.hoverSlotId = null;
            }
        }
        if (this.hoverSlotId === null) {
            // in case of null, just use the topmost right slot
            if (this.uiState === 'inventory') {
                this.hoverSlotId = 'inv_' + (INV_LEN - 6);
            } else if (this.uiState === 'chest') {
                this.hoverSlotId = 'chest_0';
            }
        }
        this.ensureSlotVisibleById(this.hoverSlotId);
    }

    invCursorLeft() {
        if (this.uiState === 'normal') return;
        // new selected is last selected slot in left inv
        this.hoverSlotId = this.lastHoveredLeft;
        if (this.hoverSlotId === null) {
            // in case of null, just use the topmost left slot
            this.hoverSlotId = 'inv_0';
        }
        this.ensureSlotVisibleById(this.hoverSlotId);
    }

    invCursorHome() {
        if (this.uiState === 'normal') return;
        const id = this.hoverSlotId;
        if (id === null) {
            this.hoverSlotId = 'inv_0';
            return;
        }
        const idSplit = id.split('_');
        const idNum = parseInt(idSplit[1]);
        const isLeft = idSplit[0] === 'inv' && idNum < LEFT_INV_LEN;
        if (isLeft) {  // somewhere in left inv
            this.hoverSlotId = 'inv_0';
        } else if (idSplit[0] === 'inv' || idSplit[0] === 'recipe') {  // somewhere in right inventory
            this.hoverSlotId = 'inv_' + LEFT_INV_LEN;
        } else if (idSplit[0] === 'chest') {  // in chest
            this.hoverSlotId = 'chest_0';
        }
        this.ensureSlotVisibleById(this.hoverSlotId);
        if (isLeft) {
            this.lastHoveredLeft = this.hoverSlotId;
        } else {
            this.lastHoveredRight = this.hoverSlotId;
        }
    }

    invCursorEnd() {
        if (this.uiState === 'normal') return;
        const id = this.hoverSlotId;
        if (id === null) {
            this.hoverSlotId = 'inv_0';
            return;
        }
        const idSplit = id.split('_');
        const idNum = parseInt(idSplit[1]);
        const isLeft = idSplit[0] === 'inv' && idNum < LEFT_INV_LEN;
        if (isLeft) {  // somewhere in left inv
            this.hoverSlotId = 'inv_' + (LEFT_INV_LEN-1);
        } else if (idSplit[0] === 'inv' || idSplit[0] === 'recipe') {  // somewhere in right inventory
            this.hoverSlotId = 'inv_' + (INV_LEN-1);
        } else if (idSplit[0] === 'chest') {  // in chest
            const chestLen = this.chest.inventory.getNumExistingSlots();
            this.hoverSlotId = 'chest_' + (chestLen-1);
        }
        this.ensureSlotVisibleById(this.hoverSlotId);
        if (isLeft) {
            this.lastHoveredLeft = this.hoverSlotId;
        } else {
            this.lastHoveredRight = this.hoverSlotId;
        }
    }

    snapCursorToMouse() {
        this.mouserecentlymoved = true;
        const offsetx = this.cameralocked ? this.camerax - this.x : 0;
        const offsety = this.cameralocked ? this.cameray - this.y : 0;
        if (this.cursorSet(
            this.mousex + offsetx,
            this.mousey + offsety,
        )){
            this.usingmousecontrols = true;
        }
    }

    cursorSet(x, y) {

        const prevx = this.cursorx;
        const prevy = this.cursory;

        const absx = Math.abs(x);
        const absy = Math.abs(y);
        // if cursor is slightly beyond reach,
        // project it onto edge of reach
        if ((absx > this.reach || absy > this.reach)
            && absx <= this.reach+CURSOR_TOLERANCE_BEYOND_REACH_X
            && absy <= this.reach+CURSOR_TOLERANCE_BEYOND_REACH_Y) {
            if (absx > absy) {
                // left/right of diagonal.
                // Projecting onto vertical.
                // a small bias makes it easier to aim at the corner of reach
                const bias = (1+(absx-this.reach+1)*0.05);
                y = Math.round(y*this.reach/absx*bias);
                x = Math.sign(x)*this.reach;
            } else {
                // above/below the diagonal.
                // Projecting onto horizontal.
                // a small bias makes it easier to aim at the corner of reach
                const bias = (1+(absy-this.reach+1)*0.05);
                x = Math.round(x*this.reach/absy*bias);
                y = Math.sign(y)*this.reach;
            }
            // absx and absy were now invalidated and can not be used any more!
            // fix if result is still out of reach (can happen due to bias)
            if (Math.abs(x) > this.reach) {
                x = Math.sign(x)*this.reach;
            }
            if (Math.abs(y) > this.reach) {
                y = Math.sign(y)*this.reach;
            }
        }

        if (Math.abs(x) <= this.reach && Math.abs(y) <= this.reach) {
            this.cursorx = x;
            this.cursory = y;
        }

        return prevx !== this.cursorx || prevy !== this.cursory;

    }

    registerSlotHandler(id, handler) {
        this.slotHandlers[id] = handler;
    }

    getSlotHandlerById(id) {
        const handler = this.slotHandlers[id];
        if (handler === undefined) {
            console.error("invalid slot id: "+id);
            console.trace();
            return null;
        }
        return handler;
    }

    getSlotScreenPosById(id) {
        if (id === null) return null;
        return this.getSlotHandlerById(id).getSlotScreenPosById(id);
    }

    getItemBySlotId(id) {
        if (id === null) return null;
        return this.getSlotHandlerById(id).getItemBySlotId(id);
    }

    ensureSlotVisibleById(id) {
        if (id === null) return;
        this.getSlotHandlerById(id).ensureSlotVisibleById(id);
    }

    getViewX() {
        if (this.cameralocked) {
            return this.camerax;
        }
        return this.x;
    }

    getViewY() {
        if (this.cameralocked) {
            return this.cameray;
        }
        return this.y;
    }

    getViewCursorX() {
        if (this.cameralocked) {
            return this.x-this.camerax+this.cursorx;
        }
        return this.cursorx;
    }

    getViewCursorY() {
        if (this.cameralocked) {
            return this.y-this.cameray+this.cursory;
        }
        return this.cursory;
    }

    attachCameraIfOutsideScreen(w, h) {
        if (!this.cameralocked) return;
        const dx = Math.abs(this.x - this.camerax);
        const dy = Math.abs(this.y - this.cameray);
        // check if player is off screen
        if (dx > w/2 || dy > h/2) {
            // reattach camera
            this.cameralocked = false;
        }
    }

    focusAAterminalByCoordinates(x, y) {
        for (let i = 0; i < this.openedAAterminals.length; i++) {
            const xy = this.openedAAterminals[i];
            if (xy.x !== x) continue;
            if (xy.y !== y) continue;
            this.focusedAAterminal = i;
            return true;
        }
        return false;
    }

}
