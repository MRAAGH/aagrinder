// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * The Commands class in this file contains logic
 * for all client-side commands.
 * If a command is not recognized as a client command,
 * it is sent to the server.
 */

"use strict";

class Commands {

    // commands class gets reference to entire game object
    // and has access to everything
    constructor(game, cli, settings, layout, music, commandDatabase) {
        this.game = game;
        this.cli = cli;
        this.settings = settings;
        this.layout = layout;
        this.music = music;
        this.commandDatabase = commandDatabase;
        this.inGame = false;

        this.commands = CLIENTSIDE_COMMANDS.map(command => new command(this));

        // make them indexed by alias
        this.commandByAlias = {};
        for (const command of this.commands) {
            // save it by its main name
            this.commandByAlias[command.name] = command;
            // save it by every alias
            for (const alias of command.aliases) {
                this.commandByAlias[alias] = command;
            }
            // add it to the database
            if (!command.isHidden) {
                this.commandDatabase.addCommand(command.name,
                    command.aliases, command.signatures, command.help);
            }
        }
    }

    // this is very bad code, because it's deliberately keeping
    // the old reference when requested to set to null, and
    // instead setting inGame to false
    setGame(game) {
        this.inGame = game !== null;
        if (game !== null) this.game = game;
        // and repeat this same thing in every command object:
        for (const command of this.commands) {
            command.inGame = game !== null;
            if (game !== null) command.game = game;
        }
    }

    static makeUsageString(name, signatures, usageExtraLine) {
        const start = 'usage: ' + name;
        if (signatures.length < 1
            || signatures[0][0].name === '') return start;
        let usage = signatures.map(signature => start + ' '
            + signature.map(s => (s.mandatory ? '<' : '[') + s.name + (s.mandatory ? '>' : ']'))
            .join(' ')).join('\\[n\\]');
        if (usageExtraLine) {
            usage += '\\[n\\]'+usageExtraLine;
        }
        return usage;
    }

    async executeCommand(typed) {

        let args = splitByQuotes(typed);
        args = args.map(a=>a.replace(/"/g,''));

        // remove blank arguments
        args = args.filter(a => a.length > 0);

        // check if client-side command exists
        const command = this.commandByAlias[args[0]];
        if (command === undefined) {
            // client-side command does not exist.
            if (!this.inGame) {
                // no, not allowed here
                this.cli.printerror('Unknown command. Try /help');
                return;
            }
            // in-game, non-client commands will be sent to the server.
            // commands are sent over chat endpoint
            this.game.socket.emit('chat', {message: typed});
            return;
        }

        // it is a client-side command!

        // is on login screen?
        if (!this.inGame) {
            // yes, is on login screen.
            // check if command allowed on login screen
            if (!command.isLoginScreen) {
                // no, not allowed here
                this.cli.printerror('Can\'t use on login screen.');
                return;
            }
        }

        await command.execute(typed, args);

    }
}

// subclass sandbox for client-side commands
class Command {
    name = null;
    aliases = [];
    signatures = [];
    isLoginScreen = false;
    isHidden = false;
    help = 'No help provided for this command.';

    constructor(commands) {
        this.commands = commands;
        this.game = commands.game;
        this.cli = commands.cli;
        this.settings = commands.settings;
        this.layout = commands.layout;
        this.music = commands.music;
        this.commandDatabase = commands.commandDatabase;
        this.inGame = this.game !== null;
    }

    printUsage() {
        this.cli.printerror(Commands.makeUsageString(
            this.name, this.signatures, this.usageExtraLine));
    }
}

const CLIENTSIDE_COMMANDS = [

    class extends Command {
        name = '/map';
        signatures = [[{name: 'function', type: 'mappablefunction', mandatory: true}]];
        isLoginScreen = true;
        help = 'Map a keyboard key to a specified function.';
        async execute(typed, args) {
            // needs 1 argument
            if (args.length < 2) {
                // missing argument
                this.printUsage();
                this.cli.printerror('Available functions:\\[n\\]'
                    + ACTIONS.join(', '));
                return;
            }
            // check if valid action
            if (!ACTIONS.includes(args[1])) {
                // invalid
                this.cli.printerror('Available functions:\\[n\\]'
                    +ACTIONS.join(', '));
                return;
            }
            // request key to be recorded:
            this.cli.println('\\[#fff\\]Press button for '+args[1]+' ...');
            const key = await this.game.recordKey();
            // prevent this key from being used elsewhere:
            this.game.keys.clearFresh();
            this.game.keys.handleKeyUp(key);
            // check if can map
            if (!this.layout.keyCanMap(key)) {
                // no
                this.cli.printerror('can\'t change this key');
                return;
            }
            // save the mapping
            this.layout.mapKey(key, args[1]);
            this.cli.println('mapping '+key+'->'+args[1]
                +' saved in localstorage');
            this.layout.saveLayout();
        }
    },

    class extends Command {
        name = '/unmap';
        isLoginScreen = true;
        help = 'Unmap a mapped keyboard key.';
        async execute(typed, args) {
            if (args.length !== 1) {
                this.cli.printerror('/unmap does not take an argument!');
                return;
            }
            // request key to be recorded:
            this.cli.println('\\[#fff\\]Press button to unmap ...');
            const unkey = await this.game.recordKey();
            // prevent this key from being used elsewhere:
            this.game.keys.clearFresh();
            this.game.keys.handleKeyUp(unkey);
            // check if can unmap
            if (!this.layout.keyCanMap(unkey)) {
                // no
                this.cli.printerror('can\'t unmap this key');
                return;
            }
            // save the mapping
            this.layout.unmapKey(unkey);
            this.cli.println('unmapped key '+unkey+'.');
            this.layout.saveLayout();
        }
    },

    class extends Command {
        name = '/unmapallkeys';
        isLoginScreen = true;
        help = 'Unmap all keyboard keys.';
        async execute(typed, args) {
            this.layout.changeLayout('emptylayout');
            this.cli.println('all keys were unmapped!');
            this.cli.println('hint: /layout qwerty');
            this.layout.saveLayout();
        }
    },

    class extends Command {
        name = '/layout';
        signatures = [[{name: 'name', type: 'keyboardlayoutname', mandatory: true}]];
        isLoginScreen = true;
        help = 'Set a predefined keyboard layout.';
        async execute(typed, args) {
            // needs 1 argument
            if (args.length < 2) {
                // missing argument
                this.printUsage();
                this.cli.printerror('Available layouts: '
                    +Object.keys(LAYOUTS).join(', '));
                return;
            }
            // check if valid layout
            if (typeof(LAYOUTS[args[1]]) === 'undefined') {
                // invalid
                this.cli.printerror('Available layouts: '
                    +Object.keys(LAYOUTS).join(', '));
                return;
            }
            // change the layout
            this.layout.changeLayout(args[1]);
            this.cli.println('layout '+args[1]+' saved in localstorage');
            this.layout.saveLayout();
        }
    },

    class extends Command {
        name = '/hello';
        isLoginScreen = true;
        isHidden = true;
        help = 'uwuzie';
        async execute(typed, args) {
            this.cli.println('Hi there, nice to meet you! ^~^');
        }
    },

    class extends Command {
        name = '/claims';
        help = 'List claims at your current position.';
        async execute(typed, args) {
            // currently, it is only possible to get the topmost claim
            // via getClaim method. Not array of all claims.
            // But this is fine. This is the claim that has effect
            const claim = this.game.map.getClaim(
                this.game.player.x, this.game.player.y);
            if (claim === null) {
                this.cli.println('This area is not claimed.');
                return;
            }
            this.cli.println('Claimed by '+claim.p+' [x'+claim.x
                +' to x'+claim.x2+', y'+claim.y+' to y'+claim.y2+']');
        }
    },

    class extends Command {
        name = '/myclaims';
        help = 'List your claims within currently loaded client-side area.';
        async execute(typed, args) {
            const foundClaims = [];
            for (const chunky in this.game.map.chunks) {
                for (const chunkx in this.game.map.chunks[chunky]) {
                    const chunk = this.game.map.chunks[chunky][chunkx];
                    for (const claim of chunk.claims) {
                        // filter out other players claims
                        if (claim.p !== this.game.player.name) continue;
                        // filter out duplicate claims
                        let isDupe = false;
                        for (const otherClaim of foundClaims) {
                            if (claim.x === otherClaim.x
                                && claim.y === otherClaim.y
                                && claim.x2 === otherClaim.x2
                                && claim.y2 === otherClaim.y2) {
                                isDupe = true;
                                break;
                            }
                        }
                        if (isDupe) continue;
                        foundClaims.push(claim);
                    }
                }
            }
            if (foundClaims.length < 1) {
                this.cli.printerror('no claims within loaded area.');
                return;
            }
            this.cli.println(foundClaims.map(c => '[x'+c.x+' to x'+c.x2
                + ', y'+c.y+' to y'+c.y2+']').join(',\\[n\\]'));
        }
    },

    class extends Command {
        name = '/pos';
        help = 'Display your position: block coordinates, chunk coordinates, biome name and raininess.';
        async execute(typed, args) {
            const x = this.game.player.x;
            const y = this.game.player.y;
            const biome = this.game.map.biomeAt(x, y);
            // copied from gui.js
            const chunkx = x >> 8;
            const chunky = y >> 8;
            let coords =
                'block: ' + x + ', ' + y
                + '; chunk: ' + chunkx + ', ' + chunky
                + '; biome: ' + BIOMES[biome].name;
            if (biome === 's' || biome === 'R') {
                coords += '; raininess: ' + raininessInChunk(chunkx, chunky);
            }
            this.cli.println(coords);
        }
    },

    class extends Command {
        name = '/twirt';
        aliases = ['/pop', '/cricket', '/crickets'];
        help = 'Test the notification sound effect.';
        async execute(typed, args) {
            this.game.playPop();
        }
    },

    class extends Command {
        name = '/passwd';
        aliases = ['/password', '/pass'];
        help = 'Change your account password.';
        async execute(typed, args) {
            const currentpassword = await this.cli.promptPassword('Current password: ');

            const checkresult = await ajax({
                name: this.game.player.name,
                password: currentpassword,
            },
                '/api/users/checkpass',
            );
            if (!checkresult.success) {
                this.cli.println(checkresult.message);
                return;
            }

            const newpassword = await this.cli.promptPassword('New password: ');
            const retypenewpassword = await this.cli.promptPassword('Retype new password: ');
            if (newpassword !== retypenewpassword) {
                this.cli.printerror('Sorry, passwords do not match.\\[n\\]Password unchanged.');
                return;
            }

            const changeresult = await ajax({
                name: this.game.player.name,
                oldpassword: currentpassword,
                newpassword: newpassword,
            },
                '/api/users/changepass',
            );
            if (!changeresult.success) {
                this.cli.printerror(changeresult.message);
                return;
            }
            else {
                this.cli.println(changeresult.message);
            }
        }
    },

    class extends Command {
        name = '/set';
        aliases = ['/se', '/s'];
        signatures = [[{name: '', type: 'setting', mandatory: true}]];
        isLoginScreen = true;
        help = 'Manipulate client settings.\\[n\\]'
            + 'List all:        /set\\[n\\]'
            + 'Display state:   /set music?\\[n\\]'
            + 'Display value:   /set volume=?\\[n\\]'
            + 'Enable setting:  /set music\\[n\\]'
            + 'Disable setting: /set nomusic\\[n\\]'
            + 'Toggle setting:  /set !music\\[n\\]'
            + 'Set value:       /set volume=79';
        async execute(typed, args) {
            if (args.length === 1) {
                this.settings.printhelp(this.cli);
                return;
            }
            if (['l', 'lo', 'loc', 'loca', 'local',
                'nol', 'nolo', 'noloc', 'noloca', 'nolocal']
                .indexOf(args[1]) > -1) {
                // deprecated:
                this.cli.printerror('this setting was removed. Try /local and /global');
                return;
            }
            const setresult = this.settings.set(args);
            if (!setresult) {
                this.cli.printerror('Invalid setting. Run /set');
                return;
            }
            // if it was just a query
            if (setresult.q) {
                // only display the result of the query
                this.cli.println(setresult.name + ' = ' + setresult.value);
                return;
            }
            this.settings.saveSettings();
            // Notify player if there was no change:
            if (!setresult.eq && setresult.value === setresult.prev) {
                if (setresult.value && (setresult.name === 'music'
                    || setresult.name === 'listmusic')) {
                    // this is the one exception where we allow the code to
                    // be executed even though the setting was not actually
                    // changed. Because something is still supposed to happen
                } else {
                    if (setresult.value) {
                        this.cli.printerror(setresult.name
                            + ' already enabled. Disable with /set no'
                            + setresult.name);
                    } else {
                        this.cli.printerror(setresult.name
                            + ' already disabled. Enable with /set '
                            + setresult.name);
                    }
                    return;
                }
            }
            // TODO: if !this.inGame,
            // filter out settings which have
            // no immediate effect on login screen
            switch (setresult.name) {
                case 'volume':
                    // set volume
                    this.music.resetVolume();
                    this.cli.println('Volume set.')
                    break;
                case 'music':
                    if(setresult.value) {
                        // make sure music restarts,
                        // even if state not changed
                        this.music.forceUpdateState();
                    }
                    else {
                        this.music.setState('disabled');
                        this.cli.println('Music disabled.')
                    }
                    break;
                case 'listmusic':
                    if (setresult.value) {
                        this.music.printNowPlaying();
                    } else {
                        this.cli.println('Music will not be listed.');
                    }
                    break;
                case 'maxchatlines':
                    this.cli.setLineLimit(setresult.value);
                    this.cli.println('Set chat lines to '+setresult.value);
                    break;
                case 'gap':
                    this.cli.bigterminal.reformat();
                    break;
                case 'typingnotifications':
                    // clear typing indicator if exists
                    this.game.socket.emit('typing', {type: 'stop'});
                    break;
                case 'revealbeforesending':
                    if (setresult.value) {
                        this.cli.println('Keep in mind: everyone sees what you are typing, even before you send it.');
                    } else {
                        this.cli.println('The content of your messages will be hidden until you press enter to send.');
                    }
                    break;
                case 'twirt':
                    if (setresult.value) {
                        this.cli.println('Cricket noises are back.');
                    } else {
                        this.cli.println('Crickets are gone.');
                    }
                    break;
                case '???':
                    if (setresult.value) {
                        this.cli.println('???');
                    } else {
                        this.cli.println('...');
                    }
                    break;
                default:
                    if (setresult.eq) {
                        this.cli.println(setresult.name
                            + ' set to ' + setresult.value);
                    } else {
                        if (setresult.value) {
                            this.cli.println(setresult.name
                                + ' enabled');
                        } else {
                            this.cli.println(setresult.name
                                + ' disabled');
                        }
                    }
                    break;

            }
        }
    },

    class extends Command {
        name = '/unset';
        aliases = ['/unse', '/uns'];
        signatures = [[{name: 'setting', type: 'boolsetting', mandatory: true}]];
        isLoginScreen = true;
        help = 'Shortcut to unset a setting.\\[n\\]'
            + 'Example: /unset music\\[n\\]'
            + '(same as "/set nomusic")';
        async execute(typed, args) {
            if (args.length < 2) {
                this.printUsage();
                return;
            }
            await this.commands.executeCommand('/set no'+args[1]);
        }
    },

    class extends Command {
        name = '/resetallsettings';
        isLoginScreen = true;
        help = 'Reset all settings to default (clears localstorage)';
        async execute(typed, args) {
            this.settings.setAllDefaultSettings();
            this.settings.clearLocalStorage();
        }
    },

    class extends Command {
        name = '/mute';
        isLoginScreen = true;
        help = 'Disables music and notification sounds.';
        async execute(typed, args) {
            this.cli.println('executing /set nomusic');
            await this.commands.executeCommand('/set nomusic');
            this.cli.println('executing /set notwirt');
            await this.commands.executeCommand('/set notwirt');
        }
    },

    class extends Command {
        name = '/unmute';
        isLoginScreen = true;
        help = 'Enables music and notification sounds.';
        async execute(typed, args) {
            this.cli.println('executing /set music');
            await this.commands.executeCommand('/set music');
            this.cli.println('executing /set twirt');
            await this.commands.executeCommand('/set twirt');
        }
    },

    class extends Command {
        name = '/music';
        signatures = [[
            {name:'enable/disable', type:'enabledisable', mandatory:false},
        ]];
        isLoginScreen = true;
        help = 'Accepts one argument. Enable, disable or toggle music.';
        async execute(typed, args) {
            let action = 'toggle';
            if (args.length > 1) {
                switch (args[1].toLowerCase()) {
                    case 'on': case 'enable': case 'true': case 'yes': case 'unmute': case '1': case 'start':
                        action = 'on';
                        break;
                    case 'off': case 'disable': case 'false': case 'no': case 'mute': case '0': case 'stop':
                        action = 'off';
                        break;
                }
            }
            switch (action) {
                case 'on':
                    this.cli.println('turning music on:');
                    this.cli.println('executing /set music');
                    await this.commands.executeCommand('/set music');
                    break;
                case 'off':
                    this.cli.println('turning music off:');
                    this.cli.println('executing /set nomusic');
                    await this.commands.executeCommand('/set nomusic');
                    break;
                case 'toggle':
                    this.cli.println('toggling music:');
                    // TODO: ideally this would simply be /set !music
                    if (this.settings.s['music']) {
                        this.cli.println('executing /set nomusic');
                        await this.commands.executeCommand('/set nomusic');
                    } else {
                        this.cli.println('executing /set music');
                        await this.commands.executeCommand('/set music');
                    }
                    break;
            }
        }
    },

    class extends Command {
        name = '/stopmusic';
        aliases = [ '/nomusic' ];
        isLoginScreen = true;
        isHidden = true;
        async execute(typed, args) {
            this.cli.println('turning music off:');
            this.cli.println('executing /set nomusic');
            await this.commands.executeCommand('/set nomusic');
        }
    },

    class extends Command {
        name = '/nowplaying';
        aliases = [ '/nowplaying', '/playing' ];
        isLoginScreen = true;
        help = 'Displays currently playing music.';
        async execute(typed, args) {
            this.music.printNowPlaying();
            if (!this.settings.s['listmusic']) {
                this.cli.println('Hint: try /set listmusic');
            }
        }
    },

    class extends Command {
        name = '/local';
        aliases = ['/l', '/setlocal'];
        signatures = [[
            {name:'message', type:'incompletable', mandatory:false},
        ]];
        help = 'Sends arguments as a \\[#cc0\\]local\\[/\\] message (not bridged). If no argument given, change scope to local. /setlocal always changes scope.';
        async execute(typed, args) {
            if (args.length === 1) {
                // just /local
                this.game.player.chatScope = 'local';
                return;
            }
            if (args[0] === '/setlocal') {
                // this alias always sets scope, despite arguments
                this.game.player.chatScope = 'local';
            }
            // some arguments after /local
            // this is equivalent to prefixing with L
            await this.commands.executeCommand('L '+typed.substr(args[0].length+1));
        }
    },

    class extends Command {
        name = '/global';
        aliases = ['/public', '/setglobal', '/setpublic'];
        signatures = [[
            {name:'message', type:'incompletable', mandatory:false},
        ]];
        help = 'Sends arguments as a global message (bridged to Matrix). If no argument given, change scope to global. /setglobal always changes scope.';
        async execute(typed, args) {
            if (args.length === 1) {
                // just /global
                this.game.player.chatScope = 'global';
                return;
            }
            if (args[0] === '/setglobal' || args[0] === '/setpublic') {
                // this alias always sets scope, despite arguments
                this.game.player.chatScope = 'global';
            }
            // some arguments after /global
            // this is equivalent to not prefixing with L
            await this.commands.executeCommand(typed.substr(args[0].length+1));
        }
    },

    class extends Command {
        name = '/shrug';
        shrug = '¯\\_(ツ )_/¯';
        help = this.shrug;
        async execute(typed, args) {
            if (this.settings.s['local']) {
                await this.commands.executeCommand('L '+this.shrug);
            } else {
                await this.commands.executeCommand(this.shrug);
            }
        }
    },

    class extends Command {
        name = '/credits';
        help = 'Play credits.';
        async execute(typed, args) {
            this.game.creditsIndex = 0;
            this.game.creditsIndexMod = 0;
        }
    },

    class extends Command {
        name = '/about';
        aliases = ['/source'];
        isLoginScreen = true;
        help = 'Display link to source code.';
        async execute(typed, args) {
            const url = 'https://gitlab.com/MRAAGH/aagrinder';
            this.cli.println(url);
            if (typeof navigator.clipboard !== 'undefined') {
                navigator.clipboard.writeText(url)
                .then(() => {
                    this.cli.println('\\[#373\\]Copied to clipboard!');
                });
            }
        }
    },

    class extends Command {
        name = '/bye';
        aliases = ['/cya', '/exit', '/quit', '/leave', '/logout', '/signout'];
        help = 'Log out from AAGRINDER.';
        async execute(typed, args) {
            this.game.socket.disconnect();
            this.game.socket.connect();
        }
    },

    class extends Command {
        name = '/clear';
        isLoginScreen = true;
        help = 'Clear the chatbox history.';
        async execute(typed, args) {
            this.cli.clear();
        }
    },

    class extends Command {
        name = '/copylast';
        aliases = ['/cl'];
        signatures = [[
            {name:'N', type:'incompletable', mandatory:false},
        ]];
        isLoginScreen = true;
        help = 'Copy the last message to clipboard. If argument given, copy last N messages.';
        async execute(typed, args) {
            let N = 1;
            if (args.length > 1) {
                N = parseInt(args[1]);
                if (isNaN(N) || N < 1) {
                    this.cli.printerror('Invalid number: ' + args[1]);
                    return;
                }
            }
            if (typeof navigator.clipboard === 'undefined') {
                this.cli.printerror('Clipboard unavailable!');
                return;
            }
            let lines = this.cli.getLines();
            if (lines.length > N) {
                // decrease number of lines to the requested N
                lines = lines.slice(lines.length - N);
            }
            const content = lines.map(line => {
                const match = line.match(/^\\\[#444\\\]\d\d?:\d\d? \\\[\/\\\]\\\[#[0-9a-f]+\\\][^\\]+\\\[\/\\\]: /);
                if (match === null) return line;  // return unmodified
                // recognized a typically formatted chat message. example:
                // \[#444\]12:00 \[/\]\[#ff00ff\]maze\[/\]: meow
                // the recognized prefix will be removed:
                return line.substr(match[0].length);
            }).map(line => AASTRING_DECODE(line).map(l=>l.raw).join('\n'));
            navigator.clipboard.writeText(content.join('\n'))
            .then(() => {
                this.cli.println('\\[#373\\]Copied to clipboard!');
            }, () => {
                this.cli.printerror('Failed to copy!');
            });
        }
    },

    class extends Command {
        name = '/copyall';
        aliases = ['/ca'];
        isLoginScreen = true;
        help = 'Copy entire chatbox history to the clipboard.';
        async execute(typed, args) {
            if (typeof navigator.clipboard === 'undefined') {
                this.cli.printerror('Clipboard unavailable!');
                return;
            }
            const lines = this.cli.getLines();
            const content = lines.map(li=>
                AASTRING_DECODE(li).map(l=>l.raw).join('\n')
            ).join('\n')
            navigator.clipboard.writeText(content)
            .then(() => {
                this.cli.println('\\[#373\\]Copied to clipboard!');
            }, () => {
                this.cli.printerror('Failed to copy!');
            });
        }
    },

    class extends Command {
        name = '/verify';
        isLoginScreen = true;
        isHidden = true;
        async execute(typed, args) {
            const code = 169-20*7+'KcIJV'+(11+83)+'dWqW4g';
            this.cli.println('Your verification code is:\\[n\\]\\[#ff0\\]'+code);
            if (typeof navigator.clipboard !== 'undefined') {
                navigator.clipboard.writeText(code)
                .then(() => {
                    this.cli.println('\\[#373\\]Copied to clipboard!');
                });
            }
        }
    },

    class extends Command {
        name = '/log';
        help = 'Print debug logs to the browser console.';
        async execute(typed, args) {
            if (!IS_DEBUGLOG_CLIENT) {
                this.cli.printerror('Not supported. Try debug.html');
                return;
            }
            const log = this.game.syncher.debugLog.join('\n');
            console.log(log);
            this.cli.println('debug log was printed to console');
        }
    },

    class extends Command {
        name = '/whisper';
        aliases = ['/w'];
        signatures = [[
            {name:'player', type:'onlineplayer', mandatory:true},
            {name:'message', type:'incompletable', mandatory:false},
        ]];
        help = 'Whisper a private message to player (not logged). Omit message for whisper mode.';
        async execute(typed, args) {
            if (args.length !== 2) {
                // server will handle this instead.
                // emit it same as an unrecognized command:
                this.game.socket.emit('chat', {message: typed});
                return;
            }
            // There are exactly 2 arguments.
            // Enable whisper mode:
            this.cli.println('Whisper mode. Exit with /global');
            this.game.player.chatScope = 'whisper';
            this.game.player.whisperTarget = args[1];
        }
    },

    class extends Command {
        name = '/rainbow';
        signatures = [[
            {name:'message', type:'incompletable', mandatory:true},
        ]];
        isAdmin = false;
        help = '\\[#ff0000\\]R\\[#ff8b00\\]A\\[#e8ff00\\]I\\[#5dff00\\]N\\[#00ff2e\\]B\\[#00ffb9\\]O\\[#00b9ff\\]W';
        usageExtraLine = 'example: /rainbow meow';
        async execute(typed, args) {
            if (args.length < 2) {
                this.printUsage();
                return;
            }
            // determine text that will be made into rainbow color
            const textToRainbow = args.slice(1).join(' ');
            // length of rainbow
            const len = textToRainbow.length;
            // prefix each character with some color code
            const rainbowText = textToRainbow.split('')
                .map((ch,i)=>'\\['+getRainbowColorAt(len,i)+'\\]'+ch).join('');
            // send to server like a normal message
            const scopedMessage = this.game.adjustMessageToScope(rainbowText);
            this.game.socket.emit('chat', {message: scopedMessage});
        }
    },

    class extends Command {
        name = '/rainbowme';
        signatures = [[
            {name:'message', type:'incompletable', mandatory:true},
        ]];
        isAdmin = false;
        help = 'same as /me, but \\[#ff0000\\]R\\[#ff8b00\\]A\\[#e8ff00\\]I\\[#5dff00\\]N\\[#00ff2e\\]B\\[#00ffb9\\]O\\[#00b9ff\\]W';
        usageExtraLine = 'example: /rainbowme jumps';
        async execute(typed, args) {
            if (args.length < 2) {
                this.printUsage();
                return;
            }
            // determine text that will be made into rainbow color
            const textToRainbow = args.slice(1).join(' ');
            // length of rainbow
            const len = textToRainbow.length;
            // prefix each character with some color code
            const rainbowText = textToRainbow.split('')
                .map((ch,i)=>'\\['+getRainbowColorAt(len,i)+'\\]'+ch).join('');
            // send to server like a /me command
            const scopedMessage = '/me '+rainbowText;
            this.game.socket.emit('chat', {message: scopedMessage});
        }
    },

    class extends Command {
        name = '/help';
        aliases = ['/h', '/?'];
        signatures = [[{name: 'command', type: 'command', mandatory: false}]];
        isLoginScreen = true;
        help = 'PANIC PANIC';
        async execute(typed, args) {
            if (args.length > 1) {
                // wants help for specific command
                let help;
                let command;
                // try finding help for one of these 4 things:
                const queries = [
                    args[1],
                    this.commandDatabase.resolveAlias(args[1]),
                    '/' + args[1],
                    this.commandDatabase.resolveAlias('/' + args[1]),
                ];
                for (const query of queries) {
                    command = query;
                    help = this.commandDatabase.getHelp(command);
                    if (help !== null) break;
                }
                if (help === null) {
                    this.cli.printerror('Unknown command: ' + args[1]
                        + '\\[n\\]Try /help without arguments');
                    return;
                }
                this.cli.println('help for '+command+': \\[n\\]'+help);
                const signatures = this.commandDatabase.getSignatures(command);
                const usageExtraLine = this.commandDatabase.getUsageExtraLine(command);
                const usageString = Commands.makeUsageString(
                    command, signatures, usageExtraLine);
                // print usageString only if non-trivial
                if (usageString !== 'usage: '+command) {
                    this.cli.println(usageString);
                }
                const aliases = this.commandDatabase.getAliases(command);
                if (aliases.length > 0) {
                    this.cli.println('aliases for ' + command + ': '
                        + aliases.join(', '));
                }
                return;
            }
            // different depending on whether logged in
            let knownCommands = this.inGame
                ? this.commandDatabase.getCommandNames()
                : this.commands.commands.filter(c => c.isLoginScreen && !c.isHidden).map(c => c.name);
            this.cli.println('available commands: '
                + knownCommands.sort().join(', '));
            this.cli.println('for details, try /help /command');
            if (this.inGame) {
                this.cli.println('press H to list buttons');
            }
        }
    },

];

