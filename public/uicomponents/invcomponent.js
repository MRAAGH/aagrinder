// Copyright (C) 2018-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * An abstract class for inventory gui components.
 */

"use strict";

class InventoryComponent extends ScrollableComponent {
    constructor(player, inventory, idPrefix, title, displayedInStates, cutoffStart, cutoffLength){
        super();
        this.player = player;
        this.inventory = inventory;
        this.idPrefix = idPrefix;
        this.title = title;
        this.displayedInStates = displayedInStates;
        this.compact = false;
        this.pallete = {
            default: '#aaa',
            percent_100: '#7a7',
            percent_70: '#897',
            percent_40: '#987',
            percent_10: '#955',
        };
        for (let i = 0; i < this.getCutoffLength(); i++) {
            const slotNumber = i + this.getCutoffStart();
            const slotId = this.idPrefix + slotNumber;
            this.player.registerSlotHandler(slotId, this);
        }
    }

    isInventory() {
        return true;
    }

    // TODO: put this function in a better place.
    static makeInventorySprite(item_code, player) {
        if (item_code === 'E') {
            const blinkblock = 'E'+player.detectorBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        if (item_code === 'Eo') {
            const blinkblock = 'E'+player.seatectorBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        if (item_code === 'Wo') {
            const blinkblock = 'Wo'+player.followBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        const block = Inventory.item2blockPlain(item_code);
        return BIOME_SPRITES_NO_EFFECT[block]['A'];
    }
    static formatItem(item_code, player) {
        const sprite = InventoryComponent.makeInventorySprite(
            item_code, player);
        // TODO: maybe AASTRING should have a function to do all this
        let formatted = '\\[' + sprite.color + '\\]';
        if (sprite.blinkspeed > 0) {
            // Encode blinkspeed in modifier
            const modifier = (sprite.blinkspeed-1)*2+1;
            formatted += '\\[M' + modifier + '\\]';
        }
        formatted += sprite.char + '\\[/\\]';
        return formatted;
    }

    getInventoryTopYpos() {
        let ypos = Math.floor(this.h/2) - 16;
        if (ypos < 6) ypos = 6;
        if (this.h < 21) ypos = this.h - 15;
        if (ypos < 1) ypos = 1;
        return ypos;
    }

    getInventoryBottomYpos() {
        let ypos = this.getInventoryTopYpos() + 12 + 21;
        if (ypos > this.h - 5) ypos = this.h - 5;
        if (this.h < 15) ypos = this.h - 4;
        if (this.h < 10) ypos = this.h - 3;
        return ypos;
    }

    getFramePos() {
        let xoffset = this.getXpos();
        if (this.w < 42) {
            if (xoffset < 0) {
                xoffset += 12;
            }
            else {
                xoffset -= 1;
            }
            this.compact = true;
        }
        else {
            this.compact = false;
        }
        return {
            x: Math.floor(this.w/2) - 9 + xoffset,
            y: this.getYpos(),
        }
    }

    getXpos() {
        throw "InventoryComponent getXpos called!"
    }

    getYpos() {
        throw "InventoryComponent getYpos called!"
    }

    getCutoffStart() {
        throw "InventoryComponent getCutoffStart called!"
    }

    getCutoffLength() {
        throw "InventoryComponent getCutoffLength called!"
    }

    getFrameHeight() {
        throw "InventoryComponent getFrameHeight called!"
    }

    getContentHeight() {
        return this.getCutoffLength();
    }

    // with 0,0 at start of canvas
    isClickWithin(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        if (local.x >= 0 && local.x < 2+lineWidth
            && local.y >= 0 && local.y < this.getFrameHeight()) {
            return true;
        }
        return false;
    }

    // click is in character coordinates
    getSlotIdAtScreenCoordinates(x, y) {
        if (!this.isShown()) return null;
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        const numVisualizedSlots = this.getNumVisualizedSlots();
        if (local.x > 0 && local.x < 1+lineWidth
            && local.y > 0 && local.y < 1+numVisualizedSlots
            && local.y-1+this.scrollPos < this.getCutoffLength()) {
            const slotNumber = local.y-1+this.scrollPos + this.getCutoffStart();
            const slotId = this.idPrefix + slotNumber;
            return slotId;
        }
        return null;
    }

    isShown() {
        return this.displayedInStates.includes(this.player.uiState);
    }

    getFrame() {
        const numVisualizedSlots = this.getNumVisualizedSlots();

        // top
        const frame = [
            this.title[this.compact ? 0 : 1],
        ]

        this.limitScrollPos();

        const total_slots = this.getCutoffLength();
        const displayedHeight = Math.min(total_slots, numVisualizedSlots);
        const maxScrollPos = this.getMaxScrollPos();
        const isCurrentlyScrollable = maxScrollPos > 0;
        const isScrolledToEnd = this.scrollPos === maxScrollPos;
        const BARS = isCurrentlyScrollable ? ['╵',' ',' '] : ['│','│','│'];
        const isInventoryEmpty = this.inventory.isEmpty();
        for (let i = 0; i < displayedHeight; i++) {
            // there is a slot to show
            const slotNumberHere = i+this.scrollPos+this.getCutoffStart();
            const item = this.inventory.getSlotAt(slotNumberHere);
            const BAR = BARS[(i+this.scrollPos)%3];
            const slotIdHere = this.idPrefix+slotNumberHere;
            const isHovered = slotIdHere === this.player.hoverSlotId;
            const isDragged = slotIdHere === this.player.dragFromId;
            const hoverSpace = isHovered
                ? '\\[m8\\]\\[#7f7f7f\\] \\[/\\]'
                : ' ';
            if (item === null) {
                // empty item
                frame.push(BAR + ' ' + hoverSpace
                    + (this.compact ? '-  ' : '     -        ')
                    + BAR);
                continue;
            }
            const item_code = item.code;
            const formattedItem = (isDragged ? '\\[m8\\]' : '') + InventoryComponent.formatItem(item_code, this.player);
            const item_name = Inventory.item_short_names[item_code];
            const itemNameText = this.compact ? '' : (item_name+'               ').substring(0, 11);
            const amount = item.amount;
            const amountNumber = this.nthSlotAmount(slotNumberHere);
            const formattedAmount = amountNumber + '   '.substring(0, 3-AASTRING_LENGTH(amountNumber));
            const line = BAR
                + formattedItem
                + hoverSpace
                + '\\[' + this.pallete.default + '\\]'
                + itemNameText
                + formattedAmount
                + BAR;
            frame.push(line);
        }

        let BOTTOMLINE;
        if (isCurrentlyScrollable) {
            const number = this.getScrollPercentage();
            BOTTOMLINE = this.compact
                ? '└scr  '.substring(0, 6-number.length)+number+'┘'
                : '└──(scroll   '.substring(0, 13-number.length)+number+'%)──┘';
        } else {
            BOTTOMLINE = this.compact
                ? '└─────┘'
                : '└────────────────┘';
        }
        frame.push(BOTTOMLINE);

        return frame;

    }

    // Get the number that should be displayed in n-th slot
    nthSlotAmount(n) {
        const item = this.inventory.getSlotAt(n);
        if (item === null) return '';
        const item_code = item.code;
        if (item_code[0] === 'G') {
            // grinder. Display durability
            if (typeof item.data !== 'object') {
                return '\\[#f00\\]Nda\\[/\\]';
            }
            if (typeof item.data.d !== 'number') {
                return '\\[#f00\\]NOd\\[/\\]';
            }
            const dur = item.data.d;
            const max = Inventory.item_data_templates[item_code].d;
            if (dur === max) {
                // can't display '100%', too many characters
                // better to display nothing
                return '';
            }
            const percent = Math.floor(dur/max*100);
            const str = percent.toString()+'%';
            let color = this.pallete.percent_100;
            if (percent < 70) color = this.pallete.percent_70;
            if (percent < 40) color = this.pallete.percent_40;
            if (percent < 10) color = this.pallete.percent_10;
            return '\\['+color+'\\]'+str+'\\[/\\]';
        }
        const color = this.pallete.default;
        const str = item.amount.toString();
        return '\\['+color+'\\]'+str+'\\[/\\]';
    }

    getSlotScreenPosById(id) {
        // if component hidden, slot does not exist on screen
        if (!this.isShown()) return null;
        // figure out where the slot is
        const slotNumber = parseInt(id.split('_')[1]);
        const visualSlotNumber = slotNumber - this.getCutoffStart();
        if (visualSlotNumber < this.scrollPos) {
            // not visible, outside displayed area
            return null;
        }
        const numVisualizedSlots = this.getNumVisualizedSlots();
        if (visualSlotNumber >= this.scrollPos + numVisualizedSlots) {
            // not visible, outside displayed area
            return null;
        }
        return this.localToScreen(1, 1+visualSlotNumber-this.scrollPos);
    }

    getItemBySlotId(id) {
        const slotNumber = parseInt(id.split('_')[1]);
        const item = this.inventory.getSlotAt(slotNumber);
        return item;
    }

    ensureSlotVisibleById(id) {
        const idSplit = id.split('_');
        if (idSplit[0] !== 'inv' && idSplit[0] !== 'chest') return;
        const idNum = parseInt(idSplit[1]);
        const numVisualizedSlots = this.getNumVisualizedSlots();
        if (idNum > this.getCutoffStart() + this.scrollPos + numVisualizedSlots - 1) {
            this.scrollPos = idNum - this.getCutoffStart() - numVisualizedSlots + 1;
        }
        if (idNum < this.getCutoffStart() + this.scrollPos) {
            this.scrollPos = idNum - this.getCutoffStart();
        }
    }

}
