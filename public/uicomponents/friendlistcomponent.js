// Copyright (C) 2022-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class FriendlistComponent extends ScrollableComponent {
    constructor(player, socket) {
        super();
        this.player = player;
        this.socket = socket;
    }

    static CENTERING(text, len) {
        const padTotal = len-text.length;
        const padLeft = Math.floor(padTotal/2);
        const padRight = padTotal-padLeft;
        const spaces = '                                              ';
        const spacesLeft = spaces.substr(0, padLeft);
        const spacesRight = spaces.substr(0, padRight);
        return spacesLeft + text + spacesRight;
    }

    getFramePos() {
        return {
            x: Math.floor(this.w/2) - 15,
            y: 2,
        };
    }

    // click is in character coordinates
    handleClick(x, y) {
        if (!this.isShown()) return false;
        if (!this.isClickWithin(x, y)) return false;
        const local = this.screenToLocal(x, y);
        const clickedFriend = this.player.friends.requests[local.y-1];
        if (local.x >= 14 && local.x < 22) {
            this.socket.emit('chat', {message: '/friend add "'+clickedFriend+'"'});
            return true;
        }
        if (local.x >= 22 && local.x < 30) {
            this.socket.emit('chat', {message: '/friend remove "'+clickedFriend+'"'});
            return true;
        }
        return false;
    }

    show() {
    }

    hide() {
    }

    isShown() {
        return this.player.uiState === 'friendlist';
    }

    getContentHeight() {
        let content = 0;
        content += this.player.friends.requests.length;
        content += this.player.friends.friends.length;
        content += this.player.friends.pending.length;
        return content;
    }

    // with 0,0 at start of canvas
    isClickWithin(x, y) {
        const local = this.screenToLocal(x, y);
        const lineWidth = 30;
        if (local.x >= 0 && local.x < 2+lineWidth
            && local.y >= 0 && local.y < this.getFrameHeight()) {
            return true;
        }
        return false;
    }

    getFrameHeight() {
        let height = this.getContentHeight()+2;
        if (this.h-6 < height) height = this.h-6;
        if (this.player.friends.requests.length > 0 && this.h-9 < height) height = this.h-9;
        if (height < 3) height = 3;
        return height;
    }

    formatFriend(friend, BAR) {
        // check if online
        if (friend.name in this.player.positionList) {
            // friend is online!
            const friendPos = this.player.positionList[friend.name];
            let coordText = 'x:'+friendPos.x+' y:'+friendPos.y;
            if (coordText.length > 16) {
                // use shorter version
                coordText = friendPos.x+','+friendPos.y;
            }
            if (coordText.length > 16) {
                // use even shorter version
                coordText = Math.floor(friendPos.x/1000)+'k,'
                    +Math.floor(friendPos.y/1000)+'k';
            }
            if (coordText.length > 16) {
                // use EVEN SHORTER version
                coordText = Math.floor(friendPos.x/1000000)+'M,'
                    +Math.floor(friendPos.y/1000000)+'M';
            }
            return BAR+'\\[#'+friendPos.c+'\\]'
                + FriendlistComponent.CENTERING(friend.name, 12)
                + '\\[/\\]'+BAR
                + FriendlistComponent.CENTERING(coordText, 16)+BAR;
        }
        // friend is offline.
        const curTime = Math.floor((new Date()).getTime() / 1000);
        const seenAgo = curTime - friend.seen;
        let when;
        if (seenAgo < 0) {
            when = 'in the future??';
        }
        else if (seenAgo < 60) {
            when = seenAgo+'s ago';
        }
        else if (seenAgo < 3600) {
            when = Math.floor(seenAgo/60)+' min ago';
        }
        else if (seenAgo < 172800) {  // < 2 days
            when = Math.floor(seenAgo/3600)+' h ago';
        }
        else if (seenAgo < 8640000) {
            when = Math.floor(seenAgo/86400)+' days ago';
        }
        else if (seenAgo < 86400000) {
            when = Math.floor(seenAgo/86400)+'days ago';
        }
        else {
            when = Math.floor(seenAgo/86400)+'daysago';
        }
        return BAR+'\\[#'+friend.color+'\\]'
            + FriendlistComponent.CENTERING(friend.name, 12)
            + '\\[/\\]'+BAR+'\\['+GUI_SLIGHT_COLOR+'\\]'
            + FriendlistComponent.CENTERING('seen '+when, 16)
            + '\\[/\\]'+BAR;
    }

    getFrame(time, hoverx, hovery) {
        this.limitScrollPos();
        const local = this.screenToLocal(hoverx, hovery);

        const displayedHeight = this.getFrameHeight() - 2;

        const frame =
            [
                '┌─────────Friends─────────────┐',
            ];

        // prepare sorted order for friends
        const sortedFriends = this.player.friends.friends.slice();
        sortedFriends.sort((a,b)=>{
            const aonline = a.name in this.player.positionList;
            const bonline = b.name in this.player.positionList;
            if (aonline !== bonline) return bonline;
            return a.seen < b.seen;
        });
        const numRequests = this.player.friends.requests.length;
        const numFriends = this.player.friends.friends.length;
        const numPending = this.player.friends.pending.length;

        const maxScrollPos = this.getMaxScrollPos();
        const isCurrentlyScrollable = maxScrollPos > 0;
        const BARS = isCurrentlyScrollable ? ['╵',' ',' '] : ['│','│','│'];

        for (let i = 0; i < displayedHeight; i++) {
            const BAR = BARS[(i+this.scrollPos)%3];
            const nthRequest = i + this.scrollPos;
            const nthFriend = nthRequest - numRequests;
            const nthPending = nthFriend - numFriends;
            if (nthRequest < numRequests) {
                // display incoming friend request here
                const friend = this.player.friends.requests[nthRequest];
                let line = BAR + FriendlistComponent.CENTERING(friend, 12) + BAR + ' ';
                if (i === local.y-1 && local.x >= 14 && local.x < 22) {
                    line += '\\[m8\\]';
                }
                line += '\\[#373\\]ACCEPT\\[m0\\]  ';
                if (i === local.y-1 && local.x >= 22 && local.x < 30) {
                    line += '\\[m8\\]';
                }
                line += '\\[#733\\]REJECT\\[/\\]\\[m0\\] ' + BAR;
                frame.push(line);
                continue;
            }
            if (nthFriend < numFriends) {
                // display real friend here
                const friend = sortedFriends[nthFriend];
                frame.push(this.formatFriend(friend, BAR));
                continue;
            }
            if (nthPending < numPending) {
                const friend = this.player.friends.pending[nthPending];
                frame.push(BAR + FriendlistComponent.CENTERING(friend, 12)
                    + BAR+ '    \\[#337\\]PENDING\\[/\\]     ' + BAR);
                continue;
            }
            // this should only appear in completely empty friend list
            frame.push('│   \\[#777\\]use  /friend add "name"\\[/\\]   │');
        }

        if (this.player.friends.requests.length > 0) {
            // since there are some friend requests,
            // let's display instructions on how to accept
            frame.push('├─────────────────────────────┤');
            frame.push('│   click button to accept,   │');
            frame.push('│ or use \\[#bfb\\]/friend accept "name"\\[/\\]│');
        }

        let BOTTOMLINE;
        if (isCurrentlyScrollable) {
            const number = this.getScrollPercentage();
            BOTTOMLINE = '└──────(scrollable   '.substring(0, 21-number.length)+number+'%)──────┘';
        } else {
            BOTTOMLINE = '└─────────────────────────────┘';
        }
        frame.push(BOTTOMLINE);


        return frame;

    }

}
