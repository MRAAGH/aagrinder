// Copyright (C) 2025 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

class AAFileComponent extends ScrollableComponent {
    constructor(player) {
        super();
        this.player = player;
    }

    getFrameHeight() {
        let height = this.getContentHeight()+2;
        if (this.h-6 < height) height = this.h-6;
        if (this.player.friends.requests.length > 0 && this.h-9 < height) height = this.h-9;
        if (height < 3) height = 3;
        return height;
    }

    getFrameWidth() {
        return 14;
    }

    getFramePos() {
        return {
            x: Math.floor(this.w/2) - 15,
            y: 2,
        };
    }

    isShown() {
        return this.player.uiState === 'aafile';
    }

    isFocused() {
        return this.player.uiState === 'aafile';
    }

    getContentHeight() {
        const slot_index = this.player.aafileSlot;
        const slot = this.player.inventory.getSlotAt(slot_index);
        if (slot === null) return 0;
        const text = slot.data.text + blinkingCursor;
        const displayedWidth = this.getFrameWidth() - 2;
        const lines = AASTRING_WRAP_WITHOUT_DECODING(text, displayedWidth);
        return lines.length;
    }

    getFrame(time, hoverx, hovery) {
        this.limitScrollPos();
        const displayedHeight = this.getFrameHeight() - 2;
        const displayedWidth = this.getFrameWidth() - 2;
        const frame = ['┌───────────AAFILE────────────┐'];
        const slot_index = this.player.aafileSlot;
        const slot = this.player.inventory.getSlotAt(slot_index);
        if (slot === null) return frame;
        const blinkingCursor = this.isFocused()
            && time % 10 < 5 ? '▇' : ' ';
        const text = slot.data.text + blinkingCursor;
        const lines = AASTRING_WRAP_WITHOUT_DECODING(text, displayedWidth);
        const maxScrollPos = this.getMaxScrollPos();
        const isCurrentlyScrollable = maxScrollPos > 0;
        const BARS = isCurrentlyScrollable ? ['╵',' ',' '] : ['│','│','│'];
        if (!lines) return frame;
        const startLine = Math.max(0, lines.length - 4);
        for (let i = 0; i < displayedHeight; i++) {
            const BAR = BARS[(i+this.scrollPos)%3];
            const nthLine = i + this.scrollPos;
            if (nthLine < lines.length) {
                frame.push(BAR+lines[nthLine]+BAR);
                continue;
            }
            frame.push(BAR+lines[nthLine].raw+BAR);
        }
        let BOTTOMLINE;
        if (isCurrentlyScrollable) {
            const number = this.getScrollPercentage();
            BOTTOMLINE = '└──────(scrollable   '.substring(0, 21-number.length)+number+'%)──────┘';
        } else {
            BOTTOMLINE = '└─────────────────────────────┘';
        }
        frame.push(BOTTOMLINE);
        return frame;
    }

}
