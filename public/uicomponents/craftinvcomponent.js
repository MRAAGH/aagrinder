// Copyright (C) 2018-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * An extended version of InventoryComponent which
 * additionaly has a list of craftable items that
 * automatically updates based on recipes from Inventory
 */

"use strict";

class CraftInventoryComponent extends InventoryComponent {
    constructor(player){
        super(player, player.inventory, 'inv_',
            ['┌Craft┐', '┌─Crafting───────┐'],
            ['inventory']);
        this.craftableRecipes = [];
        this.inventory.onInventoryChanged(() => this.updateRecipes());
        for (let i = 0; i < Inventory.recipes.length; i++) {
            const slotId = 'recipe_'+i;
            this.player.registerSlotHandler(slotId, this);
        }
    }

    updateRecipes() {
        this.craftableRecipes = this.inventory.getCraftableRecipes();
    }

    getFrameHeight() {
        const top = this.getInventoryTopYpos();
        const bot = this.getInventoryBottomYpos();
        let frameHeight = bot - top - 3;
        if (frameHeight < 5) return 5;
        return frameHeight;
    }

    getNumVisualizedSlots() {
        const frameHeight = this.getFrameHeight();
        if (frameHeight >= 9) return 5;
        return frameHeight - 4;
    }

    getNumVisualizedRecipes() {
        const frameHeight = this.getFrameHeight();
        const numIngredientSlots = this.getNumVisualizedSlots();
        return frameHeight - numIngredientSlots - 3;
    }

    getYRecipesStart() {
        return this.getNumVisualizedSlots() + 2;
    }

    getSlotIdAtScreenCoordinates(x, y) {
        let slotId = super.getSlotIdAtScreenCoordinates(x, y);
        if (slotId !== null) {
            return slotId;
        }
        if (!this.isShown()) return null;
        const local = this.screenToLocal(x, y);
        const lineWidth = this.compact ? 5 : 16;
        const numVisualizedRecipes = this.getNumVisualizedRecipes();
        const yRecipesStart = this.getYRecipesStart();
        if (local.x > 0 && local.x < 1+lineWidth
            && local.y >= yRecipesStart
            && local.y < yRecipesStart + numVisualizedRecipes
            && local.y < yRecipesStart + this.craftableRecipes.length) {
            const slotNumber = local.y-yRecipesStart;
            const recipeId = 'recipe_'+this.craftableRecipes[slotNumber];
            return recipeId;
        }
        return null;
    }

    getFrame() {
        const frame = super.getFrame();
        // last row is no longer last row
        frame.pop()
        frame.push(this.compact ? '├─ ↓ ─┤' : '├────── ↓↓ ──────┤');
        const numVisualizedRecipes = this.getNumVisualizedRecipes();
        // add recipe list
        for (let i = 0; i < numVisualizedRecipes; i++) {
            if (i >= this.craftableRecipes.length) {
                // no more items to show
                if (this.compact) {
                    frame.push('│     │');
                } else {
                    frame.push('│                │');
                }
                continue;
            }
            // there is a recipe to show
            const recipeIndex = this.craftableRecipes[i];
            const recipe = Inventory.recipes[recipeIndex];
            const item_code = recipe.get.item;
            const count = recipe.get.amount;
            const slotIdHere = 'recipe_'+recipeIndex;
            const isHovered = slotIdHere === this.player.hoverSlotId;
            const isDragged = slotIdHere === this.player.dragFromId;
            const hoverSpace = isHovered
                ? '\\[m8\\]\\[#7f7f7f\\] \\[/\\]'
                : ' ';
            const formattedItem = (isDragged ? '\\[m8\\]' : '') + InventoryComponent.formatItem(item_code, this.player);
            const item_name = Inventory.item_short_names[item_code];
            const itemNameText = this.compact ? '' : (item_name+'               ').substring(0, 11);
            const haveIngredients = this.inventory.howManyRecipesCanExpend(recipeIndex) > 0;
            let formattedAmount = count.toString();
            if (!haveIngredients) {
                formattedAmount = '\\[#733\\]' + formattedAmount + '\\[/\\]';
            }
            formattedAmount += '   ';
            formattedAmount = formattedAmount.substring(0,
                formattedAmount.length - AASTRING_LENGTH(formattedAmount) + 3);
            const line = '│'
                + formattedItem
                + hoverSpace
                + '\\[' + this.pallete.default + '\\]'
                + itemNameText
                + formattedAmount
                + '│';
            frame.push(line);
        }
        frame.push(this.compact ? '└─────┘' : '└────────────────┘');
        return frame;
    }

    getSlotScreenPosById(id) {
        // if component hidden, slot does not exist on screen
        if (!this.isShown()) return null;
        const idSplit = id.split('_');
        if (idSplit[0] === 'recipe') {
            // is a recipe ID
            const recipeIndex = parseInt(idSplit[1]);
            for (let i = 0; i < this.craftableRecipes.length
                && i < this.getNumVisualizedRecipes(); i++) {
                if (this.craftableRecipes[i] === recipeIndex) {
                    const yRecipesStart = this.getYRecipesStart();
                    return this.localToScreen(1, yRecipesStart + i);
                }
            }
            // recipe not craftable. (or for other reason not visible in UI)
            return null;
        }
        // otherwise is regular inventory slot
        return super.getSlotScreenPosById(id);
    }

    getItemBySlotId(id) {
        const idSplit = id.split('_');
        if (idSplit[0] === 'recipe') {
            // is a recipe
            const recipeIndex = parseInt(idSplit[1]);
            const recipe = Inventory.recipes[recipeIndex];
            const item = {
                code: recipe.get.item,
                amount: 1,
                displayMultiplier: recipe.get.amount,
            }
            return item;
        }
        // otherwise is regular inventory slot
        return super.getItemBySlotId(id);
    }

    getXpos() {
        return 12;
    }

    getYpos() {
        return this.getInventoryTopYpos();
    }

    getCutoffStart() {
        return Inventory.PLAYER_INVENTORY_SLOT_COUNT-6;
    }

    getCutoffLength() {
        return 5;
    }

}
