// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A different representation of data in invcomponent,
 * to be able to display a fully featured hotbar in the UI.
 * Additionally, display hunger
 */

"use strict";


class HelpComponent extends UiComponent {
    constructor(player){
        super();
        this.player = player;
    }

    static GUI_HELP_LEFT_COLOR = '#aaaa88';
    static GUI_HELP_MID_COLOR = '#aa88aa';
    static GUI_HELP_RIGHT_COLOR = '#88aaaa';

    static HELP =
        [
            ['move '+PLAYER_STRING, 'wasd', ''],
            ['move cursor', '←↑→↓', ''],
            ['break', 'r', 'left'],
            ['place', 'q', 'right'],
            ['interact', 'q', 'right'],
            ['pick block', '', 'mid'],
            ['sprint', 'shift', ''],
            ['scroll hotbar', ', .', ''],
            ['inventory', 'e', ''],
            ['consume (eat)', 'c', ''],
            ['focus chat', 'enter', ''],
            ['map', 'm', ''],
            ['lock camera', 'l', ''],
            ['inspect', 'tab', ''],
            ['friend list', 'f', ''],
            ['', '', ''],
            ['list commands', '/help', ''],
            ['map keys', '/map', ''],
            ['settings', '/set', ''],
        ];

    getFramePos() {
        return {
            x: Math.floor(this.w/2) - 17,
            y: Math.floor(this.h/2) - 9,
        };
    }

    getFrame(time) {
        const frame = [];
        frame.push('┌──────────────────────────┐');
        frame.push('│              \\[#dd77cc\\]KEY:  \\[#55bbaa\\]MOUSE:\\[/\\]│');
        for (const h of HelpComponent.HELP) {
            const left = '\\['+HelpComponent.GUI_HELP_LEFT_COLOR+'\\]'
                +h[0]+'\\[/\\]'
                +'              '.substr(h[0].length);
            const mid = '\\['+HelpComponent.GUI_HELP_MID_COLOR+'\\]'
                +h[1]+'\\[/\\]'
                +'      '.substr(h[1].length);
            const right = '\\['+HelpComponent.GUI_HELP_RIGHT_COLOR+'\\]'
                +h[2]+'\\[/\\]'
                +'      '.substr(h[2].length);
            frame.push('│'+left+mid+right+'│');
        }
        frame.push('├──────────────────────────┤');
        frame.push('│  press any key to close  │');
        frame.push('└──────────────────────────┘');
        return frame;
    }

    isShown() {
        return this.player.uiState === 'help';
    }
}
