// Copyright (C) 2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class HotbarInventoryComponent extends InventoryComponent {
    constructor(player) {
        super(player, player.inventory, 'inv_',
            ['┌─Hot─┐', '┌─Hotbar─────────┐'],
            ['inventory', 'chest']);
    }

    getFrame() {
        const frame = super.getFrame();
        for (const i in frame) {
            if (i > 0 && i < this.getNumVisualizedSlots()+1) {
                frame[i] = ((parseInt(i)+this.scrollPos)%10) + frame[i].slice(1);
            }
        }
        return frame;
    }

    getXpos() {
        return -11;
    }

    getYpos() {
        return this.getInventoryTopYpos();
    }

    getCutoffStart() {
        return 0;
    }

    getCutoffLength() {
        return 10;
    }

    getFrameHeight() {
        const top = this.getInventoryTopYpos();
        const bot = this.getInventoryBottomYpos();
        // show all slots if possible
        if (bot - top >= 15) return 12;
        // calculate how much fits beside the main inv
        let frameHeight = bot - top - 3;
        // must show at least 3
        if (frameHeight < 3) return 3;
        return frameHeight;
    }

}
