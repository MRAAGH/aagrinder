// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A different representation of data in invcomponent,
 * to be able to display a fully featured hotbar in the UI.
 * Additionally, display hunger
 */

"use strict";

class HotbarComponent extends UiComponent {
    constructor(invComponent, player){
        super();
        this.invComponent = invComponent;
        this.player = player;
        this.cursor = 0;
    }

    cursorUp() {
        this.cursor = (this.cursor + 10 - 1) % 10;
    }

    cursorDown() {
        this.cursor = (this.cursor + 1) % 10;
    }

    cursorSet(cursor) {
        this.cursor = cursor;
    }

    getSelected() {
        const slot = this.invComponent.inventory.getSlotAt(this.cursor);
        if (slot === null) return null;
        return slot.code;
    }

    getFramePos() {
        return {
            x: Math.floor(this.w/2) - 6,
            y: Math.floor(this.h) - 4,
        };
    }

    isClickWithin(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.x >= 0 && local.x < 12
            && local.y >= 0 && local.y < 4) {
            return true;
        }
        return false;
    }

    // click is in character coordinates
    handleClick(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.x < 1) return false;
        if (local.x > 11) return false;
        if (local.y < 1) return false;
        if (local.y > 3) return false;
        this.invComponent.cursor = local.x - 1;
        return true;
    }

    makeHotbarSprite(item) {
        if (item === 'E') {
            const blinkblock = 'E'+this.player.detectorBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        if (item === 'Wo') {
            const blinkblock = 'Wo'+this.player.followBlinkSpeed;
            return BIOME_SPRITES[blinkblock]['A'];
        }
        const block = Inventory.item2blockPlain(item);
        return BIOME_SPRITES_NO_EFFECT[block]['A'];
    }

    getFrame(time) {
        const slotNumber = this.cursor;
        const item = this.invComponent.inventory.getSlotAt(this.cursor);
        let item_code;
        let amount
        if (item === null) {
            item_code = ' ';
            amount = 0;
        } else {
            item_code = item.code;
            amount = item.amount;
        }

        let current = '     ';

        if (item_code[0] === 'G') {
            // hotbar with durability
            const dur = item.data.d;
            const max = Inventory.item_data_templates[item_code].d;
            const percent = dur === max ? ''
                : Math.floor(dur/max*100)+'%';
            current = InventoryComponent.formatItem(item_code, this.player);
            current += ' '+ percent+'   '.substring(0, 3-percent.length);
        } else if (item_code !== ' ') {
            // hotbar with item amount
            current = InventoryComponent.formatItem(item_code, this.player);
            if (amount === 0) {
                current += '     ';
            } else if (amount > 999) {
                current += '999+';
            } else {
                const number = amount.toString();
                current += ' '+ number+'   '.substring(0, 3-number.length);
            }
        }

        let bottomList = '';
        for (let i = 0; i < 10; i++) {
            const item = this.invComponent.inventory.slots[i];
            if (item === null) {
                bottomList += ' ';
                continue;
            }
            const item_code = item.code;
            if (i === slotNumber) {
                bottomList += '\\[m8\\]';
            }
            bottomList += InventoryComponent.formatItem(item_code, this.player);
        }

        const lineLeft = Array(slotNumber).fill('─').join('');
        const lineRight = Array(9-slotNumber).fill('─').join('');

        // render hunger above hotbar
        let shade = Math.sin(time*0.3)*0.3+0.7;
        const baseColor = '#000000';
        const addColor = '#cf1005';
        const blinkColor = ADD_COLORS_HEX(baseColor, addColor, shade);
        let foodString = '';

        if (this.player.food === 0) {
            foodString += '\\['+blinkColor+'\\]C=eat';
        } else {
            foodString += '\\['+addColor+'\\]';
            for (let i = 0; i < 5; i++) {
                if (i < Math.ceil(this.player.food/PLAYER_MAXFOOD*5)) {
                    if (5*this.player.food < (i+0.2)*PLAYER_MAXFOOD) {
                        foodString += '\\['+blinkColor+'\\]';
                    }
                    foodString += 'a';
                    continue;
                }
                foodString += '@';
            }
        }


        const curveWithIndicator =
            (slotNumber === 5 ? '├' : '└') +
            (slotNumber === 6 ? '┬' : '─') +
            (slotNumber === 7 ? '┬' : '─') +
            (slotNumber === 8 ? '┬' : '─') +
            (slotNumber === 9 ? '┬' : '─') +
            '┐';

        const frame =
            [
                '┌─────┐'+foodString,
                '│'+current+curveWithIndicator,
                '│'+bottomList+'│',
                '└'+lineLeft+'┴'+lineRight+'┘',
            ];

        return frame;
    }

    selectItemByCode(item_code) {
        // if item exists in other inventory, (other than delete), move it to target slot, or swap slots. return.
        // if gamemode === 0, return (failed).
        // delete the target slot and magically gain item there. Probably best if magicallygetitem automatically deletes the target slot.

        // actually magicallygetitem will handle all cases, ensuring the item shows up somewhere in the hotbar. Then this func is called again. Same as previous logic

        // Select existing item in hotbar, if exists
        for (let i = 0; i < 10; i++) {
            const item = this.invComponent.inventory.getSlotAt(i);
            if (item_code === null && item === null) {
                // found empty slot, as requested.
                this.cursor = i;
                return true;
            }
            if (item === null) continue;
            if (item.code === item_code) {
                // found a slot with the requested item.
                this.cursor = i;
                return true;
            }
        }
        // Item was not in hotbar.
        return false;
    }

    getTooltipAt(x, y) {
        const local = this.screenToLocal(x, y);
        if (local.y > 1) {
            return 'hotbar (scrollable)';
        }
        if (local.x <= 2 && local.y === 1) {
            return 'selected item';
        }
        if (local.x >= 3 && local.x <= 6 && local.y === 1) {
            return 'amount/duability';
        }
        if (local.x >= 7 && local.y === 0) {
            if (this.player.food === 0) {
                return 'select food, press C'
            }
            return 'energy';
        }
        return '';
    }

}
