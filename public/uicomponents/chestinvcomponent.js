// Copyright (C) 2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

const MAX_POSSIBLE_CHEST_SLOTS = 30;

class ChestInventoryComponent extends InventoryComponent {
    constructor(player) {
        super(player, player.chest.inventory, 'chest_',
            ['┌Chest┐', '┌─Chest──────────┐'],
            ['chest']);
        // register all slots up to the highest possible in any chest
        for (let i = 0; i < MAX_POSSIBLE_CHEST_SLOTS; i++) {
            const slotId = 'chest_' + i;
            this.player.registerSlotHandler(slotId, this);
        }
    }

    getXpos() {
        return 12;
    }

    getYpos() {
        return this.getInventoryTopYpos();
    }

    getCutoffStart() {
        return 0;
    }

    getCutoffLength() {
        return this.player.chest.inventory.getNumExistingSlots();
    }

    getFrameHeight() {
        const top = this.getInventoryTopYpos();
        const bot = this.getInventoryBottomYpos();
        // calculate how much fits beside the deletion inventory
        let frameHeight = bot - top - 3;
        const heightFittingAll = this.getCutoffLength()+2;
        // do not show more than entire chest inventory
        if (frameHeight > heightFittingAll)
            frameHeight = heightFittingAll;
        // must show at least 3, overriding previous calculations
        if (frameHeight < 3) frameHeight = 3;
        return frameHeight;
    }

}
