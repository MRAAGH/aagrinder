// Copyright (C) 2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class MainInventoryComponent extends InventoryComponent {
    constructor(player, hotbarInvComponent) {
        super(player, player.inventory, 'inv_',
            ['┌─Inv─┐', '┌─Inventory──────┐'],
            ['inventory', 'chest']);
        this.hotbarInvComponent = hotbarInvComponent;
    }

    getXpos() {
        return -11;
    }

    getYpos() {
        return this.hotbarInvComponent.getYpos()
            + this.hotbarInvComponent.getFrameHeight();
    }

    getCutoffStart() {
        return 10;
    }

    getCutoffLength() {
        return Inventory.PLAYER_INVENTORY_SLOT_COUNT-10-6;
    }

    getFrameHeight() {
        const bot = this.getInventoryBottomYpos();
        let frameHeight = bot - this.getYpos();
        if (frameHeight < 3) return 3;
        return frameHeight;
    }

}
