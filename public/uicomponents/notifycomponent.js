// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class NotifyComponent extends UiComponent {

    constructor(notify) {
        super();
        this.notify = notify;
    }

    getFramePos() {
        return {
            x: 2,
            y: this.h - 1 - this.notify.messages.length,
        };
    }

    show() {
    }

    hide() {
    }

    isShown() {
        return true;
    }

    getFrame() {
        const frame = [];

        for (let i = this.notify.messages.length-1; i >= 0; i--) {
            const message = this.notify.messages[i];
            const shade = Math.min(1,
                4-4*message.age/Notify.MESSAGE_LIFETIME)
            const color = ADD_COLORS_HEX(
                '#222222', '#bbbbbb', shade);
            const formatted = '\\['+color+'\\]'+message.text;
            frame.push(formatted);
        }

        return frame;

    }

}
