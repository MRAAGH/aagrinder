// Copyright (C) 2023-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


"use strict";

class DeleteInventoryComponent extends InventoryComponent {
    constructor(player) {
        super(player, player.inventory, 'inv_',
            ['┌DELET┐', '┌─!DELETE!───────┐'],
            ['inventory', 'chest']);
        this.pallete = {
            default: '#555',
            percent_100: '#474',
            percent_70: '#453',
            percent_40: '#543',
            percent_10: '#511',
        };
    }

    getXpos() {
        return 12;
    }

    getYpos() {
        return this.getInventoryBottomYpos() - 3;
    }

    getCutoffStart() {
        return Inventory.PLAYER_INVENTORY_SLOT_COUNT-1;
    }

    getCutoffLength() {
        return 1;
    }

    getFrameHeight() {
        return 3;
    }

}

