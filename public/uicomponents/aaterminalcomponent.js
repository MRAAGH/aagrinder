// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

AATERMINAL_TYPED_CHARACTERS = {

    ' ':  '\\[s\\]',
    'Enter':  '\\[n\\]',
    'ArrowUp': 'w',
    'ArrowLeft': 'a',
    'ArrowDown': 's',
    'ArrowRight': 'd',
    '!':  '!',
    '"':  '"',
    '#':  '#',
    '$':  '$',
    '%':  '%',
    '&':  '&',
    '\'': '\'',
    '(':  '(',
    ')':  ')',
    '*':  '*',
    '+':  '+',
    ',':  ',',
    '-':  '-',
    '.':  '.',
    '/':  '/',
    '0':  '0',
    '1':  '1',
    '2':  '2',
    '3':  '3',
    '4':  '4',
    '5':  '5',
    '6':  '6',
    '7':  '7',
    '8':  '8',
    '9':  '9',
    ':':  ':',
    ';':  ';',
    '<':  '<',
    '=':  '=',
    '>':  '>',
    '?':  '?',
    '@':  '@',
    'A':  'A',
    'B':  'B',
    'C':  'C',
    'D':  'D',
    'E':  'E',
    'F':  'F',
    'G':  'G',
    'H':  'H',
    'I':  'I',
    'J':  'J',
    'K':  'K',
    'L':  'L',
    'M':  'M',
    'N':  'N',
    'O':  'O',
    'P':  'P',
    'Q':  'Q',
    'R':  'R',
    'S':  'S',
    'T':  'T',
    'U':  'U',
    'V':  'V',
    'W':  'W',
    'X':  'X',
    'Y':  'Y',
    'Z':  'Z',
    '[':  '[',
    '\\': '\\',
    ']':  ']',
    '^':  '^',
    '_':  '_',
    '`':  '`',
    'a':  'a',
    'b':  'b',
    'c':  'c',
    'd':  'd',
    'e':  'e',
    'f':  'f',
    'g':  'g',
    'h':  'h',
    'i':  'i',
    'j':  'j',
    'k':  'k',
    'l':  'l',
    'm':  'm',
    'n':  'n',
    'o':  'o',
    'p':  'p',
    'q':  'q',
    'r':  'r',
    's':  's',
    't':  't',
    'u':  'u',
    'v':  'v',
    'w':  'w',
    'x':  'x',
    'y':  'y',
    'z':  'z',
    '{':  '{',
    '|':  '|',
    '}':  '}',
    '~':  '~',
    '€':  '€',

}


"use strict";

class AATerminalComponent extends UiComponent {
    constructor(player, map, index) {
        super();
        this.player = player;
        this.map = map;
        this.index = index;
    }

    getXpos() {
        return 2;
    }

    getYpos() {
        if (this.h > 7) return this.h-7;
        return this.h - 5;
    }

    getFrameHeight() {
        return 4;
    }

    getFramePos() {
        return {
            x: this.getXpos(),
            y: this.getYpos(),
        };
    }

    getFrame(time, hoverx, hovery) {
        const frame = [];
        const title = this.isFocused() ? 'TERM' : 'term';
        const xbutton = this.isClickOnXButton(hoverx, hovery)
            ? '\\[m8\\][x]\\[/\\]'
            : '[x]';
        frame.push('┌'+title+'─────'+xbutton+'┐');

        const tilex = this.player.openedAAterminals[this.index].x;
        const tiley = this.player.openedAAterminals[this.index].y;
        const tiles = this.map.getTileEntityAt(tilex, tiley);
        if (tiles.length > 0 && tiles[0].t === 'aaterminal') {
            const tile = tiles[0];
            const lines = tile.d.lines;
            if (!lines) return ['undefined'];
            const blinkingCursor = this.isFocused()
                && time % 10 < 5 ? '▇' : ' ';
            const blinkingLines = AASTRING_MAKE_COPY(lines);
            blinkingLines[blinkingLines.length-1] += blinkingCursor;
            AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(blinkingLines, 12);
            const startLine = Math.max(0, blinkingLines.length - 4);
            for (let l = startLine; l < startLine + 4; l++) {
                if (l >= blinkingLines.length) {
                    frame.push('│            │');
                    continue;
                }
                frame.push('│'
                    + blinkingLines[l]
                    + '            '.substring(AASTRING_LENGTH(blinkingLines[l]))
                    + '\\[/\\]│');
            }
            const lineEnd = tile.d.out !== ' ' ? tile.d.out : '┘';
            frame.push('└────────────'+lineEnd);
        }

        return frame;
    }

    isShown() {
        return this.player.openedAAterminals.length > 0;
    }

    isFocused() {
        return this.player.focusedAAterminal === 0;
    }

    // with 0,0 at start of canvas
    isClickWithin(x, y) {
        if (!this.isShown()) return false;
        const local = this.screenToLocal(x, y);
        if (local.x >= 0 && local.x < 14
            && local.y >= 0 && local.y < 6) {
            return true;
        }
        return false;
    }

    isClickOnXButton(x, y) {
        const local = this.screenToLocal(x, y);
        const title = this.isFocused() ? 'TERM' : 'term';
        return local.y === 0 && local.x > 9 && local.x < 13;
    }

    // click is in character coordinates
    handleClick(x, y) {
        if (!this.isShown()) return false;
        if (this.isClickOnXButton(x, y)) {
            this.player.openedAAterminals.splice(this.index);
            return true;
        }
        return false;
    }

}
