// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * The client-side command database, used for tabcompletion and /help
 */

"use strict";

class CommandDatabase {

    constructor() {
        this.commands = {};
    }

    addCommand(name, aliases, signatures, help, usageExtraLine) {
        if (this.commands[name]) return false;
        this.commands[name] = {
            name: name,
            aliases: aliases,
            signatures: signatures,
            help: help,
            usageExtraLine: usageExtraLine,
        };
        return true;
    }

    getCommandNames() {
        return Object.keys(this.commands).sort();
    }

    commandExists(name) {
        return this.commands[name] ? true : false;
    }

    commandOrAliasExists(alias) {
        for (const name in this.commands) {
            if (name === alias) return true;
            if (this.commands[name].aliases.indexOf(alias) > -1) return true;
        }
        return false;
    }

    resolveAlias(alias) {
        for (const name in this.commands) {
            if (name === alias) return name;
            if (this.commands[name].aliases.indexOf(alias) > -1) return name;
        }
        return null;
    }

    getAliases(name) {
        if (!this.commandExists(name)) return null;
        return this.commands[name].aliases;
    }

    getSignatures(name) {
        if (!this.commandExists(name)) return null;
        return this.commands[name].signatures;
    }

    getUsageExtraLine(name) {
        if (!this.commandExists(name)) return null;
        return this.commands[name].usageExtraLine;
    }

    getHelp(name) {
        if (!this.commandExists(name)) return null;
        return this.commands[name].help;
    }

}
