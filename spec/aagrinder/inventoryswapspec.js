const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots1() {
    return [
        /* 0*/ { code: 'Ha', amount: 3000 },
        /* 1*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
        /* 2*/ null,
        /* 3*/ null,
    ];
}

function getDefaultSlots2() {
    return [
        /* 0*/ null,
        /* 1*/ { code: 'B', amount: 900 },
        /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood2'} },
    ];
}

describe('Inventory', function() {
    let inventory1;
    let inventory2;

    beforeEach(function() {
        inventory1 = new Inventory(getDefaultSlots1(), false);
        inventory2 = new Inventory(getDefaultSlots2(), false);
    });

    it('should swap own empty slots', function() {
        inventory1.swapSlots(inventory1, 2, 3);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
    });

    it('should swap empty slots', function() {
        inventory1.swapSlots(inventory2, 2, 0);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
        expect(inventory2.slots).toEqual(getDefaultSlots2());
    });

    it('should swap slot with own empty', function() {
        inventory1.swapSlots(inventory1, 0, 3);
        expect(inventory1.slots).toEqual([
            /* 0*/ null,
            /* 1*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 2*/ null,
            /* 3*/ { code: 'Ha', amount: 3000 },
        ]);
        inventory1.swapSlots(inventory1, 3, 0);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
    });

    it('should swap slot with empty', function() {
        inventory1.swapSlots(inventory2, 1, 0);
        expect(inventory1.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 3000 },
            /* 1*/ null,
            /* 2*/ null,
            /* 3*/ null,
        ]);
        expect(inventory2.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 1*/ { code: 'B', amount: 900 },
            /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood2'} },
        ]);
        inventory1.swapSlots(inventory2, 1, 0);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
        expect(inventory2.slots).toEqual(getDefaultSlots2());
    });

    it('should swap own slots', function() {
        inventory1.swapSlots(inventory1, 0, 1);
        expect(inventory1.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 1*/ { code: 'Ha', amount: 3000 },
            /* 2*/ null,
            /* 3*/ null,
        ]);
        inventory1.swapSlots(inventory1, 0, 1);  // swap back
        expect(inventory1.slots).toEqual(getDefaultSlots1());
    });

    it('should swap slots', function() {
        inventory1.swapSlots(inventory2, 0, 2);
        expect(inventory1.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 30, data: {name: 'namedwood2'} },
            /* 1*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 2*/ null,
            /* 3*/ null,
        ]);
        expect(inventory2.slots).toEqual([
            /* 0*/ null,
            /* 1*/ { code: 'B', amount: 900 },
            /* 2*/ { code: 'Ha', amount: 3000 },
        ]);
        inventory2.swapSlots(inventory1, 2, 0);  // swap back
        expect(inventory1.slots).toEqual(getDefaultSlots1());
        expect(inventory2.slots).toEqual(getDefaultSlots2());
    });

});
