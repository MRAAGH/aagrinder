const Inventory = require('../../shared/inventory').Inventory;

// Test upgrading old game data to newest version

describe('Backwards compatibility', function() {

    it('should upgrade empty old inventory (type 0&1)', function() {
        expect(new Inventory({}, true).slots).toEqual(new Array(
            Inventory.PLAYER_INVENTORY_SLOT_COUNT).fill(null));
        expect(new Inventory({}, false).slots).toEqual(
            new Array(10).fill(null));
    });

    it('should upgrade empty old inventory (type 2)', function() {
        expect(new Inventory({s: {}, n: 15}, true).slots).toEqual(
            new Array(15).fill(null));
    });

    it('should upgrade old inventory (type 0)', function() {
        expect(new Inventory({
            B: 10,
            Ha: 20,
        }, false).slots).toEqual([
            { code: 'B', amount: 10 },
            { code: 'Ha', amount: 20 },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]);
    });

    it('should upgrade old inventory (type 1)', function() {
        expect(new Inventory({
            B: {c: 10},
            Ha: {c: 20},
        }, false).slots).toEqual([
            { code: 'B', amount: 10 },
            { code: 'Ha', amount: 20 },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]);
    });

    it('should upgrade old inventory (type 1)', function() {
        expect(new Inventory({s: {
            B: {c: 10},
            Ha: {c: 20},
        }, n: 4}, false).slots).toEqual([
            { code: 'B', amount: 10 },
            { code: 'Ha', amount: 20 },
            null,
            null,
        ]);
    });

    it('should upgrade old inventory with item data (type 0)', function() {
        expect(new Inventory({s: {
            'G#': 3,
            Ha: 20,
        }, n: 5}, false).slots).toEqual([
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'Ha', amount: 20 },
            null,
        ]);
    });

    it('should upgrade old inventory with item data (type 1)', function() {
        expect(new Inventory({s: {
            'G#': {c: 3, d: [{d: 100}, {d: 200}, {d:300}]},
            Ha: {c: 20},
        }, n: 5}, false).slots).toEqual([
            { code: 'G#', amount: 1, data: {d: 100} },
            { code: 'G#', amount: 1, data: {d: 200} },
            { code: 'G#', amount: 1, data: {d: 300} },
            { code: 'Ha', amount: 20 },
            null,
        ]);
    });

    it('should upgrade old overfull inventory (type 0)', function() {
        expect(new Inventory({
            B: 3000,
            Ha: 20,
        }, false).slots).toEqual([
            { code: 'B', amount: 3000 },
            { code: 'Ha', amount: 20 },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]);
    });

    it('should upgrade old overfull inventory (type 1)', function() {
        expect(new Inventory({
            B: {c: 3000},
            Ha: {c: 20},
        }, false).slots).toEqual([
            { code: 'B', amount: 3000 },
            { code: 'Ha', amount: 20 },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        ]);
    });

    it('should upgrade old inventory with too many slots', function() {
        expect(new Inventory({
            B: {c: 10},
            Ha: {c: 20},
            Hb: {c: 20},
            Hu: {c: 20},
            Hp: {c: 20},
            Aa: {c: 20},
            Ab: {c: 20},
            Au: {c: 20},
            Ap: {c: 20},
            DD: {c: 20},
            M: {c: 20},
            WA: {c: 20},
        }, false).slots).toEqual([
            { code: 'B', amount: 10},
            { code: 'Ha', amount: 20},
            { code: 'Hb', amount: 20},
            { code: 'Hu', amount: 20},
            { code: 'Hp', amount: 20},
            { code: 'Aa', amount: 20},
            { code: 'Ab', amount: 20},
            { code: 'Au', amount: 20},
            { code: 'Ap', amount: 20},
            { code: 'DD', amount: 20},
            { code: 'M', amount: 20},
            { code: 'WA', amount: 20},
        ]);
    });

    it('should upgrade old inventory with too many slots due to data splitting', function() {
        expect(new Inventory({
            B: 10,
            'G#': 10,
        }, false).slots).toEqual([
            { code: 'B', amount: 10},
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
            { code: 'G#', amount: 1, data: {d: 8000} },
        ]);
    });

});
