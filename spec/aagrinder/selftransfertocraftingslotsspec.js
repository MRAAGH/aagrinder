const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots() {
    return [
        /* 0*/ { code: 'B', amount: 20 },
        /* 1*/ { code: 'GB', amount: 1, data: {d: 9} },
        /* 2*/ null,
        /*c3*/ { code: 'Ha', amount: 30 },
        /*c4*/ null,
        /*c5*/ { code: 'B', amount: 990 },
        /*c6*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*c7*/ { code: 'B', amount: 3000 },
        /*d8*/ { code: 'B', amount: 500 },
    ];
}

function getFullSlots() {
    return [
        /* 0*/ { code: 'B', amount: 20 },
        /* 1*/ { code: 'GB', amount: 1, data: {d: 9} },
        /* 2*/ { code: 'B', amount: 8 },
        /*c3*/ { code: 'Ha', amount: 30 },
        /*c4*/ { code: 'b', amount: 1 },
        /*c5*/ { code: 'B', amount: 990 },
        /*c6*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*c7*/ { code: 'B', amount: 3000 },
        /*d8*/ { code: 'B', amount: 500 },
    ];
}

describe('Sending items to crafting', function() {
    let inventory;
    let fullinventory;

    beforeEach(function() {
        inventory = new Inventory(getDefaultSlots(), true);
        fullinventory = new Inventory(getFullSlots(), true);
    });

    it('should be able to send stack to crafting slots when there is space', function() {
        expect(inventory.howManyItemsCanMoveToCraftingSlots(inventory, 0)).toEqual(20);
        expect(inventory.canMoveItemsToCraftingSlots(inventory, 0, 0)).toEqual(true);
        expect(inventory.canMoveItemsToCraftingSlots(inventory, 0, 20)).toEqual(true);
        expect(inventory.canMoveItemsToCraftingSlots(inventory, 0, 21)).toEqual(false);
        expect(inventory.howManyItemsCanMoveToCraftingSlots(inventory, 1)).toEqual(1);
    });

    it('should limit how many items can be sent into crafting', function() {
        expect(fullinventory.howManyItemsCanMoveToCraftingSlots(fullinventory, 0)).toEqual(9);
        expect(fullinventory.howManyItemsCanMoveToCraftingSlots(fullinventory, 1)).toEqual(0);
        expect(fullinventory.howManyItemsCanMoveToCraftingSlots(fullinventory, 2)).toEqual(8);
        // Move some stone to crafting slots
        fullinventory.moveItemsToCraftingSlots(fullinventory, 0, 5);
        // Now fewer spaces are available
        expect(fullinventory.howManyItemsCanMoveToCraftingSlots(fullinventory, 0)).toEqual(4);
        expect(fullinventory.howManyItemsCanMoveToCraftingSlots(fullinventory, 1)).toEqual(0);
        expect(fullinventory.howManyItemsCanMoveToCraftingSlots(fullinventory, 2)).toEqual(4);
    });

    it('should not be able to stack data items when sending stack to crafting', function() {
        // Move the stone to crafting, blocking all slots
        expect(inventory.howManyItemsCanMoveToCraftingSlots(inventory, 1)).toEqual(1);
        inventory.moveItemsToCraftingSlots(inventory, 0, 20);
        expect(inventory.howManyItemsCanMoveToCraftingSlots(inventory, 1)).toEqual(0);
        expect(inventory.canMoveItemsToCraftingSlots(inventory, 1, 0)).toEqual(true);
        expect(inventory.canMoveItemsToCraftingSlots(inventory, 1, 1)).toEqual(false);
        expect(inventory.slots).toEqual([
            /* 0*/ null,
            /* 1*/ { code: 'GB', amount: 1, data: {d: 9} },
            /* 2*/ null,
            /*c3*/ { code: 'Ha', amount: 30 },
            /*c4*/ { code: 'B', amount: 11 },
            /*c5*/ { code: 'B', amount: 999 },
            /*c6*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*c7*/ { code: 'B', amount: 3000 },
            /*d8*/ { code: 'B', amount: 500 },
        ]);
        expect(inventory.moveItemsToCraftingSlots(inventory, 1, 1)).toEqual(false);
    });

});
