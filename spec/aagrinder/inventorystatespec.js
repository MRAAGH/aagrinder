const Inventory = require('../../shared/inventory').Inventory;

describe('Inventory', function() {
    let inventory;

    beforeEach(function() {
        inventory = new Inventory([
            /* 0*/ { code: 'B', amount: 10 },
            /* 1*/ { code: 'B', amount: 20 },
            /* 2*/ { code: 'B', amount: 30 },
            /* 3*/ { code: 'Ha', amount: 30 },
            /* 4*/ null,
            /* 5*/ { code: 'DD', amount: 2 },
            /* 6*/ null,
            /* 7*/ null,
            /* 8*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 9*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*10*/ null,
            /*11*/ { code: 'GB', amount: 1, data: {d: 58} },
            /*12*/ null,
            /*13*/ null,
            /*14*/ { code: 'B', amount: 3000 },
        ], false);
    });

    it('should report correct slot num', function() {
        expect(inventory.getNumUsedSlots()).toEqual(9);
        expect(inventory.getNumExistingSlots()).toEqual(15);
        expect(inventory.isEmpty()).toEqual(false);
    });

    it('should count items correctly', function() {
        expect(inventory.numItems('B')).toEqual(10 + 20 + 30 + 3000);
        expect(inventory.numItems('Ha')).toEqual(30 + 30);
        expect(inventory.numItems('b')).toEqual(0);
    });

    it('should have room for the correct number of items', function() {
        expect(inventory.howManyCanGain('B')).toEqual(
            999-10 + 999-20 + 999-30 + 6*999);
        expect(inventory.howManyCanGain('Ha')).toEqual(999-30 + 6*999);
    });

    it('should not allow stacking data items together', function() {
        expect(inventory.howManyCanGain('Ha', {name: 'namedwood'}))
            .toEqual(6*999);
    });

    it('should have correct room in specific slots', function() {
        expect(inventory.howManyCanGainAtIndex('Ha', 0)).toEqual(0);
        expect(inventory.howManyCanGainAtIndex('Ha', 3)).toEqual(999-30);
        expect(inventory.howManyCanGainAtIndex('Ha', 4)).toEqual(999);
        expect(inventory.howManyCanGainAtIndex('B', 14)).toEqual(0);
    });

    it('should not allow mixing data items in slot', function() {
        expect(inventory.howManyCanGainAtIndex('Ha', 3,
            {name: 'namedwood'})).toEqual(0);
        expect(inventory.howManyCanGainAtIndex('Ha', 8)).toEqual(0);
        expect(inventory.howManyCanGainAtIndex('Ha', 8,
            {name: 'namedwood'})).toEqual(0);
    });

});
