const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots() {
    return [
        /* 0*/ { code: 'B', amount: 10 },
        /* 1*/ { code: 'B', amount: 20 },
        /* 2*/ { code: 'B', amount: 30 },
        /* 3*/ { code: 'Ha', amount: 30 },
        /* 4*/ null,
        /* 5*/ { code: 'DD', amount: 2 },
        /* 6*/ null,
        /* 7*/ null,
        /* 8*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
        /* 9*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*10*/ null,
        /*11*/ { code: 'GB', amount: 1, data: {d: 58} },
        /*12*/ null,
        /*13*/ null,
        /*14*/ { code: 'B', amount: 3000 },
    ];
}

function getDefaultWoodSlots() {
    return [
        /* 0*/ { code: 'Ha', amount: 2 },
        /* 1*/ { code: 'Ha', amount: 3, data: {name: 'namedwood'} },
        /* 2*/ { code: 'Ha', amount: 4, data: {name: 'namedwood'} },
    ];
}

function getDefaultMixSlots() {
    return [
        /* 0*/ { code: 'Ha', amount: 2 },
        /* 1*/ { code: 'B', amount: 3 },
        /* 2*/ { code: 'B', amount: 3000 },
    ];
}

describe('Inventory', function() {
    let inventory;
    let woodinventory;
    let mixinventory;

    beforeEach(function() {
        inventory = new Inventory(getDefaultSlots(), false);
        woodinventory = new Inventory(getDefaultWoodSlots(), false);
        mixinventory = new Inventory(getDefaultMixSlots(), false);
    });

    it('should be able to move to empty slot', function() {
        inventory.moveItemsToSpecificSlot(inventory, 0, 4, 10);
        inventory.moveItemsToSpecificSlot(inventory, 1, 6, 5);
        inventory.moveItemsToSpecificSlot(inventory, 14, 7, 3000);
        inventory.moveItemsToSpecificSlot(inventory, 9, 14, 1);
        expect(inventory.slots).toEqual([
            /* 0*/ null,
            /* 1*/ { code: 'B', amount: 15 },
            /* 2*/ { code: 'B', amount: 30 },
            /* 3*/ { code: 'Ha', amount: 30 },
            /* 4*/ { code: 'B', amount: 10 },
            /* 5*/ { code: 'DD', amount: 2 },
            /* 6*/ { code: 'B', amount: 5 },
            /* 7*/ { code: 'B', amount: 3000 },
            /* 8*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 9*/ null,
            /*10*/ null,
            /*11*/ { code: 'GB', amount: 1, data: {d: 58} },
            /*12*/ null,
            /*13*/ null,
            /*14*/ { code: 'GB', amount: 1, data: {d: 52} },
        ]);
    });

    it('should be able to move to slot with same item', function() {
        inventory.moveItemsToSpecificSlot(inventory, 0, 2, 10);
        inventory.moveItemsToSpecificSlot(inventory, 1, 2, 5);
        expect(inventory.moveItemsToSpecificSlot(inventory, 14, 2, 1000)).toEqual(false);
        expect(inventory.moveItemsToSpecificSlot(inventory, 14, 2, 954)).toEqual(true);
        expect(inventory.slots).toEqual([
            /* 0*/ null,
            /* 1*/ { code: 'B', amount: 15 },
            /* 2*/ { code: 'B', amount: 999 },
            /* 3*/ { code: 'Ha', amount: 30 },
            /* 4*/ null,
            /* 5*/ { code: 'DD', amount: 2 },
            /* 6*/ null,
            /* 7*/ null,
            /* 8*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 9*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*10*/ null,
            /*11*/ { code: 'GB', amount: 1, data: {d: 58} },
            /*12*/ null,
            /*13*/ null,
            /*14*/ { code: 'B', amount: 2046 },
        ]);
    });

    it('should fail when moved item has data', function() {
        expect(woodinventory.moveItemsToSpecificSlot(woodinventory, 1, 0, 2)).toEqual(false);
        expect(woodinventory.slots).toEqual(getDefaultWoodSlots());
    });

    it('should fail when occupying item has data', function() {
        expect(woodinventory.moveItemsToSpecificSlot(woodinventory, 0, 1, 1)).toEqual(false);
        expect(woodinventory.slots).toEqual(getDefaultWoodSlots());
    });

    it('should fail when both items have data', function() {
        expect(woodinventory.moveItemsToSpecificSlot(woodinventory, 1, 2, 2)).toEqual(false);
        expect(woodinventory.slots).toEqual(getDefaultWoodSlots());
    });

    it('should fail when item types differ', function() {
        expect(mixinventory.moveItemsToSpecificSlot(mixinventory, 0, 1, 1)).toEqual(false);
        expect(woodinventory.slots).toEqual(getDefaultWoodSlots());
    });

    it('should have no effect when moving a slot to itself', function() {
        mixinventory.moveItemsToSpecificSlot(mixinventory, 0, 0, 2);
        mixinventory.moveItemsToSpecificSlot(mixinventory, 1, 1, 3);
        mixinventory.moveItemsToSpecificSlot(mixinventory, 2, 2, 3000);
        expect(woodinventory.slots).toEqual(getDefaultWoodSlots());
    });

});
