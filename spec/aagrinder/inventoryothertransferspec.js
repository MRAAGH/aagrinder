const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots1() {
    return [
        /* 0*/ { code: 'B', amount: 400 },
        /* 1*/ { code: 'Ha', amount: 30 },
        /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
        /* 3*/ { code: 'B', amount: 3000 },
    ];
}

function getDefaultSlots2() {
    return [
        /* 0*/ null,
        /* 1*/ { code: 'B', amount: 900 },
        /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
        /* 3*/ null,
    ];
}

describe('Inventory', function() {
    let inventory1;
    let inventory2;

    beforeEach(function() {
        inventory1 = new Inventory(getDefaultSlots1(), false);
        inventory2 = new Inventory(getDefaultSlots2(), false);
    });

    it('should move items to other inventory', function() {
        inventory1.moveItemsTo(inventory2, 0, 7);
        inventory1.moveItemsTo(inventory2, 1, 8);
        inventory1.moveItemsTo(inventory2, 2, 9);
        expect(inventory1.slots).toEqual([
            /* 0*/ { code: 'B', amount: 393 },
            /* 1*/ { code: 'Ha', amount: 22 },
            /* 2*/ { code: 'Ha', amount: 21, data: {name: 'namedwood'} },
            /* 3*/ { code: 'B', amount: 3000 },
        ]);
        expect(inventory2.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 8 },
            /* 1*/ { code: 'B', amount: 907 },
            /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 3*/ { code: 'Ha', amount: 9, data: {name: 'namedwood'} },
        ]);
    });

    it('should move only as many items as fit', function() {
        // only 599 should fit
        expect(inventory2.howManyItemsCanMoveTo(inventory1, 1)).toEqual(599);
        expect(inventory2.moveItemsTo(inventory1, 1, 600)).toEqual(false);
        expect(inventory2.moveItemsTo(inventory1, 1, 599)).toEqual(true);
        expect(inventory1.numItems('B')).toEqual(999+3000);
        expect(inventory2.numItems('B')).toEqual(301);
        // none should fit any more
        expect(inventory2.canMoveItemsTo(inventory1, 1, 1)).toEqual(false);
        expect(inventory2.moveItemsTo(inventory1, 1, 1)).toEqual(false);
        expect(inventory1.numItems('B')).toEqual(999+3000);
        expect(inventory2.numItems('B')).toEqual(301);
    });

    it('should move items to specific slot', function() {
        inventory1.moveItemsToSpecificSlot(inventory2, 0, 3, 7);
        inventory1.moveItemsToSpecificSlot(inventory2, 3, 1, 8);
        inventory1.moveItemsToSpecificSlot(inventory2, 3, 0, 9);
        expect(inventory1.slots).toEqual([
            /* 0*/ { code: 'B', amount: 393 },
            /* 1*/ { code: 'Ha', amount: 30 },
            /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 3*/ { code: 'B', amount: 2983 },
        ]);
        expect(inventory2.slots).toEqual([
            /* 0*/ { code: 'B', amount: 9 },
            /* 1*/ { code: 'B', amount: 908 },
            /* 2*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 3*/ { code: 'B', amount: 7 },
        ]);
    });

    it('should move to specific slot only as many as fit', function() {
        // only 99 should fit
        expect(inventory1.howManyItemsCanMoveToSpecificSlot(inventory2, 0, 1)).toEqual(99);
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 0, 1, 300)).toEqual(false);
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 0, 1, 99)).toEqual(true);
        expect(inventory1.numItems('B')).toEqual(301+3000);
        expect(inventory2.numItems('B')).toEqual(999);
        // none should fit any more
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 0, 1, 1)).toEqual(false);
        expect(inventory1.numItems('B')).toEqual(301+3000);
        expect(inventory2.numItems('B')).toEqual(999);
        // none should fit into the overfull slot
        expect(inventory2.moveItemsToSpecificSlot(inventory1, 1, 3, 5)).toEqual(false);
        expect(inventory1.numItems('B')).toEqual(301+3000);
        expect(inventory2.numItems('B')).toEqual(999);
    });

    it('should not swap slots in "moveItems" call, this was removed', function() {
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 1, 2, 5)).toEqual(false);
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 2, 2, 6)).toEqual(false);
        expect(inventory2.moveItemsToSpecificSlot(inventory1, 2, 1, 7)).toEqual(false);
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 1, 2, 30)).toEqual(false);
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 1, 1, 5)).toEqual(false);
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 1, 1, 30)).toEqual(false);
        expect(inventory2.moveItemsToSpecificSlot(inventory1, 1, 3, 900)).toEqual(false);
        expect(inventory2.moveItemsToSpecificSlot(inventory1, 1, 3, 900)).toEqual(false);
        expect(inventory2.moveItemsToSpecificSlot(inventory1, 2, 3, 30)).toEqual(false);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
        expect(inventory2.slots).toEqual(getDefaultSlots2());
    });

    it('should be able to move overfull slot to empty slot', function() {
        inventory1.moveItemsToSpecificSlot(inventory2, 3, 0, 3000);
        expect(inventory1.numItems('B')).toEqual(400);
        expect(inventory2.numItems('B')).toEqual(3000+900);
    });

    it('should be able to move overfull slot to empty slot partially', function() {
        inventory1.moveItemsToSpecificSlot(inventory2, 3, 0, 1500);
        expect(inventory1.numItems('B')).toEqual(1500+400);
        expect(inventory2.numItems('B')).toEqual(1500+900);
    });

    it('should be impossible to move an empty slot', function() {
        console.log('EXPECT 3 LOGS ABOUT EMPTY SLOT, WITH TRACES:');
        // to specific slot
        expect(inventory2.moveItemsToSpecificSlot(inventory1, 0, 1, 1)).toEqual(false);
        // to empty slot
        expect(inventory2.moveItemsToSpecificSlot(inventory2, 0, 3, 1)).toEqual(false);
        // to not specific slot
        expect(inventory2.moveItemsTo(inventory1, 0, 1)).toEqual(false);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
        expect(inventory2.slots).toEqual(getDefaultSlots2());
    });

    it('should be impossible to move more items than are in the source', function() {
        console.log('EXPECT 4 LOGS ABOUT 400 < 401, WITH TRACES:');
        // to specific slot
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 0, 1, 401)).toEqual(false);
        // to empty slot
        expect(inventory1.moveItemsToSpecificSlot(inventory2, 0, 3, 401)).toEqual(false);
        // to itself
        expect(inventory1.moveItemsToSpecificSlot(inventory1, 0, 0, 401)).toEqual(false);
        // to not specific slot
        expect(inventory1.moveItemsTo(inventory2, 0, 401)).toEqual(false);
        expect(inventory1.slots).toEqual(getDefaultSlots1());
        expect(inventory2.slots).toEqual(getDefaultSlots2());
    });

});
