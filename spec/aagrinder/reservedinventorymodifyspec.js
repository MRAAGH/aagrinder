const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ null,
        /*2*/ { code: 'Ha', amount: 30 },
        /*3*/ null,
        /*4*/ { code: 'DD', amount: 2 },
        /*5*/ null,
        /*6*/ { code: 'B', amount: 30 },
        /*7*/ { code: 'Ha', amount: 30 },
        /*8*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*9*/ { code: 'B', amount: 30 },
    ];
}

describe('Reserved inventory', function() {
    let inventory;

    beforeEach(function() {
        inventory = new Inventory(getDefaultSlots(), true);
    });

    it('should normally gain only in non-reserved slots', function() {
        inventory.gainItemByCode('B', 10);
        expect(inventory.numItems('B')).toEqual(
            10 + 30 + 30 + 10);
        expect(inventory.getSlotAt(0).amount).toEqual(20);
        inventory.gainItemByCode('B', 2000);
        expect(inventory.numItems('B')).toEqual(2080);
        expect(inventory.gainItemByCode('B', 99999)).toEqual(3*999+60-2080);
        expect(inventory.numItems('B')).toEqual(3*999+60);
        expect(inventory.gainItemByCode('B', 10)).toEqual(0);
        expect(inventory.numItems('B')).toEqual(3*999+60);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 999 },
            /*1*/ { code: 'B', amount: 999 },
            /*2*/ { code: 'Ha', amount: 30 },
            /*3*/ { code: 'B', amount: 999 },
            /*4*/ { code: 'DD', amount: 2 },
            /*5*/ null,
            /*6*/ { code: 'B', amount: 30 },
            /*7*/ { code: 'Ha', amount: 30 },
            /*8*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*9*/ { code: 'B', amount: 30 },
        ]);
    });

    it('should lose in all slots', function() {
        inventory.gainItemByCode('B', -9999);
        expect(inventory.numItems('B')).toEqual(0);
    });

    it('should gain items in specific reserved slots', function() {
        inventory.gainItemAtIndex('B', 6, 10);
        inventory.gainItemAtIndex('B', 9, 20);
        inventory.gainItemAtIndex('Ha', 7, -10);
        inventory.gainItemAtIndex('B', 5, 15);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ null,
            /*2*/ { code: 'Ha', amount: 30 },
            /*3*/ null,
            /*4*/ { code: 'DD', amount: 2 },
            /*5*/ { code: 'B', amount: 15 },
            /*6*/ { code: 'B', amount: 40 },
            /*7*/ { code: 'Ha', amount: 20 },
            /*8*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*9*/ { code: 'B', amount: 50 },
        ]);
    });

    it('should be impossible to mix different items in craft slot', function() {
        console.log('EXPECT LOG ABOUT GAINING DD in B:');
        expect(inventory.gainItemAtIndex('DD', 7, 10)).toEqual(0);
        expect(inventory.slots).toEqual(getDefaultSlots());
    });

});
