const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 500 },
    ];
}

describe('Reserved inventory', function() {
    let inventory;

    beforeEach(function() {
        inventory = new Inventory(getDefaultSlots(), true);
    });

    it('should move from other slot to crafting', function() {
        inventory.moveItemsToSpecificSlot(inventory, 0, 2, 10);
        expect(inventory.slots).toEqual([
            /*0*/ null,
            /*1*/ { code: 'B', amount: 990 },
            /*2*/ { code: 'B', amount: 10 },
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'B', amount: 500 },
        ]);
    });

    it('should move from crafting to main', function() {
        inventory.moveItemsToSpecificSlot(inventory, 1, 0, 100);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 110 },
            /*1*/ { code: 'B', amount: 890 },
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'B', amount: 500 },
        ]);
    });

    it('should delete when moving incompatible item to deletion slot', function() {
        inventory.moveItemsToSpecificSlot(inventory, 3, 6, 30);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ { code: 'B', amount: 990 },
            /*2*/ null,
            /*3*/ null,
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'Ha', amount: 30 },
        ]);
    });

    it('should delete when partially moving incompatible item to deletion slot', function() {
        inventory.moveItemsToSpecificSlot(inventory, 3, 6, 10);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ { code: 'B', amount: 990 },
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 20 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'Ha', amount: 10 },
        ]);
    });

    it('should delete when deletion slot goes over stack limit', function() {
        expect(inventory.moveItemsToSpecificSlot(inventory, 1, 6, 990)).toEqual(true);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ null,
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'B', amount: 999 },
        ]);
    });

    it('should delete when partialy moving to deletion slot', function() {
        expect(inventory.moveItemsToSpecificSlot(inventory, 1, 6, 800)).toEqual(true);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ { code: 'B', amount: 190 },
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'B', amount: 999 },
        ]);
    });

    it('should transfer only as many as possible out of deletion slot', function() {
        expect(inventory.howManyItemsCanMoveToSpecificSlot(inventory, 6, 1, 400)).toEqual(9);
        expect(inventory.moveItemsToSpecificSlot(inventory, 6, 1, 400)).toEqual(false);
        expect(inventory.moveItemsToSpecificSlot(inventory, 6, 1, 9)).toEqual(true);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ { code: 'B', amount: 999 },
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'B', amount: 491 },
        ]);
    });

    it('should fail when partialy incompatible moving from deletion slot', function() {
        expect(inventory.moveItemsToSpecificSlot(inventory, 6, 3, 400)).toEqual(false);
        expect(inventory.slots).toEqual(getDefaultSlots());
    });

    it('should move whole overfull slot to deletion', function() {
        inventory.moveItemsToSpecificSlot(inventory, 5, 6, 3000);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ { code: 'B', amount: 990 },
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*5*/ null,
            /*6*/ { code: 'B', amount: 3000 },
        ]);
    });

    it('should delete when moving data item to delete', function() {
        inventory.moveItemsToSpecificSlot(inventory, 4, 6, 1);
        expect(inventory.slots).toEqual([
            /*0*/ { code: 'B', amount: 10 },
            /*1*/ { code: 'B', amount: 990 },
            /*2*/ null,
            /*3*/ { code: 'Ha', amount: 30 },
            /*4*/ null,
            /*5*/ { code: 'B', amount: 3000 },
            /*6*/ { code: 'GB', amount: 1, data: {d: 52} },
        ]);
    });

});
