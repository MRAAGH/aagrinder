const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlots() {
    return [
        /* 0*/ { code: 'B', amount: 10 },
        /* 1*/ { code: 'B', amount: 20 },
        /* 2*/ { code: 'B', amount: 30 },
        /* 3*/ { code: 'Ha', amount: 30 },
        /* 4*/ null,
        /* 5*/ { code: 'DD', amount: 2 },
        /* 6*/ null,
        /* 7*/ null,
        /* 8*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
        /* 9*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*10*/ null,
        /*11*/ { code: 'GB', amount: 1, data: {d: 58} },
        /*12*/ null,
        /*13*/ null,
        /*14*/ { code: 'B', amount: 3000 },
    ];
}

describe('Inventory', function() {
    let inventory;

    beforeEach(function() {
        inventory = new Inventory(getDefaultSlots());
    });

    it('should gain the correct number of items', function() {
        inventory.gainItemByCode('B', 10);
        inventory.gainItemByCode('B', 1000);
        inventory.gainItemByCode('DD', 3);
        inventory.gainItemByCode('Ha', -10);
        inventory.gainItemByCode('Dg', 9);
        inventory.gainItemByCode('GB', -1);
        // should not stack data items together even if matching
        inventory.gainItemByCode('Ha', 10, {name: 'namedwood'});
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'B', amount: 999 },
            /* 1*/ { code: 'B', amount: 41 },
            /* 2*/ { code: 'B', amount: 30 },
            /* 3*/ { code: 'Ha', amount: 20 },
            /* 4*/ { code: 'Dg', amount: 9 },
            /* 5*/ { code: 'DD', amount: 5 },
            /* 6*/ { code: 'Ha', amount: 10, data: {name: 'namedwood'} },
            /* 7*/ null,
            /* 8*/ { code: 'Ha', amount: 30, data: {name: 'namedwood'} },
            /* 9*/ null,
            /*10*/ null,
            /*11*/ { code: 'GB', amount: 1, data: {d: 58} },
            /*12*/ null,
            /*13*/ null,
            /*14*/ { code: 'B', amount: 3000 },
        ]);
    });

    it('should gain only as many items as fit', function() {
        console.log('EXPECT A LOG ABOUT FAILED TO FIT REMAINING B:');
        expect(inventory.gainItemByCode('B', 9999)).toEqual(9*999-60);
        expect(inventory.numItems('B')).toEqual(9*999 + 3000);
    });

    it('should lose only as many items as have', function() {
        expect(inventory.gainItemByCode('B', -9999)).toEqual(-3060);
        expect(inventory.numItems('B')).toEqual(0);
    });

    it('should gain items in specific slots', function() {
        inventory.gainItemAtIndex('B', 1, 10);
        // gaining items should be unlimited (when positive):
        expect(inventory.gainItemAtIndex('B', 2, 1000)).toEqual(1000);
        inventory.gainItemAtIndex('B', 14, 50);
        inventory.gainItemAtIndex('B', 7, 10);
        inventory.gainItemAtIndex('GB', 11, -1);
        inventory.gainItemAtIndex('Ha', 8, -10);
        inventory.gainItemAtIndex('Ha', 12, 10, {name: 'namedwood'});
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'B', amount: 10 },
            /* 1*/ { code: 'B', amount: 30 },
            /* 2*/ { code: 'B', amount: 1030 },
            /* 3*/ { code: 'Ha', amount: 30 },
            /* 4*/ null,
            /* 5*/ { code: 'DD', amount: 2 },
            /* 6*/ null,
            /* 7*/ { code: 'B', amount: 10 },
            /* 8*/ { code: 'Ha', amount: 20, data: {name: 'namedwood'} },
            /* 9*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*10*/ null,
            /*11*/ null,
            /*12*/ { code: 'Ha', amount: 10, data: {name: 'namedwood'} },
            /*13*/ null,
            /*14*/ { code: 'B', amount: 3050 },
        ]);
    });

    it('should be impossible to mix different items', function() {
        console.log('EXPECT LOG ABOUT GAINING DD in B:');
        expect(inventory.gainItemAtIndex('DD', 2, 10)).toEqual(0);
        expect(inventory.slots).toEqual(getDefaultSlots());
    });

    it('should be impossible to mix data items', function() {
        console.log('EXPECT 3 LOGS ABOUT MIXING DATA, WITH TRACES:');
        expect(inventory.gainItemAtIndex('Ha', 3, 10, {name: 'namedwood'})).toEqual(0);
        expect(inventory.slots).toEqual(getDefaultSlots());
        expect(inventory.gainItemAtIndex('Ha', 8, 10, {name: 'namedwood'})).toEqual(0);
        expect(inventory.slots).toEqual(getDefaultSlots());
        expect(inventory.gainItemAtIndex('Ha', 8, 10)).toEqual(0);
        expect(inventory.slots).toEqual(getDefaultSlots());
    });

});
