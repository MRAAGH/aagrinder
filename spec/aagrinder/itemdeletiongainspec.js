const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlotsEmpty() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ null,
    ];
}

function getDefaultSlotsPartial() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 500 },
    ];
}

function getDefaultSlotsOverfull() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 2000 },
    ];
}

function getDefaultSlotsData() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 10, data: {name: "namedstone"} },
    ];
}

describe('Deletion slot', function() {
    let inventoryEmpty;
    let inventoryPartial;
    let inventoryOverfull;
    let inventoryData;
    let inventories;

    beforeEach(function() {
        inventoryEmpty = new Inventory(getDefaultSlotsEmpty(), true);
        inventoryPartial = new Inventory(getDefaultSlotsPartial(), true);
        inventoryOverfull = new Inventory(getDefaultSlotsOverfull(), true);
        inventoryData = new Inventory(getDefaultSlotsData(), true);
        inventories = [inventoryEmpty, inventoryPartial,
            inventoryOverfull, inventoryData];
    });

    it('should be able to gain overfull items when empty', function() {
        expect(inventoryEmpty.gainItemAtIndex('b', 6, 2000)).toEqual(2000);
        expect(inventoryEmpty.numItems('b')).toEqual(2000);
    });

    it('should be able to gain data items when empty', function() {
        expect(inventoryEmpty.gainItemAtIndex('B', 6, 15, {name: 'namedstone'})).toEqual(15);
        expect(inventoryEmpty.numItems('B')).toEqual(4015);
        expect(inventoryEmpty.getSlotAt(6)).toEqual({
            code: 'B', amount: 15, data: {name: 'namedstone'}});
    });

    it('should not be able to gain negative items when empty', function() {
        expect(inventoryEmpty.gainItemAtIndex('b', 6, -10)).toEqual(0);
        expect(inventoryEmpty.gainItemAtIndex('B', 6, -10)).toEqual(0);
        expect(inventoryEmpty.slots).toEqual(getDefaultSlotsEmpty());
    });

    it('should delete item when gaining different item', function() {
        expect(inventoryPartial.gainItemAtIndex('b', 6, 10)).toEqual(10);
        expect(inventoryPartial.numItems('b')).toEqual(10);
        expect(inventoryPartial.numItems('B')).toEqual(4000);
    });

    it('should delete different item and replace with overfull slot', function() {
        expect(inventoryPartial.gainItemAtIndex('b', 6, 2000)).toEqual(2000);
        expect(inventoryPartial.numItems('b')).toEqual(2000);
        expect(inventoryPartial.numItems('B')).toEqual(4000);
    });

    it('should delete overfull slot when gaining different item', function() {
        expect(inventoryOverfull.gainItemAtIndex('b', 6, 10)).toEqual(10);
        expect(inventoryOverfull.numItems('b')).toEqual(10);
        expect(inventoryOverfull.numItems('B')).toEqual(4000);
    });

    it('should delete item when gaining data item', function() {
        expect(inventoryPartial.gainItemAtIndex('B', 6, 10, {name: 'namedstone'})).toEqual(10);
        expect(inventoryPartial.numItems('B')).toEqual(4010);
        expect(inventoryPartial.getSlotAt(6)).toEqual({
            code: 'B', amount: 10, data: {name: 'namedstone'}});
    });

    it('should delete data item when gaining normal item', function() {
        expect(inventoryData.gainItemAtIndex('B', 6, 10)).toEqual(10);
        expect(inventoryData.numItems('B')).toEqual(4010);
        expect(inventoryData.getSlotAt(6)).toEqual({
            code: 'B', amount: 10});
    });

    it('should delete when mixing data items', function() {
        expect(inventoryData.gainItemAtIndex('B', 6, 10, {name: 'namedstone'})).toEqual(10);
        expect(inventoryData.numItems('B')).toEqual(4010);
        expect(inventoryData.getSlotAt(6)).toEqual({
            code: 'B', amount: 10, data: {name: 'namedstone'}});
    });

    it('should lose gained items when slot is matching and full', function() {
        // first, make the deletion slot full:
        inventoryPartial.gainItemAtIndex('B', 6, 499);
        expect(inventoryPartial.numItems('B')).toEqual(4999);
        // now gain on a full slot
        expect(inventoryPartial.gainItemAtIndex('B', 6, 123)).toEqual(123);
        expect(inventoryPartial.numItems('B')).toEqual(4999);
    });

    it('should gain overfull even when deleted items are matching', function() {
        expect(inventoryPartial.gainItemAtIndex('B', 6, 1000)).toEqual(1000);
        expect(inventoryPartial.numItems('B')).toEqual(5000);
    });

    it('should end up at stack size when adding to overfull', function() {
        expect(inventoryOverfull.gainItemAtIndex('B', 6, 10)).toEqual(10);
        expect(inventoryOverfull.numItems('B')).toEqual(4999);
    });

    it('should replace overfull slot with overfull gained items', function() {
        expect(inventoryOverfull.gainItemAtIndex('B', 6, 1500)).toEqual(1500);
        expect(inventoryOverfull.numItems('B')).toEqual(5500);
    });

    it('should be limited by stack limit', function() {
        expect(inventoryPartial.gainItemAtIndex('B', 6, 700)).toEqual(700);
        expect(inventoryPartial.numItems('B')).toEqual(4999);
    });

    it('should be able to add items normally', function() {
        expect(inventoryPartial.gainItemAtIndex('B', 6, 100)).toEqual(100);
        expect(inventoryPartial.numItems('B')).toEqual(4600);
    });

    it('should be able to gain negative (matching) items', function() {
        expect(inventoryPartial.gainItemAtIndex('B', 6, -100)).toEqual(-100);
        expect(inventoryPartial.numItems('B')).toEqual(4400);
    });

    it('should not go below zero', function() {
        expect(inventoryPartial.gainItemAtIndex('B', 6, -700)).toEqual(-500);
        expect(inventoryPartial.numItems('B')).toEqual(4000);
    });

    it('should not subtract item if mismatching', function() {
        expect(inventoryPartial.gainItemAtIndex('b', 6, -10)).toEqual(0);
        expect(inventoryPartial.slots).toEqual(getDefaultSlotsPartial());
    });

});
