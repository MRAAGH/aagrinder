const Inventory = require('../../shared/inventory').Inventory;

// Recipe 8 is to craft 10 Ha into 1 hh
// Recipe 16 is to craft 1 hg and 1 Ha into 1 hH
// Recipe 33 is to craft 1 M and 1 DD into 1 G0
// Recipe 34 is to craft 1 G0 and 20 #s into 1 G#
// Recipe 45 is to craft 1 R and 15 - into 15 =

describe('Crafting', function() {

    it('should use only craft slots', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ null,
            /* 2*/ { code: 'Ha', amount: 7 },
            /*c3*/ { code: 'Ha', amount: 7 },
            /*c4*/ { code: 'Ha', amount: 7 },
            /*c5*/ { code: 'Ha', amount: 7 },
            /*c6*/ { code: 'Ha', amount: 7 },
            /*c7*/ { code: 'Ha', amount: 7 },
            /* 8*/ { code: 'Ha', amount: 7 },
        ], true);
        expect(inventory.howManyCanCraft(8)).toEqual(3);
        expect(inventory.canCraft(8, 4)).toEqual(false);
        expect(inventory.canCraft(8, 3)).toEqual(true);
        expect(inventory.howManyRecipesCanExpend(8)).toEqual(3);
        inventory.craft(8, 1);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'hh', amount: 1 },
            /* 2*/ { code: 'Ha', amount: 7 },
            /*c3*/ null,
            /*c4*/ { code: 'Ha', amount: 4 },
            /*c5*/ { code: 'Ha', amount: 7 },
            /*c6*/ { code: 'Ha', amount: 7 },
            /*c7*/ { code: 'Ha', amount: 7 },
            /* 8*/ { code: 'Ha', amount: 7 },
        ]);
        expect(inventory.howManyCanCraft(8)).toEqual(2);
        expect(inventory.howManyRecipesCanExpend(8)).toEqual(2);
        inventory.craft(8, 2);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'hh', amount: 3 },
            /* 2*/ { code: 'Ha', amount: 7 },
            /*c3*/ null,
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /*c7*/ { code: 'Ha', amount: 5 },
            /* 8*/ { code: 'Ha', amount: 7 },
        ]);
        expect(inventory.howManyCanCraft(8)).toEqual(0);
        expect(inventory.howManyRecipesCanExpend(8)).toEqual(0);
    });

    it('should be limited by space in rest of inventory', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'hh', amount: 998 },
            /*c2*/ { code: 'Ha', amount: 7 },
            /*c3*/ { code: 'Ha', amount: 7 },
            /*c4*/ { code: 'Ha', amount: 7 },
            /*c5*/ null,
            /*c6*/ { code: 'Ha', amount: 7 },
            /* 7*/ null,
        ], true);
        expect(inventory.howManyCanCraft(8)).toEqual(1);
        expect(inventory.canCraft(8, 2)).toEqual(false);
        expect(inventory.canCraft(8, 1)).toEqual(true);
        expect(inventory.howManyRecipesCanExpend(8)).toEqual(2);
    });

    it('should report correct available recipes', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 1 },
            /* 1*/ { code: '-', amount: 50 },
            /* 2*/ null,
            /*c3*/ { code: 'Ha', amount: 1 },
            /*c4*/ { code: 'hg', amount: 10 },
            /*c5*/ { code: 'Ha', amount: 1 },
            /*c6*/ null,
            /*c7*/ null,
            /* 8*/ { code: '-', amount: 50 },
        ], true);
        // recipe for hh
        expect(inventory.couldCraft(8)).toEqual(true);
        // recipe for hH
        expect(inventory.couldCraft(16)).toEqual(true);
        // recipe for GH
        expect(inventory.couldCraft(37)).toEqual(true);
    });

    it('should be able to craft into specific slot and prevent overfilling', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'hh', amount: 995 },
            /*c2*/ { code: 'Ha', amount: 900 },
            /*c3*/ { code: 'hh', amount: 7 },
            /*c4*/ null,
            /*c5*/ { code: 'Ha', amount: 100 },
            /*c6*/ null,
            /* 7*/ null,
        ], true);
        expect(inventory.howManyCanCraftIntoSlot(8,0)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,1)).toEqual(4);
        expect(inventory.howManyCanCraftIntoSlot(8,2)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,3)).toEqual(100);
        expect(inventory.howManyCanCraftIntoSlot(8,4)).toEqual(100);
        expect(inventory.howManyCanCraftIntoSlot(8,5)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,7)).toEqual(100);
        expect(inventory.canCraftIntoSlot(8,1,4)).toEqual(true);
        expect(inventory.canCraftIntoSlot(8,1,5)).toEqual(false);
    });

    it('should be able to craft into empty slot without overfiling, but unlimited in deletion', function() {
        const inventory = new Inventory([
            /* 0*/ null,
            /* 1*/ { code: 'hH', amount: 900 },
            /*c2*/ { code: 'Ha', amount: 900 },
            /*c3*/ { code: 'hg', amount: 900 },
            /*c4*/ { code: 'Ha', amount: 900 },
            /*c5*/ { code: 'hg', amount: 900 },
            /*c6*/ null,
            /* 7*/ null,
        ], true);
        expect(inventory.howManyCanCraftIntoSlot(16,0)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(16,1)).toEqual(99);
        expect(inventory.howManyCanCraftIntoSlot(16,2)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(16,3)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(16,4)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(16,5)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(16,7)).toEqual(1800);
        expect(inventory.canCraftIntoSlot(16,7,1800)).toEqual(true);
        expect(inventory.canCraftIntoSlot(16,7,1801)).toEqual(false);
        expect(inventory.howManyCanCraft(16)).toEqual(1098);
    });

    it('should craft into specific slot', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'hh', amount: 995 },
            /*c2*/ { code: 'Ha', amount: 900 },
            /*c3*/ { code: 'hh', amount: 7 },
            /*c4*/ null,
            /*c5*/ { code: 'Ha', amount: 100 },
            /*c6*/ null,
            /* 7*/ null,
        ], true);
        inventory.craftIntoSlot(8,1,3);
        inventory.craftIntoSlot(8,4,2);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'hh', amount: 998 },
            /*c2*/ { code: 'Ha', amount: 850 },
            /*c3*/ { code: 'hh', amount: 7 },
            /*c4*/ { code: 'hh', amount: 2 },
            /*c5*/ { code: 'Ha', amount: 100 },
            /*c6*/ null,
            /* 7*/ null,
        ]);
    });

    it('should craft data item', function() {
        const inventory = new Inventory([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ { code: 'M', amount: 5 },
            /*c3*/ { code: 'DD', amount: 5 },
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ { code: '#s', amount: 50 },
            /* 7*/ null,
        ], true);
        inventory.craftIntoSlot(33,4,1);
        inventory.craftIntoSlot(33,5,1);
        inventory.craft(33,1);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'G0', amount: 1, data: {d: 1500} },
            /* 1*/ null,
            /*c2*/ { code: 'M', amount: 2 },
            /*c3*/ { code: 'DD', amount: 2 },
            /*c4*/ { code: 'G0', amount: 1, data: {d: 1500} },
            /*c5*/ { code: 'G0', amount: 1, data: {d: 1500} },
            /*c6*/ { code: '#s', amount: 50 },
            /* 7*/ null,
        ]);
        inventory.getSlotAt(0).data.d = 1900;
        inventory.getSlotAt(4).data.d = 1904;
        inventory.getSlotAt(5).data.d = 1905;
        inventory.craft(34,1);
        inventory.craftIntoSlot(34,7,1);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'G0', amount: 1, data: {d: 1900} },
            /* 1*/ { code: 'G#', amount: 1, data: {d: 8000} },
            /*c2*/ { code: 'M', amount: 2 },
            /*c3*/ { code: 'DD', amount: 2 },
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ { code: '#s', amount: 10 },
            /* 7*/ { code: 'G#', amount: 1, data: {d: 8000} },
        ]);
    });

    it('should lose unnecessary item data when crafting', function() {
        const inventory = new Inventory([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ { code: 'Ha', amount: 5 },
            /*c3*/ { code: 'Ha', amount: 90, data: {name: 'namedwood'} },
            /*c4*/ { code: 'Ha', amount: 5 },
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ null,
        ], true);
        inventory.craftIntoSlot(8,6,1);
        inventory.craft(8,2);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'hh', amount: 2},
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ { code: 'Ha', amount: 65, data: {name: 'namedwood'} },
            /*c4*/ { code: 'Ha', amount: 5 },
            /*c5*/ null,
            /*c6*/ { code: 'hh', amount: 1},
            /* 7*/ null,
        ]);
    });

    it('should craft correct amount when overfull slots are present', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'hh', amount: 1500 },
            /* 1*/ { code: 'hh', amount: 950 },
            /*c2*/ { code: 'Ha', amount: 500 },
            /*c3*/ { code: 'Ha', amount: 500 },
            /*c4*/ { code: 'Ha', amount: 500 },
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ { code: 'hh', amount: 1500 },
        ], true);
        expect(inventory.howManyCanCraft(8)).toEqual(49);
        expect(inventory.howManyCanCraftIntoSlot(8,0)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,1)).toEqual(49);
        expect(inventory.howManyCanCraftIntoSlot(8,2)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,3)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,4)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(8,5)).toEqual(150);
        expect(inventory.howManyCanCraftIntoSlot(8,6)).toEqual(150);
        expect(inventory.howManyCanCraftIntoSlot(8,7)).toEqual(150);
        inventory.craftIntoSlot(8,7,150);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'hh', amount: 1500 },
            /* 1*/ { code: 'hh', amount: 950 },
            /*c2*/ null,
            /*c3*/ null,
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ { code: 'hh', amount: 999 },
        ]);
    });

    it('should craft correct amount when ingredient is overfull', function() {
        const inventory = new Inventory([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ { code: 'Ha', amount: 50 },
            /*c4*/ { code: 'Ha', amount: 30000 },
            /*c5*/ { code: 'hh', amount: 10 },
            /*c6*/ null,
            /* 7*/ null,
        ], true);
        expect(inventory.howManyCanCraft(8)).toEqual(1998);
        expect(inventory.howManyCanCraftIntoSlot(8,0)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(8,1)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(8,2)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(8,3)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(8,4)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(8,5)).toEqual(989);
        expect(inventory.howManyCanCraftIntoSlot(8,6)).toEqual(999);
        expect(inventory.howManyCanCraftIntoSlot(8,7)).toEqual(3005);
        inventory.craft(8,1000);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'hh', amount: 999 },
            /* 1*/ { code: 'hh', amount: 1 },
            /*c2*/ null,
            /*c3*/ null,
            /*c4*/ { code: 'Ha', amount: 20050 },
            /*c5*/ { code: 'hh', amount: 10 },
            /*c6*/ null,
            /* 7*/ null,
        ]);
    });

    it('should overwrite item in deletion slot by crafting there', function() {
        const inventory = new Inventory([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ { code: 'Ha', amount: 50 },
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ { code: 'Ha', amount: 10 },
        ], true);
        expect(inventory.howManyCanCraftIntoSlot(8,7)).toEqual(5);
        inventory.craftIntoSlot(8,7,5);
        expect(inventory.slots).toEqual([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ null,
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ { code: 'hh', amount: 5 },
        ]);
    });

    it('should overwrite data item in deletion slot by crafting there', function() {
        const inventory = new Inventory([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ { code: 'Ha', amount: 50 },
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ { code: 'hh', amount: 10, data: {name: 'namedchest'} },
        ], true);
        expect(inventory.howManyCanCraftIntoSlot(8,7)).toEqual(5);
        inventory.craftIntoSlot(8,7,5);
        expect(inventory.slots).toEqual([
            /* 0*/ null,
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ null,
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ { code: 'hh', amount: 5 },
        ]);
    });

    it('woodgrindr should be limited by available slots', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ null,
            /* 2*/ null,
            /*c5*/ { code: 'Ha', amount: 820 },
            /*c6*/ { code: '-', amount: 820 },
            /*c3*/ { code: 'Ha', amount: 7 },
            /*c4*/ { code: 'Ha', amount: 7 },
            /*c7*/ { code: 'Ha', amount: 7 },
            /* 8*/ null,
        ], true);
        expect(inventory.howManyCanCraft(37)).toEqual(2);
        expect(inventory.canCraftIntoSlot(37, 0, 1)).toEqual(true);
        expect(inventory.canCraftIntoSlot(37, 1, 1)).toEqual(true);
        expect(inventory.howManyRecipesCanExpend(37)).toEqual(41);
        inventory.craft(37, 1);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'GH', amount: 1, data: {d:250} },
            /* 2*/ null,
            /*c5*/ { code: 'Ha', amount: 800 },
            /*c6*/ { code: '-', amount: 800 },
            /*c3*/ { code: 'Ha', amount: 7 },
            /*c4*/ { code: 'Ha', amount: 7 },
            /*c7*/ { code: 'Ha', amount: 7 },
            /* 8*/ null,
        ]);
        expect(inventory.howManyCanCraft(37)).toEqual(1);
        expect(inventory.canCraftIntoSlot(37, 0, 1)).toEqual(true);
        expect(inventory.canCraftIntoSlot(37, 1, 1)).toEqual(false);
        expect(inventory.canCraftIntoSlot(37, 2, 1)).toEqual(true);
        expect(inventory.howManyRecipesCanExpend(37)).toEqual(40);
        inventory.craft(37, 1);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'Ha', amount: 7 },
            /* 1*/ { code: 'GH', amount: 1, data: {d:250} },
            /* 2*/ { code: 'GH', amount: 1, data: {d:250} },
            /*c5*/ { code: 'Ha', amount: 780 },
            /*c6*/ { code: '-', amount: 780 },
            /*c3*/ { code: 'Ha', amount: 7 },
            /*c4*/ { code: 'Ha', amount: 7 },
            /*c7*/ { code: 'Ha', amount: 7 },
            /* 8*/ null,
        ]);
        expect(inventory.howManyCanCraft(37)).toEqual(0);
        expect(inventory.canCraftIntoSlot(37, 0, 1)).toEqual(false);
        expect(inventory.canCraftIntoSlot(37, 1, 1)).toEqual(false);
        expect(inventory.howManyRecipesCanExpend(37)).toEqual(39);
    });

    it('should automatically move occupied slot out of the way if possible', function() {
        const inventory = new Inventory([
            /* 0*/ { code: 'Ha', amount: 50 },
            /* 1*/ null,
            /*c2*/ null,
            /*c3*/ { code: 'Ha', amount: 50 },
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ null,
        ], true);
        expect(inventory.howManyCanCraftIntoSlot(8,0)).toEqual(5);
        expect(inventory.howManyCanCraftIntoSlot(8,1)).toEqual(5);
        inventory.craftIntoSlot(8,0,1);
        expect(inventory.slots).toEqual([
            /* 0*/ { code: 'hh', amount: 1 },
            /* 1*/ { code: 'Ha', amount: 50 },
            /*c2*/ null,
            /*c3*/ { code: 'Ha', amount: 40 },
            /*c4*/ null,
            /*c5*/ null,
            /*c6*/ null,
            /* 7*/ null,
        ]);
        expect(inventory.howManyCanCraftIntoSlot(8,0)).toEqual(4);
        expect(inventory.howManyCanCraftIntoSlot(8,1)).toEqual(0);
    });

    it('should not move occupied slot out of the way if item type matches', function() {
        const inventory = new Inventory([
            /* 0*/ { code: '=', amount: 999 },
            /* 1*/ { code: '=', amount: 995 },
            /* 2*/ null,
            /*c3*/ null,
            /*c4*/ { code: 'R', amount: 50 },
            /*c5*/ { code: '-', amount: 30 },
            /*c6*/ null,
            /*c7*/ null,
            /* 8*/ null,
        ], true);
        expect(inventory.howManyCanCraftIntoSlot(45,0)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(45,1)).toEqual(0);
        expect(inventory.howManyCanCraftIntoSlot(45,2)).toEqual(2);
    });

});
