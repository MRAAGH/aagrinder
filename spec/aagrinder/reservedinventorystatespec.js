const Inventory = require('../../shared/inventory').Inventory;

describe('Inventory', function() {
    let inventory;

    beforeEach(function() {
        inventory = new Inventory([
            /* 0*/ { code: 'B', amount: 10 },
            /* 1*/ null,
            /* 2*/ { code: 'Ha', amount: 30 },
            /* 3*/ null,
            /*c4*/ { code: 'DD', amount: 2 },
            /*c5*/ null,
            /*c6*/ { code: 'B', amount: 30 },
            /*c7*/ { code: 'Ha', amount: 30 },
            /*c8*/ { code: 'GB', amount: 1, data: {d: 52} },
            /*d9*/ { code: 'B', amount: 30 },
        ], true);
    });

    it('should report correct slot num', function() {
        expect(inventory.getNumUsedSlots()).toEqual(7);
        expect(inventory.getNumExistingSlots()).toEqual(10);
        expect(inventory.getNumNormalSlots()).toEqual(4);
        expect(inventory.isEmpty()).toEqual(false);
    });

    it('should count items correctly', function() {
        expect(inventory.numItems('B')).toEqual(10 + 30 + 30);
        expect(inventory.numItems('Ha')).toEqual(30 + 30);
        expect(inventory.numItems('b')).toEqual(0);
    });

    it('should have room for the correct number of items', function() {
        expect(inventory.howManyCanGain('B')).toEqual(
            999-10 + 2*999);
        expect(inventory.howManyCanGain('b')).toEqual(2*999);
    });

    it('should have correct room in specific slots', function() {
        expect(inventory.howManyCanGainAtIndex('Ha', 0)).toEqual(0);
        expect(inventory.howManyCanGainAtIndex('Ha', 2)).toEqual(999-30);
        expect(inventory.howManyCanGainAtIndex('Ha', 5)).toEqual(999);
        expect(inventory.howManyCanGainAtIndex('B', 6)).toEqual(999-30);
        // in deletion slot, space for maximum of any item
        expect(inventory.howManyCanGainAtIndex('B', 9)).toEqual(999999999);
        expect(inventory.howManyCanGainAtIndex('Ha', 9)).toEqual(999999999);
    });

    it('should be able to spend reserved items', function() {
        expect(inventory.canGain('Ha', -60)).toEqual(true);
        expect(inventory.canGain('Ha', -61)).toEqual(false);
        expect(inventory.canGain('B', -70)).toEqual(true);
        expect(inventory.canGain('B', -71)).toEqual(false);
        expect(inventory.canGain('b', -1)).toEqual(false);
    });

    it('should have correct room in crafting slots', function() {
        expect(inventory.howManyCanGainInCraftingSlots('B')).toEqual(
            999 + 999-30);
    });

});
