const Inventory = require('../../shared/inventory').Inventory;

function getDefaultSlotsEmpty() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ null,
    ];
}

function getDefaultSlotsPartial() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 500 },
    ];
}

function getDefaultSlotsOverfull() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 2000 },
    ];
}

function getDefaultSlotsData() {
    return [
        /*0*/ { code: 'B', amount: 10 },
        /*1*/ { code: 'B', amount: 990 },
        /*2*/ null,
        /*3*/ { code: 'Ha', amount: 30 },
        /*4*/ { code: 'GB', amount: 1, data: {d: 52} },
        /*5*/ { code: 'B', amount: 3000 },
        /*6*/ { code: 'B', amount: 10, data: {name: "namedstone"} },
    ];
}

describe('Inventory', function() {
    let inventoryEmpty;
    let inventoryPartial;
    let inventoryOverfull;
    let inventoryData;
    let inventories;

    beforeEach(function() {
        inventoryEmpty = new Inventory(getDefaultSlotsEmpty(), true);
        inventoryPartial = new Inventory(getDefaultSlotsPartial(), true);
        inventoryOverfull = new Inventory(getDefaultSlotsOverfull(), true);
        inventoryData = new Inventory(getDefaultSlotsData(), true);
        inventories = [inventoryEmpty, inventoryPartial,
            inventoryOverfull, inventoryData];
    });

    it('should always return true for positive canGain', function() {
        for (const inventory of inventories) {
            expect(inventory.canGainAtIndex('B', 6, 10)).toEqual(true);
            expect(inventory.canGainAtIndex('B', 6, 2000)).toEqual(true);
            expect(inventory.canGainAtIndex('Ha', 6, 10)).toEqual(true);
            expect(inventory.canGainAtIndex('B', 6, 10, {name: "namedstone"})).toEqual(true);
        }
    });

    it('can still return false for negative canGain', function() {
        expect(inventoryEmpty.canGainAtIndex('B', 6, -10)).toEqual(false);
        expect(inventoryPartial.canGainAtIndex('B', 6, -10)).toEqual(true);
        expect(inventoryPartial.canGainAtIndex('B', 6, -600)).toEqual(false);
        expect(inventoryOverfull.canGainAtIndex('B', 6, -10)).toEqual(true);
        expect(inventoryOverfull.canGainAtIndex('B', 6, -1500)).toEqual(true);
        expect(inventoryOverfull.canGainAtIndex('B', 6, -3500)).toEqual(false);
    });

});
