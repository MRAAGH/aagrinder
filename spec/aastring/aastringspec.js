const AASTRING = require('../../shared/aastring');

describe('aastring', function() {

    it('should treat incomplete control code as text', function() {
        expect(AASTRING.AASTRING_DECODE('a\\[\\a'))
            .toEqual([{raw:'a\\[\\a', color:['','','','',''], modifiers:[0,0,0,0,0]}]);
        expect(AASTRING.AASTRING_DECODE('a\\[]a'))
            .toEqual([{raw:'a\\[]a', color:['','','','',''], modifiers:[0,0,0,0,0]}]);
        expect(AASTRING.AASTRING_DECODE('a\\[#\\a'))
            .toEqual([{raw:'a\\[#\\a', color:['','','','','',''], modifiers:[0,0,0,0,0,0]}]);
        expect(AASTRING.AASTRING_DECODE('a\\[#]a'))
            .toEqual([{raw:'a\\[#]a', color:['','','','','',''], modifiers:[0,0,0,0,0,0]}]);
    });

    it('should ignore invalid control codes', function() {
        expect(AASTRING.AASTRING_DECODE('a\\[HELLO\\]a'))
            .toEqual([{raw:'aa', color:['',''], modifiers:[0,0]}]);
    });

    it('should pass unicode characters through', function() {
        expect(AASTRING.AASTRING_DECODE('č'))
            .toEqual([{raw:'č', color:[''], modifiers:[0]}]);
        expect(AASTRING.AASTRING_DECODE('🦄'))
            .toEqual([{raw:'🦄', color:['',''], modifiers:[0,0]}]);
        expect(AASTRING.AASTRING_DECODE('🇨🇭'))
            .toEqual([{raw:'🇨🇭', color:['','','',''], modifiers:[0,0,0,0]}]);
    });

    it('should parse all control codes', function() {
        expect(AASTRING.AASTRING_DECODE('\\[#123\\]a\\[/\\]a'))
            .toEqual([{raw:'aa', color:['#123',''], modifiers:[0,0]}]);
        expect(AASTRING.AASTRING_DECODE('\\[m1\\]a\\[m4\\]a\\[/\\]a'))
            .toEqual([{raw:'aaa', color:['','',''], modifiers:[1,4,0]}]);
        expect(AASTRING.AASTRING_DECODE('\\[m1\\]a\\[M4\\]a\\[/\\]a'))
            .toEqual([{raw:'aaa', color:['','',''], modifiers:[1,5,0]}]);
        expect(AASTRING.AASTRING_DECODE('a\\[n\\]\\[n\\]a'))
            .toEqual([
                {raw:'a', color:[''], modifiers:[0]},
                {raw:'', color:[], modifiers:[]},
                {raw:'a', color:[''], modifiers:[0]},
            ]);
        expect(AASTRING.AASTRING_DECODE('a\\[s\\]a'))
            .toEqual([{raw:'a a', color:['','',''], modifiers:[0,0,0]}]);
        expect(AASTRING.AASTRING_DECODE('a a'))
            .toEqual([{raw:'a a', color:['','',''], modifiers:[0,0,0]}]);
    });

    it('should keep color across newlines', function() {
        expect(AASTRING.AASTRING_DECODE('\\[#123\\]a\\[n\\]a'))
            .toEqual([
                {raw:'a', color:['#123'], modifiers:[0]},
                {raw:'a', color:['#123'], modifiers:[0]},
            ]);
    });

    it('should respect default color', function() {
        expect(AASTRING.AASTRING_DECODE('a\\[#123\\]a\\[/\\]a', '#fff'))
            .toEqual([{raw:'aaa', color:['#fff','#123','#fff'], modifiers:[0,0,0]}]);
    });

    it('should not wrap short strings', function() {
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('', 2))
            .toEqual([{raw:'', color:[], modifiers:[]}]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('a', 2))
            .toEqual([{raw:'a', color:[''], modifiers:[0]}]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('aa', 2))
            .toEqual([{raw:'aa', color:['',''], modifiers:[0,0]}]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('\\[s\\]', 2))
            .toEqual([{raw:' ', color:[''], modifiers:[0]}]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('\\[#123\\]a\\[/\\]', 2))
            .toEqual([{raw:'a', color:['#123'], modifiers:[0]}]);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('', 2)).toEqual(['']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('a', 2)).toEqual(['a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aa', 2)).toEqual(['aa']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('\\[s\\]', 2)).toEqual(['\\[s\\]']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('\\[#123\\]a\\[/\\]', 2)).toEqual(['\\[#123\\]a\\[/\\]']);
    });

    it('should decode with wrapping', function() {
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('abc', 2))
            .toEqual([
                {raw:'ab', color:['',''], modifiers:[0,0]},
                {raw:'c', color:[''], modifiers:[0]},
            ]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('aaaaaa', 2))
            .toEqual([
                {raw:'aa', color:['',''], modifiers:[0,0]},
                {raw:'aa', color:['',''], modifiers:[0,0]},
                {raw:'aa', color:['',''], modifiers:[0,0]},
            ]);
    });

    it('should respect newline while wrapping', function() {
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('aaa\\[n\\]a', 2))
            .toEqual([
                {raw:'aa', color:['',''], modifiers:[0,0]},
                {raw:'a', color:[''], modifiers:[0]},
                {raw:'a', color:[''], modifiers:[0]},
            ]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('aa\\[n\\]aa', 2))
            .toEqual([
                {raw:'aa', color:['',''], modifiers:[0,0]},
                {raw:'aa', color:['',''], modifiers:[0,0]},
            ]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('aaaa\\[n\\]', 2))
            .toEqual([
                {raw:'aa', color:['',''], modifiers:[0,0]},
                {raw:'aa', color:['',''], modifiers:[0,0]},
                {raw:'', color:[], modifiers:[]},
            ]);
    });

    it('should respect color and modifier while wrapping', function() {
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('a\\[#123\\]aa', 2))
            .toEqual([
                {raw:'aa', color:['','#123'], modifiers:[0,0]},
                {raw:'a', color:['#123'], modifiers:[0]},
            ]);
        expect(AASTRING.AASTRING_DECODE_WITH_WRAPPING('a\\[m1\\]aa', 2))
            .toEqual([
                {raw:'aa', color:['',''], modifiers:[0,1]},
                {raw:'a', color:[''], modifiers:[1]},
            ]);
    });

    it('should wrap without decoding', function() {
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('abc', 2)).toEqual(['ab','c']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aa\\[n\\]', 2)).toEqual(['aa','']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aa\\[n\\]a', 2)).toEqual(['aa','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('a\\[n\\]a', 2)).toEqual(['a','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('a\\[n\\]a', 2)).toEqual(['a','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('a\\[#123\\]a\\[/\\]', 2)).toEqual(['a\\[#123\\]a\\[/\\]']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aa\\[#123\\]', 2)).toEqual(['aa\\[#123\\]']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aa\\[#123\\]a\\[/\\]', 2)).toEqual(['aa\\[#123\\]','a\\[/\\]']);
    });

    it('should not wrap incomplete control sequence', function() {
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaa', 12)).toEqual(['aaaaaaaaaaaa']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaa', 12)).toEqual(['aaaaaaaaaaaa','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaa\\', 12)).toEqual(['aaaaaaaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaa\\[', 12)).toEqual(['aaaaaaaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaa\\[s', 12)).toEqual(['aaaaaaaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaaaaa\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaa\\', 12)).toEqual(['aaaaaaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaa\\[', 12)).toEqual(['aaaaaaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaa\\[s', 12)).toEqual(['aaaaaaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaaaa\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaa\\', 12)).toEqual(['aaaaaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaa\\[', 12)).toEqual(['aaaaaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaa\\[s', 12)).toEqual(['aaaaaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaaa\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaa\\', 12)).toEqual(['aaaaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaa\\[', 12)).toEqual(['aaaaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaa\\[s', 12)).toEqual(['aaaaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaa\\[s\\']);
    });

    it('should not wrap incomplete control sequence even if there are previous control sequences', function() {
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaa', 12)).toEqual(['aaa\\[s\\]aaaaaaaa']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaaa', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaa\\', 12)).toEqual(['aaa\\[s\\]aaaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaa\\[', 12)).toEqual(['aaa\\[s\\]aaaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaa\\[s', 12)).toEqual(['aaa\\[s\\]aaaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaa\\[s\\', 12)).toEqual(['aaa\\[s\\]aaaaaaa\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaa\\', 12)).toEqual(['aaa\\[s\\]aaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaa\\[', 12)).toEqual(['aaa\\[s\\]aaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaa\\[s', 12)).toEqual(['aaa\\[s\\]aaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaa\\[s\\', 12)).toEqual(['aaa\\[s\\]aaaaaa\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaa\\', 12)).toEqual(['aaa\\[s\\]aaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaa\\[', 12)).toEqual(['aaa\\[s\\]aaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaa\\[s', 12)).toEqual(['aaa\\[s\\]aaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaa\\[s\\', 12)).toEqual(['aaa\\[s\\]aaaaa\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaa\\', 12)).toEqual(['aaa\\[s\\]aaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaa\\[', 12)).toEqual(['aaa\\[s\\]aaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaa\\[s', 12)).toEqual(['aaa\\[s\\]aaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaa\\[s\\', 12)).toEqual(['aaa\\[s\\]aaaa\\[s\\']);
    });

    it('should wrap incomplete control sequence if starts after wrap point', function() {
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaa', 12)).toEqual(['aaaaaaaaaaaa']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaa', 12)).toEqual(['aaaaaaaaaaaa','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaa\\', 12)).toEqual(['aaaaaaaaaaaa','\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaa\\[', 12)).toEqual(['aaaaaaaaaaaa','\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaa\\[s', 12)).toEqual(['aaaaaaaaaaaa','\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaaaaaa','\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaa\\', 12)).toEqual(['aaaaaaaaaaaa','a\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaa\\[', 12)).toEqual(['aaaaaaaaaaaa','a\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaa\\[s', 12)).toEqual(['aaaaaaaaaaaa','a\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaaaaaa','a\\[s\\']);
    });

    it('should wrap incomplete control sequence if starts after wrap point even if there are previous control sequences', function() {
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaa', 12)).toEqual(['aaa\\[s\\]aaaaaaaa']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaaa', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','a']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaa\\', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaa\\[', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaa\\[s', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaa\\[s\\', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','\\[s\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaaa\\', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','a\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaaa\\[', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','a\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaaa\\[s', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','a\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaa\\[s\\]aaaaaaaaa\\[s\\', 12)).toEqual(['aaa\\[s\\]aaaaaaaa','a\\[s\\']);
    });

    it('should not wrap incomplete control sequence even if at end of second line', function() {
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaaaaaaaaaaaa\\', 12)).toEqual(['aaaaaaaaaaaa','aaaaaaaaaaa\\']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaaaaaaaaaaaa\\[', 12)).toEqual(['aaaaaaaaaaaa','aaaaaaaaaaa\\[']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaaaaaaaaaaaa\\[s', 12)).toEqual(['aaaaaaaaaaaa','aaaaaaaaaaa\\[s']);
        expect(AASTRING.AASTRING_WRAP_WITHOUT_DECODING('aaaaaaaaaaaaaaaaaaaaaaa\\[s\\', 12)).toEqual(['aaaaaaaaaaaa','aaaaaaaaaaa\\[s\\']);
    });

    it('should re-wrap last line', function() {
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aa'],2)).toEqual(['aa']);
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aaa'],2)).toEqual(['aa', 'a']);
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aa', 'aaa'],2)).toEqual(['aa', 'aa', 'a']);
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aa', 'aa\\[n\\]aa'],2)).toEqual(['aa', 'aa', 'aa']);
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aa', 'a\\[n\\]aaa'],2)).toEqual(['aa', 'a', 'aa', 'a']);
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aa', 'aaa\\[n\\]a'],2)).toEqual(['aa', 'aa', 'a', 'a']);
        expect(AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(['aa', 'aa\\[n\\]'],2)).toEqual(['aa', 'aa', '']);
    });

    it('should remove all control sequences in raw', function() {
        expect(AASTRING.AASTRING_RAW('a\\[#123\\]')).toEqual('a');
        expect(AASTRING.AASTRING_RAW('a\\[#123\\]b\\[n\\]c\\[d\\[/\\]\\\\')).toEqual('abc\\[d\\\\');
    });

    it('should recursively remove all control characters in sanitize', function() {
        expect(AASTRING.AASTRING_SANITIZE('a\\[#123\\]')).toEqual('a#123');
        expect(AASTRING.AASTRING_SANITIZE('a\\\\[[#123\\\\]]')).toEqual('a#123');
        expect(AASTRING.AASTRING_SANITIZE('a\\\\\\[[[#123\\\\\\]]]')).toEqual('a#123');
        expect(AASTRING.AASTRING_SANITIZE('a\\[#123\\]b\\[n\\]c\\[d\\[/\\]\\\\')).toEqual('a#123bncd/\\\\');
    });

    it('should make a deep copy of lines', function() {
        const lines = ['a\\[#123\\]b','c'];
        const copiedlines = AASTRING.AASTRING_MAKE_COPY(lines);
        copiedlines[1] += 'd';
        expect(lines).toEqual(['a\\[#123\\]b','c']);
        expect(copiedlines).toEqual(['a\\[#123\\]b','cd']);
    });

});
