// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * How to add a new item:
 *
 * 1. add it to item_definitions
 * 2. optionally add a recipe for it
 * 3. add sprites for it in public/sprites.js
 * 4. optionally add special rules for it in
 *     - stack_sizes
 *     - item_data_templates
 *     - item2block
 *     - item2blockPlain
 *     - block2item
 *     - isGrassAttachment
 *     - isWireAttachment
 *     - shared/chunk.js
 *     - shared/blockprops.js
 *     - shared/placelogic.js
 *     - shared/diggrinderlogic.js
 *     - shared/interactlogic.js
 */

if (typeof exports !== 'undefined') {
    SPRITES = require('./sprites').SPRITES;
}

"use strict";

const NORMAL_STACK_LIMIT = 999;

class Inventory{

    // BEGIN CONSTANT STATIC FIELDS

    static PLAYER_INVENTORY_SLOT_COUNT = 35; // constant

    static item_definitions = [ // constant
        {code:'B',   short:'stone',       name:'stone'},
        {code:'d',   short:'dirt',        name:'dirt'},
        {code:'S',   short:'sand',        name:'sand'},
        {code:'b',   short:'boulder',     name:'boulder'},
        {code:'Aa',  short:'leaves',      name:'leaves'},
        {code:'Ai',  short:'pine lvs',    name:'pine leaves'},
        {code:'Al',  short:'almond lvs',  name:'almond leaves'},
        {code:'Ap',  short:'aprico lvs',  name:'apricot leaves'},
        {code:'Av',  short:'avocad lvs',  name:'avocado leaves'},
        {code:'Am',  short:'palm lvs',    name:'palm leaves'},
        {code:'Ab',  short:'bush lvs',    name:'bush leaves'},
        {code:'Ar',  short:'cranby lvs',  name:'cranberry leaves'},
        {code:'Au',  short:'blueby lvs',  name:'blueberry leaves'},
        {code:'aa',  short:'leaves ND',   name:'leaves (non-decaying)'},
        {code:'ai',  short:'pine lvs ND', name:'pine leaves (non-decaying)'},
        {code:'al',  short:'almn lvs ND', name:'almond leaves (non-decaying)'},
        {code:'ap',  short:'apri lvs ND', name:'apricot leaves (non-decaying)'},
        {code:'av',  short:'avoc lvs ND', name:'avocado leaves (non-decaying)'},
        {code:'am',  short:'palm lvs ND', name:'palm leaves (non-decaying)'},
        {code:'ab',  short:'bush lvs ND', name:'bush leaves (non-decaying)'},
        {code:'ar',  short:'crnb lvs ND', name:'cranberry leaves (non-decaying)'},
        {code:'au',  short:'blub lvs ND', name:'blueberry leaves (non-decaying)'},
        {code:'Waa', short:'leaves D',    name:'leaves (decaying)'},
        {code:'Wai', short:'pine lvs D',  name:'pine leaves (decaying)'},
        {code:'Wal', short:'almn lvs D',  name:'almond leaves (decaying)'},
        {code:'Wap', short:'apri lvs D',  name:'apricot leaves (decaying)'},
        {code:'Wav', short:'avoc lvs D',  name:'avocado leaves (decaying)'},
        {code:'Wam', short:'palm lvs D',  name:'palm leaves (decaying)'},
        {code:'Wab', short:'bush lvs D',  name:'bush leaves (decaying)'},
        {code:'War', short:'crnb lvs D',  name:'cranberry leaves (decaying)'},
        {code:'Wau', short:'blub lvs D',  name:'blueberry leaves (decaying)'},
        {code:'ta',  short:'tiny tree',   name:'tiny tree'},
        {code:'ti',  short:'tiny pine',   name:'tiny pine'},
        {code:'tl',  short:'tiny alm t',  name:'tiny almond tree'},
        {code:'tp',  short:'tiny apr t',  name:'tiny apricot tree'},
        {code:'tv',  short:'tiny avo t',  name:'tiny avocado tree'},
        {code:'tm',  short:'tiny palm',   name:'tiny palm tree'},
        {code:'tb',  short:'tiny bush',   name:'tiny bush'},
        {code:'tr',  short:'tiny crn b',  name:'tiny cranberry bush'},
        {code:'tu',  short:'tiny blu b',  name:'tiny blueberry bush'},
        {code:'Wt',  short:'dead tree',   name:'dead tree'},
        {code:'fp',  short:'apricot',     name:'apricot'},
        {code:'fl',  short:'almond',      name:'almond'},
        {code:'fv',  short:'avocado',     name:'avocado'},
        {code:'fa',  short:'apple',       name:'apple'},
        {code:'fm',  short:'ananas',      name:'ananas'},
        {code:'fr',  short:'cranberry',   name:'cranberry'},
        {code:'fu',  short:'blueberry',   name:'blueberry'},
        {code:'fc',  short:'candy',       name:'candy'},
        {code:'fR',  short:'ruby apple',  name:'ruby apple'},
        {code:'Wfp', short:'apricot P',   name:'placed apricot'},
        {code:'Wfl', short:'almond P',    name:'placed almond'},
        {code:'Wfv', short:'avocado P',   name:'placed avocado'},
        {code:'Wfm', short:'ananas P',    name:'placed ananas'},
        {code:'Wfa', short:'apple P',     name:'placed apple'},
        {code:'Wfr', short:'cranb P',     name:'placed cranberry'},
        {code:'Wfu', short:'blueb P',     name:'placed blueberry'},
        {code:'Ha',  short:'wood',        name:'wood'},
        {code:'Hi',  short:'pine wood',   name:'pine wood'},
        {code:'Hl',  short:'almnd wood',  name:'almond wood'},
        {code:'Hp',  short:'apric wood',  name:'apricot wood'},
        {code:'Hv',  short:'avoca wood',  name:'avocado wood'},
        {code:'Hm',  short:'palm wood',   name:'palm wood'},
        {code:'Hb',  short:'bush wood',   name:'bush wood'},
        {code:'Hr',  short:'cranb wood',  name:'cranberry wood'},
        {code:'Hu',  short:'blueb wood',  name:'blueberry wood'},
        {code:'Hd',  short:'ladder',      name:'ladder'},
        {code:'hh',  short:'chest',       name:'chest'},
        {code:'hg',  short:'gold chest',  name:'gold chest'},
        {code:'hH',  short:'camo chest',  name:'camo chest'},
        {code:'hB',  short:'camo chest',  name:'camo chest'},
        {code:'hA',  short:'camo chest',  name:'camo chest'},
        {code:'hD',  short:'camo chest',  name:'camo chest'},
        {code:'p',   short:'hopper',      name:'hopper'},
        {code:'DD',  short:'diamond',     name:'diamond'},
        {code:'R',   short:'ruby',        name:'ruby'},
        {code:'R0',  short:'pink B',      name:'pink B'},
        {code:'Rd',  short:'ruby D',      name:'disabled ruby'},
        {code:'=',   short:'wire',        name:'wire'},
        {code:'%',   short:'crossing',    name:'crossing'},
        {code:':',   short:'gate',        name:'gate'},
        {code:'-',   short:'vine',        name:'vine'},
        {code:'sw',  short:'switch',      name:'switch'},
        {code:'M',   short:'motor',       name:'motor'},
        {code:'O0',  short:'button',      name:'button'},
        {code:'R>',  short:'arrow',       name:'arrow'},
        {code:'G0',  short:'grinder',     name:'grinder'},
        {code:'G#',  short:'GRINDER',     name:'GRINDER'},
        {code:'Gy',  short:'yaygrinder',  name:'yay grinder'},
        {code:'GB',  short:'stongrindr',  name:'stone grinder'},
        {code:'GH',  short:'woodgrindr',  name:'wooden grinder'},
        {code:'GW',  short:'WARPGRINDR',  name:'WARPGRINDER'},
        {code:'GA',  short:'AAGRINDER',   name:'AAGRINDER'},
        {code:'WP',  short:'pickaxe',     name:'pickaxe'},
        {code:'#s',  short:'spooky',      name:'spooky'},
        {code:'#y',  short:'yay',         name:'yay'},
        {code:'E',   short:'detector',    name:'detector'},
        {code:'Eo',  short:'seatector',   name:'sea detector'},
        {code:'Ws',  short:'SSPOOKY',     name:'SSPOOKY'},
        {code:'Wo',  short:'FOLLOW',      name:'FOLLOW'},
        {code:'X0',  short:'browncoral',  name:'brown coral'},
        {code:'X1',  short:'bluecoral',   name:'blue coral'},
        {code:'X2',  short:'redcoral',    name:'red coral'},
        {code:'X3',  short:'greencoral',  name:'green coral'},
        {code:'X4',  short:'neoncoral',   name:'neon coral'},
        {code:'X5',  short:'orangcoral',  name:'orange coral'},
        {code:'X6',  short:'yellocoral',  name:'yellow coral'},
        {code:'X7',  short:'purplcoral',  name:'purple coral'},
        {code:'X8',  short:'pinkcoral',   name:'pink coral'},
        {code:'Xx',  short:'deadcoral',   name:'dead coral'},
        {code:'w',   short:'water',       name:'water'},
        {code:'WW',  short:'warp',        name:'warp'},
        {code:'WV',  short:'customwarp',  name:'custom warp'},
        {code:'WC',  short:'warp core',   name:'warp core'},
        {code:'WF',  short:'four forg',   name:'four forg'},
        {code:'WB',  short:'corrupston',  name:'corrupted stone'},
        {code:' ',   short:'air',         name:'air'},
        {code:'..',  short:'pebble',      name:'pebble'},
        {code:'?',   short:'?',           name:'?'},
        {code:'||',  short:'stick',       name:'stick'},
        {code:'|/',  short:'rstick',      name:'rstick'},
        {code:'|\\', short:'lstick',      name:'lstick'},
        {code:'Fx',  short:'grey block',  name:'grey block'},
        {code:'F0',  short:'brown blk',   name:'brown block'},
        {code:'F1',  short:'blue block',  name:'blue block'},
        {code:'F2',  short:'ored block',  name:'orange red block'},
        {code:'F3',  short:'green blk',   name:'green block'},
        {code:'F4',  short:'neon block',  name:'neon block'},
        {code:'F5',  short:'orange blk',  name:'orange block'},
        {code:'F6',  short:'yellow blk',  name:'yellow block'},
        {code:'F7',  short:'purple blk',  name:'purple block'},
        {code:'F8',  short:'pink block',  name:'pink block'},
        {code:'F9',  short:'cyan block',  name:'cyan block'},
        {code:'Fa',  short:'magent blk',  name:'magenta block'},
        {code:'Fb',  short:'white blk',   name:'white block'},
        {code:'Fc',  short:'gold block',  name:'gold block'},
        {code:'Fd',  short:'red block',   name:'red block'},
        {code:'Fe',  short:'dred block',  name:'dark red block'},
        {code:'si',  short:'sign',        name:'sign'},
        {code:'*0',  short:'yello flwr',  name:'yellow flower'},
        {code:'*1',  short:'blue flowr',  name:'blue flower'},
        {code:'*2',  short:'cyan flowr',  name:'cyan flower'},
        {code:'*3',  short:'magen flwr',  name:'magenta flower'},
        {code:'*4',  short:'pink flowr',  name:'pink flower'},
        {code:'*5',  short:'white flwr',  name:'white flower'},
        {code:'*6',  short:'goldn flwr',  name:'golden flower'},
        {code:'*|',  short:'stem',        name:'flower stem'},
        {code:'WI',  short:'stalagmite',  name:'stalagmite'},
        {code:'WT',  short:'stalactite',  name:'stalactite'},
        {code:'Wl',  short:'icicle',      name:'icicle'},
        {code:'WA',  short:'tentacle',    name:'tentacle'},
        {code:'WO',  short:'grabber',     name:'grabber'},
        {code:'WD',  short:'grabber D',   name:'disabled grabber'},
        {code:'WR',  short:'carrotatle',  name:'carrotatle'},
        {code:'~',   short:'cloud',       name:'cloud'},
        {code:'W~',  short:'cotoncandy',  name:'cotton candy'},
        {code:'>',   short:'flowing w',   name:'flowing water'},
        {code:'sn',  short:'snow layer',  name:'snow layer'},
        {code:'sN',  short:'snow block',  name:'snow block'},
        {code:'Dg',  short:'chalk',       name:'chalk'},
        {code:'Dm',  short:'magnetite',   name:'magnetite'},
        {code:'DM',  short:'magnetite D', name:'disabled magnetite'},
        {code:'Wi',  short:'ice',         name:'ice'},
        {code:'Wc',  short:'ice block',   name:'ice block'},
        {code:'sp',  short:'inspector',   name:'inspector (deprecated)'},
        {code:'WS',  short:'quicksand',   name:'quicksand'},
        {code:'sB',  short:'stone slab',  name:'stone slab'},
        {code:'st',  short:'trapdoor',    name:'trapdoor'},
        {code:'sT',  short:'trapdoor O',  name:'trapdoor (open)'},
        {code:'ue',  short:'bucket',      name:'bucket'},
        {code:'uw',  short:'waterbuket',  name:'water bucket'},
        {code:'u~',  short:'cloudbuket',  name:'cloud bucket'},
        {code:'us',  short:'snowbucket',  name:'snow bucket'},
        {code:'uB',  short:'qsandbuket',  name:'quicksand bucket'},
        {code:'uc',  short:'cotoncandy',  name:'cotton candy bucket'},
        {code:'uu',  short:'undefbuket',  name:'bucket of undefined'},
        {code:'Wp',  short:'aaprinter',   name:'aaprinter'},
        {code:'sr',  short:'aascammer',   name:'aascammer'},
        {code:'sc',  short:'aascanner',   name:'aascanner'},
        {code:'sm',  short:'aaterminal',  name:'aaterminal'},
        {code:'sf',  short:'aafile',      name:'aafile'},
        {code:'WX',  short:'framework',   name:'framework'},
        {code:'NaN', short:'NaN (ERR)',   name:'NaN (ERROR!)'},
        {code:'null', short:'NUL (ERR)', name:'null (ERROR!)'},
        {code:'undefined', short:'UDF (ERR)', name:'undefined (ERROR!)'},
        {code:'unknownnn', short:'UNK (ERR)', name:'UNKNOWN BLOCK (ERROR!)'},
    ];

    static item_data_templates = { // constant
        'G0': {d:1500},
        'G#': {d:8000},
        'Gy': {d:2500},
        'GB': {d:1000},
        'GH': {d:250},
        'GW': {d:20000},
        'GA': {d:44444444},
        'sf': {text:''},
    };

    // object with codes for keys and names for values, for easy access
    // constant
    static item_names = Inventory.item_definitions.reduce((acc, cur, i)=>{
        acc[cur.code] = cur.name;
        return acc;
    },{});
    static item_short_names = Inventory.item_definitions.reduce((acc, cur, i)=>{
        acc[cur.code] = cur.short;
        return acc;
    },{});

    // an array of existing item codes
    // (is used for checking for valid items)
    static item_codes = Inventory.item_definitions.map(a=>a.code); // constant

    // object with codes for keys and stack sizes for values
    // constant
    static stack_sizes = Inventory.item_definitions.reduce((acc, cur, i)=>{
        const key = cur.code;
        if (key[0] === 'G') {
            acc[key] = 1;
        }
        else if (key === 'WP') {
            acc[key] = 1;
        }
        else if (key === '#s') {
            acc[key] = 20;
        }
        else if (key === '#y') {
            acc[key] = 20;
        }
        else if (key === 'E' || key === 'Ws') {
            acc[key] = 10;
        }
        else if (key === 'WA' || key === 'WO' || key === 'WB' || key === 'WR') {
            acc[key] = 666;
        }
        else if (key === 'WV') {
            acc[key] = 1;
        }
        else if (key === 'sf') {
            acc[key] = 1;
        }
        else {
            acc[key] = NORMAL_STACK_LIMIT;
        }
        return acc;
    },{});

    // object with codes for keys and indices for values (used for sorting)
    // constant
    static index_by_code = Inventory.item_definitions.reduce((acc, cur, i)=>{
        acc[cur.code] = i;
        return acc;
    },{});

    static recipes = [ // constant
        {need:[{ item: 'Aa', amount: 15 }], get:{item: 'ta', amount:1}},
        {need:[{ item: 'Ai', amount: 15 }], get:{item: 'ti', amount:1}},
        {need:[{ item: 'Al', amount: 10 }], get:{item: 'tl', amount:1}},
        {need:[{ item: 'Ap', amount: 20 }], get:{item: 'tp', amount:1}},
        {need:[{ item: 'Av', amount: 20 }], get:{item: 'tv', amount:1}},
        {need:[{ item: 'Ab', amount: 5  }], get:{item: 'tb', amount:1}},
        {need:[{ item: 'Ar', amount: 5  }], get:{item: 'tr', amount:1}},
        {need:[{ item: 'Au', amount: 5  }], get:{item: 'tu', amount:1}},
        {need:[{ item: 'Ha', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hi', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hp', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hv', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hl', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hb', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hr', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'Hu', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Ha', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hi', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hp', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hv', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hl', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hb', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hr', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hu', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'B', amount: 1 }], get:{item: 'hB', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Aa', amount: 1 }], get:{item: 'hA', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'DD', amount: 1 }], get:{item: 'hD', amount:1}},
        {need:[{ item: 'B',  amount: 1, reveal: true  }, { item: '=', amount: 1 }], get:{item: 'O0', amount:1}},
        {need:[{ item: 'O0', amount: 1  }, { item: '=', amount: 2, reveal: true }], get:{item: 'sw', amount:1}},
        {need:[{ item: '?',  amount: 5  }, { item: '=', amount: 5, reveal: true }, { item: 'hh', amount: 1, reveal: true }], get:{item: 'Wp', amount:1}},
        {need:[{ item: '?',  amount: 5  }, { item: '=', amount: 5 }], get:{item: 'sr', amount:0}},
        {need:[{ item: '?',  amount: 5  }, { item: '=', amount: 5, reveal: true }], get:{item: 'sc', amount:1}},
        {need:[{ item: 'Dm', amount: 5  }], get:{item: 'p', amount:1}},
        {need:[{ item: '=',  amount: 20 }, { item: 'Dm', amount: 10, reveal: true }], get:{item: 'M', amount:1}},
        {need:[{ item: 'M',  amount: 1  }, { item: 'DD', amount: 1, reveal: true }], get:{item: 'G0', amount:1}},
        {need:[{ item: 'G0', amount: 1  }, { item: '#s', amount: 20, reveal: true }, { item: 'WB', amount: 20, reveal: true }, { item: 'Ws', amount: 1, reveal: true }], get:{item: 'G#', amount:1}},
        {need:[{ item: 'G0', amount: 1  }, { item: '#y', amount: 20 }, { item: 'Ws', amount: 1, reveal: true }], get:{item: 'Gy', amount:1}},
        {need:[{ item: 'M',  amount: 1  }, { item: 'B', amount: 20, reveal: true }], get:{item: 'GB', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Ha', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hi', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hp', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hv', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hl', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hb', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hr', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hu', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: 'R',  amount: 1  }, { item: '-', amount: 15, reveal: true }], get:{item: '=', amount:15}},
        {need:[{ item: '=',  amount: 2  }], get:{item: '%', amount:1}},
        {need:[{ item: '=',  amount: 5  }], get:{item: ':', amount:1}},
        {need:[{ item: '..', amount: 10 }], get:{item: '?', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'Xx', amount: 2 }], get:{item: 'Fx', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X0', amount: 2 }], get:{item: 'F0', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X1', amount: 2 }], get:{item: 'F1', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X2', amount: 2 }], get:{item: 'F2', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X3', amount: 2 }], get:{item: 'F3', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X4', amount: 2 }], get:{item: 'F4', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X5', amount: 2 }], get:{item: 'F5', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X6', amount: 2 }], get:{item: 'F6', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X7', amount: 2 }], get:{item: 'F7', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'X8', amount: 2 }], get:{item: 'F8', amount:1}},
        {need:[{ item: '..', amount: 10 }], get:{item: 'Fx', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: '*0', amount: 2 }], get:{item: 'F6', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: '*1', amount: 2 }], get:{item: 'F1', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: '*2', amount: 2 }], get:{item: 'F9', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: '*3', amount: 2 }], get:{item: 'Fa', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: '*4', amount: 2 }], get:{item: 'F8', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: '*5', amount: 2 }], get:{item: 'Fb', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fa', amount: 2 }], get:{item: 'Fd', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fp', amount: 2 }], get:{item: 'F5', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fl', amount: 2 }], get:{item: 'Fc', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fv', amount: 2 }], get:{item: 'F4', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fr', amount: 2 }], get:{item: 'Fe', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fu', amount: 2 }], get:{item: 'F1', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'WA', amount: 2 }], get:{item: 'F7', amount:1}},
        {need:[{ item: 'B',  amount: 1, reveal: true}, { item: 'Dg', amount: 5 }], get:{item: 'si', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'uc', amount: 2 }], get:{item: 'F8', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fc', amount: 2 }], get:{item: 'F8', amount:1}},
        {need:[{ item: 'Wi', amount: 6  }], get:{item: 'Wc', amount:1}},
        {need:[{ item: 'Wl', amount: 10 }], get:{item: 'Wc', amount:1}},
        {need:[{ item: 'B',  amount: 2  }], get:{item: 'sB', amount:1}},
        {need:[{ item: 'Ha', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hi', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hp', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hv', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hl', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hb', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hr', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hu', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'B',  amount: 1  }, {item: '#s',  amount: 4}], get:{item: 'WB', amount:1}},
        {need:[{ item: 'Ws', amount: 7 }, {item: '#s',  amount: 20, reveal: true}], get:{item: 'Wo', amount:1}},
        {need:[{ item: 'Ha', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hi', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hp', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hv', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hl', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hb', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hr', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hu', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'sN', amount: 1  }], get:{item: 'sn', amount:1}},
        {need:[{ item: 'Ha', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hi', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hp', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hv', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hl', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hb', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hr', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: 'Hu', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: '||', amount: 1  }], get:{item: '|/', amount:1}},
        {need:[{ item: '||', amount: 1  }], get:{item: '|\\', amount:1}},
        {need:[{ item: '|/', amount: 1  }], get:{item: '||', amount:1}},
        {need:[{ item: '|/', amount: 1  }], get:{item: '|\\', amount:1}},
        {need:[{ item: '|\\', amount: 1  }], get:{item: '||', amount:1}},
        {need:[{ item: '|\\', amount: 1  }], get:{item: '|/', amount:1}},
        {need:[{ item: 'X3',  amount: 1  }, { item: 'X4',  amount: 1  }, { item: 'X5',  amount: 1  }, { item: 'X6', amount: 1 }], get:{item: 'X7', amount:1}},
        {need:[{ item: 'X8',  amount: 1  }, { item: 'Dm',  amount: 1  }], get:{item: 'R', amount:1}},
        {need:[{ item: 'WC',  amount: 7  }, { item: 'DD',  amount: 2, reveal: true  }, { item: 'R',  amount: 2, reveal: true  }], get:{item: 'WV', amount:1}},
        {need:[{ item: 'WV',  amount: 1  }], get:{item: 'WC', amount:7}},
        {need:[{ item: '||',  amount: 2 }, { item: 'B', amount: 3}], get:{item: 'WP', amount:1}},
        {need:[{ item: 'u~',  amount: 1, reveal: true }, { item: 'uc', amount: 1 }], get:{item: 'uc', amount:2}},
        {need:[{ item: 'u~',  amount: 1 }, { item: 'fc', amount: 1, reveal: true }], get:{item: 'uc', amount:1}},
        {need:[{ item: 'Dm',  amount: 8 }], get:{item: 'WX', amount:1}},
        {need:[{ item: 'G#', amount: 1 , reveal: true}, { item: 'WC', amount: 7}], get:{item: 'GW', amount:1}},
        {need:[{ item: '||', amount: 7  }], get:{item: 'Hd', amount:1}},
        {need:[{ item: 'GB', amount: 1, reveal:true}, { item: 'G0', amount: 1, reveal:true}, { item: 'G#', amount: 1, reveal:true}, { item: 'Gy', amount: 1, reveal:true}, { item: 'GW', amount: 1}], get:{item: 'GA', amount:1}},
        {need:[{ item: 'Am', amount: 5  }], get:{item: 'tm', amount:1}},
        {need:[{ item: 'Hm', amount: 10 }], get:{item: 'hh', amount:1}},
        {need:[{ item: 'hg', amount: 1  }, { item: 'Hm', amount: 1 }], get:{item: 'hH', amount:1}},
        {need:[{ item: '-',  amount: 20, reveal: true }, { item: 'Hm', amount: 20}], get:{item: 'GH', amount:1}},
        {need:[{ item: 'Hm', amount: 4  }, {item: '=',  amount: 1}], get:{item: 'st', amount:1}},
        {need:[{ item: 'Hm', amount: 5  }], get:{item: 'ue', amount:1}},
        {need:[{ item: 'Hm', amount: 2  }], get:{item: '||', amount:4}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'fm', amount: 2 }], get:{item: 'F6', amount:1}},
        {need:[{ item: 'WX', amount: 1 }], get:{item: 'WX', amount:1}},
        {need:[{ item: 'sc', amount: 1 }, { item: 'si', amount: 1, reveal: true }], get:{item: 'sf', amount:1}},
        {need:[{ item: 'sf', amount: 1 }, { item: 'Wp', amount: 1, reveal: true }], get:{item: 'sm', amount:1}},
        {need:[{ item: 'R', amount: 1 }, { item: 'fa', amount: 1 }], get:{item: 'fR', amount:1}},
        {need:[{ item: '..', amount: 4 }], get:{item: 'B', amount:1}},
        {need:[{ item: '..', amount: 10, reveal: true }, { item: 'Dg', amount: 2 }], get:{item: 'Fb', amount:1}},
    ];

    static grassBlob = '0022002244664466002200224466446611331133557755771133113355775577002200224466446600220022446644661133113355775577113311335577557788aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff88aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff0022002244664466002200224466446611331133557755771133113355775577002200224466446600220022446644661133113355775577113311335577557788aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff88aa88aacceeccee88aa88aacceeccee99bb99bbddffddff99bb99bbddffddff'; // constant

    // END CONSTANT STATIC FIELDS

    static isDataItem(item_code) {
        return this.item_data_templates[item_code] !== undefined;
    }

    static makeItemData(item_code) {
        if (!Inventory.isDataItem(item_code)) {
            // Not a data item. Data will be undefined
            return undefined;
        }
        const data = {};
        Object.assign(data, this.item_data_templates[item_code]);
        return data;
    }

    static isGrassAttachment(block) {
        return block[0] === '-';
    };

    static isWireAttachment(block) {
        return block[0] === '='
            || block[0] === '%'
            || block[0] === ':'
            || block[0] === 'G'
            || block[0] === 'M'
            || block === 'R1'
            || block === 'st'
            || block === 'sw'
            || block === 's1'
            || block === 'O0'
            || block === 'O1'
            || block === 'sT'
            || block === 'WO'
            || block === 'WD'
            || block === 'Wp'
            || block === 'sc'
            || block === 'sC'
            || block === 'sm'
            || block === 'sf'
        ;
    };

    // Convert item to block (for the purpose of placing into the world)
    static item2block(item_code, relativeCheck) {
        if (typeof item_code !== "string") {
            console.error("item2block for non-string "+item_code);
        }
        if (item_code === 'p') {
            // Hopper Direction check
            // The priortization of the "right" direction is intentional

            const block_here_right = relativeCheck(1, 0)
            const block_here_left = relativeCheck(-1, 0)
            const block_here_top = relativeCheck(0, 1)
            const block_here_bottom = relativeCheck(0, -1)

            // Always take the hint from neighboring hoppers first
            if (block_here_right === 'p>' || block_here_left === 'p>') return 'p>';
            if (block_here_right === 'p<' || block_here_left === 'p<') return 'p<';
            if (block_here_top === 'pv' || block_here_bottom === 'pv') return 'pv';

            // If there's a chest we should point towards it
            if (block_here_right[0] === 'h') return 'p>';
            if (block_here_left[0] === 'h') return 'p<';
            if (block_here_top[0] === 'h' || block_here_bottom[0] === 'h') return 'pv';

            // Right is the default
            return 'p>';

        }
        if (item_code === '-') {
            const prev_block = relativeCheck(0,0);
            let type_code = 0;
            let power_of_2 = 1;
            for (let dy = -1; dy < 2; dy++) {
                for (let dx = -1; dx < 2; dx++) {
                    // the "-" is there because the logic is back from
                    // when the world coordinates were mirrored
                    const block_here = relativeCheck(dx, -dy);
                    if (prev_block === 'w') {
                        // underwater we only connect up and down
                        if (dx === 0) {
                            if (this.isGrassAttachment(block_here)
                                // more attachemt options underwater
                                || (dy === 1 && (
                                    block_here === 'B'
                                    || block_here === 'b'
                                    || block_here === 'd'
                                    || block_here === 'S'
                                ))) {
                                type_code += power_of_2;
                            }
                        }
                    }
                    else {
                        if (this.isGrassAttachment(block_here))
                            type_code += power_of_2;
                    }
                    power_of_2 *= 2;
                }
            }
            const grass_block = '-' + this.grassBlob[type_code];
            return grass_block;
        }
        if (item_code === '=') {
            let type_code = 0;
            let power_of_2 = 1;
            for (let dy = -1; dy < 2; dy++) {
                for (let dx = -1; dx < 2; dx++) {
                    // the "-" is there because the logic is back from
                    // when the world coordinates were mirrored
                    const block_here = relativeCheck(dx, -dy);
                    if (this.isWireAttachment(block_here))
                        type_code += power_of_2;
                    power_of_2 *= 2;
                }
            }
            const wire_block = '=' + this.grassBlob[type_code] + '0R';
            return wire_block;
        }
        if (item_code === '%') return '%0';
        if (item_code === ':') return ':0:';
        if (item_code[0] === 't') return item_code + '0';
        if (item_code === 'R>') return 'R>0';
        if (item_code === 'R') return 'R1';
        if (item_code === 'M') return 'M0';
        // leaves become non-decaying leaves
        if (item_code[0] === 'A') return 'a'+item_code[1];
        // candy does not change
        if (item_code === 'fc') return item_code;
        // ruby apple does not change
        if (item_code === 'fR') return item_code;
        // fruit becomes placed fruit
        if (item_code[0] === 'f') return 'W'+item_code;
        // camo chest becomes camo block
        if (item_code === 'hH') return 'Ha';
        if (item_code === 'hB') return 'B';
        if (item_code === 'hA') return 'aa';
        if (item_code === 'hD') return 'DD';
        if (item_code === '?') return '??';
        if (item_code === '>') return '>0';
        if (item_code === 'E') return 'E0';
        // in other cases, block code is same as item code
        return item_code;
    };

    // Convert item to block (for the purpose of visualizing an item)
    static item2blockPlain(item_code) {
        if (typeof item_code !== "string") {
            console.error("item2blockPlain for non-string "+item_code);
        }
        if (item_code === 'p') return 'p>'
        if (item_code === '-') return '-0';
        // of note: =00 is not a valid block code, it is a sprite
        if (item_code === '=') return '=00';
        if (item_code === '%') return '%0';
        // of note: :0 is not a valid block code, it is a sprite
        if (item_code === ':') return ':';
        if (item_code === 't') return 't0';
        if (item_code === 'R>') return 'R>0';
        if (item_code === 'R') return 'R1';
        if (item_code === 'M') return 'M0';
        if (item_code === '?') return '??';
        if (item_code === '>') return '>0';
        if (item_code === 'E') return 'E0';
        if (item_code[0] === 't') return item_code.substring(0,2);
        // in other cases, block code is same as item code
        return item_code;
    };

    // Convert a block code to an item code.
    // This function may only be used with valid block codes.
    static block2item(block) {
        if (typeof block !== "string") {
            console.error("block2item for non-string "+block);
        }
        // undeveloped ruby turns to stone
        if (block === 'R0') return 'B';
        // grinder keeps type in item
        if (block[0] === 'G') return block;
        // coral keeps color in item
        if (block[0] === 'X') return block;
        // chest remains chest
        if (block[0] === 'h') return block;
        // any kind of leaves become regular
        // while keeping type ofc
        if (block[0] === 'a') return 'A'+block[1];
        if (block.substring(0,2) === 'Wa') return 'A'+block[2];
        if (block[0] === 'A') return block;
        // blueberry wood becomes bush wood (for easier management)
        if (block === 'Hu') return 'Hb';
        // cranberry wood becomes bush wood (for easier management)
        if (block === 'Hr') return 'Hb';
        // other wood keeps type
        if (block[0] === 'H') return block;
        // fruits
        if (block[0] === 'f') return block;
        // frame keeps color in item
        if (block[0] === 'F') return block;
        // flower keeps color in item
        if (block[0] === '*') return block;
        // this should not be possible:
        if (block === '*|') return '*0';
        // blinking detector becomes generic detector
        if (block[0] === 'E') {
            switch (block[1]) {
                case '0': case '1': case '2': case '3': case '4':
                    return 'E';
                case 'o': case 'a': case 'b': case 'c': case 'd':
                    return 'Eo';
            }
        }
        // spooky and yay stay
        if (block[0] === '#') return block;
        // tiny tree keeps type but not growth stage
        if (block[0] === 't') return block.substring(0,2);
        // on switch becomes off switch
        if (block === 's1') return 'sw';
        // on button becomes off button
        if (block === 'O1') return 'O0';
        // open trapdoor becomes closed trapdoor
        if (block === 'sT') return 'st';
        // powered aascanner becomes closed aascanner
        if (block === 'sC') return 'sc';
        // disabled grabber becomes grabber
        if (block === 'WD') return 'WO';
        // disabled magnetite becomes magnetite
        if (block === 'DM') return 'Dm';
        // disabled ruby becomes ruby
        if (block === 'Rd') return 'R';
        // I started using 2-character blocks
        if (block[0] === 's') return block;
        if (block[0] === 'W') {
            if (block[1] === 'f') {
                return 'f'+block[2];
            }
            return block;
        }
        if (block[0] === 'D') return block;
        if (block[0] === '.') return block;
        if (block[0] === 'O') return block;
        if (block[0] === 'u') return block;
        if (block[0] === '|') return block;
        if (block[0] === 'N') return block;
        if (block[0] === 'n') return block;
        // other blocks just become their substring
        return block[0];
    };

    static itemCodeExists(item_code) {
        if (this.item_codes.indexOf(item_code) === -1) return false;
        return true;
    };

    static humanInput2itemCode(input) {
        if (this.itemCodeExists(input)) {
            // human entered an item code
            return input;
        }
        for (const def of this.item_definitions) {
            if (def.short === input || def.name === input) {
                // human entered an item name
                return def.code;
            }
        }
        // unable to recognize input
        return null;
    }

    static humanInput2blockCode(input) {
        if (this.blockExists(input)) {
            // human entered a block code
            return input;
        }
        for (const def of this.item_definitions) {
            if (def.short === input || def.name === input) {
                // human entered an item name
                return this.item2blockPlain(def.code);
            }
        }
        // unable to recognize input
        return null;
    }

    static blockExists(block) {
        // currently the only way to check if a block code exists
        // is to check if there is a sprite defined for it.
        // note: block2item can not be used for this purpose
        // because it converts a lot of invalid block codes
        // into valid item codes! Example: "wolf"
        if (SPRITES[block] === undefined) return false;
        return true;
    }

    static canItemsStackTogether(item1, item2) {
        // Allowed if one of these is an empty slot
        if (item1 === null) return true;
        if (item2 === null) return true;
        // Not allowed if either of them is a data item
        if (typeof item1.data !== 'undefined') return false;
        if (typeof item2.data !== 'undefined') return false;
        // Allowed if they are of same code
        return item1.code === item2.code;
    }

    assertValidItemCount(count) {
        if (typeof(count) !== 'number') {
            console.error("Invalid item count (not a number): "+count);
            console.trace();
            return false;
        }
        if (count <= 0) {
            console.error("Invalid item count (not positive): "+count);
            console.trace();
            return false;
        }
        if (parseInt(count) !== count) {
            console.error("Invalid item count (non-integer): "+count);
            console.trace();
            return false;
        }
        return true;
    }

    assertValidSlotIndex(slot_index) {
        if (typeof(slot_index) !== 'number') {
            console.error("Invalid inventory slot index (not a number): "+slot_index);
            console.trace();
            return false;
        }
        if (slot_index < 0 || slot_index >= this.slots.length) {
            console.error("Invalid inventory slot index (out of bound): "+slot_index);
            console.trace();
            return false;
        }
        if (parseInt(slot_index) !== slot_index) {
            console.error("Invalid inventory slot index (non-integer): "+slot_index);
            console.trace();
            return false;
        }
        return true;
    }

    assertNonEmptySlotByIndex(slot_index) {
        if (!this.assertValidSlotIndex(slot_index)) return false;
        if (this.slots[slot_index] === null) {
            console.error("Slot is empty: "+slot_index);
            console.trace();
            return false;
        }
        return true;
    }

    assertMinimumItemsInSlotByIndex(slot_index, count) {
        if (!this.assertNonEmptySlotByIndex(slot_index)) return false;
        if (!this.assertValidItemCount(count)) return false;
        if (this.slots[slot_index].amount < count) {
            console.error("Not enough items in slot: "
                + this.slots[slot_index].amount + " < " + count);
            console.trace();
            return false;
        }
        return true;
    }

    // Constructor accepts different parameter types:
    // - if parameter is a number, it creates empty slots
    // - if parameter is an array, it uses it as a list of slots
    // - if parameter is an object, it converts from legacy format
    constructor(parameter=3, isInventoryWithReservedSlots = false) {
        this.onInventoryChangedCallbacks = [];
        this.isInventoryWithReservedSlots = isInventoryWithReservedSlots;
        this.initializeFrom(parameter, isInventoryWithReservedSlots);
    }

    // See constructor comment (above).
    initializeFrom(parameter=3, isInventoryWithReservedSlots=false) {
        if (typeof parameter === 'number') {
            this.slots = Array(parameter).fill(null);
        } else if (Array.isArray(parameter)) {
            this.slots = parameter;
        } else {
            const defaultSize = isInventoryWithReservedSlots
                ? Inventory.PLAYER_INVENTORY_SLOT_COUNT : 10;
            const object = parameter;
            if (typeof object.s !== 'undefined') {
                this.slots = this.fromLegacyStateFormat(object.s);
                this.expandToTotalSlots(object.n !== undefined
                    ? object.n : defaultSize);
            } else {
                this.slots = this.fromLegacyStateFormat(object);
                this.expandToTotalSlots(defaultSize);
            }
        }
        // Safety check every time when creating a new inventory
        // because data can come from all kinds of sources
        this.errorCheck();
        this.inventoryChanged();
        this.recalculateNumNormalSlots();
    }

    recalculateNumNormalSlots() {
        this.numNormalSlots = this.slots.length;
        if (this.isInventoryWithReservedSlots) {
            this.numNormalSlots -= 6;  // 5 for crafting, 1 for delete
        }
    }

    // remove zeroes from slots object
    fix() {
        for (const index in this.slots) {
            if (this.slots[index] === null) continue;
            if (this.slots[index].amount <= 0) {
                this.slots[index] = null;
                continue;
            }
            if (typeof this.slots[index].data === 'undefined') {
                delete this.slots[index].data;
            }
        }
    }

    errorCheck() {
        for (const item of this.slots) {
            if (item === null) continue;
            if (!Inventory.itemCodeExists(item.code)) {
                console.error('FOUND AN INEXISTENT ITEM'
                    +' CODE IN AN INVENTORY!'
                    +' THE ITEM CODE IS '+item.code);
            }
            this.assertValidItemCount(item.amount);
        }
    }

    isDeletionSlot(slot_index) {
        // if reserved slots exist, last slot is deletion
        return this.isInventoryWithReservedSlots
            && slot_index === this.slots.length - 1;
    }

    getSlotAt(slot_index) {
        if (!this.assertValidSlotIndex(slot_index)) return null;
        return this.slots[slot_index];
    }

    getNumUsedSlots() {
        return this.slots.reduce((acc,item) => acc+(item!==null), 0);
    }

    getNumNormalSlots() {
        return this.numNormalSlots;
    }

    getNumExistingSlots() {
        return this.slots.length;
    }

    indexOfEmptyNormalSlot() {
        for (let i = 0; i < this.numNormalSlots; i++) {
            if (this.slots[i] === null) return i;
        }
        return -1;
    }

    hasEmptyNormalSlot() {
        return this.indexOfEmptyNormalSlot() !== -1;
    }

    isEmpty() {
        return this.getNumUsedSlots() === 0;
    }

    numItems(item_code) {
        return this.slots
            .filter(item => item !== null && item.code === item_code)
            .reduce((a,b) => a+b.amount, 0);
    }

    moveItemsToSpecificSlot(invto, slot_index_from, slot_index_to, count) {
        if (!this.canMoveItemsToSpecificSlot(invto, slot_index_from, slot_index_to, count))
            return false;
        const item = this.slots[slot_index_from];
        const item_code = item.code;
        const item_data = item.data;
        this.gainItemAtIndex(item_code, slot_index_from, -count);
        invto.gainItemAtIndex(item_code, slot_index_to, count, item_data);
        return true;
    }

    moveItemsTo(invto, slot_index, count) {
        if (!this.canMoveItemsTo(invto, slot_index, count))
            return false;
        const item_code = this.slots[slot_index].code;
        const item_data = this.slots[slot_index].data;
        this.gainItemAtIndex(item_code, slot_index, -count);
        invto.gainItemByCode(item_code, count, item_data);
        return true;
    }

    moveItemsToCraftingSlots(invto, slot_index, count) {
        if (!this.canMoveItemsToCraftingSlots(invto, slot_index, count))
            return false;
        const item_code = this.slots[slot_index].code;
        const item_data = this.slots[slot_index].data;
        this.gainItemAtIndex(item_code, slot_index, -count);
        invto.gainItemByCodeInCraftingSlots(item_code, count, item_data);
        return true;
    }

    canMoveItemsToSpecificSlot(invto, slot_index_from, slot_index_to, count) {
        const item = this.slots[slot_index_from];
        if (item === null) return false;
        const other_item = invto.slots[slot_index_to];
        // Always allowed to move something into empty slot
        if (item.amount >= count && other_item === null) return true;
        return count <= this.howManyItemsCanMoveToSpecificSlot(invto, slot_index_from, slot_index_to);
    }

    howManyItemsCanMoveToSpecificSlot(invto, slot_index_from, slot_index_to) {
        const item = this.slots[slot_index_from];
        if (item === null) return 0;
        const item_code = item.code;
        const item_data = item.data;
        const gain_limit = invto.howManyCanGainAtIndex(item_code, slot_index_to, item_data);
        return Math.min(item.amount, gain_limit);
    }

    canMoveItemsTo(invto, slot_index, count) {
        const item = this.slots[slot_index];
        if (item === null) return false;
        return count <= this.howManyItemsCanMoveTo(invto, slot_index);
    }

    howManyItemsCanMoveTo(invto, slot_index) {
        const item = this.slots[slot_index];
        if (item === null) return 0;
        const item_code = item.code;
        const item_data = item.data;
        const gain_limit = invto.howManyCanGain(item_code, item_data);
        return Math.min(item.amount, gain_limit);
    }

    canMoveItemsToCraftingSlots(invto, slot_index, count) {
        const item = this.slots[slot_index];
        if (item === null) return false;
        return count <= this.howManyItemsCanMoveToCraftingSlots(invto, slot_index);
    }

    howManyItemsCanMoveToCraftingSlots(invto, slot_index) {
        const item = this.slots[slot_index];
        if (item === null) return 0;
        const item_code = item.code;
        const item_data = item.data;
        const gain_limit = invto.howManyCanGainInCraftingSlots(item_code, item_data);
        return Math.min(item.amount, gain_limit);
    }

    swapSlots(invto, slot_index_from, slot_index_to) {
        // No checks needed. Swapping slots can not fail.
        const item = this.slots[slot_index_from];
        const other_item = invto.slots[slot_index_to];
        this.overwriteSlot(slot_index_from, other_item);
        invto.overwriteSlot(slot_index_to, item);
        return true;
    }

    canGain(item_code, count, data) {
        if (count > 0) {
            // checking against inventory capacity
            return count <= this.howManyCanGain(item_code, data);
        } else {
            // checking if I have items to spend
            // (item data is ignored in this case)
            const current_amount = this.numItems(item_code);
            return current_amount+count >= 0;
        }
    }

    canGainAtIndex(item_code, slot_index, count, data) {
        if (!this.assertValidSlotIndex(slot_index)) return false;
        const item = this.slots[slot_index];
        if (count > 0) {
            // intentional bug to allow some exploits:
            if (item === null) return true;
            // slot for item deletion?
            if (this.isDeletionSlot(slot_index)) {
                // deletion slot always accepts items.
                return true;
            }
            return count <= this.howManyCanGainAtIndex(
                item_code, slot_index, data);
        } else {
            // checking if I have items to spend
            if (item === null) return false;
            if (item.code !== item_code) return false;
            return item.amount + count >= 0;
        }
    }

    // Determine max amount of items of this type that
    // can be gained (excluding reserved slots)
    howManyCanGain(item_code, data) {
        // checking against inventory capacity
        const stack_size = Inventory.stack_sizes[item_code];
        const normalSlots = this.slots.slice(0, this.numNormalSlots);
        return this.howManyCanGainInGivenSlotList(
            normalSlots, item_code, data);
    }

    // Determine max amount of items of this type that can be gained
    howManyCanGainInCraftingSlots(item_code, data) {
        const craftingSlots = this.slots.slice(this.numNormalSlots, -1);
        return this.howManyCanGainInGivenSlotList(
            craftingSlots, item_code, data);
    }

    howManyCanGainInGivenSlotList(slotList, item_code, data) {
        // checking against inventory capacity
        const stack_size = Inventory.stack_sizes[item_code];
        return slotList.reduce((acc,item) => {
            // able to fit max into empty slot?
            if (item === null)
                return acc+stack_size;
            // able to fit some into occupied slot?
            // (only non-data items may be stacked)
            if (item.code === item_code
                && typeof item.data === 'undefined'
                && typeof data === 'undefined'
                && item.amount < stack_size)
                return acc+stack_size-item.amount;
            // none fit into this slot
            return acc+0;
        }, 0);
    }

    // Determine max amount of items of this type that can be gained
    // in a specific slot
    // Items may only be gained in an empty slot, or slot with same item type
    // Exception: always allowed to move something into deletion slot.
    howManyCanGainAtIndex(item_code, slot_index, data) {
        // checking against slot capacity
        const stack_size = Inventory.stack_sizes[item_code];
        // item deletion slot always unlimited gain
        if (this.isDeletionSlot(slot_index)) {
            return 999999999;
        }
        const item = this.slots[slot_index];
        // able to fit max into empty slot?
        if (item === null) return stack_size;
        // able to fit some into occupied slot?
        // (only non-data items may be stacked)
        if (item.code === item_code
            && typeof item.data === 'undefined'
            && typeof data === 'undefined'
            && item.amount < stack_size)
            return stack_size-item.amount;
        // none fit into this slot
        return 0;
    }

    hasItem(item_code) {
        for (const item of this.slots) {
            if (item === null) continue;
            if (item.code === item_code) return true;
        }
        return false;
    };

    clear() {
        for (let i = 0; i < this.slots.length; i++) {
            this.slots[i] = null;
        }
    }

    gainItemByCode(item_code, count, data) {
        return this.gainItemByCodeInGivenSlotRange(
            0, this.numNormalSlots, item_code, count, data);
    }

    gainItemByCodeInCraftingSlots(item_code, count, data) {
        const craftingSlots = this.slots.slice(this.numNormalSlots, -1);
        return this.gainItemByCodeInGivenSlotRange(
            this.numNormalSlots, this.numNormalSlots+5,
            item_code, count, data);
    }

    gainItemByCodeInGivenSlotRange(
        rangeStart, rangeEnd, item_code, count, data) {
        let remaining_count = count;
        if (count > 0) {
            // GAINING ITEMS
            const stack_size = Inventory.stack_sizes[item_code];
            if (typeof data === 'undefined') {
                // Not a data item.
                // Possible to merge with existing slots.
                // First, look for non-full slots with matching item code
                for (let index = rangeStart; index < rangeEnd; index++) {
                    const item = this.slots[index];
                    if (item === null) continue;
                    if (item.code === item_code
                        && item.amount < stack_size
                        && typeof item.data === 'undefined') {
                        // we can put some items in this slot
                        if (item.amount + remaining_count <= stack_size) {
                            item.amount += remaining_count;
                            this.inventoryChanged();
                            return count;
                        } else {
                            // actually we hit the stack size limit
                            const gain_in_this_slot = stack_size - item.amount;
                            item.amount = stack_size;
                            remaining_count -= gain_in_this_slot;
                        }
                    }
                }
            }
            // Put leftover items into currently unoccupied slots
            for (let index = rangeStart; index < rangeEnd; index++) {
                if (this.slots[index] === null) {
                    if (remaining_count <= stack_size) {
                        this.slots[index] = {
                            code: item_code,
                            amount: remaining_count,
                            data: data,
                        };
                        remaining_count = 0;
                        break;
                    } else {
                        // actually we hit the stack size limit
                        this.slots[index] = {
                            code: item_code,
                            amount: stack_size,
                            data: data,
                        };
                        remaining_count -= stack_size;
                    }
                }
            }
            if (remaining_count > 0) {
                console.error('Failed to fit remaining '
                    + remaining_count + ' of ' + item_code + ' into inventory!');
            }
            this.fix();
            this.inventoryChanged();
            return count - remaining_count;
        } else {
            // SPENDING ITEMS
            // in this implementation, range is not honored when spending.
            for (const item of this.slots) {
                if (item === null) continue;
                if (item.code !== item_code) continue;
                if (item.amount + remaining_count < 0) {
                    // Unable to remove that many items from this slot.
                    // So just set it to zero.
                    remaining_count += item.amount;
                    item.amount = 0; // will be removed by this.fix()
                } else {
                    item.amount += remaining_count;
                    break;
                }
            }
            this.fix();
            this.inventoryChanged();
            if (remaining_count > 0) {
                console.error('Failed to remove remaining '
                    + remaining_count + ' of ' + item_code + ' into inventory!');
                return false;
            }
            return count - remaining_count;
        }
    }

    gainItemAtIndex(item_code, index, count, data) {
        if (!this.assertValidSlotIndex(index)) return 0;
        const item = this.slots[index];
        if (item === null) {
            if (count < 0) {
                console.error('Attempt to gain negative items on empty slot');
                console.trace();
                return 0;
            }
            // populate this empty slot
            this.slots[index] = {
                code: item_code,
                amount: count,
                data: data,
            };
            this.fix();
            this.inventoryChanged();
            return count;
        }
        if (count < 0) {
            // gaining negative on non-empty slot
            if (item.code !== item_code) {
                console.error('Attempt to subtract ' + item_code
                    + ' in slot containing ' + item.code);
                console.trace();
                return 0;
            }
            const partial_count = Math.max(count, -this.slots[index].amount);
            this.slots[index].amount += partial_count;
            this.fix();
            this.inventoryChanged();
            return partial_count;
        }
        if (item.code !== item_code
            || typeof item.data !== 'undefined'
            || typeof data !== 'undefined') {
            if (this.isDeletionSlot(index)) {
                // delete item and replace with gained
                this.slots[index] = {
                    code: item_code,
                    amount: count,
                    data: data,
                };
                this.fix();
                this.inventoryChanged();
                return count;
            }
            console.error('Attempt to gain incompatible item ' + item_code
                + ' in slot containing ' + item.code);
            console.trace();
            return 0;
        }
        // gained item matches existing, and neither is data
        // item deletion due to stack limit?
        const stack_size = Inventory.stack_sizes[item_code];
        if (this.slots[index].amount + count > stack_size && this.isDeletionSlot(index)) {
            this.slots[index].amount = Math.max(count, stack_size);
            this.fix();
            this.inventoryChanged();
            // still return whole count,
            // because no items failed to be gained
            return count;
        }
        this.slots[index].amount += count;
        this.fix();
        this.inventoryChanged();
        return count;
    }

    // This function should be avoided if possible
    overwriteSlot(slot_index, new_content) {
        if (!this.assertValidSlotIndex(slot_index)) {
            return false;
        }
        this.slots[slot_index] = new_content;
        this.fix();
        this.inventoryChanged();
    }

    canCraft(recipe_index, ncrafted) {
        return this.howManyCanCraft(recipe_index) >= ncrafted;
    }

    canCraftIntoSlot(recipe_index, slot_index, ncrafted) {
        return this.howManyCanCraftIntoSlot(recipe_index, slot_index) >= ncrafted;
    }

    // Check if have right items for crafting, without checking amount (used for displaying options in UI)
    couldCraft(recipe_index) {
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') {
            return false;
        }
        for (const need of recipe.need) {
            if (need.reveal) continue;
            let found = false;
            for (let i = this.slots.length-6; i < this.slots.length-1; i++) {
                const item = this.slots[i];
                if (item === null) continue;
                if (item.code !== need.item) continue;
                found = true;
                break;
            }
            if (!found) return false;
        }
        return true;
    }

    getCraftableRecipes() {
        const craftableRecipes = [];
        for (let r = 0; r < Inventory.recipes.length; r++) {
            if (this.couldCraft(r)) {
                let yesAddThis = true;
                for (const existingRecipe of craftableRecipes) {
                    if (Inventory.recipes[existingRecipe].get.item
                        === Inventory.recipes[r].get.item) {
                        // skip this recipe because it would be confusing
                        yesAddThis = false;
                        break;
                    }
                }
                if (yesAddThis) {
                    craftableRecipes.push(r);
                }
            }
        }
        return craftableRecipes;
    }

    howManyCanCraft(recipe_index) {
        if (!this.isInventoryWithReservedSlots) return 0;
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') return 0;
        // How many can we craft, according to available free slots?
        let how_many = Math.floor(
            this.howManyCanGain(recipe.get.item) / recipe.get.amount);
        for (const need of recipe.need) {
            // How many can we craft, according to this recipe part?
            let h = 0;
            for (let i = this.slots.length-6; i < this.slots.length-1; i++) {
                const item = this.slots[i];
                if (item === null) continue;
                if (item.code !== need.item) continue;
                h += item.amount;
            }
            h = Math.floor(h / need.amount);
            if (h < how_many) {
                how_many = h;
            }
        }
        return how_many;
    }

    howManyCanCraftIntoSlot(recipe_index, slot_index) {
        if (!this.isInventoryWithReservedSlots) return 0;
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') return 0;
        if (!this.assertValidSlotIndex(slot_index)) return 0;
        // How many can we craft, according to available free slots?
        const slot = this.slots[slot_index];
        let how_many = Math.floor(
            this.howManyCanGainAtIndex(recipe.get.item, slot_index)
            / recipe.get.amount);
        if (how_many === 0
            && !this.isDeletionSlot(slot_index)
            && this.slots[slot_index] !== null
            && this.slots[slot_index].code !== recipe.get.item) {
            // Trying to craft into mismatching slot.
            // Check if it would be possible to
            // automatically move existing items to end of inventory
            // to free up this slot:
            if (this.hasEmptyNormalSlot()) {
                // Yes! This slot can be emptied before crafting.
                // So we can fit up to a full stack of new item!
                how_many = Inventory.stack_sizes[recipe.get.item];
            }
        }
        for (const need of recipe.need) {
            // How many can we craft, according to this recipe part?
            let h = 0;
            for (let i = this.slots.length-6; i < this.slots.length-1; i++) {
                const item = this.slots[i];
                if (item === null) continue;
                if (item.code !== need.item) continue;
                h += item.amount;
            }
            h = Math.floor(h / need.amount);
            if (h < how_many) {
                how_many = h;
            }
        }
        return how_many;
    }

    howManyHaveAsCraftingIngredient(item_code) {
        let h = 0;
        for (let i = this.slots.length-6; i < this.slots.length-1; i++) {
            const item = this.slots[i];
            if (item === null) continue;
            if (item.code !== item_code) continue;
            h += item.amount;
        }
        return h;
    }

    // calculate how many times the recipe can be crafted
    // by checking only how many resources you have.
    // Not checking where these crafted items will be stored.
    howManyRecipesCanExpend(recipe_index) {
        if (!this.isInventoryWithReservedSlots) return 0;
        const recipe = Inventory.recipes[recipe_index];
        if (typeof recipe === 'undefined') return 0;
        let how_many = -1;  // magic number -1 means first loop
        for (const need of recipe.need) {
            // How many can we craft, according to this recipe part?
            let h = this.howManyHaveAsCraftingIngredient(need.item);
            h = Math.floor(h / need.amount);
            if (h < how_many || how_many === -1) {
                how_many = h;
            }
        }
        return how_many;
    }

    craft(recipe_index, ncrafted) {
        const recipe = Inventory.recipes[recipe_index];
        this.expendRecipe(recipe_index, ncrafted);
        const item_code = recipe.get.item;
        let item_data = undefined;
        if (Inventory.isDataItem(item_code)) {
            item_data = Inventory.makeItemData(item_code);
        }
        this.gainItemByCode(item_code, recipe.get.amount*ncrafted, item_data);
    }

    craftIntoSlot(recipe_index, slot_index, ncrafted) {
        const recipe = Inventory.recipes[recipe_index];
        this.expendRecipe(recipe_index, ncrafted);
        if (!this.isDeletionSlot(slot_index)
            && this.slots[slot_index] !== null
            && this.slots[slot_index].code !== recipe.get.item) {
            // Slot occupied by mismatching item!
            // Find a slot to move this existing item to:
            const moveSlot = this.indexOfEmptyNormalSlot();
            if (moveSlot === -1) {
                console.error('No slot to move occupying items into!', this.slots);
                console.trace();
                return;
            }
            this.moveItemsToSpecificSlot(this, slot_index,
                moveSlot, this.slots[slot_index].amount);
        }
        let item_data = undefined;
        if (Inventory.isDataItem(recipe.get.item)) {
            item_data = Inventory.makeItemData(recipe.get.item);
        }
        this.gainItemAtIndex(recipe.get.item, slot_index,
            recipe.get.amount*ncrafted, item_data);
    }

    expendRecipe(recipe_index, ncrafted) {
        const recipe = Inventory.recipes[recipe_index];
        for (const need of recipe.need) {
            let remaining_count = need.amount * ncrafted;
            for (let i = this.slots.length-6; i < this.slots.length-1; i++) {
                const item = this.slots[i];
                if (item === null) continue;
                if (item.code !== need.item) continue;
                if (item.amount - remaining_count < 0) {
                    // Unable to remove that many items from this slot.
                    // So just set it to zero.
                    remaining_count -= item.amount;
                    item.amount = 0; // will be removed by this.fix()
                } else {
                    item.amount -= remaining_count;
                    break;
                }
            }
        }
        this.fix();
    }


    spendAllItems(item_code) {
        for (const index in this.slots) {
            if (this.slots[index] === null) continue;
            if (this.slots[index].code !== item_code) continue;
            this.slots[index] = null;
        }
    }

    onInventoryChanged(callback) {
        this.onInventoryChangedCallbacks.push(callback);
    }

    inventoryChanged() {
        for (const callback of this.onInventoryChangedCallbacks) {
            callback();
        }
    }

    // used by aaprinter,
    // possibly also by item search UI in the future.
    findSlotByScanCode(searchedCode) {
        for (const index in this.slots) {
            if (this.slots[index] === null) continue;
            const item_code = this.slots[index].code;
            const block = Inventory.item2blockPlain(item_code);
            const scanCode = Inventory.block2scanCode(block);
            if (scanCode === searchedCode) return index;
        }
        return null;
    }

    // used by aascanner and aaprinter,
    // possibly also by item search UI in the future.
    static block2scanCode(b) {
        // special scan codes
        if (b[0] === '-') return '-';
        if (b[0] === '=') return '-';
        if (b[0] === ':') return ':';
        // find sprite for this block
        const sprite = SPRITES[b];
        // abort if sprite not exist
        if (!sprite) return null;
        // char of sprite becomes the scan code
        const scanCode = sprite.char;
        // validate scan code length
        if (scanCode.length !== 1) return null;
        // convert special scan codes
        if (scanCode === '╳') return 'X';
        if (scanCode === '▁') return '_';
        if (scanCode === '”') return '"';
        // remaining scan codes are allowed only if matching ? exists
        if (!SPRITES['?'+scanCode]) return null;
        return scanCode;
    }

    saveToJSON() {
        return this.slots;
    }

    expandToTotalSlots(total_slots) {
        while (this.slots.length < total_slots) {
            this.slots.push(null);
        }
    }

    fromLegacyStateFormat(s) {
        // Convert from old format (object) to new (array)
        const slots = [];
        let slot_index = 0;
        for (const item_code in s) {
            const item = s[item_code];
            if (typeof item === 'number') {
                // Is a VERY OLD format, before data items existed
                const amount = item;  // only amount is in this format
                if (Inventory.isDataItem(item_code)) {
                    // need to create default data for each item
                    for (let i = 0; i < amount; i++) {
                        const data = Inventory.makeItemData(item_code);
                        slots[slot_index] = {
                            code: item_code,
                            amount: 1,
                            data: data,
                        };
                        slot_index++;
                    }
                } else {
                    slots[slot_index] = {
                        code: item_code,
                        amount: amount,
                    };
                    slot_index++;
                }
            }
            else if (typeof item.d === 'undefined') {
                // Not data items. Keep stack (even if too big)
                slots[slot_index] = {
                    code: item_code,
                    amount: item.c,
                };
                slot_index++;
            } else {
                // Data items. Split to multiple entries.
                for (const data of item.d) {
                    slots[slot_index] = {
                        code: item_code,
                        amount: 1,
                        data: data,
                    };
                    slot_index++;
                }
            }
        }
        return slots;
    }

}

if (typeof exports !== 'undefined') {
    exports.Inventory = Inventory;
    exports.PLAYER_INVENTORY_SLOT_COUNT = Inventory.PLAYER_INVENTORY_SLOT_COUNT;
}
