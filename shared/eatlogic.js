// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    SPRITES = require('../shared/sprites').SPRITES;
    FADE_COLORS_HEX = require('../shared/aastring').FADE_COLORS_HEX;
}

function eatLogic(view, data) {
    if (typeof data.slotindex !== 'number') {
        return false;
    }
    const slot_index = parseInt(data.slotindex);
    const item = view.player.inventory.getSlotAt(slot_index);
    if (item === null) {
        view.notifySend('no item selected');
        return false;
    }
    const item_code = item.code;
    // must be a food item
    if (item_code[0] !== 'f' && item_code !== 'W~'
        && item_code !== 'WO' && item_code !== 'WR'
        && item_code !== 'b') {
        // you can't eat this
        view.notifySend('item not edible');
        return false;
    }
    // can not be full
    if (view.player.food > 800) {
        // can't eat when not hungry at all
        view.notifySend('already full');
        return false;
    }
    view.gainItemAtIndex(item_code, slot_index, -1, undefined);
    // round up
    const foodlevelbefore = view.player.food;
    if (item_code === 'fR') {
        view.player.food = 4000;
    } else if (item_code === 'WR') {
        view.player.food = 200*Math.ceil(view.player.food/200);
        if (view.player.food === 0) view.player.food = 200;
    } else {
        view.player.food = 200*Math.ceil(view.player.food/200+1);
        // safety check
        if (view.player.food > 1000) view.player.food = 1000;
    }
    const fooddiff = view.player.food - foodlevelbefore;
    view.notifySendAmount('+', fooddiff, ' energy');
    if (item_code === 'WO') {
        view.notifySend('EWW did you really eat that?');
    }
    if (item_code === 'fc') {
        view.notifySend('nom!');
    }
    if (item_code === 'b') {
        view.notifySend('HGRHRGHHGHGRH');
    }
    if (item_code === 'WR') {
        if (view.isServer) {
            const startColor = '#' + view.player.color;
            const endColor = SPRITES['WR'].color;
            // abort if no fading needed
            if (startColor === endColor) return true;
            let fadeColor = startColor;
            // try different intensities until there's a difference
            let mixIntensity = 0.01;
            do {
                fadeColor = FADE_COLORS_HEX(startColor, endColor, mixIntensity);
                mixIntensity += 0.01;
                if (mixIntensity > 0.1) {
                    // too many iterations.
                    // Force to become exact color
                    fadeColor = endColor;
                    break;
                }
            } while (fadeColor === startColor);
            FADE_COLORS_HEX(startColor, endColor, 0.01);
            view.player.color = fadeColor.substr(1);
            view.player.changedColor = true;
            view.syncher.updateMyAndOtherPositionLists(view.player);
        }
    }
    return true;
}


if (typeof exports !== 'undefined') {
    exports.eatLogic = eatLogic;
}
