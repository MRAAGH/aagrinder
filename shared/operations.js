// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * SHARED FILE
 */

// split typed command string by quoted sections
function splitByQuotes(typed) {
    // previously, we were using regex split:
    // let args = typed.split(/ +(?=(?:(?:[^"]*"){2})*[^"]*$)/g);
    // but was not robust enough for incomplete quotes.
    // so now we match the arguments, not the space between them.
    // this regex has 3 options, separated by |
    //   first option: incomplete quotes at the end of input
    //   second option: quoted string, spaces allowed
    //   third option: non-quoted string (no spaces allowed)
    const reg = /[^ "]*("[^"]*"[^ "]*)*"[^"]*$|[^ "]*("[^"]*"[^ "]*)+|[^ "]+/g;
    var args = [];
    while (true) {
        const match = reg.exec(typed);
        if (match === null) break;
        args.push(match[0]);
    }
    // fix for non-quoted empty string failing to parse
    if (typed[typed.length-1] === ' ') {
        args.push('');
    }
    return args;
}

function clipColor(x) {
    return Math.floor(Math.max(Math.min((x+0.5)*200,255),90));
}

function getRainbowColorAt(len, i) {
    const frequency = 3 / len;
    const pos = frequency * i;
    const r = clipColor(Math.sin(pos+1.6));
    const g = clipColor(Math.sin(pos-0.0));
    const b = clipColor(Math.sin(pos-1.6));
    const colorHere = '#'+('00' + r.toString(16)).slice(-2)
        + ('00' + g.toString(16)).slice(-2)
        + ('00' + b.toString(16)).slice(-2);
    return colorHere;
}

if (typeof exports !== 'undefined') {
    exports.splitByQuotes = splitByQuotes;
    exports.getRainbowColorAt = getRainbowColorAt;
}
