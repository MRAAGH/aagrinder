// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

function magicallygetitemLogic(view, data) {
    if (typeof data.item !== 'string') return false;
    if (typeof data.preferredslot !== 'number') return false;
    const item_code = data.item;
    const preferredslot = data.preferredslot;
    // Since the item was not found in hotbar,
    // decide which slot the item should appear in:
    let target_slot = preferredslot;
    for (let i = 0; i < 10; i++) {
        const item = view.player.inventory.getSlotAt(i);
        if (item === null) {
            // found an empty slot.
            // This is the best slot for the item to appear!
            target_slot = i;
            break;
        }
    }
    // Look for item in the rest of the inventory
    // (skipping hotbar and reserved slots)
    let found_slot = null;
    for (let i = 10; i < view.player.inventory.getNumNormalSlots(); i++) {
        const item = view.player.inventory.getSlotAt(i);
        if (item === null) continue;
        if (item.code === item_code) {
            // found a slot with the requested item.
            found_slot = i;
            break;
        }
    }
    if (found_slot !== null) {
        // Item was found somewhere in inventory.
        if (view.player.inventory.getSlotAt(target_slot) === null) {
            // Target slot is empty. Move items directly.
            const amount = view.player.inventory.getSlotAt(found_slot).amount;
            view.player.inventory.moveItemsToSpecificSlot(
                view.player.inventory, found_slot, target_slot, amount);
            return true;
        } else {
            // Target slot is occupied. Swap slots.
            view.player.inventory.swapSlots(view.player.inventory, found_slot, target_slot);
            return true;
        }
    }
    // Item was not found anywhere in inventory.
    // must be gamemode 1 or 2 to magically gain item:
    if (view.player.gamemode === 0) {
        return false;
    }
    view.player.inventory.overwriteSlot(target_slot, {
        code: item_code,
        amount: 1,
        data: Inventory.makeItemData(item_code),
    });
}


if (typeof exports !== 'undefined') {
    exports.magicallygetitemLogic = magicallygetitemLogic;
}
