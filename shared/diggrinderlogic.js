// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


// Determines which surrounding leaves will be checked by grinder
const leafSurround = [
    {dx:0, dy:-1, dir:0},
    {dx:-1, dy:-1, dir:-1},
    {dx:+1, dy:-1, dir:+1},
    {dx:-1, dy:0, dir:-1},
    {dx:+1, dy:0, dir:+1},
    {dx:0, dy:+1, dir:0},
    {dx:-1, dy:+1, dir:-1},
    {dx:+1, dy:+1, dir:+1},
    {dx:0, dy:+2, dir:0},
];

const DXDYS = [
    {dx:1, dy:0},
    {dx:0, dy:1},
    {dx:-1,dy:0},
    {dx:0, dy:-1},
]

// Determines which block types will be dug in a 3x3 pattern with grinder
const diggableStones = {
    'G0': ['B'],
    'G#': ['B'],
    'Gy': ['b'],
    'GB': [],
    'WP': [],
    'GW': [],
    'GA': [],
};

function triggerRemovalUpdates(x, y, dug, view) {

    // special stuff for sign
    if (dug === 'si') {
        view.playerDeleteTileEntityAt(x, y);
    }

    // special stuff for detector
    if (view.isServer && dug[0] === 'E') {
        view.deleteTileEntityAt(x, y);
    }

    // special stuff for SSPOOKY
    if (view.isServer && dug === 'Ws') {
        view.deleteTileEntityAt(x, y);
    }

    // Hopper Logic
    // Break relationships when any block in the right coords are broken
    if ( view.isServer && (dug[0] === 'p' || dug[0] === 'h' || dug === "Wp") ) {
        view.hopperInvalidate(x, y);
    }

}

function triggerChangeUpdates(x, y, dug, view) {

    // special stuff for diamond
    if (dug === 'DD') {
        for (let dx = -3; dx <= 3; dx++) {
            for (let dy = -3; dy <= 3; dy++) {
                const otherBlock = view.getBlock(x+dx, y+dy);
                if (otherBlock === 'R0') {
                    // this ruby stone (pink B) disappears
                    view.addAnimation(x+dx, y+dy,
                        Math.abs(dx)/2 + Math.abs(dy)/2);
                    view.setBlock(x+dx, y+dy, 'B', true);
                }
            }
        }
    }

    // special stuff for wires and water
    if (view.isServer) {
        if (dug === 'sw'
            || dug === 's1'
            || dug === 'O0'
            || dug === 'O1'
            || dug[0] === 'R'
            || dug[0] === '%'
            || dug[0] === ':'
        ) {
            GLS.enqueueWire4(x, y, view);
        }
        if (dug[0] === '=') {
            GLS.enqueueWire8(x, y, view);
        }

        // update the powered scanner above
        const above = view.getBlock(x, y+1);
        if (above === 'sC') {
            view.enqueueWire(x, y+2, view);
        }

        GLS.checkEnqueueWater(x, y, view);
        GLS.checkEnqueueWater(x+1, y, view);
        GLS.checkEnqueueWater(x-1, y, view);
        GLS.checkEnqueueWater(x, y+1, view);
        GLS.checkEnqueueWater(x, y-1, view);
    }

}

// dig a block properly.
// if gain is true, corresponding item will be gained
// if force is true, block will be dug regardless of
// whether it can be gained or not.
function fullDig(x, y, view, gain, force=false) {

    if (GLS.checkViolateClaim(view, x, y)) return false;

    // block that will be dug:
    const dug = view.getBlock(x, y);
    // corresponding item that will be gained:
    let gained_item = Inventory.block2item(dug);

    let yes_will_gain = gain;

    if (dug === '*|') {
        // Don't gain flower stem. Gain what's on it, instead.
        const theflower = view.getBlock(x, y+1);
        gained_item = Inventory.block2item(theflower);
    }
    else if (dug === 'Wt'
            || dug === 'sn'
            || dug === 'WS'
            || dug === '~') {
        // don't gain these unobtainable items
        gained_item = null;
        yes_will_gain = false;
    }

    if (yes_will_gain && !view.player.inventory.canGain(gained_item, 1)) {
        // ohwait I can't gain this item because no inv space.
        if (force) {
            // Since you're forcing me, I will dig without gaining.
            // That's all I can do.
            gained_item = null;
            yes_will_gain = false;
        }
        else {
            // I guess no dig after all ...
            view.notifySend('no inventory space');
            return false;
        }
    }

    // after successful dig, it will be air.
    let blockAfterDigging = ' ';

    // if digging fruit, a leaf will be left behind
    if (dug[0] === 'f' && dug !== 'fc' && dug !== 'fR' && dug !== 'fm') {
        blockAfterDigging = 'Wa'+dug[1];
    }

    let didGain = false;
    // dig the block
    view.setBlock(x, y, blockAfterDigging, true);
    // block above might break
    const above = view.getBlock(x, y+1);
    if (BP.isUnstable(view.getBlock(x, y+1))) {
        // recursively dig the unstable block that's above
        // (this is forced, otherwise it would look real weird)
        fullDig(x, y+1, view, gain, force=true);
    }

    // flower has a special digging procedure
    if (dug[0] === '*') {
        if (dug === '*|') {
            // dug the stem block.
            // there is a flower on it.
            const theflower = view.getBlock(x, y+1);
            if (theflower[0] !== '*') {
                console.log('uhhhhh, so, I found this weird flower: '+theflower);
                return false;
            }
            if (yes_will_gain) {
                view.gainItemByCode(gained_item, 1, undefined);
                didGain = true;
            };
            // the flower breaks as well
            view.setBlock(x, y+1, ' ', true);
            // make sure it also updates water
            if (view.isServer) {
                GLS.checkEnqueueWater(x+1, y+1, view);
                GLS.checkEnqueueWater(x-1, y+1, view);
                GLS.checkEnqueueWater(x, y+2, view);
            }
        }
        else {
            // dug the actual flower
            // gain it
            if (yes_will_gain) {
                view.gainItemByCode(gained_item, 1, undefined);
                didGain = true;
            };
            // now break the stem
            if (view.getBlock(x, y-1) === '*|') {
                // ok
                view.setBlock(x, y-1, ' ', true);
                // make sure it also updates water
                if (view.isServer) {
                    GLS.checkEnqueueWater(x+1, y-1, view);
                    GLS.checkEnqueueWater(x-1, y-1, view);
                    GLS.checkEnqueueWater(x, y-2, view);
                }
            }
            else {
                console.log('wait so this flower didn\'t have a stem??');
                // in this situation, we do not abort
                // because this is not as critical as the other one.
            }
        }
    }
    else {
        // for all other items, dig and gain normally
        if (yes_will_gain) {
            didGain = true;
            const is_data_item = view.player.inventory.constructor.isDataItem(
                gained_item);
            if (is_data_item) {
                const tiles = view.getTileEntityAt(x, y);
                // safety check for number of tiles
                if (tiles.length === 1) {
                    const tile = tiles[0];
                    view.playerDeleteTileEntityAt(x, y);
                    // tile data becomes item data
                    view.gainItemByCode(gained_item, 1, tile.d);
                } else {
                    console.error('incorrect number of tile entities at '
                        +x+', '+y+': '+tiles.length);
                    console.trace();
                    const data = Inventory.makeItemData(gained_item);
                    view.gainItemByCode(gained_item, 1, data);
                }
            }
            else {
                view.gainItemByCode(gained_item, 1, undefined);
            }
            // Still need to cleanup tile entities
            if (dug === 'sm') {
                view.playerDeleteTileEntityAt(x, y);
            }
        }
    }

    // on the server, need to check for tile entity
    if (view.isServer) {
        const tiles = view.getTileEntityAt(x, y);
        if (tiles.length > 0) {
            // there is a tile entity
            const tile = tiles[0];
            if (tile.t === 'chest') {
                if (!tile.inventory.isEmpty()) {
                    // chest has items in it!
                    // Non-empty chests can't be destroyed.
                    // Of course, the client does not have this information
                    // The client goes "I just broke this block"
                    // and now it's the server's job to respond
                    // "YOU DID NOT JUST BREAK THAT BLOCK"
                    view.surpriseSetBlock(x, y, dug, false);
                    if (didGain) {
                        // lose this item:
                        view.surpriseGainItemInverted(gained_item, -1, undefined);
                    }
                }
                else {
                    // okay allow breaking this chest.
                    view.deleteTileEntityAt(x, y);
                    // camo chests:
                    if (tile.inventory.slots.length === 20
                        && dug !== 'hh' && dug !== 'hg'
                        && dug !== 'Wp') {
                        view.surpriseGainItem('hg', 1, undefined);
                    }
                }
            }
        }
    }

    triggerRemovalUpdates(x, y, dug, view);
    triggerChangeUpdates(x, y, dug, view);

    return true;
}


function digTree(view, x, y, limit) {
    // recursively dig whole tree
    let numdug = 0

    const visited = [];
    for (let i = 0; i < 4; i++) {
        visited.push(Array(39).fill(false));
    }
    const avisited = [];
    for (let i = 0; i < 4; i++) {
        avisited.push(Array(39).fill(false));
    }

    const queue = [{
        x:14,
        y:1,
        type:view.getBlock(x, y),
    }];
    visited[1][14] = true;
    while (queue.length > 0) {
        // stop if at limit
        if (numdug+1 > limit) return numdug;
        // next in queue
        const v = queue.shift();
        // animated block digging
        view.addAnimation(x+v.x, y+v.y, v.y);
        // break this wood block
        const successful_dig = fullDig(x+v.x, y+v.y, view, true);
        // stop if unsuccessful dig
        if (!successful_dig) return numdug;
        numdug++;

        // break surrounding vines
        let dy = -1;
        while (v.y + dy > 0) {
            const vine = view.getBlock(x+v.x, y+v.y+dy);
            if (vine === '-2' || vine === '-a') {
                // yes, this definitely is a vine
                // stop if at limit
                if (numdug+1 > limit) return numdug;
                // animated block digging
                view.addAnimation(x+v.x, y+v.y+dy, v.y-dy*0.5);
                // break this vine block
                const successful_dig = fullDig(x+v.x, y+v.y+dy, view, true);
                // stop if unsuccessful dig
                if (!successful_dig) return numdug;
                numdug++;
            }
            else {
                // no more vine!
                break;
            }
            dy--;
        }

        // break surrounding leaves
        for (const s of leafSurround) {
            for (let i = 0; i < 6; i++) {
                const xlocal = v.x + s.dx + i*s.dir;
                const ylocal = v.y + s.dy;
                if (xlocal<0 || xlocal>=avisited[0].length
                    || ylocal<0 || ylocal>=avisited.length) {
                    break;
                }
                const xglobal = x + xlocal;
                const yglobal = y + ylocal;
                const blockhere = view.getBlock(xglobal, yglobal);
                if (blockhere[0] === 'A'
                    || blockhere[0] === 'a'
                    || blockhere[0] === 'f'
                    || blockhere.substring(0,2) === 'Wa') {
                    // is leaf or fruit
                    if (!avisited[ylocal][xlocal]) {
                        // not visited yet!
                        // dig!
                        avisited[ylocal][xlocal] = true;
                        // gain only if it's not a fruit
                        // (fruit is lost with grinder)
                        let gain = (blockhere[0] !== 'f');
                        // stop if at limit
                        if (numdug+1 > limit) return numdug;
                        // animated block digging
                        view.addAnimation(xglobal, yglobal, v.y+i);
                        // break this leaf or fruit block
                        const successful_dig = fullDig(xglobal, yglobal, view, gain);
                        // stop if unsuccessful dig
                        if (!successful_dig) return numdug;
                        numdug++;

                        // break surrounding vines
                        let dy = -1;
                        while (ylocal + dy > 0) {
                            const vine = view.getBlock(xglobal, yglobal+dy);
                            if (vine === '-2' || vine === '-a') {
                                // yes, this definitely is a vine
                                // stop if at limit
                                if (numdug+1 > limit) return numdug;
                                // animated block digging
                                view.addAnimation(xglobal, yglobal+dy,
                                    v.y+i-dy*0.5);
                                // break this vine block
                                const successful_dig = fullDig(
                                    xglobal, yglobal+dy, view, true);
                                // stop if unsuccessful dig
                                if (!successful_dig) return numdug;
                                numdug++;
                            }
                            else {
                                // no more vine!
                                break;
                            }
                            dy--;
                        }

                    }
                }
                else {
                    // in all other cases, stop following this path
                    // because it is not connected!
                    break;
                }
            }
        }

        // BFS code
        if (v.y < visited.length-1) {
            for (let dx = -1; dx <= 1; dx++) {
                let blockhere = view.getBlock(v.x+dx+x, v.y+1+y);
                if (blockhere[0] === 'H') {
                    if (!visited[v.y+1][v.x+dx]) {
                        visited[v.y+1][v.x+dx] = true;
                        queue.push({
                            x: v.x+dx,
                            y: v.y+1,
                            type: blockhere,
                        });
                    }
                }
            }
        }
    }

    return numdug;
}

function digCorrupston(view, x, y) {
    // recursively dig all corrupted stone
    let numdug = 0

    const queue = [{
        x:x,
        y:y,
        d:0,
    }];
    while (queue.length > 0) {
        // next in queue
        const v = queue.shift();
        // verify that it is still corrupted stone
        if (view.getBlock(v.x, v.y) !== 'WB') continue;
        // animated block digging
        view.addAnimation(v.x, v.y, v.d/5+Math.random()*2);
        // break this corrupted stone block
        // TODO: instead of setting true here, could set false and later calculate what needs to be gained.
        const successful_dig = fullDig(v.x, v.y, view, false);
        // stop if unsuccessful dig
        // TODO: this condition can be removed. If fullDig and/or gainItemByCode gets fixed to no longer crash the game when attempting to gain beyond inventory capacity.
        if (!successful_dig) return numdug;
        numdug++;

        // BFS code
        for (const dxdy of DXDYS) {
            const ox = v.x + dxdy.dx;
            const oy = v.y + dxdy.dy;
            const dist = (x-ox)*(x-ox)+(y-oy)*(y-oy)*2;
            if (dist > 1500) continue;
            if (dist > 1000 && (ox*ox+oy*oy)%(ox+oy)%3===0) continue;
            const otherblock = view.getBlock(ox, oy);
            if (otherblock === 'WB') {
                queue.push({
                    x: ox,
                    y: oy,
                    d: v.d+1,
                });
            }
        }
    }

    return numdug;
}

function digLogic(view, data) {
    if (isNaN(data.x)) return false;
    if (isNaN(data.y)) return false;
    const x = view.player.x + parseInt(data.x);
    const y = view.player.y + parseInt(data.y);
    // must be within reach
    if (Math.abs(view.player.x - x) > view.player.reach) return false;
    if (Math.abs(view.player.y - y) > view.player.reach) return false;

    // which tool?
    let tool_item;
    let tool_code;
    const slot_index = parseInt(data.slot_index);
    if (data.slot_index === -1) {
        tool_code = ' ';
    } else if (!view.player.inventory.assertValidSlotIndex(slot_index)) {
        return false;
    } else {
        tool_item = view.player.inventory.getSlotAt(slot_index);
        if (tool_item === null) tool_code = ' ';
        else if (tool_item.code[0] === 'G') tool_code = tool_item.code;
        else if (tool_item.code === 'WP') tool_code = 'WP';
        else tool_code = ' ';
    }

    // must be a diggable block
    const dug = view.getBlock(x, y);
    if (BP.isUndiggable(dug, tool_code)) {
        if (dug === ' ') return false;
        if (dug === 'w') return false;
        if (dug === '~') return false;
        if (dug === 'W~') return false;
        if (dug[0] === '>') return false;
        view.notifySend('unable to dig');
        return false;
    }

    // hotfix for durability not being spent by GH in underground biome
    let force_spend_durability = false;

    let effective_tool_code = tool_code;

    // undiggable blocks in underground
    if (dug === 'B' && view.player.gamemode === 0) {
        const biomeHere = view.syncher.map.biomeAt(x, y);
        if (biomeHere === 'u'){
            // digging stone underground with fist
            if (tool_code === ' ') {
                view.notifySend('unable to dig');
                return false;
            }
            // digging stone underground with woodgrinder
            if (tool_code === 'GH') {
                force_spend_durability = true;
                effective_tool_code = ' ';
            }
            // digging stone underground with stonegrinder
            if (tool_code === 'GB') effective_tool_code = 'GH';
        }
    }

    // gamemode 0 suffers dig duration
    if (view.player.gamemode === 0) {
        const duration = BP.digDuration(dug, effective_tool_code);
        if (duration > 0) {
            view.player.digduration = duration;
            if (view.player.digx !== x || view.player.digy !== y) {
                view.player.digx = x;
                view.player.digy = y;
                view.player.digtime = 0;
            }
            view.player.digtime++;
            if (view.player.digtime < duration) {
                // not enough digging time yet!
                // but this action was "successful" anyway.
                return true;
            }
        }
    }
    view.player.digtime = 0;

    // Special case for digging corrupted stone
    if (dug === 'WB') {
        if (view.player.inventory.canGain(dug, 1)) {
            const numdug = digCorrupston(view, x, y);
            if (numdug > 0) {
                view.gainItemByCode('WB', 1, undefined);
            }
            return true;
        }
        view.notifySend('no inventory space');
        return false;
    }

    if (GLS.checkViolateClaim(view, x, y)) return false;

    if (tool_code === 'Gy') {
        const cur_durability = tool_item.data.d;
        if (cur_durability === 1) {
            view.notifySend('yay');
            if (dug === 'B') {
                view.setBlock(x, y, 'R0', true);
                view.player.inventory.getSlotAt(slot_index).data.d = 0;
                view.notifySendAmount('', -1, ' durability');
                return true;
            }
            return false;
        }
        if (cur_durability === 0) {
            view.notifySend('yay');
            return false;
        }
    }

    if (tool_code === 'GW') {
        blockHere = view.getBlock(view.player.x, view.player.y);
        if (BP.isUndiggable(blockHere, tool_code)) return false;
        view.setBlock(view.player.x, view.player.y, dug);
        triggerChangeUpdates(view.player.x, view.player.y, dug, view);
        view.setBlock(x, y, blockHere);
        triggerChangeUpdates(x, y, blockHere, view);
        // tile entities only need to be processed server-side
        // because it is currently not allowed to swap client-side entities
        if (view.isServer) {
            view.serverSwapTileEntities(x, y, view.player.x, view.player.y);
        }
        view.notifySendAmount('', -1, ' durability');
        if (tool_item.data.d-1 < 0) {
            // warpgrinder breaks
            view.gainItemAtIndex(tool_item.code, slot_index, -1);
        }
        else {
            // lose this much durability
            view.player.inventory.getSlotAt(slot_index).data.d -= 1;
        }
        return true;
    }

    if (tool_code === ' ' || tool_code === 'GH') {
        // "normal" dig (not surroundings)
        const success = fullDig(x, y, view, true);
        if (!success) return false;
        // skip durability if tool did not have an advantage
        if (BP.digDuration(dug, effective_tool_code) === BP.digDuration(dug, ' ')
            && !force_spend_durability) return success;
        // lose durability on this non-fist tool
        view.notifySendAmount('', -1, ' durability');
        if (tool_item.data.d-1 < 0) {
            // tool breaks
            view.gainItemAtIndex(tool_item.code, slot_index, -1, undefined);
        }
        else {
            // lose durability
            view.player.inventory.getSlotAt(slot_index).data.d -= 1;
        }
        return success;
    }

    // grinder dig
    let numdug = 0;
    let force_lose_durability = false;
    const diggable_stones = diggableStones[tool_code];
    const diggable_vines = ['-2', '-8', '-a'];

    // stone
    if (diggable_stones.includes(dug)) {
        // dig all around
        for (let dx = -1; dx <= 1; dx++) {
            for (let dy = -1; dy <= 1; dy++) {
                const otherBlock = view.getBlock(x+dx, y+dy);
                if (!diggable_stones.includes(otherBlock)) continue;
                view.addAnimation(x+dx, y+dy, (dx-dy+2)/2);
                if (fullDig(x+dx, y+dy, view, false)) numdug++;
                if (otherBlock !== 'b') continue;
                // normal boulder rules
                if (GLS.checkViolateClaim(view, x, y)) continue;
                numdug += 99;
                if (view.player.inventory.canGain('..', 25)) {
                    view.gainItemByCode('..', 25, undefined);
                }
            }
        }
        if (numdug > 5) {
            if (view.player.inventory.canGain('..', 1)) {
                view.gainItemByCode('..', 1, undefined);
            } else {
                view.setBlock(x, y, '..');
            }
        }
        // this code is a big mess, I don't know why this is needed
        if (numdug < 1) {
            return false;
        }
    }
    else if (dug[0] === 'H') {
        if (view.player.inventory.canGain(dug, 1)) {
            // dig whole tree!
            numdug = digTree(view, x-14, y-1, tool_item.data.d);
        }
    }
    else if (diggable_vines.includes(dug)) {
        const gained_item = '-';
        if (!view.player.inventory.canGain(gained_item, 1)) {
            view.notifySend('no inventory space');
            return false;
        }
        for (let dy = 0; dy <= 2; dy++) {
            const otherBlock = view.getBlock(x, y+dy);
            if (!diggable_vines.includes(otherBlock)) break;
            view.addAnimation(x, y+dy, dy);
            fullDig(x, y+dy, view, false);
            numdug++;
        }
        for (let dy = -1; dy >= -2; dy--) {
            const otherBlock = view.getBlock(x, y+dy);
            if (!diggable_vines.includes(otherBlock)) break;
            view.addAnimation(x, y+dy, -dy);
            fullDig(x, y+dy, view, false);
            numdug++;
        }
        view.gainItemByCode(gained_item, numdug, undefined);
    }
    else if (dug === 'b') {
        if (view.player.inventory.canGain('..', 1)) {
            fullDig(x, y, view, false);
            // a lot of pebbles
            view.gainItemByCode('..', 25, undefined);
            // a lot of durability
            numdug = 100;
        }
    }
    else if (dug === 'B' && tool_code === 'GB') {
        fullDig(x, y, view, false);
        numdug = 1;
        const blockLeft = view.getBlock(x-1, y);
        if (blockLeft === 'B') {
            fullDig(x-1, y, view, false);
            numdug = 2;
        }
        // some chance of getting pebble
        const xp = x>0?x:-x;
        const yp = y>0?y:-y;
        const rand = ((6789*xp)%11987+(9297*yp)%3873)%(((481*xp%941)*(9871*yp%567))%11111*2+1)
        if (rand % 8 === 1) {
            if (view.player.inventory.canGain('..', 1)) {
                view.gainItemByCode('..', 1, undefined);
            }
            else {
                view.setBlock(x, y, '..');
            }
        }
    }
    else if (dug === 'B' && (tool_code === 'Gy' || tool_code === 'GA')) {
        const success = fullDig(x, y, view, true);
        if (success) {
            numdug = 1;
            force_lose_durability = true;
        }
    }
    else if (dug === 'B' && tool_code === 'WP') {
        if (view.player.inventory.canGain('..', 1)) {
            fullDig(x, y, view, false);
            view.gainItemByCode('..', 1, undefined);
            numdug = 2;
        }
    }
    else {
        // Use grinder like a normal tool
        // (and no loss of durability)
        const success = fullDig(x, y, view, true);
        return success;
    }

    if (numdug > 1 || force_lose_durability) {
        // lose some durability
        if (tool_code === 'WP') {
            // pickaxe breaks
            view.gainItemAtIndex(tool_item.code, slot_index, -1);
            view.notifySend('\\[#ffffff\\]you feel like you should be using a grinder');
            return true;
        }
        view.notifySendAmount('', -numdug, ' durability');

        if (tool_code === 'Gy' && tool_item.data.d-numdug < 1) {
            // yaygrinder has special conditions but does not break
            view.player.inventory.getSlotAt(slot_index).data.d = 1;
        }
        else if (tool_code !== 'Gy' && tool_item.data.d-numdug < 0) {
            // other grinder breaks
            view.gainItemAtIndex(tool_item.code, slot_index, -1);
        }
        else {
            // lose this much durability
            view.player.inventory.getSlotAt(slot_index).data.d -= numdug;
        }
    }

    if (numdug < 1) {
        view.notifySend('no inventory space');
        return false;
    }

    return true;

}

function digresetLogic(view, data) {
    if (view.player.digtime > 0) {
        view.player.digtime = 0;
        return true;
    }
    return false;
}


if (typeof exports !== 'undefined') {
    exports.digLogic = digLogic;
    exports.digresetLogic = digresetLogic;
}
