// Copyright (C) 2018-2022 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}

function falljumpLogic(view, data) {
    if(view.player.gamemode !== 0){
        // falling or jumping makes no sense in gamemode other than 0
        return false;
    }
    const x = view.player.x;
    const y = view.player.y;
    const blockhere = view.getBlock(x, y);
    let j = view.player.jump;
    if (blockhere === 'WO') {
        // stuck on the grabber
        return false;
    }
    if (BP.isClimbable(blockhere)) {
        // no gravity inside ladders
        // but jump meter does get refreshed
        if (j > 0) {
            // restart reset refresh jump
            view.player.jump += -j;
            j = 0;
        }
        return true;
    }
    if (BP.isSwimmable(blockhere)) {
        // infinite jump in water
        if (j > 0) {
            // restart reset refresh jump
            view.player.jump += -j;
            j = 0;
        }
    }
    // wanna jump?
    if(data.jump){
        // is jump allowed?
        var ok = false;
        if (BP.isSwimmable(blockhere)) {
            // always ok to jump in water
            ok = true;
        }
        else if (blockhere === 'WS') {
            // can't jump in quicksand
            ok = false;
        }
        else if(GLS.checkStandSpot(view, x, y)){
            // standing on ground, jump can be started
            // remove whole current jump (set to 0)
            view.player.jump += -j;
            ok = true;
        }
        else if(j < view.player.maxjump){
            ok = true;
        }
        if(ok){
            // okay jumping allowed. increase counter
            view.player.jump += 1;
            if(
                GLS.checkEnterableFromBelow(view, x, y+1)
            ){
                // ok jump
                view.player.y += +1;
                view.playerPosChanged();
                // stop, skipping the falling part
                return true;
            }
            else if (BP.isSwimmable(blockhere)) {
                // can't go up
                // but since it's water here,
                // don't fall
                return true;
            }
            else {
                // epic fail
                // (hit head on ceiling or something)
            }
        }
    }
    // something failed or whatever. Anyway fall instead
    if(!GLS.checkStandSpot(view, x, y)){
        // make jumping impossible because standing on air
        // this is very ugly code, don't look at it too much
        if(view.player.jump < view.player.maxjump || data.jump){
            view.player.jump += view.player.maxjump;
        }
        // ok fall
        view.player.y += -1;
        view.playerPosChanged();
    }
    else{
        // wut there's nowhere to fall
        return data.jump;
        // Here, jumps don't get rejected. They just do nothing.
        // Yes I know it's an ugly solution
    }
    return true;
}

if (typeof exports !== 'undefined') {
    exports.falljumpLogic = falljumpLogic;
}
