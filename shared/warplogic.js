// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

function warpLogic(view, data) {
    const x = view.player.x;
    const y = view.player.y;
    const warpblock = view.getBlock(x, y-1);
    if (warpblock !== 'WW') {
        return false;
    }
    if (view.getBlock(x, y+5) !== 'R1') {
        return false;
    }
    const tiles = view.getTileEntityAt(x, y-1);
    if (tiles.length === 0) {
        console.log('missing warp tile entity');
        return false;
    }
    // there is a tile
    const tile = tiles[0];
    if (tile.t !== 'warp') {
        console.log('bad warp tile type: ', tile.t);
        return false;
    }
    const targetx = tile.d.tx;
    const targety = tile.d.ty;

    // warp animation is only client-side
    if (!view.isServer) {
        for (let dy = -50; dy <= 50; dy++) {
            for (let dx = -50; dx <= 50; dx++) {
                const dist = Math.abs(dx) + Math.abs(dy);
                if (dist > 70) continue;
                view.player.blockanimations.push({
                    x: targetx+3+dx,
                    y: targety-1+dy,
                    b: view.getBlock(x+dx, y+dy),
                    a: dist * 0.3 + Math.random()*3 - 2
                });
            }
        }
    }

    view.player.x = targetx + 3;
    view.player.y = targety - 1;
    view.playerPosChanged();
    if (view.getBlock(targetx, targety+6) === 'R1') {
        // spend R at exit warp
        view.setBlock(targetx, targety+6, ' ');
    }
    else {
        // huh, it's not there.
        // spend R at entry warp instead
        view.setBlock(x, y+5, ' ');
    }
    return true;
}


if (typeof exports !== 'undefined') {
    exports.warpLogic = warpLogic;
}
