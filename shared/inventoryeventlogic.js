// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

function parseSlotId(view, id) {
    if (typeof id !== 'string') {
        console.log('bad slot id: '+id);
        console.trace();
        return false;
    }
    const parts = id.split('_');
    const VALID_INVENTORY_NAMES = ['inv', 'chest', 'recipe'];
    if (parts.length !== 2
        || !VALID_INVENTORY_NAMES.includes(parts[0])
        || isNaN(parts[1])
    ) {
        console.log('bad slot: '+id);
        console.trace();
        return false;
    }
    if (parts[0] === 'recipe') {
        const recipe_index = parseInt(parts[1]);
        return {
            inventory: 'recipe',  // no source inventory
            slot_index: recipe_index,
            callback: ()=>{},
        }
    }
    const inventory = findInventoryByName(view, parts[0]);
    if (inventory === false) return false;
    const slot_index = parseInt(parts[1]);
    return {
        inventory: inventory.inventory,
        slot_index: slot_index,
        callback: inventory.callback,
    };
}


function findInventoryByName(view, name) {
    // name is either "inv" or "chest"
    if (name === 'inv') {
        return {
            inventory: view.player.inventory,
            callback: () => {}
        };
    }
    if (name === 'chest') {
        // (means the player's currently open chest)
        if (view.player.chestSubscription === false) {
            console.log('no chest subscription');
            console.trace();
            return false;
        }
        const chestx = view.getChestX();
        const chesty = view.getChestY();
        if (chestx > view.player.x + 10
            || chestx < view.player.x - 10
            || chesty > view.player.y + 10
            || chesty < view.player.y - 10) {
            // too far. Out of reach.
            return false;
        }
        if (GLS.checkViolateClaimAllowForg(view, chestx, chesty)) {
            return false;
        }
        const tile = view.getMyOpenedChestAt(chestx, chesty);
        if (tile === null) {
            // no tile entities
            return false;
        }
        return {
            inventory: tile.inventory,
            callback: view.isServer ? () => view.syncher.updateTileEntity(tile, view.player.name) : () => {},
        };
    }
    return false;
}


function inventoryEventLogic(view, data) {
    if (typeof data.amount !== 'number'
        && data.amount !== 'swap') {
        console.log('bad amount: '+data.amount);
        return false;
    }
    const source = parseSlotId(view, data.source_slot_id)
    if (source === false) return false;
    if (data.destination_slot_id === 'inv_normal') {
        // shift-clicking to send items to non-reserved inventory
        // swap would not make sense in this context
        if (data.amount === 'swap') return false;
        // sourcing from a recipe is unsupported
        if (source.inventory === 'recipe') return false;
        if (!source.inventory.canMoveItemsTo(
            view.player.inventory, source.slot_index,
            data.amount)) return false;
        source.inventory.moveItemsTo(
            view.player.inventory, source.slot_index, data.amount)
        source.callback();
        return true;
    }
    if (data.destination_slot_id === 'inv_crafting') {
        // shift-clicking to send items to crafting slots
        // swap would not make sense in this context
        if (data.amount === 'swap') return false;
        // sourcing from a recipe is unsupported
        if (source.inventory === 'recipe') return false;
        if (!source.inventory.canMoveItemsToCraftingSlots(
            view.player.inventory, source.slot_index,
            data.amount)) return false;
        source.inventory.moveItemsToCraftingSlots(
            view.player.inventory, source.slot_index,
            data.amount);
        source.callback();
        return true;
    }
    if (data.destination_slot_id === 'chest_any') {
        // shift-clicking to send items to chest
        // swap would not make sense in this context
        if (data.amount === 'swap') return false;
        // sourcing from a recipe is unsupported
        if (source.inventory === 'recipe') return false;
        const chest = findInventoryByName(view, 'chest');
        if (!chest) return false;
        if (!source.inventory.canMoveItemsTo(
            chest.inventory, source.slot_index,
            data.amount)) return false;
        source.inventory.moveItemsTo(
            chest.inventory, source.slot_index, data.amount)
        source.callback();
        chest.callback();
        return true;
    }
    if (data.destination_slot_id === 'default') {
        // no target slot.
        // "default" is (currently) used only in shift+crafting.
        if (source.inventory !== 'recipe') return false;
        // use default crafting functionality in player inventory.
        const recipe_index = source.slot_index;
        // swap would not make sense in this context
        if (data.amount === 'swap') return false;
        const ncrafted = data.amount;
        if (!view.player.inventory.canCraft(recipe_index, ncrafted)) return false;
        view.player.inventory.craft(recipe_index, ncrafted);
        return true;
    }
    const destination = parseSlotId(view, data.destination_slot_id)
    if (destination === false) return false;
    if (destination.inventory === 'recipe') return false;
    if (source.inventory === 'recipe') {
        // crafting
        const recipe_index = source.slot_index;
        // swap would not make sense in this context
        if (data.amount === 'swap') return false;
        const ncrafted = data.amount;
        if (!destination.inventory.canCraftIntoSlot(
            recipe_index, destination.slot_index, ncrafted)) return false;
        destination.inventory.craftIntoSlot(recipe_index, destination.slot_index, ncrafted);
    } else if (data.amount === 'swap') {
        // swap two source and destination slots
        const swap_successful = source.inventory
            .swapSlots(destination.inventory,
                source.slot_index, destination.slot_index);
        // swap should never fail. But just in case, check:
        if (!swap_successful) return false;
    } else {
        // normal item transfer
        const amount = data.amount;
        const move_successful = source.inventory
            .moveItemsToSpecificSlot(destination.inventory,
                source.slot_index, destination.slot_index,
                amount);
        if (!move_successful) return false;
    }
    // TODO: maybe this can be done without callbacks. But need ref to the tile of the chest involved. The problem with callbacks is, that two identical callbacks might be executed for no reason.
    source.callback();
    destination.callback();
    return true;
}


if (typeof exports !== 'undefined') {
    exports.inventoryEventLogic = inventoryEventLogic;
}
