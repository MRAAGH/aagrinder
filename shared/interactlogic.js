// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}


// determines what happens if you click on grass with grass
const cycleGrass = {
    '1':'2',
    '2':'4',
    '4':'8',
    '8':'1',
    '3':'6',
    '6':'c',
    'c':'9',
    '9':'3',
    '5':'a',
    'a':'5',
    'b':'7',
    '7':'e',
    'e':'d',
    'd':'b',
    '0':'a',
    'f':'b',
};

function isFlowSpace(block) {
    return block === ' '
    || block === '~'
    || block === 'w'
    || block === 'sn'
    || block === 'Wi'
    || block[0] === '='
    || (block[0] === '-' && block !== '-8' && block !== '-a')
    || block[0] === '>';
}


function interactLogic(view, data) {
    if (isNaN(data.x)) return false;
    if (isNaN(data.y)) return false;
    const x = view.player.x + parseInt(data.x);
    const y = view.player.y + parseInt(data.y);
    // must be within reach
    if (Math.abs(view.player.x - x) > view.player.reach) return false;
    if (Math.abs(view.player.y - y) > view.player.reach) return false;

    const space = view.getBlock(x, y);

    if (space === 'WF' || space === 'hh') {
        if (GLS.checkViolateClaimAllowForg(view, x, y)) return false;
    } else {
        if (GLS.checkViolateClaim(view, x, y)) return false;
    }

    // which tool?
    let tool_code = '';
    let tool_item = null;
    const slot_index = parseInt(data.slot_index);
    if (slot_index !== -1) {
        tool_item = view.player.inventory.getSlotAt(slot_index);
        if (tool_item !== null) tool_code = tool_item.code;
    }

    // interacting grass on grass (or wire)
    if ((space[0] === '-' || space[0] === '=')
        && space[0] === tool_code) {
        // cycle
        const previousDirection = space[1];
        const newDirection = cycleGrass[previousDirection];
        view.setBlock(x, y, space.replaceAt(1, newDirection));
        // special stuff for wires (triggering circuits by rotating wires)
        if (view.isServer && space[0] === '=') {
            GLS.enqueueWire5(x, y, view);
        }
        return true;
    }

    // interacting on forg with extra data
    if (space === 'WF') {
        // must have data
        if (typeof data.interactedtext !== 'string') return false;
        const word = data.interactedtext;
        // must be repairable item
        if (tool_code[0] !== 'G') return false;
        // must not have max durability
        const max = Inventory.item_data_templates[tool_code].d;
        if (tool_item.data.d >= max) return false;
        // must be four
        if (!/^[a-z][a-z][a-z][a-z]$/.test(word)) return false;
        // on server, check known words
        if (view.isServer) {
            const entity = view.getTileEntityAt(
                parseInt(x), parseInt(y));
            let ok = true;
            if (entity.length !== 1) ok = false;
            else if (entity[0].t !== 'forg') ok = false;
            else if (entity[0].d.know.indexOf(word.substr(1,3)) > -1) ok = false;
            if (ok) {
                // successful repair!
                // append word so it can't be used again.
                entity[0].d.know += word;
            } else {
                // something failed on server!
                // but succeeded on client
                // so we need to force-update state on client
                view.surpriseUpdateInventory();
                // succeed anyway, to keep consistency with client
                return true;
            }
        }
        // repair item (is safe on server, unsafe on client)
        tool_item.data.d = max;
        view.notifyPrint('your item been fixd');
        return true;
    }

    // switch
    if (space === 'sw') {
        view.setBlock(x, y, 's1');
        if (view.isServer) {
            GLS.enqueueWire4(x, y, view);
        }
        return true;
    }
    if (space === 's1') {
        view.setBlock(x, y, 'sw');
        if (view.isServer) {
            GLS.enqueueWire4(x, y, view);
        }
        return true;
    }
    if (space === 'O0') {
        view.setBlock(x, y, 'O1');
        if (view.isServer) {
            GLS.enqueueWire5(x, y, view);
        }
        return true;
    }

    // fruit picking
    if (space[0] === 'f' && space !== 'fc' && space !== 'fR') {
        const item = Inventory.block2item(space);
        if (view.player.inventory.canGain(item, 1)) {
            if (space === 'fm') {  // special case for ananas
                view.setBlock(x, y, ' ');
            } else {
                // all other fruits become leaf
                view.setBlock(x, y, 'a' + space[1]);
            }
            view.gainItemByCode(item, 1);
            return true;
        }
        else {
            view.notifySend('no inventory space');
            return false;
        }
    }

    // trapdoor toggle
    if (space === 'st') {
        view.setBlock(x, y, 'sT');
        return true;
    }
    if (space === 'sT') {
        view.setBlock(x, y, 'st');
        return true;
    }

    // flip hopper
    if (space[0] === 'p') {
        let block = undefined;
        if (space === 'p<') block = 'pv';
        else if (space === 'p>') block = 'p<';
        else if (space === 'pv') block = 'p>';

        view.setBlock(x, y, block);
        if (view.isServer) {
            view.hopperInvalidate(x, y);
            view.processHopper(x, y, block);
        }
        return true;
    }

    // potentially a chest tile entity (hidden from client)
    if (view.isServer) {
        const tiles = view.getTileEntityAt(x, y);
        if (tiles.length > 0) {
            // there is a tile
            const tile = tiles[0];
            if (tile.t === 'chest') {
                // found chest!
                view.subscribeChest(tile);
            }
        }
    }

    // handle interacting with aaterminal
    if (space === 'sm') {
        // only has effect on client (open aaterminal UI)
        if (!view.isServer) {
            if (view.syncher.player.focusAAterminalByCoordinates(x, y)) {
                // found an opened terminal at these coordinates
                return true;
            }
            if (view.syncher.player.openedAAterminals.length < MAX_OPENED_AATERMINALS) {
                view.syncher.player.openedAAterminals.push(
                    {x:x, y:y});
                view.syncher.player.focusAAterminalByCoordinates(x, y);
            }
        }
        return true;
    }

    // handle all bucket types
    if (tool_code[0] === 'u' && tool_code[1] !== 'n') {
        let bucketItemToGain = undefined;
        let blockToSet = undefined;
        const blockAnimations = [];
        if (tool_code === 'ue') {
            // Filling this empty bucket
            if (space === 'w') {
                bucketItemToGain = 'uw';
            } else if (space === '~') {
                bucketItemToGain = 'u~';
            } else if (space === 'sn' || space === 'sN') {
                bucketItemToGain = 'us';
            } else if (space === 'WS') {
                bucketItemToGain = 'uB';
            } else if (space === 'W~') {
                bucketItemToGain = 'uc';
            } else if (space === 'undefined') {
                bucketItemToGain = 'uu';
            } else {
                return true;
            }
            blockToSet = ' ';
        } else {
            // Emptying this non-empty bucket
            bucketItemToGain = 'ue';
        }
        if (tool_code === 'uw') {  // Water bucket
            // Watering tree?
            if (space[0] === 't') {
                // Yes, watering tree
                // Tree skip to final stage
                blockToSet = space[0] + space[1] + '2';
                blockAnimations.push({x:x, y:y+1, d:2, b:'w'});
                blockAnimations.push({x:x, y:y, d:7, b:'tw'});
                blockAnimations.push({x:x, y:y, d:3, b:'w'});
                blockAnimations.push({x:x, y:y, d:1, b:space});
            } else if (isFlowSpace(space)) {
                blockToSet = 'w';
            } else {
                // Can't place water here
                return true;
            }
        }
        else if (tool_code === 'u~') {  // Cloud bucket
            if (space !== ' ') {
                // Can't place cloud here
                return true;
            }
            blockToSet = '~';
        }
        else if (tool_code === 'uc') {  // Cotton candy bucket
            if (space !== ' ') {
                // Can't place cotton candy here
                return true;
            }
            blockToSet = 'W~';
        }
        else if (tool_code === 'us') {  // Snow bucket
            if (space !== ' '
                && space !== 'w'
                && space !== '~'
                && space !== 'sn'
                && space[0] !== '>') {
                // Can't place snow here
                return true;
            }
            blockToSet = 'sN';
        }
        else if (tool_code === 'uB') {  // Quicksand bucket
            if (space !== ' '
                && space !== 'w'
                && space !== '~'
                && space !== 'sn'
                && space[0] !== '>') {
                // Can't place quicksand here
                return true;
            }
            blockToSet = 'WS';
        }
        else if (tool_code === 'uu') {  // Bucket of undefined
            if (space !== ' '
                && space !== 'w'
                && space !== '~'
                && space !== 'sn'
                && space[0] !== '>') {
                // Can't place quicksand here
                return true;
            }
            blockToSet = 'undefined';
        }

        const bucket_item = view.player.inventory.getSlotAt(slot_index);
        if (bucket_item.amount === 1) {
            // Only one bucket in this slot.
            // So gain new item in same slot
            view.gainItemAtIndex(tool_code, slot_index, -1);
            view.gainItemAtIndex(bucketItemToGain, slot_index, 1);
        }
        else if (view.player.inventory.canGain(bucketItemToGain, 1)) {
            view.gainItemAtIndex(tool_code, slot_index, -1);
            view.gainItemByCode(bucketItemToGain, 1);
        }
        else {
            // Impossible to gain new bucket
            view.notifySend('no inventory space');
            return true;
        }

        view.setBlock(x, y, blockToSet);

        for (const a of blockAnimations) {
            view.addAnimation(a.x, a.y, a.d, a.b);
        }

        if (view.isServer) {
            if (tool_code === 'uw') {
                GLS.checkEnqueueWater(x, y, view);
            }
            GLS.checkEnqueueWater(x+1, y, view);
            GLS.checkEnqueueWater(x-1, y, view);
            GLS.checkEnqueueWater(x, y+1, view);
            GLS.checkEnqueueWater(x, y-1, view);
        }
        return true;
    }

    return true;

}

if (typeof exports !== 'undefined') {
    exports.interactLogic = interactLogic;
}

