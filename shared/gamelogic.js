// Copyright (C) 2018-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
A collection of client-triggered action functions,
abstracted so it can be used by both node.js server and browser client.
*/

if (typeof exports !== 'undefined') {
    Inventory = require('../shared/inventory').Inventory;
    falljumpLogic = require('../shared/falljumplogic').falljumpLogic;
    updownLogic = require('../shared/updownlogic');
    upLogic = updownLogic.upLogic;
    downLogic = updownLogic.downLogic;
    leftrightLogic = require('../shared/leftrightlogic');
    leftLogic = leftrightLogic.leftLogic;
    rightLogic = leftrightLogic.rightLogic;
    diggrinderLogic = require('../shared/diggrinderlogic');
    digLogic = diggrinderLogic.digLogic;
    digresetLogic = diggrinderLogic.digresetLogic;
    placeLogic = require('../shared/placelogic').placeLogic;
    interactLogic = require('../shared/interactlogic').interactLogic;
    inventoryEventLogic = require('../shared/inventoryeventlogic').inventoryEventLogic;
    eatLogic = require('../shared/eatlogic').eatLogic;
    magicallygetitemLogic = require('../shared/magicallygetitemlogic').magicallygetitemLogic;
    warpLogic = require('../shared/warplogic').warpLogic;
}

const sharedActionFunctions = {
    'falljump': falljumpLogic,
    'left': leftLogic,
    'right': rightLogic,
    'down': downLogic,
    'up': upLogic,
    'dig': digLogic,
    'digreset': digresetLogic,
    'place': placeLogic,
    'interact': interactLogic,
    'inventoryevent': inventoryEventLogic,
    'eat': eatLogic,
    'magicallygetitem': magicallygetitemLogic,
    'warp': warpLogic,
};


if (typeof exports !== 'undefined')
    exports.sharedActionFunctions = sharedActionFunctions;
