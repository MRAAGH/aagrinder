// Copyright (C) 2018-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * SHARED FILE
 *
 * Class for holding raw data about terrain.
 * Each block is a string of one or more characters.
 * The "compressed" format is nothing more than a serialized
 * representation of the contained strings.
 * Data arrives in serialized form and needs to be parsed.
 *
 * parsing function (deserialize) is exported separately
 * and can be used elsewhere for other purposes.
 */

"use strict";

class Chunk {
    constructor(){
        this.terrain = undefined; // a 2d string array
        this.biomes = undefined; // serialized string
        this.tileEntities = [];
        this.subscribers = []; // only used on the server
        this.dirty = false; // only used on the server
        this.persistent = false; // only used on the server
    }

    setCompressed(str){
        const res = deserialize(256, 256, str);
        this.terrain = res.terrain;
        return res.err;
    }

    getCompressed(){
        return this.terrain.map(t=>t.join('')).join('');
    }

}

function deserialize(h, w, str){
    const terrain = [];
    let offset = 0;
    for(let y = 0; y < h; y++){
        terrain[y] = [];
        for(let x = 0; x < w; x++){
            let block = str[y * w + x + offset];
            if(
                block !== ' '
                && block !== 'B'
                && block !== 'w'
                && block !== 'd'
                && block !== 'S'
                && block !== 'b'
                && block !== '~'
            ){
                if (block === '=') {
                    block += str[y * w + x + offset + 1];
                    offset++;
                    block += str[y * w + x + offset + 1];
                    offset++;
                }
                if (
                    block === 't'
                    || block === ':'
                ) {
                    block += str[y * w + x + offset + 1];
                    offset++;
                }
                block += str[y * w + x + offset + 1];
                offset++;
                // Wa is the beginning of a decaying leaf block
                // Wf is the beginning of a placed fruit block
                // Na is the beginning of NaN (yes in Javascript
                // this needs to be taken into account)
                if (block === 'Wa' || block === 'Wf' || block === 'Na') {
                    block += str[y * w + x + offset + 1];
                    offset++;
                }
                // un is the beginning of undefined or unknownnn
                if (block === 'un') {
                    block = str.substr(y * w + x + offset - 1, 9);
                    offset += 7;
                }
                // nu is the beginning of null
                if (block === 'nu') {
                    block = str.substr(y * w + x + offset - 1, 4);
                    offset += 2;
                }
            }
            terrain[y][x] = block;
        }
    }
    const err = str.length-(h*w+offset);
    return {err: err, terrain: terrain};
}

if (typeof exports !== 'undefined') {
    exports.Chunk = Chunk;
    exports.deserialize = deserialize;
}
