// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Common functions for determining properties of specific blocks.
 * Used in shared client&server game logic.
 * Also used for various server-only mechanics.
 */

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

class BlockProps {

    static isEnterable(block) {
        // blocks the player can move into
        return block === ' '
            || block === '~'
            || block === 'w'
            || block === '..'
            || block === 'sn'
            || block === 'si'
            || block === 'WA'
            || block === 'WO'
            || block === 'WR'
            || block === 'WD'
            || block === 'WS'
            || block === 'Wi'
            || block === 'sB'
            || block === 'st'
            || block === 'sT'
            || block === 'WI'
            || block === 'WC'
            || block === 'WX'
            || block === 'W~'
            || block[0] === 'A'
            || block[0] === 'a'
            || block[0] === 'f'
            || block[0] === 'W' && (block[1] === 'a' || block[1] === 'f')
            || block[0] === '>'
            || block[0] === 'H'
            || block[0] === 'h'
            || block[0] === '#'
            || block[0] === 't'
            || block[0] === '*'
            || block[0] === '-'
            || block[0] === '='
            || block[0] === '%'
            || block[0] === ':'
            || block[0] === '|'
        ;
    }

    static hasOnlyBottomSurface(block) {
        // blocks the player can stand inside
        // without falling out
        // (not climbing)
        return block === 'Wi'
            || block === 'sB'
            || block === 'st'
        ;
    }

    static hasOnlyTopSurface(block) {
        // blocks the player can stand on
        // but can also go inside
        // (not climbing)
        return block === 'sT'
        ;
    }

    static isNotSurface(block) {
        // blocks the player can not stand on
        return block === ' '
            || block === '~'
            || block === 'w'
            || block === '..'
            || block === 'sn'
            || block === 'si'
            || block === 'WO'
            || block === 'WD'
            || block === 'WS'
            || block === 'Wi'
            || block === 'sB'
            || block === 'st'
            || block === 'WC'
            || block[0] === 'A'
            || block[0] === 'a'
            || block[0] === 'f'
            || block[0] === 'W' && (block[1] === 'a' || block[1] === 'f')
            || block[0] === '>'
            || block[0] === '#'
            || block[0] === 't'
            || block[0] === '*'
            || block[0] === '-'
            || block[0] === '='
            || block[0] === ':'
            || block[0] === '|'
        ;
    }

    static isUndiggable(block, tool = ' ') {
        // blocks that can't be destroyed by player
        if (block === 'b') {
            return !(tool[0] === 'G' && tool !== 'GB' && tool !== 'GH' && tool !== 'GW');
        }
        if (block === 'b'
            || block === 'WW'
            || block === 'WF'
            || block === 'NaN'
            || block === 'null'
            || block === 'undefined') return true;

        // GW can currently not dig
        //  - hopper due to lack of updates
        //  - diamond due to lack of updates
        //  - corrupted stone due to lack of updates
        //  - clientside tile entities due to lack of synchronization
        if (tool === 'GW') return block[0] === 'p'
            || block === 'DD'
            || block === 'WB'
            || block === 'si'
            || block[0] === 'G';

        // other grinders can not dig "empty spaces"
        return block === ' '
            || block === 'w'
            || block[0] === '>'
            || block === '~'
            || block === 'W~';
    }

    static digDuration(block, tool) {
        if (block === 'si') return 15;
        if (block === 'sm') return 15;
        if (block === 'sf') return 15;
        if (block === 'WS') return 50;
        if (block[0] === 'E') return 15;
        if (block[0] === 'h') return 6;
        if (block === 'B'
            || block[0] === 'X'
            || block === 'WI'
            || block === 'WT') {
            if (tool === ' ') return 10;
            if (tool === 'GH' || tool === 'WP') return 4;
            if (tool === 'G0') return 3;
            if (tool === 'Gy') return 0;
            if (tool === 'GW') return 0;
            return 2;
        }
        if (block === 'Dm' || block === 'Dg') {
            if (tool === ' ' || tool === 'WP') return 15;
            if (tool === 'GH') return 5;
            return 2;
        }
        if (block === 'WB') {
            if (tool === ' ') return 10;
            if (tool === 'WP') return 6;
            return 3;
        }
        if (block[0] === 'a'
            || block[0] === 'A'
            || block === 'Hm'
            || (block[0] === 'W' && block[1] === 'a')
            || block === 'WA'
            || block === 'WO'
            || block === 'WR'
            || block[0] === '*') {
            if (tool === ' ' || tool === 'WP') return 2;
            return 0;
        }
        if (block === 'd'
            || block === 'S'
            || block[0] === '-'
            || block[0] === 'H'
            || block[0] === '|') {
            if (tool === ' ' || tool === 'WP') return 4;
            if (tool === 'GH') return 3;
            if (tool === 'GW') return 0;
            return 0;
        }
        if (block[0] === '?') {
            if (tool === ' ') return 8;
            if (tool === 'WP') return 5;
            return 2;
        }
        if (block === 'sB'
            || block === 'DD'
            || block[0] === 'R'
            || block[0] === 'F'
            || block[0] === 'p'
            || block[0] === 'M'
            || block[0] === 'O'
            || block === 'sw'
            || block === 's1'
            || block === 'sc'
            || block === 'sC'
            || block === 'Wp'
            || block === 'WX'
            || block === 'Wc'
            || block === 'WC') {
            if (tool === ' ' || tool === 'WP') return 5;
            return 0;
        }
        if (block === 'b') {
            return 100;
        }
        return 0;
    }

    static isLadder(block) {
        // blocks that act as a climbing surface
        return block[0] === 'H'
            || block[0] === 'h'
            || block[0] === '%'
            || block === 'WI'
            || block === 'WX'
            || block === 'WA'
            || block === 'WR'
            || block === 'WO';
    }

    static isClimbable(block) {
        // blocks that act as a climbing surface but
        // do not necessarily count as being inside a ladder
        // (this distinction is needed to fix the
        // behavior on a snowy wooden bridge)
        return block[0] === 'H'
            || block[0] === 'h'
            || block[0] === '%'
            || block === '-2'
            || block === '-a'
            || block === '-b'
            || block === '-e'
            || block === '-f'
            || block === 'WI'
            || block === 'WX'
            || block === 'W~'
            || block === 'sn'
            || block === 'WO'
            || block === 'WA'
            || block === 'WR';
    }

    static isClimbSprintable(block) {
        // blocks that act as a climbing surface
        return block[0] === 'H'
            || block[0] === 'h'
            || block === 'WI'
            || block === 'WX';
    }

    static isSwimmable(block) {
        return block === 'w'
            || block[0] === '>';
    }

    static isUnstable(block) {
        // blocks that break if the block below disappears
        return block === '*|'
            || block[0] === 't'
            || block === 'Wt'
            || block === 'sn';
    }

    static isSolidBlock(block) {
        const b0 = block[0];
        return (block !== ' ')
        && (b0 !== 'w')
        && (b0 !== '>')
        && (b0 !== '~')
        && (b0 !== 't')
        && (b0 !== 'f')
        && (b0 !== '=')
        && (b0 !== ':')
        && (b0 !== '%')
        && (b0 !== '-')
        && (b0 !== '*')
        && (b0 !== '#')
        && (b0 !== 'p')
        && (b0 !== '|')
        && (b0 !== 'W~')
        && (block !== 'sn')
        && (block !== 'sB')
        && (block !== 'st')
        && (block !== 'sp')
        && (block !== 'sT')
        && (block !== '..')
        && (block !== 'Wi');
    }

    static validquestions = '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~€';

}

if (typeof exports !== 'undefined') {
    exports.BlockProps = BlockProps;
} else {
    BP = BlockProps;
}
