// Copyright (C) 2022-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * SHARED FILE
 *
 * Class for working with aagrinder markup and control characters
 */

function AASTRING_RAW(str) {
    return str.replaceAll('\\[s\\]',' ').replace(/\\\[[^\\]*\\\]/g,'');
}

function AASTRING_DECODE(str, defaultColor='') {
    let color = [];
    let currentColor = defaultColor;
    let modifiers = [];
    let currentModifiers = 0;
    const lines = [];
    let lastNewline = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '\\') {
            // might be a control code
            const sub = str.substr(i);
            const match = sub.match(/^\\\[([^\\]*)\\\]/);
            if (match !== null) {
                const controlseq = match[0];
                i += controlseq.length-1;  // skip parsing control code
                const code = match[1];
                if (code === '/') {  // clear formatting
                    currentColor = defaultColor;
                    currentModifiers = 0;
                }
                else if (code[0] === '#') {  // set color
                    currentColor = code;
                }
                else if (code[0] === 'm') {  // set modifiers
                    currentModifiers = parseInt(code.substring(1));
                }
                else if (code[0] === 'M') {  // add modifiers
                    currentModifiers |= parseInt(code.substring(1));
                }
                else if (code === 'n') {  // newline
                    lines.push({
                        raw: AASTRING_RAW(str.substring(lastNewline, i+1)),
                        color: color,
                        modifiers: modifiers,
                    });
                    color = [];
                    modifiers = [];
                    lastNewline = i+1;
                }
                else if (code === 's') {  // escaped space
                    color.push(currentColor);
                    modifiers.push(currentModifiers);
                }
                continue;
            }
        }
        color.push(currentColor);
        modifiers.push(currentModifiers);
    }
    lines.push({
        raw: AASTRING_RAW(str.substring(lastNewline)),
        color: color,
        modifiers: modifiers,
    });
    return lines;
}

function AASTRING_DECODE_WITH_WRAPPING(str, width, defaultColor='') {
    const linesNoWrap = AASTRING_DECODE(str, defaultColor);
    const linesWrap = [];
    for (const lineNoWrap of linesNoWrap) {
        while (lineNoWrap.raw.length > width) {
            const spaceIndex = lineNoWrap.raw.lastIndexOf(' ', width);
            const cutIndex = spaceIndex > 0 ? spaceIndex : width;
            const nextStartIndex = spaceIndex > 0 ? cutIndex+1 : cutIndex;
            linesWrap.push({
                raw: lineNoWrap.raw.substring(0, cutIndex),
                color: lineNoWrap.color.slice(0, cutIndex),
                modifiers: lineNoWrap.modifiers.slice(0, cutIndex),
            });
            lineNoWrap.raw = lineNoWrap.raw.substring(nextStartIndex);
            lineNoWrap.color = lineNoWrap.color.slice(nextStartIndex);
            lineNoWrap.modifiers = lineNoWrap.modifiers.slice(nextStartIndex);
        }
        linesWrap.push(lineNoWrap);
    }
    return linesWrap;
}

function AASTRING_WRAP_WITHOUT_DECODING(str, width) {
    const lines = [''];
    let currentLineLength = 0;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '\\') {
            // might be a control code
            const sub = str.substr(i);
            const match = sub.match(/^\\\[([^\\]*)\\\]/);
            if (match !== null) {
                const controlseq = match[0];
                i += controlseq.length-1;  // skip parsing control code
                const code = match[1];
                if (code === 'n') {  // newline
                    currentLineLength = 0;
                    lines.push('');
                    continue;  // skip appending control sequence to line
                }
                if (code === 's') {  // escaped space
                    // line length increases like a normal character
                    // and also it might go into a new line
                    if (currentLineLength >= width) {
                        lines.push('');
                        currentLineLength = 0;
                    }
                    currentLineLength++;
                }
                lines[lines.length-1] += controlseq;  // append it to the line
                continue;
            }
        }
        // regular character
        if (currentLineLength >= width) {
            // check for exceptions that do not wrap
            const incompleteMatch = lines[lines.length-1].match(/\\(\[(([mM](\d)?|[/ns]|#.{0-6})(\\)?)?)?$/);
            if (incompleteMatch !== null
                && incompleteMatch.index < i+width-currentLineLength) {
                // the input string ends with an incomplete control sequence
                // this control sequence begins BEFORE the wrap point
                // so it should not be wrapped (yet)

            } else {
                lines.push('');
                currentLineLength = 0;
            }
        }
        lines[lines.length-1] += str[i];
        currentLineLength++;
    }
    return lines;
}

function AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(lines, width) {
    if (lines.length < 1) return lines;
    const rewrapped = AASTRING_WRAP_WITHOUT_DECODING(lines[lines.length-1], width);
    if (rewrapped.length < 2) return lines;
    lines[lines.length-1] = rewrapped[0];
    for (let i = 1; i < rewrapped.length; i++) {
        lines.push(rewrapped[i]);
    }
    return lines;
}

function AASTRING_MAKE_COPY(lines) {
    const copiedlines = [];
    for (const line of lines) {
        copiedlines.push(line);
    }
    return copiedlines;
}

function AASTRING_LENGTH(str) {
    return AASTRING_RAW(str).length;
}

function AASTRING_SANITIZE(str) {
    let len = str.length;
    let len_prev;
    do {
        str = str.replace(/\\\[|\\\]/g,'');
        len_prev = len;
        len = str.length;
    } while (len < len_prev);
    return str;
}

function AASTRING_FADE_TO_COLOR(str, color2, intensity, defaultColor='#fff') {
    const fadedDefaultColor = FADE_COLORS_HEX(
        defaultColor, color2, intensity);
    const fadedDefaultColorString = '\\[' + fadedDefaultColor + '\\]';
    let outputstr = fadedDefaultColorString; // start with new default
    for (let i = 0; i < str.length; i++) {
        if (str[i] !== '\\') {
            outputstr += str[i];
            continue;
        }
        // might be a color control code
        const sub = str.substr(i);
        const match = sub.match(/^\\\[([^\\]*)\\\]/);
        if (match === null) {
            outputstr += str[i];
            continue;
        }
        const controlseq = match[0];
        const code = match[1];
        if (code === '/') {  // clear formatting
            // becomes new default code
            outputstr += fadedDefaultColorString;
        }
        else if (code[0] === '#') {  // set color
            // becomes a faded color
            const fadedColor = FADE_COLORS_HEX(
                code, color2, intensity);
            outputstr += '\\[' + fadedColor + '\\]';
            i += controlseq.length-1;  // skip parsing control code
        }
        else {  // any other control code
            // left unchanged
            outputstr += controlseq;
            i += controlseq.length-1;  // skip parsing control code
        }
    }
    return outputstr;
}

function ADD_COLORS_RGB(a, b, intensity) {
    let dR = intensity*b[0];
    dR = dR>0?Math.floor(dR):Math.ceil(dR);
    let dG = intensity*b[1];
    dG = dG>0?Math.floor(dG):Math.ceil(dG);
    let dB = intensity*b[2];
    dB = dB>0?Math.floor(dB):Math.ceil(dB);
    return [a[0]+dR, a[1]+dG, a[2]+dB];
}

function FADE_COLORS_RGB(a, b, intensity) {
    let dR = intensity*(b[0]-a[0]);
    dR = dR>0?Math.floor(dR):Math.ceil(dR);
    let dG = intensity*(b[1]-a[1]);
    dG = dG>0?Math.floor(dG):Math.ceil(dG);
    let dB = intensity*(b[2]-a[2]);
    dB = dB>0?Math.floor(dB):Math.ceil(dB);
    return [a[0]+dR, a[1]+dG, a[2]+dB];
}

function ADD_COLORS_HEX(a, b, intensity) {
    return (RGB2HEX(ADD_COLORS_RGB(HEX2RGB(a),HEX2RGB(b),intensity)));
}

function FADE_COLORS_HEX(a, b, intensity) {
    return (RGB2HEX(FADE_COLORS_RGB(HEX2RGB(a),HEX2RGB(b),intensity)));
}

// this function is not concise at all
// but I think the current form is the most readable.
function HEX2RGB(hex) {
    if (hex.length === 7) {  // #33aa99
        const r = parseInt(hex.substring(1,3), 16);
        const g = parseInt(hex.substring(3,5), 16);
        const b = parseInt(hex.substring(5,7), 16);
        return [r,g,b];
    }
    if (hex.length === 6) {  // 33aa99
        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
        return [r,g,b];
    }
    if (hex.length === 4) {  // #3a9
        const r = parseInt(hex[1], 16);
        const g = parseInt(hex[2], 16);
        const b = parseInt(hex[3], 16);
        return [r,g,b];
    }
    if (hex.length === 3) {  // 3a9
        const r = parseInt(hex[0], 16);
        const g = parseInt(hex[1], 16);
        const b = parseInt(hex[2], 16);
        return [r,g,b];
    }
    // fallback
    return [255, 255, 255];
}

function RGB2HEX(rgb) {
    // sanitize
    let sanergb = [];
    for (let i = 0; i < 3; i++) {
        if (rgb[i] < 0) sanergb[i] = '00';
        else if (rgb[i] > 255) sanergb[i] = 'ff';
        else {
            sanergb[i] = rgb[i].toString(16);
            if (sanergb[i].length < 2)
                sanergb[i] = '0'+sanergb[i];
        }
    }
    // serialize
    return '#'+sanergb[0]+sanergb[1]+sanergb[2];
}

if (typeof exports !== 'undefined') {
    exports.AASTRING_RAW = AASTRING_RAW;
    exports.AASTRING_DECODE = AASTRING_DECODE;
    exports.AASTRING_LENGTH = AASTRING_LENGTH;
    exports.AASTRING_SANITIZE = AASTRING_SANITIZE;
    exports.AASTRING_FADE_TO_COLOR = AASTRING_FADE_TO_COLOR;
    exports.FADE_COLORS_HEX = FADE_COLORS_HEX;
    exports.AASTRING_DECODE_WITH_WRAPPING = AASTRING_DECODE_WITH_WRAPPING;
    exports.AASTRING_WRAP_WITHOUT_DECODING = AASTRING_WRAP_WITHOUT_DECODING;
    exports.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING = AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING;
    exports.AASTRING_MAKE_COPY = AASTRING_MAKE_COPY;
}
