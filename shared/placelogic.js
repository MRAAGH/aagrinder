// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
High-level description of the game logic triggered by clients
that also applies to the server.

The abstraction here is the view, allowing the same interface
on the client and on the server.
The view is part of the Syncher and makes sure changes do not
interfere with client-server synchronization.

Write operations (which change state of player or world)
ABSOLUTELY MUST be done through dedicated methods of the View,
otherwise rollback is impossible!
*/

/*
THE AAGRINDER SERVER EXPECTS THIS CODE TO BE RUNNING ON THE CLIENT.
IF THE CLIENT IS NOT RUNNING THIS CODE OR EQUIVALENT,
THE SERVER AND CLIENT WILL GO OUT OF SYNCH.
*/

if (typeof exports !== 'undefined') {
    BP = require('../shared/blockprops').BlockProps;
    GLS = require('../shared/gamelogicstandard').GameLogicStandard;
}


// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}


// Determines how a block changes when it sticks to the block above
const stickTop = {
    '0':'8',
    '1':'9',
    '2':'a',
    '3':'b',
    '4':'c',
    '5':'d',
    '6':'e',
    '7':'f',
};

// Determines how a block changes when it sticks to the block below
const stickBottom = {
    '0':'2',
    '1':'3',
    '4':'6',
    '5':'7',
    '8':'a',
    '9':'b',
    'c':'e',
    'd':'f',
};

// Determines how a block changes when it sticks to the block on the left
const stickLeft = {
    '0':'1',
    '2':'3',
    '4':'5',
    '6':'7',
    '8':'9',
    'a':'b',
    'c':'d',
    'e':'f',
};

// Determines how a block changes when it sticks to the block on the right
const stickRight = {
    '0':'4',
    '1':'5',
    '2':'6',
    '3':'7',
    '8':'c',
    '9':'d',
    'a':'e',
    'b':'f',
};

function placeLogic(view, data) {
    let x = view.player.x + data.x;
    let y = view.player.y + data.y;
    if (Math.abs(view.player.x - x) > view.player.reach
        || Math.abs(view.player.y - y) > view.player.reach) {
        // should not be able to reach!
        return false;
    }

    const space = view.getBlock(x, y);

    if (space === 'WF' || space === 'hh') {
        // A different check, just to prevent
        // "owned by  four forg" notification appearing
        if (GLS.checkViolateClaimAllowForg(view, x, y)) return false;
    } else {
        if (GLS.checkViolateClaim(view, x, y)) return false;
    }

    // what to place?
    const slot_index = parseInt(data.slot_index);
    if (slot_index === -1) {
        // wants to place something that is not from an inventory slot.
        // must have additional data
        if (typeof data.placedtext !== 'string') return false;
        // placing invalid slot on ?? (to modify it)
        if (space === '??') {
            if (BP.validquestions.indexOf(data.placedtext) === -1) return false;
            view.setBlock(x, y, '?'+data.placedtext, true);
            return true;
        }
        // placing invalid slot on si (to modify it)
        if (space === 'si'){
            // make sure to delete previous tile entity if exists
            view.playerDeleteTileEntityAt(x, y);
            // create sign tile entity
            view.playerCreateTileEntity({
                t: 'sign',
                x: x,
                y: y,
                d: {t: data.placedtext},
            });
            return true;
        }
        // placing WV from invalid slot to create a warp
        if (data.placedtext.length > 3
            && data.placedtext[0] === 'W'
            && data.placedtext[1] === 'V') {
            const warpscope = data.placedtext[2];
            const warpcode = data.placedtext.substr(3);
            // spend item
            if (view.player.gamemode === 0) {
                if (!view.player.inventory.hasItem('WV')) return false;
                view.gainItemByCode('WV', -1, undefined);
            }
            const scopeOk = ['o', 'f', 'a'].includes(warpscope);
            if (!scopeOk) return false;
            // WV code only exists on server
            if (view.isServer) {
                const success = view.attemptCreateCustomWarp(x, y, warpcode, warpscope);
                if (success) {
                    // notify player
                    const quotedwarpcode = warpcode.indexOf(' ') > 0 ?
                        '"' + warpcode + '"' : warpcode;
                    view.player.socket.emit('chat',
                        {message: 'created warp at ' + x + ', ' + y
                            + '.\\[n\\]use with /warp ' + quotedwarpcode
                            + '\\[n\\]remove with /takewarp ' + quotedwarpcode});
                } else {
                    // notify player
                    view.player.socket.emit('chat',
                        {message: 'warp code already taken!'});
                    // force item back (because on client it was spent)
                    if (view.player.gamemode === 0) {
                        view.surpriseGainItemInverted('WV', 1, undefined);
                    }
                }
            }
            // return true (client has to assume this was successful)
            return true;
        }
        // invalid slot but no special action possible.
        // (probably hacking)
        return false;
    }

    const item = view.player.inventory.getSlotAt(slot_index);
    if (item === null) return false;
    const item_code = item.code;

    // un-placable
    if (item_code === 'WV') return false;

    // must be an empty space
    if (space !== ' '
        && space !== 'w'
        && space !== '~'
        && space !== 'sn'
        && space[0] !== '>') {
        return false;
    }

    const relativeCheck = (rx,ry)=>{
        return view.getBlock(x+rx, y+ry);
    }
    const block_code = Inventory.item2block(item_code, relativeCheck);

    if (!BP.isEnterable(block_code)) {
        // must not be a player
        if (view.isPlayer(x,y)) {
            return false;
        }
    }
    // can't place FOLLOW
    if (item_code === 'Wo') {
        return false;
    }
    // can't place bucket
    if (item_code[0] === 'u') {
        return false;
    }
    // can't place pickaxe
    if (item_code[0] === 'WG') {
        return false;
    }
    // tiny tree only on dirt
    // (or on stone, if it is type tb)
    // (or on sand, if it is type tm)
    if (item_code[0] === 't' && view.getBlock(x, y-1) !== 'd'
        && !(item_code[1] === 'b' && view.getBlock(x, y-1) === 'B')
        && !(item_code[1] === 'm' && view.getBlock(x, y-1) === 'S')) {
        return false;
    }
    // can't place snow layer on non-solid block
    if (item_code === 'sn' && !BP.isSolidBlock(view.getBlock(x, y-1))) {
        return false;
    }
    // flower has special placement procedure (override)
    if (item_code[0] === '*') {
        // can't if not on dirt
        if (view.getBlock(x, y-1) !== 'd') {
            return false;
        }
        // can't if no space above
        if (view.getBlock(x, y+1) !== ' '
            && view.getBlock(x, y+1) !== 'w') {
            return false;
        }
        // ok place the flower
        view.setBlock(x, y+1, item_code, true);
        // and the stem
        view.setBlock(x, y, '*|', true);
        if (view.player.gamemode === 0) {
            view.gainItemAtIndex(item_code, slot_index, -1, undefined);
        }
        return true;
    }
    // create grinder tile entity if placing grinder
    if (item_code[0] === 'G') {
        // grinder has durability (d) data
        // make a new object, to avoid sharing references
        const gdata = {d: item.data.d};
        view.playerCreateTileEntity({
            t: 'grinder',
            x: x,
            y: y,
            d: gdata,
        });
    }
    // ok place here
    view.setBlock(x, y, block_code, true);
    if (view.player.gamemode === 0) {
        view.gainItemAtIndex(item_code, slot_index, -1, undefined);
    }
    // connect surrounding grass or wire to this block
    if (block_code[0] === '-'
        || Inventory.isWireAttachment(block_code)
    ) {
        // okay it is one of the blocks that might cause others to connect
        const stickblock = block_code[0] === '-' ? '-' : '=';
        { // stick the top grass
            const otherBlock = view.getBlock(x, y+1);
            if (otherBlock[0] === stickblock) {
                const found = stickTop[otherBlock[1]];
                if (found) {
                    view.setBlock(x, y+1, otherBlock.replaceAt(1, found));
                }
            }
        }
        { // stick the bottom grass
            const otherBlock = view.getBlock(x, y-1);
            if (otherBlock[0] === stickblock) {
                const found = stickBottom[otherBlock[1]];
                if (found) {
                    view.setBlock(x, y-1, otherBlock.replaceAt(1, found));
                }
            }
        }
        // unless it's underwater grass, stick left and right too
        if (!(space === 'w' && stickblock === '-')) {
            { // stick the left grass
                const otherBlock = view.getBlock(x-1, y);
                if (otherBlock[0] === stickblock) {
                    const found = stickLeft[otherBlock[1]];
                    if (found) {
                        view.setBlock(x-1, y, otherBlock.replaceAt(1, found));
                    }
                }
            }
            { // stick the right grass
                const otherBlock = view.getBlock(x+1, y);
                if (otherBlock[0] === stickblock) {
                    const found = stickRight[otherBlock[1]];
                    if (found) {
                        view.setBlock(x+1, y, otherBlock.replaceAt(1, found));
                    }
                }
            }
        }
    }
    // special stuff for chest
    if (item_code[0] === 'h' || item_code === 'Wp') {
        if (view.isServer) {
            // on the server, create the chest.
            const total_slots = (item_code === 'hh' || item_code === 'Wp') ? 10 : 20;
            view.createTileEntity({
                t: 'chest',
                x: x,
                y: y,
                inventory: new Inventory(total_slots),
                subscribers: [],
            });
            // Process adjacent hoppers
            for (let dx = -1; dx < 2; dx++) {
                for (let dy = -1; dy < 2; dy++) {
                    // Don't do diagonals or itself
                    if (Math.abs(dx) === Math.abs(dy) || (dx === 0 && dy === 0)) continue;

                    const block_here = view.getBlock(x + dx, y + dy);
                    if (block_here[0] !== 'p') continue;

                    view.processHopper(x + dx, y + dy, block_here);
                }
            }
        }
    }

    // special stuff for aafile
    if (item_code === 'sf') {
        if (view.isServer) {
            // on the server, create the tile entity.
            view.createTileEntity({
                t: 'aafile',
                x: x,
                y: y,
                d: {text: item.data.text},
                pos: 0,
                recv: false,
            });
        }
    }

    // special stuff for aaterminal
    if (item_code === 'sm') {
        // create the tile entity.
        view.playerCreateTileEntity({
            t: 'aaterminal',
            x: x,
            y: y,
            d: {
                lines: [''],
                queue: '',
                out: ' ',
                recv: false,
            },
        });
    }

    // special stuff for hoppers
    if (view.isServer && (item_code[0] === 'p') ) {
        if (view.isHopperTrailTooLong(x, y, block_code)) {
            view.surpriseSetBlock(x, y, ' ', false);
            view.surpriseGainItemInverted(item_code, 1, undefined);
            return false;
        }
        view.processHopper(x, y, block_code);
    }

    // special stuff for detector
    if (view.isServer && item_code[0] === 'E') {
        // create tile entity, but only on server
        view.createTileEntity({
            t: 'detector',
            x: x,
            y: y,
        });
    }

    // special stuff for SSPOOKY
    if (view.isServer && item_code === 'Ws') {
        // create tile entity, but only on server
        view.createTileEntity({
            t: 'sspooky',
            x: x,
            y: y,
        });
    }

    // special stuff for wires and water
    if (view.isServer) {
        if (item_code[0] === 'S'
            || item_code[0] === 'O'
            || item_code[0] === 'R'
            || item_code === '%'
            || item_code === ':'
            || item_code[0] === '=') {
            GLS.enqueueWire5(x, y, view);
        } else if (item_code === 'M'
            || item_code === 'WO'
            || item_code === 'sc'
            || item_code === 'st') {
            view.enqueueWire(x, y, view);
        }

        // update the powered scanner above
        const above = view.getBlock(x, y+1);
        if (above === 'sC') {
            view.enqueueWire(x, y+2, view);
        }

        GLS.checkEnqueueWater(x+1, y, view);
        GLS.checkEnqueueWater(x-1, y, view);
        GLS.checkEnqueueWater(x, y+1, view);
        GLS.checkEnqueueWater(x, y-1, view);
    }

    // special stuff for forg on server
    if (item_code === 'WF' && view.isServer) {
        view.createTileEntity({
            t: 'forg',
            x: x,
            y: y,
            d: { know: '' },
        });
    }

    return true;
}


if (typeof exports !== 'undefined') {
    exports.placeLogic = placeLogic;
}
