#!/usr/bin/python3

import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D

colors = [

    '#7f7f7f',
    '#8b5b6d',
    # '#5e9d8d',
    '#c85b45',
    '#428c66',
    '#82e85a',
    '#eb9432',
    '#fce44e',
    '#834aca',
    '#d38afa',

    '#4585f5', # blue
    # '#ffa888',
    '#88f0e7', # cyan
    '#ee31a8', # magenta

]


fig = pyplot.figure()
ax = pyplot.axes(projection = '3d')
ax.set_xlabel('R')
ax.set_ylabel('G')
ax.set_zlabel('B')
for color in colors:
    r = int(color[1:3], 16)
    g = int(color[3:5], 16)
    b = int(color[5:7], 16)
    ax.scatter3D(r, g, b, color=color)
pyplot.show()


