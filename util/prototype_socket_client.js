const Socket = require('net').Socket;

const client = new Socket();

intToByteString = function(integer) {
    // to 32-bit signed integer
    let byteString = '';

    for (let i = 0; i < 4; i++) {
        let byte = integer & 0xff;
        byteString = String.fromCharCode(byte) + byteString;
        integer = (integer - byte) / 256;
    }

    return byteString;
};

byteStringToInt = function(byteString) {
    var value = 0;
    for (let i = byteString.length - 1; i >= 0; i--) {
        console.log(byteString.charCodeAt(i));
        value = (value * 256) + byteString.charCodeAt(i);
    }
    return value;
};

client.connect(8089, 'localhost', () => {
    console.log('connected');

    let callback;
    let concatenated = '';
    let expectedlength = 0;

    client.on('data', message => {
        let recvstring = message.toString();
        if (expectedlength === 0) {
            expectedlength = message[0]+256*(message[1]+256*(message[2]+256*message[3]));
            recvstring = recvstring.substr(4);
        }
        concatenated += recvstring;
        if (concatenated.length >= expectedlength) {
            callback();
            expectedlength = 0;
        }
    });

    seed = 54;

    message = 'c '+seed+' '+2+' '+5+' '+3+' '+55;

    client.write(intToByteString(message.length));
    client.write(message);
    callback = ()=>{
        const obj = JSON.parse(concatenated);
        console.log(obj);
    };
});
