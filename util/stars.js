
// randomly generates stars for parallax background in loading area
// should be manually modified and prettified before using

for (y = 0; y < 40; y++) {
    let str = '';
    for (x = 0; x < 120; x++) {
        let c = Math.random() < 0.01 ? '.' : ' ';
        str += c;
    }
    console.log('"'+str+'",');
}

