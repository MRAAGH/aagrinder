#!/usr/bin/python3

from matplotlib import pyplot
import numpy
import math

# as reference, I used the string which Autumn Dawn posted on the wiki:
# https://wiki.aagrinder.xyz/index.php?title=Command_list
# <span style="color:#ff0000">R</span><span style="color:#ff8b00">A</span><span style="color:#e8ff00">I</span><span style="color:#5dff00">N</span><span style="color:#00ff2e">B</span><span style="color:#00ffb9">O</span><span style="color:#00b9ff">W</span>

xs6 = numpy.arange(7)/6

xs100 = numpy.arange(101)/100

r1 = [ 0,0,0,0,46,139,255 ]
g1 = [ 255, 255, 0xe8, 0x5d, 0, 0, 0 ]
b1 = [ 0, 0x8b, 0xff, 0xff, 0xff, 0xff, 0xb9 ]

r2 = [max(min((math.sin(x*3+1.6)+0.5)*200,255),0) for x in xs100]
g2 = [max(min((math.sin(x*3-0.2)+0.5)*200,255),0) for x in xs100]
b2 = [max(min((math.sin(x*3-2.2)+0.5)*200,255),0) for x in xs100]

pyplot.plot(xs6, r1)
pyplot.plot(xs6, g1)
pyplot.plot(xs6, b1)
pyplot.plot(xs100, r2)
pyplot.plot(xs100, g2)
pyplot.plot(xs100, b2)
pyplot.show()
