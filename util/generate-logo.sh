#!/bin/sh
convert -size 200x200 xc:black -gravity center -font "DejaVu-Sans-Mono" -pointsize 180 -fill '#1a8239' -draw "text 0,0 \"A\"" logo.png
convert -size 200x200 xc:black -gravity center -font "DejaVu-Sans-Mono" -pointsize 120 -fill '#1a8239' -draw "text 0,0 \"[A]\"" matrixlogo.png
convert -size 200x200 xc:black -gravity center -font "DejaVu-Sans-Mono" -pointsize 120 -fill '#1a8239' -draw "text 0,0 \"[ ]\""\
    -pointsize 60 -draw "text 0,30 \"AAA\"" -draw "text 0,-20 \"AAA\"" matrixlogo6.png

convert -size 200x200 xc:black -gravity center -font "DejaVu-Sans-Mono" -pointsize 120 -fill '#1a8239' -draw "text 0,0 \"A \""\
    -pointsize 120 -fill '#ff0000' -draw "text 0,0 \" !\"" logo!.png


convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#1a8239' -draw "text 0,0 \"A\"" eqleaves.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#7f7f7f' -draw "text 0,0 \"B\"" eqstone.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#2faebf' -draw "text 0,0 \"D\"" eqdiamond.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#9f5f7f' -draw "text 0,0 \"R\"" eqruby.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#91702a' -draw "text 0,0 \"H\"" eqladder.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#bd7c3d' -draw "text 0,0 \"0\"" eqgrabber.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#fbae81' -draw "text 0,0 \"a\"" eqapricot.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#d8a34b' -draw "text 0,0 \"a\"" eqalmond.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#34a625' -draw "text 0,0 \"a\"" eqavocado.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#cf1005' -draw "text 0,0 \"a\"" eqapple.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#4549a5' -draw "text 0,0 \"W\"" eqwater.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#cccccc' -draw "text 0,-50 \"~\"" eqcloud.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#2faebf' -draw "text 0,0 \"G\"" eqgrinder.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#9966cc' -draw "text 0,0 \"P\"" eqmazie.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#7f7f7f' -draw "text 0,0 \"S\"" eqswitch.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#9e2929' -draw "text 0,0 \"M\"" eqmotor.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#727280' -draw "text 0,0 \"T\"" eqstalactite.png
convert -size 200x200 xc:black -gravity center -font "Equestria" -pointsize 180 -fill '#727280' -draw "text 0,0 \"I\"" eqstalagmite.png
