﻿// AAGRINDERgrasspreprocessing.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class LogicSandbox {

private:

	int index;

	bool* original;

	void toBinary() {
		//Int into binary as bool array:
		int curr = index;
		for (unsigned int i = 0; i < 9; ++i)
		{
			//INVERTED THIS TIME:
			original[i] = (curr % 2 == 1);
			curr /= 2;
		}
	}

	void clone() {
		for (int i = 0; i < N; i++) {
			active[i] = original[i];
		}
	}

	void attachNames() {
		/* 012
		* 345
		* 678
		*/
		topleft = &(active[0]);
		top = &(active[1]);
		topright = &(active[2]);
		left = &(active[3]);
		here = &(active[4]);
		right = &(active[5]);
		bottomleft = &(active[6]);
		bottom = &(active[7]);
		bottomright = &(active[8]);
	}

	virtual int choosePrefab() = 0;

	bool subtestfor(int pos, int checktype) {
		//Ignore:
		if (checktype == x) return true;
		//Test for true:
		if (checktype == 1) return active[pos] == true;
		//Test for false:
		if (checktype == 0) return active[pos] == false;

		cout << "INVALID TESTFOR: " << checktype << endl;

		return false;
	}

	void print() {
		cout << "\n"
			<< original[0] << " " << original[1] << " " << original[2] << "\n"
			<< original[3] << " " << original[4] << " " << original[5] << "\n"
			<< original[6] << " " << original[7] << " " << original[8] << "\n";
	}

	int standardsequence(int (LogicSandbox::*oneorientation)(bool), bool positivesurface) {

		for (int side = 0; side < 4; side++) {

			int type = (this->*oneorientation)(positivesurface);

			if (type != NOT_FOUND) return type + side;

			//Next side is by standard 90 degrees ccw.
			//We get to it by rotating the shape cw:
			cyclecw90();
		}

		return NOT_FOUND;
	}

	int oneorientationDIRECT(bool positivesurface) {
		bool t = positivesurface, f = !t;

		//No surface:
		if (testfor(
			x, f, x,
			f, x, f,
			x, f, x
		)) return 0;
		//One surface:
		if (testfor(
			x, t, x,
			f, x, f,
			x, f, x
		)) return 1;
		//Two surfaces (not opposite)
		if (testfor(
			x, t, x,
			f, x, t,
			x, f, x
		)) return 5;
		//Two surfaces (opposite)
		if (testfor(
			x, t, x,
			f, x, f,
			x, t, x
		)) return 9;
		//Three surfaces:
		if (testfor(
			x, t, x,
			t, x, t,
			x, f, x
		)) return 11;
		//All surfaces:
		if (testfor(
			x, t, x,
			t, x, t,
			x, t, x
		)) return 15;

		//Not found:
		return NOT_FOUND;
	}

	int oneorientationWITHBASICDIAGONALS(bool positivesurface) {
		bool t = positivesurface, f = !t;

		//No surface:
		if (testfor(
			x, f, x,
			f, x, f,
			x, f, x
		)) return 0;
		//One surface:
		if (testfor(
			x, t, x,
			f, x, f,
			f, f, f
		)) return 1;
		if (testfor(
			x, t, x,
			f, x, f,
			f, f, t
		)) return 5;
		if (testfor(
			x, t, x,
			f, x, f,
			t, f, f
		)) return 9;
		if (testfor(
			x, t, x,
			f, x, f,
			t, f, t
		)) return 13;
		//Two surfaces (not opposite)
		if (testfor(
			x, t, x,
			f, x, t,
			f, f, x
		)) return 17;
		if (testfor(
			x, t, x,
			f, x, t,
			t, f, x
		)) return 21;
		//Two surfaces (opposite)
		if (testfor(
			x, t, x,
			f, x, f,
			x, t, x
		)) return 25;
		//Three surfaces:
		if (testfor(
			x, t, x,
			t, x, t,
			x, f, x
		)) return 27;
		//All surfaces:
		if (testfor(
			x, t, x,
			t, x, t,
			x, t, x
		)) return 31;

		//Not found:
		return NOT_FOUND;
	}

protected:

	static const int x = 2;

	bool* active;

	bool *topleft, *top, *topright, *left, *here, *right, *bottomleft, *bottom, *bottomright;

	bool testfor(int tl, int t, int tr, int l, int h, int r, int bl, int b, int br) {
		/* 012
		* 345
		* 678
		*/
		if (!subtestfor(0, tl)) return false;
		if (!subtestfor(1, t)) return false;
		if (!subtestfor(2, tr)) return false;
		if (!subtestfor(3, l)) return false;
		if (!subtestfor(4, h)) return false;
		if (!subtestfor(5, r)) return false;
		if (!subtestfor(6, bl)) return false;
		if (!subtestfor(7, b)) return false;
		if (!subtestfor(8, br)) return false;
		return true;
	}

	void cyclecw90() {
		/* 012
		* 345
		* 678
		*/
		swap(0, 6);
		swap(6, 8);
		swap(8, 2);

		swap(1, 3);
		swap(3, 7);
		swap(7, 5);
	}
	void cyclecw45() {
		/* 012
		* 345
		* 678
		*/
		swap(0, 3);
		swap(3, 6);
		swap(6, 7);
		swap(7, 8);
		swap(8, 5);
		swap(5, 2);
		swap(2, 1);
	}
	void cycleccw90() {
		/* 012
		* 345
		* 678
		*/
		swap(0, 2);
		swap(2, 8);
		swap(8, 6);

		swap(1, 5);
		swap(5, 7);
		swap(7, 3);
	}
	void cycleccw45() {
		/* 012
		* 345
		* 678
		*/
		swap(0, 1);
		swap(1, 2);
		swap(2, 5);
		swap(5, 8);
		swap(8, 7);
		swap(7, 6);
		swap(6, 3);
	}

	void swap(int x, int y) {
		bool temp = active[x];
		active[x] = active[y];
		active[y] = temp;
	}

	void fliphor() {
		/* 012
		* 345
		* 678
		*/
		swap(0, 2);
		swap(3, 5);
		swap(6, 8);
	}

	void flipver() {
		swap(0, 6);
		swap(1, 7);
		swap(2, 8);
	}

	void refresh() {
		clone();
	}

	void invert() {
		active[0] = !active[0];
		active[1] = !active[1];
		active[2] = !active[2];
		active[3] = !active[3];
		active[4] = !active[4];
		active[5] = !active[5];
		active[6] = !active[6];
		active[7] = !active[7];
		active[8] = !active[8];
	}

	int standardsequenceDIRECT(bool positivesurface) {
		return standardsequence(&LogicSandbox::oneorientationDIRECT, positivesurface);
	}
	int standardsequenceWITHBASICDIAGONALS(bool positivesurface) {
		return standardsequence(&LogicSandbox::oneorientationWITHBASICDIAGONALS, positivesurface);
	}

public:

	static const int N = 9;
	static const int MAX = 512; //2^N
	static const int NOT_FOUND = -2;
	static const int BLANK = -1;

	LogicSandbox() {

		index = 0;
		original = new bool[N];
		active = new bool[N];
		attachNames();
		toBinary();
	}
	virtual ~LogicSandbox() {};

	void Next() {
		index++;
		toBinary();
	}

	bool Done() {
		return (index == MAX);
	}

	bool AlmostDone() {
		return (index == MAX - 1);
	}

	int GetIndex() { return index; }

	int Prefab() {
		clone();
		int g = choosePrefab();
		if (g == NOT_FOUND) print(); //Something went wrong!
		return g;
	}

};

class Grass : public LogicSandbox {
public:
	int choosePrefab() {

		//Will we ignore what's bellow?
		bool ignore_down = false;
		if (testfor(
			1, x, x,
			1, x, x,
			x, x, x
		))ignore_down = true;
		if (testfor(
			x, x, 1,
			x, x, 1,
			x, x, x
		))ignore_down = true;

		//Will we ignore what's above?
		bool ignore_up = false;
		if (testfor(
			x, x, x,
			1, x, x,
			1, x, x
		))ignore_up = true;
		if (testfor(
			x, x, x,
			x, x, 1,
			x, x, 1
		))ignore_up = true;

		//Will we ignore what's left?
		bool ignore_left = testfor(
			x, x, x,
			x, x, x,
			1, 1, x
		);

		//Will we ignore what's right?
		bool ignore_right = testfor(
			x, x, x,
			x, x, x,
			x, 1, 1
		);

		//Apply ignorance:
		/*
		if (ignore_up) active[1] = false;
		if (ignore_down) active[7] = false;*/
		if (ignore_left) active[3] = false;
		if (ignore_right) active[5] = false;

		//Combine into the answer:
		int c = 0;
		if (active[5]) c += 1;
		if (active[1]) c += 2;
		if (active[3]) c += 4;
		if (active[7]) c += 8;

		return c;
	}
};

class BasicGrass : public LogicSandbox {
public:
	int choosePrefab() {
		int c = 0;
		if (active[5]) c += 1;
		if (active[1]) c += 2;
		if (active[3]) c += 4;
		if (active[7]) c += 8;

		return c;
	}
};

int main() {
	LogicSandbox* logic = new BasicGrass();
	while (!logic->Done()) {

		int j = logic->Prefab();
		cout << j;

		cout << (logic->AlmostDone() ? "" : ", ");

		logic->Next();
	}
	system("pause");
	return 0;
}