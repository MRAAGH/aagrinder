# AAGRINDER CHANGELOG

This changelog contains some of the things that were added/removed/changed.

But not everything. Most changes in aagrinder are not something a casual
player would care about, so they are not included in this changelog.

For a more complete view, read the commit message history.


### February 2025

+ Added aafile
+ Added aaterminal
+ Added some commands and aliases
+ Added ruby apple
+ Added new recipes
+ Added sprint climbing in chest
+ Added carrotatle
* Changed corrupted stone behavior
* Fixed opening chest with bucket
* Fixed Ctrl+V
* Fixed notification issues
* Fixed tentacles getting obstructed
* Tweaked dig durations
* Fixed player hidden behind typing indicator

### January 2025

+ Added particles in tentacle island

### November 2024

+ Added /rainbow typing animation
+ Added rain
- Fixed accidental digging when clicking in UI
* Fixed many bugs with typing indicator
* Fixed tab-completion with quotes
* Fixed local chat color
* Fixed /rainbow not working in local
* Fixed issues with grinders
* Made hotbar selection more visible

### October 2024

+ Added /hug
+ Added oasis biome
+ Added typing indicators
+ Added player overhead messages
- Limited distance for /spawn
* Fixed graphical glitches with background
* Better UI for scrolling
* Fixed music not playing
* Fixed durability percentage colors

### September 2024

+ Made aaprinter able to place any block
+ Allow crafting into occupied slot
+ Allow deletion directly from chest
+ New functionality for /whisper
+ Added ladder
+ Added button to lock camera to world
* Improved /copylast
* Made recipes more easily discoverable
* Better UI for scrolling

### August 2024

+ Made vines more climbable
+ Added framework block
+ Smooth biome edges
+ Added many command aliases
+ Added color preview for local messages
+ Added help for each command
+ Added more notification types
+ Display durability in tab inspect
* Changed how signals travel through wire
* Changed coral behavior
* Fixed bugs with ghost
* Fixed broken structures on chunk borders
* Changed function for stone grinder and yaygrinder
* Made /rainbow prettier
- Removed blueberry wood and cranberry wood

### July 2024

+ Added green mountain biome
+ Added dunes biome
+ Added different terrain at high altitude
+ More grass
+ New login screen
+ Added cotton candy
* Changed dead tree designs
* Fixed many bugs with circuit components
* Changed chest loot
* Fixed ruby farming
* Fixed coral not growing

### May 2024

+ Added glacier biome
+ Added four fort with forg
+ Added shipwrecks (designed by Kuki)
+ Added more background ASCII arts
+ Added sand and boulders on ocean floor
+ Added treasures and boulders in desert
+ Added tiny boulders in underground
+ Display player names and biomes in map view
+ Added pickaxe
+ Added new animations
+ Added Candy
* Fixed berries not growing
* Increased range for natural warp
* Made terrain generator faster
* Fixed many bugs with playing music
* Tweaked dig durations of blocks

### April 2024

+ Parallax background
+ Added eating notification
+ Added forg
+ Added warp core
+ Added new animations
+ Added new notifications
* Improved custom warp
* Improved fruit growth
* Improved coloring of error messages
* Fixed notification covering over hotbar
* Rebalanced boulder and grinder

### February 2024

+ Animation when watering tree
+ Made it obvious map is open
+ PageUp, PageDown, Home, End in inventory
+ Notifications when collecting items
* Changed dig durations
* Fixed bugs with grinder
* Changed fruit growth speed
* Fixed some annoying UI bugs
* Fixed chest desynch
* Made fruit easily collectable
- Removed second help screen

### January 2024

+ Support shift-clicking in inventories
+ Motor responds to wire signals (for flying machines)
+ Grinder responds to wire signals (for automated farms)
+ Made fruit grow on trees
+ Flowing water animation
+ Added functionality to corrupted stone
* Made detector more rare
* Improved command tab-completion
* Improved inventory keyboard controls
* Made bucket less annoying to use
* Fixed a lot of bugs with ghost
* Key for friend list is now F
* Key for eating is now C
* Fixed bucket bug
- Removed "captive" ghost type
- Ghost no longer able to move through wall

### December 2023

+ Tab completion for commands and arguments
+ Fixed many chatbox issues
+ A setting for local chat (/set local)
+ Made wire connectivity more consistent

### November 2023

+ Easier inventory management
+ Easier friend management
+ A setting to hide coordinates (/set nocoords)
+ Show block name when holding Tab
+ Fixed avocado tree growth
* Changed how adjacent trees grow
- Removed inspector tool

### October 2023

+ New inventory UI (again)
+ Slots can be reorganized similar to other games
+ New mouse and keyboard controls in UI

### August 2023

+ Support Ctrl+V

### February 2023

+ New hotbar

### January 2023

+ Made vine collectable by grinder
+ Improved inventory UI
- Removed old wire components

### December 2022

+ Added item deletion (and animation)
+ Added aaprinter and aascanner
+ Added logic gate
+ More color in UI
+ Text formatting in chat and on sign
+ Clickable buttons for friend accept/reject
* Fixed hotbar being reversed
* Overhaul of wires, which now carry characters
* Made % climbable
* Improved animations

### November 2022

+ Added stick
+ Made snow visible behind player
+ Allow placing certain blocks inside player
+ Added chunk versioning
* Changed water draining
* Better wire visuals
- Removed cloud self-regeneration
- Made quicksand unobtainable

### October 2022

+ Added /craft
+ Made water visible behind seagrass
+ Added fireflies
+ Biome is now displayed under coordinates
+ Added buckets
+ Added snow block
+ Made icicles grow
+ Use , and . keys for scrolling hotbar
+ Increased stack size
* Made stalagmite climbable
* Changed the effect of quicksand
* Made tree growth more easily understandable
* Snow no longer blocks you when on top of ladder
* Changed snowfall logic
- Removed "H for help" hint

### September 2022

* Made custom warp invisible and command-controlled
- Removed /sudo

### August 2022

+ Added custom warps (not yet obtainable)
+ Made hotbar clickable
+ Added pick block key
+ Added hunger
+ Made bushes farmabele
* Pony mod now uses two different fonts
* Made map view prettier

### July 2022

+ Added some spooky items
+ Added seagrass animation
+ Added bubble animation
+ Added land claiming (admin-only)
+ Added warp animations
+ Added warp warmup and cooldown
+ Added friends management
+ Added snow regeneration (snowfall)
+ Added snowflake animation
* Renamed portal to warp
* Decreased network usage
- Removed MySQL dependency

### June 2022

* Improved chest and crafting UI
* Made hotbar more interesting

### May 2022

* Improved chest and crafting UI

### April 2022

+ Made cloud regenerate on its own

### February 2022

* Made leaves non-solid
* Made chest climbable

### December 2021

+ Added hoppers

### November 2021

+ Added many debugging features
* Improved UI

### October 2021

+ Added stone grinder
* Improved inventory and crafting UI

### September 2021

+ Added new crafting menu
+ Added new recipes
+ Added Matrix bridge

### July 2021

+ Added new animations
+ Added inventory item limits
* Improved inventory UI
* Made seagrass more easily placable
* Made seagrass not destroy itself

### June 2021

* Changed climbing logic
* Changed ruby logic

### January 2021

+ Added new recipes
+ Added cloud animation
* Improved settings (/set)
* Improved signs
* Changed wire logic

### November 2020

- Removed digging cooldown

### October 2020

+ Added digging cooldown
* Changed graphite to chalk

### September 2020

+ Added wire logic
+ Added stone slab
+ Added trapdoor
+ Added ice
+ Added word wrapping in chat
+ Made fruit pickable without breaking
+ Added berry bushes
+ Added quicksand
+ Added inspector
+ Added new commands
+ Added graphite and magnetite
+ Added portal biome
* Improved movement logic
* Improved water logic
* Changed coral growth
* Improve tentacle growth

### August 2020

+ Enabled commands on login screen
* Improved movement logic

### July 2020

+ Added mixed forest biome
+ Added rainbow biome
+ Added grabber
+ Added water flow logic
* Multithreaded (faster) terrain generation
* Improved custom key mapping

### June 2020

+ Added tentacle growth
+ Added biome background colors
* Updated tree growth
* Changed coral growth

### April 2020

* Fix tree not growing next to block

### March 2020

+ Added grass and seagrass growth
+ Added coral death
+ Added basic wire logic
+ Added leaves falling animation
* Updated coral growth
* Updated recipes

### February 2020

+ Added map view
+ Added ? block
+ Added frame block
* Improved pony mod
* Updated recipes
- Fixed ladder bug

### December 2019

+ Added chest
* Made vines climbable

### November 2019

+ Added music
* Improved UI

### October 2019

+ Made player color changable
+ Made password changable
+ Added more items
+ Made aagrinder more hacking-resistant
* Decreased network usage

### September 2019

+ Added portals (warps)
+ Added basic water physics
+ Added coral
+ Added water
+ Added block animations
+ Added sprinting on ladder
* Changed ruby logic
* Decreased client RAM usage

### August 2019

+ Added colors in inventory
+ Allowed digging under player
+ Detach player from terrain
+ Added super grinder
+ Added hotbar
+ Added color variation throughout terrain
+ Added better biomes
+ Make leaves despawn
+ Added leaves animation
+ Added ghost
+ Added ruby
+ Added tree growth
+ Display other player names
* Improved mouse and keyboard controls
* Swapped mouse buttons
* Easier account registration
- Remove whisper logging

### July 2019

* Made camera not centered on player

### June 2019

+ A pretty login screen
+ Added sprinting
+ Added pony.html
+ Added support for color in chat

### November 2018

+ Added support for mouse in inventory
+ Added gravity
+ Added ladder logic
+ Added join/leave announcements
+ Added grass spinning

### September 2018

+ Added mouse controls
+ Added chatbox
+ Added placement
+ Added digging

### April 2018

- Replace MongoDB dependency with MySQL

### February 2018

+ Cursor mouse controls
+ Remember command history
+ Player colors
+ Color support
+ Ability to register from login screen
* Improved text editing

Progress before February 2018 was not tracked
