// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    aliases = ['/t', '/teleport', '/topony', '/telepony'];
    signatures = [[
        {name:'player', type:'onlineplayer', mandatory:false},
        {name:'targetplayer', type:'onlineplayer', mandatory:true},
    ],
    [
        {name:'player', type:'onlineplayer', mandatory:false},
        {name:'x', type:'xcoord', mandatory:true},
        {name:'y', type:'ycoord', mandatory:true},
    ],
    [
        {name:'x', type:'xcoord', mandatory:true},
        {name:'y', type:'ycoord', mandatory:true},
    ]];
    isAdmin = true;
    help = 'Teleport a player to another player or to a specific location.';

    execute(player, args) {

        let yesTeleport = false;
        let tpPlayer;
        let x, y;

        switch (args.length) {

            case 2: {
                // with 1 argument it's teleporting myself to another player.
                tpPlayer = player;

                const targetPlayerName = args[1];
                const targetPlayer = this.playerData.onlinePlayerByName(targetPlayerName);
                if (!targetPlayer) {
                    // no such player
                    player.sendError('can\'t find player '+targetPlayerName);
                    return;
                }

                yesTeleport = true;
                x = targetPlayer.x;
                y = targetPlayer.y;
                player.sendChat('teleporting to '+targetPlayerName);

                break;
            }

            case 3: {
                // with 2 arguments it can be two coordinates
                // or two player names
                let isCoord = true;

                x = this.parseRelative(player.x, args[1]);
                y = this.parseRelative(player.y, args[2]);
                if (x === false || y === false) {
                    // clearly not two coordinates.
                    isCoord = false;
                }

                if(isCoord){
                    // two coordinates
                    yesTeleport = true;
                    tpPlayer = player;
                    player.sendChat('teleporting to '+x+', '+y);
                }
                else {
                    // not coordinates, so it must be two player names

                    const tpPlayerName = args[1];
                    tpPlayer = this.playerData.onlinePlayerByName(tpPlayerName);
                    if (!tpPlayer) {
                        // no such player
                        player.sendError('can\'t find player '+tpPlayerName);
                        return;
                    }

                    const targetPlayerName = args[2];
                    const targetPlayer = this.playerData.onlinePlayerByName(targetPlayerName);
                    if (!targetPlayer) {
                        // no such player
                        player.sendError('can\'t find player '+targetPlayerName);
                        return;
                    }

                    yesTeleport = true;
                    x = targetPlayer.x;
                    y = targetPlayer.y;
                    player.sendChat('teleporting '+tpPlayerName+' to '+targetPlayerName);

                }
                break;
            }

            case 4: {
                // with 3 arguments, it's a player name and two coordinates

                const tpPlayerName = args[1];
                tpPlayer = this.playerData.onlinePlayerByName(tpPlayerName);
                if (!tpPlayer) {
                    // no such player
                    player.sendError('can\'t find player '+tpPlayerName);
                    return;
                }

                x = this.parseRelative(player.x, args[2]);
                y = this.parseRelative(player.y, args[3]);
                if (x === false || y === false) {
                    this.sendUsageString(player);
                    return;
                }

                yesTeleport = true;
                player.sendChat('teleporting '+tpPlayerName+' to '+x+', '+y);

                break;
            }


            default:
                this.sendUsageString(player);
                return;

        }

        if (yesTeleport) {

            this.syncher.movePlayer(tpPlayer, x, y);
            this.subscribe.resubscribe(tpPlayer, true);

            LOG.log('teleported '+tpPlayer.name+' to '+x+', '+y);

        }
    }

}
