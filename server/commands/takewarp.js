// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'code', type:'incompletable', mandatory:true},
    ]];
    isAdmin = false;
    help = 'Reclaim a customwarp item which was placed by you.';

    execute(player, args) {
        if (args.length < 2) {
            this.sendUsageString(player);
            return;
        }
        if (args.length > 2) {
            player.sendError('Use quotes:\\[n\\]/takewarp "code with spaces"');
            return;
        }
        const code = args[1];
        const warp = this.map.getCustomWarpByCode(code);
        if (warp === null || warp.p !== player.name) {
            player.sendError('not found. Try /mywarps');
            return;
        }
        if (Math.abs(warp.x - player.x) > 10
            || Math.abs(warp.y - player.y) > 10) {
            player.sendError('too far to take warp');
            return;
        }
        if (!player.inventory.canGain('WV', 1)) {
            player.sendError('no inventory space to take warp');
            return;
        }
        // delete warp from map
        this.map.deleteCustomWarp(code);
        // gain the warp item
        player.inventory.gainItemByCode('WV', 1);
        //update properly
        player.changedInventory = true;

        player.sendChat('warp taken.');
    }

}
