// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'action', type:'incompletable', mandatory:true},
    ]];
    isAdmin = false;
    help = 'Display a message as a self-action rather than a chat message.';
    usageExtraLine = 'example: /me jumps';

    execute(player, args) {
        if (args.length < 2) {
            this.sendUsageString(player);
            return;
        }
        const typed = args.slice(1).join(' ');
        const message = '*\\[#'+player.color+'\\]'+player.name+'\\[/\\] '+typed;
        const raw = '*'+typed+'\\[/\\]*';
        for(let i = 0; i < this.playerData.onlinePlayers.length; i++){
            const sendPlayer = this.playerData.onlinePlayers[i];
            sendPlayer.sendTimestampedChat(
                message,
                sendPlayer.name !== player.name,
                raw,
                player.name,
                '#aaaaaa',
            );
        }
        this.aagrinderMatrixBot.handleLocalChat(player, typed);
        this.aagrinderMatrixAppservice.handleLocalChat(player, typed);
    }

}
