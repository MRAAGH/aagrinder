// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'multiplier', type:'incompletable', mandatory:false},
    ]];
    isAdmin = true;
    help = 'Set the multiplier for server-side events.';

    execute(player, args) {
        if (args.length > 2) {
            this.sendUsageString(player);
            return;
        }
        let multiplier = 1;
        if (args.length === 2) {
            if (isNaN(args[1])) {
                this.sendUsageString(player);
                return;
            }
            multiplier = parseFloat(args[1]);
            player.sendChat('setting tick speed to '+args[1]+' times normal');
        }
        else {
            player.sendChat('setting tick speed to normal');
        }
        if (multiplier < 0.01) {
            this.plant.unregisterInterval();
        } else {
            this.plant.registerWithMultiplier(multiplier);
        }
    }

}
