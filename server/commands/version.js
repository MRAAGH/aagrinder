// Copyright (C) 2025 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');
const { promisify } = require('util');
const execAsync = promisify(require('child_process').exec);

module.exports = class extends Command {
    aliases = ['/commit'];
    isAdmin = false;
    help = 'Display the commit ID of the server and terrain generator.';

    async execute(player, args) {
        const clientGitCommit = (await execAsync('git rev-parse HEAD')).stdout.trim();
        const generatorGitCommit = (await execAsync('git -C aagrinder-terrain rev-parse HEAD')).stdout.trim();
        player.sendChat('Server running on: ' + SERVER_GIT_COMMIT
            + '\\[n\\]Client running on: ' + clientGitCommit
            + '\\[n\\]Generator running on: ' + generatorGitCommit);
    }

}
