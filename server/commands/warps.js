// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;
    help = 'List all loaded natural warps.';

    execute(player, args) {
        const foundTiles = [];
        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const chunk = this.map.chunks[chunky][chunkx];
                const tileEntities = chunk.tileEntities;
                // iterate all tile entities per loaded chunk
                for (const tile of tileEntities) {
                    // skip if not warp tile (warp)
                    if (tile.t !== 'warp') continue;
                    foundTiles.push(tile);
                }
            }
        }
        if (foundTiles.length < 1) {
            player.sendChat('no natural warps loaded.');
            return;
        }
        player.sendChat(foundTiles
            .map(w => '[x'+w.x+',y'+w.y+']->[x'+w.d.tx+',y'+w.d.ty+']')
            .join(', '));
    }

}
