// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;
    help = 'List all ghost positions and types.';

    execute(player, args) {
        if (this.plant.ghosts.ghosts.length < 1) {
            player.sendChat('no ghosts.');
            return;
        }
        player.sendChat(this.plant.ghosts.ghosts
            .map(g => 'x'+g.x+' y'+g.y+' ('+g.type[0]+')')
            .join(', '));
    }

}
