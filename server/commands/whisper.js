// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    aliases = ['/w'];
    signatures = [[
        {name:'player', type:'onlineplayer', mandatory:true},
        {name:'message', type:'incompletable', mandatory:false},
    ]];
    isAdmin = false;
    help = 'Whisper a private message to player (not logged). Omit message for whisper mode.';
    usageExtraLine = 'example: /whisper "Berry Icicle" Hey!\\[n\\]Omit message for whisper mode.';

    execute(player, args) {
        if (args.length < 3) {
            this.sendUsageString(player);
            return;
        }
        const targetPlayerName = args[1];
        const targetPlayer = this.playerData.onlinePlayerByName(targetPlayerName);
        if (!targetPlayer) {
            // no such player
            player.sendError('can\'t find player '+targetPlayerName);
            return;
        }
        const raw = args.slice(2).join(' ');
        const recvformatted = '\\[#3f3\\]' + player.name + ': ' + raw;
        const sendformatted = '\\[#3f3\\]-> ' + targetPlayer.name + ': ' + raw;
        targetPlayer.sendTimestampedChat(
            recvformatted,
            true,
            raw,
            player.name,
            '#33ff33',
        );
        player.sendTimestampedChat(
            sendformatted,
            false,
            raw,
            player.name,
            '#33ff33',
        );
    }

}
