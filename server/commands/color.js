// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');
const parseColor = require('../../shared/colors').parseColor;

module.exports = class extends Command {
    aliases = ['/colo', '/colour', '/setcolor', '/setcolour'];
    signatures = [[
        {name:'name or #code', type:'incompletable', mandatory:true},
    ]];
    isAdmin = false;
    help = 'Change your player color. Will be saved in your account.';

    execute(player, args) {
        if (args.length < 2) {
            this.sendUsageString(player);
            return;
        }
        const rawColor = args[1];
        const color = parseColor(rawColor);

        if(
            !/^[0-9a-f]{6}$/.test(color)
        ){
            player.sendError('Color invalid ^[0-9a-f]{6}$' );
            return;
        }
        {
            // check the color
            const r = parseInt(color.substring(0, 3), 16);
            const g = parseInt(color.substring(2, 5), 16);
            const b = parseInt(color.substring(4, 7), 16);
            if(r + g + b < 150){
                player.sendError('Color too dark');
                return;
            }
        }
        player.color = color;
        player.changedColor = true;
        player.changedColorLoud = true;
        // send color change to other clients
        this.syncher.updateMyAndOtherPositionLists(player);
        ACCOUNTS.accounts[player.name].color = color;
        ACCOUNTS.save();
    }
}
