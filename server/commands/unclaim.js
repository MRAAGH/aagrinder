// Copyright (C) 2022-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'x', type:'xcoord', mandatory:true},
        {name:'y', type:'ycoord', mandatory:true},
        {name:'player', type:'existingplayeraccount', mandatory:false},
    ]];
    isAdmin = true;
    help = 'Remove a claim.';

    execute(player, args) {

        let accountName;
        if (args.length === 3) {
            accountName = player.name;
        }
        else if (args.length === 4) {
            accountName = ACCOUNTS.findMatchingName(args[3]);
            if (accountName === null) {
                player.sendError('can\'t find account '+args[3]);
                return;
            }
        }
        else {
            this.sendUsageString(player);
            return;
        }

        let x = this.parseRelative(player.x, args[1]);
        let y = this.parseRelative(player.y, args[2]);
        if (x === false || y === false) {
            this.sendUsageString(player);
            return;
        }

        const claim = this.map.getClaim(x, y);
        if (claim === null) {
            player.sendError('No claim at x='+x+', y='+y);
            return;
        }
        if (claim.p !== accountName) {
            player.sendError('Not claimed by '+accountName);
            return;
        }
        this.syncher.removeClaimAt(x, y);
        player.sendChat('Removed claim x1='+claim.x+', x2='+claim.x2
            +', y1='+claim.y+', y2='+claim.y2+' by '+claim.p);

    }

}
