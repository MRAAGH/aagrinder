// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');
const BlockProps = require('../../shared/blockprops').BlockProps;

module.exports = class extends Command {
    isAdmin = false;
    help = 'goto your rand warp';

    execute(player, args) {
        const ownedWarps = this.map.customWarps.filter(
            warp => warp.p === player.name);
        if (ownedWarps.length === 0) {
            player.sendError('poor four dont care');
            return;
        }
        const filtered = ownedWarps.filter(
            warp => {
                if (warp.x > player.x+5000 || warp.x < player.x-5000
                    || warp.y > player.y+5000 || warp.y < player.y-5000) return false;
                const blockThere = this.map.getBlock(warp.x, warp.y);
                if (!BlockProps.isEnterable(blockThere)) return false;
                return true;
            });
        if (filtered.length === 0) {
            player.sendError('goto fail hard bye!');
            return;
        }
        const index = Math.floor(Math.random()*filtered.length);
        const chosenWarp = filtered[index];
        this.syncher.movePlayer(player, chosenWarp.x, chosenWarp.y);
        this.subscribe.resubscribe(player, true);
        player.sendChat('warping to '+chosenWarp.c);
    }

}
