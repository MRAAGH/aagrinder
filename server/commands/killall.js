// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = true;
    signatures = [[
        {name:'creature', type:'summonablecreaturetype', mandatory:true},
    ]];
    help = 'Kill all creatures of given type.';

    execute(player, args) {

        if (args.length !== 2) {
            this.sendUsageString(player);
            return;
        }

        const creature = args[1];
        let numkilled = 0;

        switch (creature) {

            case 'ghost':
                numkilled = this.plant.ghosts.killAllGhosts();
                break;

            case 'monster':
                numkilled = this.plant.monsters.killAllMonsters();
                break;

            case 'vortex':
                if (!this.vortex.exists()) break;
                this.vortex.state = 'completelygone';
                this.vortex.existencechanged = true;
                numkilled = 1;
                break;

            default:
                player.sendError('unknown creature: '+creature);
                return;
        }

        player.sendChat('killed '+numkilled+' '+creature);
    }

}
