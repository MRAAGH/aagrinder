// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = false;
    help = 'Teleport to your home. Set with /sethome';

    execute(player, args) {
        if (!this.props.allow_home) {
            player.sendError('/home is not allowed on this server.');
            return;
        }
        if (typeof(player.homex) === 'undefined'
            || typeof(player.homey) === 'undefined') {
            player.sendError('no home set.\\[n\\]Use /sethome');
            return;
        }

        this.syncher.movePlayer(player, player.homex, player.homey);
        this.subscribe.resubscribe(player, true);

        player.sendChat('teleporting home');
    }

}
