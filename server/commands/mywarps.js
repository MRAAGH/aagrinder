// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    isAdmin = false;
    help = 'List your warp codes, locations, and scopes.';

    execute(player, args) {
        const mywarps = this.map.getCustomWarpsByPlayer(player.name);
        const scopeNames = {'o':'owner', 'f':'friends', 'a':'all'};
        if (mywarps.length < 1) {
            player.sendChat('you have no warps.');
            return;
        }
        player.sendChat(mywarps
            .map(w => '"'+w.c+'"->[x'+w.x+',y'+w.y+']('+scopeNames[w.s]+')')
            .join(',\\[n\\]'));
    }

}
