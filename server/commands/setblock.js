// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');
const Inventory = require('../../shared/inventory').Inventory;

module.exports = class extends Command {
    signatures = [[
        {name:'x', type:'xcoord', mandatory:false},
        {name:'y', type:'ycoord', mandatory:false},
        {name:'block', type:'itemname', mandatory:true},
    ], [
        {name:'block', type:'itemname', mandatory:true},
    ]];
    isAdmin = true;
    help = 'Set the block.';

    execute(player, args) {

        if (args.length !== 2 && args.length !== 4) {
            this.sendUsageString(player);
            return;
        }

        // calculate coordinates
        let blockx;
        let blocky;
        let blockInput;
        if (args.length === 4) {
            blockx = this.parseRelative(player.x, args[1]);
            blocky = this.parseRelative(player.y, args[2]);
            blockInput = args[3];
        }
        else {
            blockx = player.x;
            blocky = player.y;
            blockInput = args[1];
        }

        // validate coordinates
        if (blockx === false || blocky === false) {
            this.sendUsageString(player);
            return;
        }

        // validate blockInput
        const block = Inventory.humanInput2blockCode(blockInput);
        if (block === null) {
            player.sendError('unknown block: ' + blockInput);
            return;
        }

        // apply
        this.syncher.serverChangeBlock(blockx, blocky, block);
        const itemName = Inventory.item_names[Inventory.block2item(block)];
        player.sendChat('set block to '+block+' ('+itemName+')');

    }

}
