// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'player', type:'onlineplayer', mandatory:true},
    ]];
    isAdmin = false;
    help = 'Send a hug request to a player, or accept a pending hug request.';

    execute(player, args) {
        if (args.length < 2) {
            this.sendUsageString(player);
            return;
        }
        const targetPlayerName = args[1];
        const targetPlayer = this.playerData.onlinePlayerByName(targetPlayerName);
        if (targetPlayer.name === player.name) {
            player.sendChat('\\[#fbf\\]selfhug, uwu');
            return;
        }
        if (!targetPlayer) {
            // no such player
            player.sendError('can\'t find player '+targetPlayerName);
            return;
        }
        if (Math.abs(player.x - targetPlayer.x) > 10
            || Math.abs(player.y - targetPlayer.y) > 6) {
            player.sendError('not in hug range');
            return;
        }
        player.hugRequestedFrom = targetPlayer.name;
        if (targetPlayer.hugRequestedFrom === player.name) {
            // both request hug from each other.
            // player teleports to targetPlayer
            const tpx = player.x < targetPlayer.x ? targetPlayer.x-1 : targetPlayer.x+1;
            const tpy = targetPlayer.y;
            this.syncher.movePlayer(player, tpx, tpy);
            this.subscribe.resubscribe(player, true);
            // clear both requests
            player.hugRequestedFrom = '';
            targetPlayer.hugRequestedFrom = '';
        } else {
            // only request hug, nothing else
            player.sendChat('\\[#fbf\\]Requesting hug from '+targetPlayer.name);
            targetPlayer.sendChat('\\[#fbf\\]'+player.name+' wants to hug. Use /hug "'+player.name+'"');
        }
    }

}
