// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'player', type:'onlineplayer', mandatory:false},
    ]];
    isAdmin = true;
    help = 'Remove all items from a player\'s inventory.';

    execute(player, args) {
        let targetPlayer = player;
        if (args.length > 2) {
            this.sendUsageString(player);
            return;
        }
        if (args.length === 2) {
            // player specified.
            targetPlayer = this.playerData.onlinePlayerByName(args[1]);
            if (targetPlayer === null) {
                player.sendError('player not found: '+args[1]);
                return;
            }
        }
        targetPlayer.inventory.clear();
        targetPlayer.changedInventory = true;
        player.sendChat('cleared inventory for '+targetPlayer.name);
    }

}
