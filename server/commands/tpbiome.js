// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */



const MAX_SEARCH_CHUNKS = 5000;

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'biome', type:'biomename', mandatory:true},
    ]];
    isAdmin = true;
    help = 'Teleport to a nearby biome of given type.';

    async execute(player, args) {

        if (args.length !== 2) {
            this.sendUsageString(player);
            return;
        }

        const biomeInput = args[1];
        // validate biome
        let biome = null;
        let biomeName = null;
        for (const validNames of Command.KNOWN_BIOMES) {
            if (validNames.includes(biomeInput)) {
                biome = validNames[0];
                biomeName = validNames[2];
                break;
            }
        }
        if (biome === null) {
            player.sendError('unknown biome: '
                + biomeInput + '\\[n\\]available biomes: '
                + Command.KNOWN_BIOMES.map(b=>b[2]).join(', '));
            return;
        }

        const originx = (player.x>>2)<<2;
        const originy = (player.y>>3)<<3;
        const found = await this.findBiome(originx, originy, biome, this.props.multithread, MAX_SEARCH_CHUNKS);
        if (found === null) {
            player.sendChat('no '+biomeName+' found in '+MAX_SEARCH_CHUNKS+' chunks.');
            return;
        }
        player.sendChat('teleporting to '+biomeName+' at '+found.x+' '+found.y);
        this.syncher.movePlayer(player, found.x, found.y);
        this.subscribe.resubscribe(player, true);
    }

}
