// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [
        [
            {name:'item', type:'itemname', mandatory:true},
            {name:'amount', type:'integer', mandatory:false},
        ],
        [
            {name:'amount', type:'integer', mandatory:false},
            {name:'item', type:'itemname', mandatory:true},
            {name:'player', type:'onlineplayer', mandatory:false},
        ],
        [
            {name:'player', type:'onlineplayer', mandatory:false},
            {name:'item', type:'itemname', mandatory:true},
        ],
        [
            {name:'player', type:'onlineplayer', mandatory:false},
            {name:'amount', type:'integer', mandatory:false},
            {name:'item', type:'itemname', mandatory:true},
        ],
    ];
    isAdmin = true;
    help = 'Give items to a player.';

    execute(player, args) {
        let recvPlayerName;
        let amount;
        let itemInput;
        // different combinations of arguments are allowed:
        // /give item
        // /give amount item
        // /give item amount
        // /give player item
        // /give item player
        // /give player amount item
        // /give amount item player
        if (args.length === 2) {
            // /give item
            recvPlayerName = player.name;
            amount = 1;
            itemInput = args[1];
        } else if (args.length === 3) {
            // /give amount item
            // or
            // /give player item
            // or
            // /give item amount
            // or
            // /give item player
            if (!isNaN(args[1])) {
                // /give amount item
                recvPlayerName = player.name;
                amount = args[1];
                itemInput = args[2];
            } else if(Inventory.humanInput2itemCode(args[1]) === null) {
                // /give player item
                recvPlayerName = args[1];
                amount = 1;
                itemInput = args[2];
            } else if (!isNaN(args[2])) {
                // /give item amount
                recvPlayerName = player.name;
                amount = args[2];
                itemInput = args[1];
            } else {
                // /give item player
                recvPlayerName = args[2];
                amount = 1;
                itemInput = args[1];
            }
        } else if (args.length === 4) {
            // /give player amount item
            // or
            // /give amount item player
            if (isNaN(args[1])) {
                // /give player amount item
                recvPlayerName = args[1];
                amount = args[2];
                itemInput = args[3];
            } else {
                // /give amount item player
                recvPlayerName = args[3];
                amount = args[1];
                itemInput = args[2];
            }
        } else {
            this.sendUsageString(player);
            return;
        }

        // find target player
        const recvPlayer = this.playerData.onlinePlayerByName(recvPlayerName);

        if (recvPlayer === null) {
            player.sendError('player not found: '+recvPlayerName);
            return;
        }

        // parse the specified item
        const item_code = Inventory.humanInput2itemCode(itemInput);
        if (item_code === null) {
            player.sendError('unknown item: '+itemInput);
            return;
        }

        // it is possible for a non-number to sneak through
        if (isNaN(amount)) {
            player.sendError('invalid amount: '+amount);
            return;
        }

        // nope didn't fail
        const itemName = Inventory.item_names[item_code];
        if (amount >= 0) {
            player.sendChat('giving '+amount+' '+itemName+' to '+recvPlayerName);
        } else {
            player.sendChat('removing '+amount * -1+' '+itemName+' from '+recvPlayerName);
        }
        const intAmount = parseInt(amount);
        if (intAmount < 0 && !recvPlayer.inventory.canGain(item_code, intAmount)) {
            // can't go negative
            const haveAmount = recvPlayer.inventory.numItems(item_code);
            recvPlayer.gainItemWithNotification(item_code, -haveAmount);
        }
        else {
            if (Inventory.isDataItem(item_code)) {
                // Must make new item data for each item given
                for (let i = 0; i < intAmount; i++) {
                    const item_data = Inventory.makeItemData(item_code);
                    recvPlayer.gainItemWithNotification(item_code, 1, item_data);
                }
            } else {
                // Allowing items to be given beyond limit
                recvPlayer.gainItemWithNotification(item_code, intAmount);
            }
        }
        //update properly
        recvPlayer.changedInventory = true;
    }

}
