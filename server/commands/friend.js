// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'add/remove', type:'friendoperation', mandatory:true},
        {name:'name', type:'onlineplayer', mandatory:true},
    ]];
    isAdmin = false;
    help = 'Friend management.';
    usageExtraLine = 'example: /friend add "Berry Icicle"';

    execute(player, args) {

        if (args.length < 3) {
            this.sendUsageString(player);
            return;
        }

        let yay;
        if (['add', 'accept', 'request'].includes(args[1])) {
            yay = true;
        }
        else if (['remove', 'decline', 'reject', 'revoke', 'cancel'].includes(args[1])) {
            yay = false;
        }
        else {
            this.sendUsageString(player);
            return;
        }

        const otherName = ACCOUNTS.findMatchingName(args[2]);
        if (otherName === null) {
            player.sendError('can\'t find account '+args[2]);
            return;
        }

        const myName = player.name;
        if (!(myName in ACCOUNTS.accounts)) {
            player.sendError('you do not have an account \\[#f00\\](this is impossible!!)');
            return;
        }

        if (otherName === myName) {
            player.sendChat(
                'Can you be friends with yourself? I\'m not sure ... I \
mean, I guess you could if you wanted, but usually \
"friend" means someone else ... either way, this is not \
implemented in AAGRINDER. Friend management is already \
very complicated as is, and allowing you to have \
yourself in the friend list would make it even more \
complicated. Sorry!');
            return;
        }

        const myAcc = ACCOUNTS.accounts[myName];
        const otherAcc = ACCOUNTS.accounts[otherName];
        // may or may not be online
        const otherPlayer = this.playerData.onlinePlayerByName(otherName);

        // prepare some lambda functions that capture the above variables

        const removeFromLists = (myListName, otherListName) => {
            // note: here I am using strings as "pointers" to
            // Javascript object members because I need to be able to
            // completely re-assign them (not only modify)
            myAcc[myListName] = myAcc[myListName].filter(
                name => name !== otherName);
            // send update to client
            player.changedFriends = true;
            // remove from other account
            otherAcc[otherListName] = otherAcc[otherListName].filter(
                name => name !== myName);
            // skip notifying other player if not online
            if (otherPlayer === null) return;
            // send update to other client
            otherPlayer.changedFriends = true;
        }

        const addToLists = (myListName, otherListName) => {
            // note: for consistency with removeFromLists,
            // I am using the same interface, even though I could
            // simply be passing the lists themselves in this case.
            if (!myAcc[myListName].includes(otherName)) {
                myAcc[myListName].push(otherName);
            }
            // send update to client
            player.changedFriends = true;
            // add to other list (but avoid duplicates)
            if (!otherAcc[otherListName].includes(myName)) {
                otherAcc[otherListName].push(myName);
            }
            // skip notifying other player if not online
            if (otherPlayer === null) return;
            // send update to other client
            otherPlayer.changedFriends = true;
        }

        if (yay) {  // yay: adding or accepting a friend

            let myListName, otherListName;
            let message;
            if (myAcc.friends.includes(otherName)) {
                player.sendError('Already friend: '+otherName);
                return;  // do not update accounts.json
            }
            else if (myAcc.requests.includes(otherName)) {
                addToLists('friends', 'friends');
                removeFromLists('requests', 'pending');
                player.sendChat('Friend request accepted from '+otherName);
            }
            else if (myAcc.pending.includes(otherName)) {
                player.sendChat('Already outgoing friend request to '+otherName);
                return;  // do not update accounts.json
            }
            else {  // other player is not in any of my lists yet
                addToLists('pending', 'requests');
                player.sendChat('Sent friend request to '+otherName);
            }

        } else {  // nay: removing, declining or cancelling a friend

            if (myAcc.friends.includes(otherName)) {
                removeFromLists('friends', 'friends');
                player.sendChat('Friend removed: '+otherName);
            }
            else if (myAcc.requests.includes(otherName)) {
                removeFromLists('requests', 'pending');
                player.sendChat('Incoming friend request declined: '+otherName);
            }
            else if (myAcc.pending.includes(otherName)) {
                removeFromLists('pending', 'requests');
                player.sendChat('Outgoing friend request cancelled from '+otherName);
            }
            else {  // other player is not in any of my lists
                player.sendError('No such friend: '+otherName);
                return;  // do not update accounts.json
            }
        }

        // immediately update player positions
        // (in case friendship makes a difference)
        this.syncher.updateMyAndOtherPositionLists(player)
        // save these changes to the accounts.json file
        ACCOUNTS.save();
    }
}
