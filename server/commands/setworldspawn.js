// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'x', type:'xcoord', mandatory:false},
        {name:'y', type:'ycoord', mandatory:false},
    ]];
    isAdmin = true;
    help = 'Change world spawn to a new location.';

    execute(player, args) {
        if (args.length !== 1 && args.length !== 3) {
            this.sendUsageString(player);
            return;
        }
        let spawnareax = player.x;
        let spawnareay = player.y;
        if (args.length === 3) {
            if (isNaN(args[1]) || isNaN(args[2])) {
                this.sendUsageString(player);
                return;
            }
            spawnareax = parseInt(args[1]);
            spawnareay = parseInt(args[2]);
        }
        this.map.spawnareax = spawnareax;
        this.map.spawnareay = spawnareay;
        player.sendChat('set world spawn to '+this.map.spawnareax
            +', '+this.map.spawnareay);
    }

}
