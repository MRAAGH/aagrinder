// Copyright (C) 2025 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

module.exports = class extends Command {
    signatures = [[
        {name:'structure', type:'structure', mandatory:true},
    ]];
    isAdmin = true;
    help = 'Locate a nearby structure of given type.';

    async execute(player, args) {

        if (args.length !== 2) {
            this.sendUsageString(player);
            return;
        }

        const structureInput = args[1];
        // validate structure (this is an
        // important filter and prevents remote code execution!)
        let structure = null;
        for (const validNames of Command.KNOWN_STRUCTURES) {
            if (validNames.includes(structureInput.toLowerCase())) {
                structure = validNames[0];
                break;
            }
        }
        if (structure === null) {
            player.sendError('unknown structure: '
                + structureInput + '\\[n\\]available structures: '
                + Command.KNOWN_STRUCTURES.map(b=>b[0]).join(', '));
            return;
        }

        const chunkx = player.x>>8;
        const chunky = player.y>>8;
        let found;
        try {
            found = await this.findStructure(chunkx, chunky, structure, this.props.multithread);
        } catch (e) {
            player.sendError(e);
            return;
        }
        player.sendChat('found '+structure+' at '+found.x+' '+found.y);

    }

}
