// Copyright (C) 2022-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');

// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}


module.exports = class extends Command {
    signatures = [
        [
            {name:'x', type:'xcoord', mandatory:false},
            {name:'y', type:'ycoord', mandatory:false},
            {name:'biome', type:'biomename', mandatory:true},
        ],
        [
            {name:'biome', type:'biomename', mandatory:true},
        ],
    ];
    isAdmin = true;
    help = 'Change the biome at specific coordinates.';

    execute(player, args) {

        if (args.length !== 2 && args.length !== 4) {
            this.sendUsageString(player);
            return;
        }

        // calculate coordinates
        let x;
        let y;
        let biomeInput;
        if (args.length === 4) {
            x = this.parseRelative(player.x, args[1]);
            y = this.parseRelative(player.y, args[2]);
            biomeInput = args[3];
        }
        else {
            x = player.x;
            y = player.y;
            biomeInput = args[1];
        }

        // validate coordinates
        if (x === false || y === false) {
            this.sendUsageString(player);
            return;
        }

        // validate biome
        let biome = null;
        let biomeName = null;
        for (const validNames of Command.KNOWN_BIOMES) {
            if (validNames.includes(biomeInput)) {
                biome = validNames[0];
                biomeName = validNames[2];
                break;
            }
        }
        if (biome === null) {
            player.sendError('unknown biome: '
                + biomeInput + '\\[n\\]available biomes: '
                + Command.KNOWN_BIOMES.map(b=>b[2]).join(', '));
            return;
        }

        // apply
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const subchunkx = ((x%256+256)%256)>>2;
        const subchunky = ((y%256+256)%256)>>3;
        if (this.map.chunks[chunky] === undefined
            || this.map.chunks[chunky][chunkx] === undefined) {
            player.sendError('not loaded. Unable to set biome');
            return;
        }
        this.map.chunks[chunky][chunkx].biomes =
            this.map.chunks[chunky][chunkx].biomes
            .replaceAt(64*subchunky+subchunkx, biome);
        this.map.chunks[chunky][chunkx].dirty = true;
        player.sendChat('set biome to '+biomeName+' (relog to see)');

    }

}
