// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

const Command = require('../command');
const Inventory = require('../../shared/inventory').Inventory;

module.exports = class extends Command {
    signatures = [[
        {name:'x1', type:'xcoord', mandatory:true},
        {name:'y1', type:'ycoord', mandatory:true},
        {name:'x2', type:'xcoord', mandatory:false},
        {name:'y2', type:'ycoord', mandatory:false},
        {name:'block', type:'itemname', mandatory:true},
    ]];
    isAdmin = true;
    help = 'Fill an area of the world with the specified block.';

    execute(player, args) {

        if (args.length !== 4 && args.length !== 6) {
            this.sendUsageString(player);
            return;
        }

        // calculate coordinates
        let areax = this.parseRelative(player.x, args[1]);
        let areay = this.parseRelative(player.y, args[2]);
        let areax2;
        let areay2;
        let blockInput;
        if (args.length === 6) {
            areax2 = this.parseRelative(player.x, args[3]);
            areay2 = this.parseRelative(player.y, args[4]);
            blockInput = args[5];
        }
        else {
            areax2 = player.x;
            areay2 = player.y;
            blockInput = args[3];
        }

        // validate coordinates
        if (areax === false || areay === false
            || areax2 === false || areay2 === false) {
            this.sendUsageString(player);
            return;
        }

        // validate blockInput
        const block = Inventory.humanInput2blockCode(blockInput);
        if (block === null) {
            player.sendError('unknown block: ' + blockInput);
            return;
        }

        // swap if needed
        if (areax > areax2) [areax, areax2] = [areax2, areax];
        if (areay > areay2) [areay, areay2] = [areay2, areay];

        const w = areax2 - areax;
        const h = areay2 - areay;

        // apply
        for (let dy = 0; dy <= h; dy++) {
            for (let dx = 0; dx <= w; dx++) {
                this.syncher.serverChangeBlock(
                    areax+dx, areay+dy, block);
            }
        }

        player.sendChat('filled '
            + (areax2-areax+1) * (areay2-areay+1)
            + ' blocks');

    }

}
