// Copyright (C) 2021-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const { ElementColors } = require('./elementcolors');

class AagrinderMatrixBot {
    constructor(props, playerData) {
        this.props = props;
        this.playerData = playerData;
        this.matrixBotRunning = false;
        if (this.props.matrix_bot_enabled !== true) return;
        try {
            this.login();
        }
        catch (e) {
            LOG.logonly(e);
            LOG.log('No Matrix support (please install matrix-bot-sdk)');
            return;
        }

        this.start();
    }

    login() {
        const sdk = require('matrix-bot-sdk');
        const MatrixClient = sdk.MatrixClient;
        const SimpleFsStorageProvider = sdk.SimpleFsStorageProvider;

        const homeserverUrl = this.props.matrix_homeserver_url;
        const accessToken = this.props.matrix_bot_access_token;

        const storage = new SimpleFsStorageProvider('mx-bot-data.json');

        this.client = new MatrixClient(
            homeserverUrl, accessToken, storage);
    }

    start() {
        this.client.start().then(() => {
            LOG.log('Matrix bot running!');
            this.matrixBotRunning = true;
        });
        this.client.on('room.message', (roomId, event) =>
            this.handleRemoteChat(roomId, event));
    }

    handleLocalChat(player, typed) {
        if (this.matrixBotRunning) {
            // do not bridge private message
            if (typed.length > 2 && typed.substr(0,2) === 'L ') return;
            const message = player.name + ': ' + typed;
            this.client.sendMessage(this.props.matrix_room_id, {
                'msgtype': 'm.text',
                'body': message,
            });
        }
    }

    handleRemoteChat(roomId, event) {
        if (!event['content']) return;
        // ignore other rooms
        if (roomId !== this.props.matrix_room_id) return;
        const sender = event['sender'];
        // ignore myself
        if (sender === this.client.userId) return;
        const body = event['content']['body'];
        this.client.getUserProfile(sender).then(profile => {
            const displayName = profile.displayname;
            const nameColor = ElementColors.getUserNameColor(sender);
            const message = '\\['+nameColor+'\\]'+displayName + '\\[/\\]: ' + body;
            // broadcast this prepared message
            for (const sendPlayer of this.playerData.onlinePlayers) {
                sendPlayer.sendTimestampedChat(message, true);
            }
        });
    }

}

exports.AagrinderMatrixBot = AagrinderMatrixBot;
