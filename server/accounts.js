// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * The data from accounts.json
 *
 * this is stored separately from the world and acts like a "database".
 * But, for simplicity, the data is accessed directly (there is no interface)
 */

"use strict";

const fs = require('fs');

class Accounts {

    constructor(props) {
        this.props = props;
        this.accounts = {};  // indexed by name
        this.autoreloadCooldown = 0;
        this.load();
        // autoreload file when changed
        fs.watch('accounts.json', ()=>{
            if (this.autoreloadCooldown > 0) {
                this.autoreloadCooldown--;
                return;
            }
            console.log('reloading accounts.json');
            this.load();
        });
    }

    load() {
        let config_exists = fs.existsSync('accounts.json');
        if (!config_exists) {
            this.save();
            return;
        }
        const loadedString = fs.readFileSync('accounts.json').toString('utf8');
        this.accounts = JSON.parse(loadedString);
    }

    save() {
        this.autoreloadCooldown++;
        fs.writeFileSync('accounts.json', JSON.stringify(this.accounts));
        console.log('updating accounts.json');
    }

    findMatchingName(name) {
        const foundNames = Object.keys(this.accounts).filter(
            a=>a.toUpperCase() === name.toUpperCase());
        if (foundNames.length < 1) {
            // no such account found
            return null;
        }
        return foundNames[0];
    }
}

exports.Accounts = Accounts;
