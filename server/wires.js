// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * the whole wire + wire + motor + flying mechanics
 *
 * Note that all terrain changes MUST go through the syncher,
 * otherwise clients would not get updates.
 *
 */

"use strict";

const SPRITES = require('../shared/sprites').SPRITES;
const Inventory = require('../shared/inventory').Inventory;
const AASTRING = require('../shared/aastring');

const MOTOR_MOVE_LIMIT = 48;

// @ts-ignore
String.prototype.replaceAt = function(index, newchar) {
    return this.substring(0, index) + newchar + this.substring(index+1);
}

const wireDirections = Object.freeze({
    '0': [ true,false, true,false],
    '1': [ true,false,false,false],
    '2': [false, true,false,false],
    '3': [ true, true,false,false],
    '4': [false,false, true,false],
    '5': [ true,false, true,false],
    '6': [false, true, true,false],
    '7': [ true, true, true,false],
    '8': [false,false,false, true],
    '9': [ true,false,false, true],
    'a': [false, true,false, true],
    'b': [ true, true,false, true],
    'c': [false,false, true, true],
    'd': [ true,false, true, true],
    'e': [false, true, true, true],
    'f': [ true, true, true, true],
});

const int2hex = '0123456789abcdef';
const hex2int = Object.freeze({
    '0': 0,
    '1': 1,
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'a': 10,
    'b': 11,
    'c': 12,
    'd': 13,
    'e': 14,
    'f': 15,
});

const DIR_RIGHT = {dx:1, dy:0, dir:0, invdir:2};
const DIR_UP = {dx:0, dy:1, dir:1, invdir:3};
const DIR_LEFT = {dx:-1, dy:0, dir:2, invdir:0};
const DIR_DOWN = {dx:0, dy:-1, dir:3, invdir:1};

const DIRS = [ DIR_RIGHT, DIR_UP, DIR_LEFT, DIR_DOWN ];

// Some hardcoded logic
// to avoid extra processing each time
const EXCLUDED_DIRS = [
    [ DIR_UP, DIR_LEFT, DIR_DOWN ],
    [ DIR_RIGHT, DIR_LEFT, DIR_DOWN ],
    [ DIR_RIGHT, DIR_UP, DIR_DOWN ],
    [ DIR_RIGHT, DIR_UP, DIR_LEFT ],
];

const MOTOR_DXDY = {
    'w': {dx: 0, dy: 1},
    'a': {dx:-1, dy: 0},
    's': {dx: 0, dy:-1},
    'd': {dx: 1, dy: 0},
};

const GRINDER_CHEST_TARGETS = [
    {dx: 1, dy: 0},
    {dx: 1, dy: 1},
    {dx: 0, dy: 1},
    {dx:-1, dy: 1},
    {dx:-1, dy: 0},
    {dx:-1, dy:-1},
    {dx: 0, dy:-1},
    {dx: 1, dy:-1},
];

class Wires {

    constructor(intervalManager, map, syncher, playerData, water, hopper) {
        this.map = map;
        this.syncher = syncher;
        this.playerData = playerData;
        this.water = water;
        this.hopper = hopper;
        this.queue = [];
        this.nextqueue = [];
        this.intervalManager = intervalManager;
        this.registerWithMultiplier(1);
    }

    registerWithMultiplier(multiplier) {
        this.intervalManager.unregister('20wires');
        this.intervalManager.register('20wires', 200/multiplier,
            () => this.doWireTick200Adjustable());
    }

    unregisterInterval() {
        this.intervalManager.unregister('20wires');
    }

    enqueue(x, y) {
        this.queue.push({x:x, y:y});
    }

    pointingDirections(block) {
        if (block[0] === '=') {
            return wireDirections[block[1]];
        }
        if (block[0] === '%') {
            return [true, true, true, true];
        }
    }

    directionalPower(block, direction) {
        const dir = DIRS[direction];

    }

    findStrongestPowerSource(x, y, dirs = DIRS,
        usePotentialDrop = false, allowWireGap = false) {
        let maxPower = 0;
        let char = 'R';
        let maxDir = 0;
        for (const dir of dirs) {
            const otherBlock = this.map.getBlockWithoutLoading(
                x + dir.dx, y + dir.dy);
            if (['R1', 's1', 'O1', 'O2'].includes(otherBlock)) {
                // UNLIMITED POWER!!
                maxPower = 15;
                maxDir = dir.dir;
                if (otherBlock === 's1') char = 'S';
                if (otherBlock[0] === 'O') char = 'O';
                break;
            }
            if (otherBlock[0] === ':') {
                const power = otherBlock[1];
                if ((dir.dir === 1 || dir.dir === 3)
                    && (power === '0' || power === '2')) {
                    // Powered by the not gate
                    maxPower = 15;
                    maxDir = dir.dir;
                    char = otherBlock[2];
                    break;
                }
            }
            if (otherBlock === 'sC' && dir.dir === 3) {
                // Maybe powered by the aascanner
                const scanBlock = this.map.getBlockWithoutLoading(x, y-2);
                const scanCode = Inventory.block2scanCode(scanBlock);
                if (scanCode === null) continue;
                maxPower = 15;
                maxDir = 3;
                char = scanCode;
                break;
            }
            if (otherBlock === 'sm' && dir.dir === 2) {
                // Maybe powered by aaterminal
                const tile = this.map.getTileEntityAt(
                    x + dir.dx, y + dir.dy)[0];
                if (tile === undefined) continue;  // should never happen
                if (tile.t !== 'aaterminal') continue;  // should never happen
                if (tile.d.out === ' ') continue;
                maxPower = 15;
                maxDir = 3;
                char = tile.d.out;
                break;
            }
            if (otherBlock[0] === '=') {
                if (
                    // checking if the other wire is pointing at me,
                    // because then, it should be powering me.
                    allowWireGap ||
                    wireDirections[otherBlock[1]][dir.invdir]
                ) {
                    let power = hex2int[otherBlock[2]];
                    if (usePotentialDrop) {
                        power -= 1;
                    }
                    if (power > maxPower) {
                        maxPower = power;
                        maxDir = dir.dir;
                        char = otherBlock[3];
                    }
                }
            }
            if (otherBlock[0] === '%') {
                // possible that power goes 2 spaces
                const otherOtherBlock = this.map.getBlockWithoutLoading(
                    x + 2*dir.dx, y + 2*dir.dy);
                if (otherOtherBlock[0] === '=') {
                    if (
                        // checking if the other wire is pointing at me,
                        // because then, it should be powering me.
                        wireDirections[otherOtherBlock[1]][dir.invdir]
                    ) {
                        // the other wire is pointing at me.
                        let power = hex2int[otherOtherBlock[2]];
                        if (usePotentialDrop) {
                            power -= 1;
                        }
                        if (power > maxPower) {
                            maxPower = power;
                            maxDir = dir.dir;
                            char = otherOtherBlock[3];
                        }
                    }
                }
            }
        }
        return {power:maxPower, char:char, dir:maxDir};
    }

    enqueueSurroundings(x, y, dirs = DIRS) {
        for (const dir of dirs) {
            const ox = x + dir.dx;
            const oy = y + dir.dy;
            const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
            if (
                otherBlock === 'st'
                || otherBlock[0] === '='
                || otherBlock === 'sT'
                || otherBlock === 'WO'
                || otherBlock === 'WD'
                || otherBlock === 'Wp'
                || otherBlock === 'sc'
                || otherBlock === 'sC'
                || otherBlock[0] === '%'
            ) {
                this.queue.push({x:ox, y:oy});
            }
            if (otherBlock[0] === '%') {
                this.queue.push({x:x+dir.dx*2, y:y+dir.dy*2});
            }
            else if (otherBlock[0] === ':'
                || otherBlock[0] === 'O'
                || otherBlock[0] === 'M'
                || otherBlock[0] === 'G'
                || otherBlock === 'sm'
            ) {
                this.nextqueue.push({x:ox, y:oy});
            }
            // else if (otherBlock === ...)
        }
    }

    onAAtype(wx, wy, text) {
        const tile = this.map.getTileEntityAt(wx, wy)[0];
        if (tile === undefined) return;
        if (tile.t !== 'aaterminal') return;
        tile.d.queue += text;
        this.appendTextToAAterminal(tile, text);
        // ensure the chunk will be saved:
        this.map.setDirtyAtXY(tile.x, tile.y);
        // queue and output will start processing on next wire tick
        this.queue.push({x:tile.x, y:tile.y});
        this.queue.push({x:tile.x+1, y:tile.y});
    }

    appendTextToAAterminal(tile, text) {
        // ensure the lines list is not empty
        // (a fix for backwards compatibility)
        if (!Array.isArray(tile.d.lines) || tile.d.lines.length < 1) {
            tile.d.lines = [''];
        }
        // append new text to last line
        tile.d.lines[tile.d.lines.length-1] += text;
        // rewrap last line
        AASTRING.AASTRING_REWRAP_LAST_LINE_WITHOUT_DECODING(tile.d.lines, 12);
        // cutoff anything beyond the last 4 lines:
        while (tile.d.lines.length > 4) {
            tile.d.lines.shift();
        }
    }

    doWireTick200Adjustable() {
        this.removeDuplicatesFromQueue();

        for (let i = 0; i < this.queue.length; i++) {
            const pos = this.queue[i];
            const block = this.map.getBlockWithoutLoading(pos.x, pos.y);
            if (block[0] === '=') {
                // it's a wire
                // find my strongest power source
                // but only from the directions where this wire is pointing
                const max = this.findStrongestPowerSource(pos.x, pos.y,
                    DIRS.filter(dir=>wireDirections[block[1]][dir.dir]), true, true);
                // propagated power decreases by 1 if coming from other wire
                const maxPower = max.power;
                const char = max.char;
                // now I know the strongest power powering me.
                const actualPowerHere = hex2int[block[2]];
                if (maxPower > actualPowerHere) {
                    // you power me uuuuuup
                    const newBlock = block.substr(0,2) + int2hex[maxPower] + char;
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                    // and send updates to all blocks I'm pointing at
                    this.enqueueSurroundings(pos.x, pos.y, DIRS);
                }
                else if (maxPower < actualPowerHere) {
                    // my max power is actually LOWER than my current power.
                    // the way we deal with this, is become zero.
                    const newBlock = block.replaceAt(2, '0');
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                    // and send updates to all blocks I'm pointing at.
                    this.enqueueSurroundings(pos.x, pos.y, DIRS);
                    // also update myself later, because turning to zero
                    // was probably not completely correct.
                    this.queue.push({x:pos.x, y:pos.y});
                }
                // there's also the case in which this wire is powered
                // exactly the way it's supposed to be.
                // We do nothing in that case.
            }
            else if (block[0] === '%') {
                // it's a wire crossing
                // find strongest wire power around this block
                // (only for visualization)
                let maxPower = 0;
                for (const dir of DIRS) {
                    const otherBlock = this.map.getBlockWithoutLoading(
                        pos.x + dir.dx, pos.y + dir.dy);
                    if (otherBlock[0] !== '=') continue;
                    if (!wireDirections[otherBlock[1]][dir.invdir]) continue;
                    const potentialPower = hex2int[otherBlock[2]];
                    if (potentialPower <= maxPower) continue;
                    maxPower = potentialPower;
                }
                // The crossing lights up with power 1 less than the wire
                let powerHere = maxPower - 1;
                if (powerHere < 0) powerHere = 0;
                const newBlock = '%' + int2hex[powerHere];
                if (newBlock !== block) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                }
            }
            else if (block[0] === ':') {
                // it's a gate
                const powerLeft = this.findStrongestPowerSource(pos.x, pos.y, [DIR_LEFT]);
                const powerRight = this.findStrongestPowerSource(pos.x, pos.y, [DIR_RIGHT]);
                let newBlock;
                if (powerLeft.power === 0 && powerRight.power === 0) {
                    // gate with both inputs off
                    newBlock = ':0:';
                }
                else if (powerLeft.power > 0 && powerRight.power > 0
                    && powerLeft.char === powerRight.char) {
                    // gate with both inputs on and same type
                    newBlock = ':2' + powerLeft.char;
                }
                else {
                    // all other cases
                    newBlock = ':1:';
                }
                if (newBlock !== block) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, newBlock);
                    this.enqueueSurroundings(pos.x, pos.y, [DIR_UP, DIR_DOWN]);
                }
            }
            else if (block === 'st' || block === 'sT') {
                // it's a trapdoor
                const maxPower = this.findStrongestPowerSource(pos.x, pos.y).power;
                if (maxPower > 0) {
                    if (block === 'st')
                        this.syncher.serverChangeBlock(pos.x, pos.y, 'sT');
                }
                else {
                    if (block === 'sT')
                        this.syncher.serverChangeBlock(pos.x, pos.y, 'st');
                }
            }
            else if (block === 'WO') {
                // it's a grabber
                const maxPower = this.findStrongestPowerSource(pos.x, pos.y).power;
                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'WD');
                }
            }
            else if (block === 'WD') {
                // it's a disabled grabber
                const maxPower = this.findStrongestPowerSource(pos.x, pos.y).power;
                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'WO');
                }
            }
            else if (block === 'Wp') {
                // it's an aaprinter
                const max = this.findStrongestPowerSource(pos.x, pos.y);
                const maxPower = max.power;
                const char = max.char;
                if (maxPower > 0) {
                    for (const dir of DIRS) {
                        const ox = pos.x + dir.dx;
                        const oy = pos.y + dir.dy;
                        const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
                        if (otherBlock[0] === '?') {
                            // change the existing ?
                            this.syncher.serverChangeBlock(ox, oy, '?'+char);
                            continue;
                        }
                        // for placing new block, it must be the
                        // opposite direction from power source
                        if (dir.invdir !== max.dir) continue;
                        // check available space
                        if (otherBlock !== ' '
                            && otherBlock !== 'w'
                            && otherBlock[0] !== '>') {
                            // nowhere to place new block
                            continue;
                        }
                        // get the tile of this aaprinter
                        const tile = this.map.getTileEntityAt(pos.x, pos.y)[0];
                        if (tile === undefined) continue;
                        if (tile.t !== 'chest') continue;
                        // check if there is a matching item in the inventory
                        const scanCodeIndex = tile.inventory.findSlotByScanCode(char);
                        let spent_item_code;
                        let placed_block;
                        if (scanCodeIndex !== null) {
                            // place this matching block from inventory.
                            spent_item_code = tile.inventory.slots[scanCodeIndex].code;
                            placed_block = Inventory.item2blockPlain(spent_item_code);
                        } else if (tile.inventory.hasItem('?')) {
                            // place ? from inventory.
                            spent_item_code = '?';
                            placed_block = '?'+char;
                        } else {
                            // no matching item in inventory, nor ? item.
                            continue;
                        }
                        this.syncher.serverChangeBlock(ox, oy, placed_block);
                        tile.inventory.gainItemByCode(
                            spent_item_code, -1, undefined);
                        this.syncher.updateTileEntity(tile);
                    }
                }
            }
            else if (block === 'sc') {
                // it's an unpowered aascanner
                const max = this.findStrongestPowerSource(pos.x, pos.y, [DIR_RIGHT, DIR_LEFT]);
                const maxPower = max.power;
                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'sC');
                    this.nextqueue.push({x:pos.x, y:pos.y+1});
                }
            }
            else if (block === 'sC') {
                // it's a powered aascanner
                const max = this.findStrongestPowerSource(pos.x, pos.y, [DIR_RIGHT, DIR_LEFT]);
                const maxPower = max.power;
                if (maxPower < 1) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'sc');
                    this.nextqueue.push({x:pos.x, y:pos.y+1});
                }
            }
            else if (block === 'M0') {
                // it's an unpowered motor
                const max = this.findStrongestPowerSource(pos.x, pos.y, DIRS);
                const maxPower = max.power;
                let motorChar = max.char;
                if (MOTOR_DXDY[motorChar] === undefined) {
                    motorChar = 'asdw'[max.dir];
                }

                if (maxPower > 0) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'M'+motorChar);
                    this.nextqueue.push({x:pos.x, y:pos.y});
                }
            }
            else if (block === 'MC') {
                // it's a motor on cooldown
                const max = this.findStrongestPowerSource(pos.x, pos.y, DIRS);
                const maxPower = max.power;
                if (maxPower === 0) {
                    // get out of cooldown
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'M0');
                    // but do not update self or anything
                }
            }
            else if (block[0] === 'M') {
                // it's a motor attempting to move
                const dxdy = MOTOR_DXDY[block[1]];
                const successfulMovement = this.attemptMotorMovement(
                    pos.x, pos.y, dxdy.dx, dxdy.dy);
                if (!successfulMovement) {
                    this.syncher.serverChangeBlock(pos.x, pos.y, 'M0');
                }
            }
            else if (block === 'O1') {
                // it's a first tick button
                this.syncher.serverChangeBlock(pos.x, pos.y, 'O2');
                // Only updating self, because output power did not change
                this.nextqueue.push({x:pos.x, y:pos.y});
            }
            else if (block === 'O2') {
                // it's a second tick button
                this.syncher.serverChangeBlock(pos.x, pos.y, 'O0');
                // Update surrounding wires that might turn off now
                this.enqueueSurroundings(pos.x, pos.y, DIRS);
            }
            else if (block[0] === 'G') {
                // it's a a grinder
                const max = this.findStrongestPowerSource(pos.x, pos.y, DIRS);
                const maxPower = max.power;
                if (maxPower > 0) {
                    const maxDir = max.dir;
                    // Try digging in each direction except where power
                    this.triggerGrinder(block,
                        pos.x, pos.y, EXCLUDED_DIRS[max.dir]);
                }
            }
            else if (block === 'sm') {
                // it's an aaterminal
                const tile = this.map.getTileEntityAt(pos.x, pos.y)[0];
                if (tile === undefined) return;
                if (tile.t !== 'aaterminal') return;
                // handle input signal
                const max = this.findStrongestPowerSource(pos.x, pos.y, [DIR_LEFT]);
                if (max.power === 0) {
                    tile.d.recv = false;
                } else if (tile.d.recv !== true) {
                    this.appendTextToAAterminal(tile, max.char);
                    tile.d.recv = true;
                }
                // process a tick of output queue
                let continueProcessing = false;
                if (tile.d.out !== ' ') {
                    // Currently outputting something (non-blank).
                    // Make a gap/pause in the output.
                    tile.d.out = ' ';
                    continueProcessing = true;
                } else if (tile.d.queue.length > 0) {
                    // We can now output the next character from the queue.
                    tile.d.out = tile.d.queue[0];
                    tile.d.queue = tile.d.queue.substring(1);
                    continueProcessing = true;
                }
                this.syncher.updateTileEntity(tile);
                this.map.setDirtyAtXY(tile.x, tile.y);
                if (continueProcessing) {
                    // in the next tick, continue processing the queue
                    this.nextqueue.push({x:tile.x, y:tile.y});
                    this.nextqueue.push({x:tile.x+1, y:tile.y});
                }
            }
        }

        // swap the queues
        this.queue = this.nextqueue;
        this.nextqueue = [];
    }

    // TODO: many possible optimizations for finding duplicates faster
    removeDuplicatesFromQueue() {
        const filteredQueue = [];
        for (const q of this.queue) {
            // Add to filtered queue if not duplicate
            if (filteredQueue.filter(fq => fq.x === q.x && fq.y === q.y)
                .length === 0) {
                filteredQueue.push(q);
            }
        }
        this.queue = filteredQueue;
    }

    attemptMotorMovement(posx, posy, dx, dy) {
        // initialize subset of world that will be moved:
        // TODO: possible optimization by using a 2D integer array of
        // static size which never gets reinitialized, only the
        // active integer gets incremented and the previous value invalidated
        const blocksToMove = {};
        // initialize DFS:
        const stack = [{x: posx, y: posy}];
        let numMoved = 0;
        // Begin DFS:
        while (stack.length > 0) {
            const cur = stack.pop();
            if (blocksToMove[cur.y] !== undefined) {
                if (blocksToMove[cur.y][cur.x] !== undefined) {
                    // Already been processed.
                    continue;
                }
            } else {
                // Initialize row because we might add this block
                blocksToMove[cur.y] = {};
            }
            const blockHere = this.map.getBlockWithoutLoading(cur.x, cur.y, 'unloaded');
            if (blockHere === 'unloaded') return false;
            if (blockHere === 'b') return false;
            if (blockHere === 'WW') return false;
            if (blockHere === 'WF') return false;
            if (blockHere === 'WB') return false;
            if (blockHere === ' ') continue;  // ignore air
            if (blockHere === '~') continue;  // ignore cloud
            if (blockHere[0] === '#') continue;  // ignore spooky
            if (blockHere === 'w') continue;  // ignore water
            if (blockHere[0] === '>') continue;  // ignore water
            if (blockHere !== 'WX' && cur.framework === true) continue;
            // Yes, will move this block.
            numMoved++;
            if (numMoved > MOTOR_MOVE_LIMIT) return false;
            blocksToMove[cur.y][cur.x] = blockHere;
            if (blockHere === 'WX') {
                // link framework diagonally:
                stack.push({x: cur.x+2, y: cur.y+2, framework: true});
                stack.push({x: cur.x-2, y: cur.y+2, framework: true});
                stack.push({x: cur.x+2, y: cur.y-2, framework: true});
                stack.push({x: cur.x-2, y: cur.y-2, framework: true});
            }
            if (blockHere === 'Dm' || blockHere === 'WX') {
                // sticky block. enqueue all directions:
                stack.push({x: cur.x, y: cur.y+1});
                stack.push({x: cur.x-1, y: cur.y});
                stack.push({x: cur.x+1, y: cur.y});
                stack.push({x: cur.x, y: cur.y-1});
                continue;
            }
            // all other blocks: enqueue only forward in direction of movement
            stack.push({x: cur.x+dx, y: cur.y+dy});
        }

        // Sort numerically (important for negative coordinates)
        const ycoordinates = Object.keys(blocksToMove).sort((a,b)=>a-b);
        if (dy > 0) {
            // Reverse the order of iteration, for a stable operation.
            // We iterate in the opposite direction of movement.
            // This way, nothing will accidentally be processed twice.
            ycoordinates.reverse();
        }
        for (const ystr of ycoordinates) {
            const row = blocksToMove[ystr];
            const y = parseInt(ystr);
            const ydy = y + dy;
            const xcoordinates = Object.keys(row).sort((a,b)=>a-b);
            if (dx > 0) {
                // see above reason for reversing ycoordinates
                xcoordinates.reverse();
            }
            for (const xstr of xcoordinates) {
                let b = row[xstr];
                const x = parseInt(xstr);
                const xdx = x + dx;
                // Cooldown all moved motors:
                if (b[0] === 'M') b = 'MC';
                // Remove previous block (before move)
                this.syncher.serverChangeBlock(x, y, ' ');
                // Place new block (after move)
                this.syncher.serverChangeBlock(xdx, ydy, b);
                // Update wires around previous position and new position
                this.enqueueSurroundings(x, y);
                this.enqueueSurroundings(xdx, ydy);
                // Update water around previous position and new position
                for (const dir of DIRS) {
                    {  // around previous position
                        const ox = x + dir.dx;
                        const oy = y + dir.dy;
                        const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
                        if (otherBlock === 'w'
                            || otherBlock[0] === '>') {
                            this.water.enqueue(ox, oy);
                        }
                    }
                    {  // around new position
                        const ox = xdx + dir.dx;
                        const oy = ydy + dir.dy;
                        const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
                        if (otherBlock === 'w'
                            || otherBlock[0] === '>') {
                            this.water.enqueue(ox, oy);
                        }
                    }
                }
                // Move tile entities
                this.syncher.moveTileEntityFromTo(
                    x, y, xdx, ydy)
                // Invalidate previous hopper positions
                // TODO: code duplicated from diggrinderlogic.js and could
                // be deduplicated as a server-side function which
                // gets called whenever a block is removed in any way.
                // As a bonus, after this is generalized, we will no
                // longer need a ref to water and hopper everywhere.
                if (b[0] === 'p' || b[0] === 'h' || b === 'Wp') {
                    this.hopper.hopperInvalidate(x, y);
                }
                // Process adjacent hoppers to new chest position
                // (equivalent/duplicated code from placelogic.js)
                if (b[0] === 'h') {
                    for (const dir of DIRS) {
                        const ox = xdx + dir.dx;
                        const oy = ydy + dir.dy;
                        const otherBlock = this.map.getBlockWithoutLoading(ox, oy);
                        if (otherBlock[0] === 'p') {
                            this.hopper.processHopper(ox, oy, otherBlock);
                        }
                    }
                }
                // Process moved hopper at new position
                if (b[0] === 'p') {
                    this.hopper.processHopper(xdx, ydy, b);
                }
            }
        }

        // Move players that are inside moved blocks
        for (const p of this.playerData.onlinePlayers) {
            let yesMovePlayer = false;
            if (blocksToMove[p.y] !== undefined) {
                if (blocksToMove[p.y][p.x] !== undefined) {
                    // Player is inside one of the moved blocks
                    // (currently this work with any block type)
                    yesMovePlayer = true;
                }
            }
            if (blocksToMove[p.y-dy] !== undefined) {
                if (blocksToMove[p.y-dy][p.x-dx] !== undefined) {
                    // Player is pushed by a moving block
                    yesMovePlayer = true;
                }
            }
            if (yesMovePlayer) {
                this.syncher.movePlayer(p, p.x+dx, p.y+dy);
            }
        }

        this.removeDuplicatesFromQueue();

        return true;
    }

    triggerGrinder(block, posx, posy, dirs = DIRS) {
        let numdug = 0;  // count how much durability to take
        for (const dir of dirs) {
            const ox = posx + dir.dx;
            const oy = posy + dir.dy;
            const grindedBlock = this.map.getBlockWithoutLoading(ox, oy);
            // do not dig these undiggable blocks
            if (
                grindedBlock[0] === 'G'
                || grindedBlock === 'Dm'
                || grindedBlock === 'WW'
                || grindedBlock === 'WB'
                || grindedBlock === 'WX'
                || grindedBlock[0] === '>'
                || grindedBlock === 'w'
                || grindedBlock === ' '
                || grindedBlock === '~'
            ) continue;
            if (block === 'GH') {
                // woodgrinder takes durability on everything
                numdug++;
            }
            if (grindedBlock === 'b') {
                // when trying to dig boulder,
                // take durability and fail.
                numdug += 10;
                continue;
            }
            if (grindedBlock === 'B') {
                // B can cost durability, depending on type and biome
                if (block === 'GH') {
                    // fail digging stone
                    continue;
                } else if (block === 'GB') {
                    // take durability
                    numdug++;
                    // succeed depending on biome
                    const biome = this.map.biomeAt(ox, oy);
                    if (biome === 'u') continue;
                } else {
                    // take durability depending on biome
                    const biome = this.map.biomeAt(ox, oy);
                    if (biome === 'u') numdug++;
                }
            }
            if (grindedBlock === 'WB') {
                // WB costs durability
                if (block === 'GH' || block === 'GB') {
                    // fail digging WB
                    continue;
                }
                numdug++;
            }
            const tileThere = this.map.getTileEntityAt(ox, oy)[0];
            if (tileThere !== undefined) {
                // There is a tile entity at the digged location
                if (tileThere.t === 'chest') {
                    // I have disabled chest digging
                    // because I can't think of where this would be useful.
                    continue;
                    // Attempt to dig a chest
                    if (grindedBlock[0] !== 'h') {
                        // Is not a normal chest. Do not dig
                        continue;
                    }
                    if (!tileThere.inventory.isEmpty()) {
                        // Do not dig non-empty chest
                        continue;
                    }
                    // Is an empty normal chest. Delete
                    this.syncher.deleteTileEntityAt(ox, oy);
                } else {
                    // Some other tile entity
                    // (obviously not grinder or warp,
                    // since those are undiggable)
                    // Simply delete it
                    this.syncher.deleteTileEntityAt(ox, oy);
                }
            }
            this.syncher.serverChangeBlock(ox, oy, ' ');
            // Update wires around position
            this.enqueueSurroundings(ox, oy, EXCLUDED_DIRS[dir.invdir]);
            // Update water around position
            for (const ddir of EXCLUDED_DIRS[dir.invdir]) {
                // Here you can really see why I want this to be centralized in blockUpdate. Duplicated code with renamed variables!
                const oox = ox + ddir.dx;
                const ooy = oy + ddir.dy;
                const otherBlock = this.map.getBlockWithoutLoading(oox, ooy);
                if (otherBlock === 'w'
                    || otherBlock[0] === '>') {
                    this.water.enqueue(oox, ooy);
                }
            }
            if (grindedBlock[0] === 'p'
                || grindedBlock[0] === 'h'
                || grindedBlock === 'Wp') {
                this.hopper.hopperInvalidate(ox, oy);
            }
            const gained_item = Inventory.block2item(grindedBlock);
            if (grindedBlock === 'Wt'
                || grindedBlock === 'sn'
                || grindedBlock === 'WS'
                || grindedBlock === '*|'
                || grindedBlock === '~') {
                // don't gain these unobtainable items
                continue;
            }
            // find chest where item would be gained
            let foundChest = null;
            for (const dxdy of GRINDER_CHEST_TARGETS) {
                const chestx = posx + dxdy.dx;
                const chesty = posy + dxdy.dy;
                const chestb = this.map.getBlockWithoutLoading(
                    chestx, chesty);
                if (chestb[0] !== 'h') continue;
                const chestThere = this.map.getTileEntityAt(
                    chestx, chesty)[0];
                // Check if item can be gained in this chest
                // (no need to check with item data,
                // since currently there are no data blocks
                // which can be collected in this way)
                if (!chestThere.inventory.canGain(
                    gained_item, 1)) continue;
                // Found a valid destination for item!
                foundChest = chestThere;
                break;
            }
            // Do not gain if no chest found
            if (foundChest === null) continue;
            // Gain item
            foundChest.inventory.gainItemByCode(
                gained_item, 1, undefined);
        }
        if (numdug > 0) {
            const tile = this.map.getTileEntityAt(posx, posy)[0];
            if (tile === undefined) {
                console.log('unable to get grinder tile entity at '+posx+', '+posy);
                // punishment for using this exploit
                this.syncher.deleteTileEntityAt(posx, posy);
                this.syncher.serverChangeBlock(posx, posy, ' ');
                return;
            }
            if (tile.d.d-numdug < 0) {
                // grinder breaks
                this.syncher.serverChangeBlock(posx, posy, ' ');
                this.syncher.deleteTileEntityAt(posx, posy);
            }
            else {
                // lose this much durability
                tile.d.d -= numdug;
                this.syncher.updateTileEntity(tile);
            }
        }
    }

    saveToJSON() {
        return {
            queue: this.queue,
            nextqueue: this.nextqueue,
        };
    }

    loadFromJSON(object) {
        if (typeof object === 'undefined') return;
        this.queue = object.queue;
        this.nextqueue = object.nextqueue;
    }

}

exports.Wires = Wires;
