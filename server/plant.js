// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * A function that runs on a random block in each loaded chunk:
 *   - every 10 ms
 *   - every 200 ms
 *   - every 2000 ms
 *
 * Takes care of events that happen rarely and spontaneously
 * (such as plants for example)
 * 
 */

"use strict";

const Ghosts = require('./ghost').Ghosts;
const Monsters = require('./monster').Monsters;
const Detectors = require('./detectors').Detectors;
const Tree = require('./tree').Tree;
const BP = require('../shared/blockprops').BlockProps;

const dirs4 = [
    {dx:1, dy:0},
    {dx:0, dy:1},
    {dx:-1, dy:0},
    {dx:0, dy:-1},
];

const dirs4d = [
    {dx:1, dy:1},
    {dx:-1, dy:1},
    {dx:-1, dy:-1},
    {dx:1, dy:-1},
];

const dirs8 = [
    {dx:1, dy:0},
    {dx:1, dy:1},
    {dx:0, dy:1},
    {dx:-1, dy:1},
    {dx:-1, dy:0},
    {dx:-1, dy:-1},
    {dx:0, dy:-1},
    {dx:1, dy:-1},
];

// tree growth probability for each tree type, for each biome
// 0: 0%
// 1: 100%
// 2: 100% and fruit is faster
const treeProbabilities = {

    //     appl  pine  almo  apri  avoc  bush  cran  blub  palm

    'j': { ta:2, ti:1, tl:1, tp:1, tv:1, tb:1, tr:1, tu:1, tm:1 },  // apple jungle
    'J': { ta:1, ti:1, tl:1, tp:1, tv:1, tb:1, tr:1, tu:1, tm:1 },  // jungle
    'A': { ta:2, ti:1, tl:1, tp:2, tv:0, tb:1, tr:1, tu:2, tm:0 },  // apple forest
    'm': { ta:2, ti:1, tl:1, tp:2, tv:0, tb:1, tr:1, tu:2, tm:0 },  // mixed forest
    'a': { ta:2, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1, tm:0 },  // apple
    'p': { ta:1, ti:1, tl:1, tp:2, tv:0, tb:1, tr:1, tu:1, tm:0 },  // apricot
    'P': { ta:1, ti:1, tl:1, tp:2, tv:0, tb:1, tr:1, tu:1, tm:0 },  // apricot fore
    'f': { ta:2, ti:1, tl:2, tp:1, tv:0, tb:1, tr:1, tu:1, tm:0 },  // flower islan
    'y': { ta:1, ti:1, tl:2, tp:1, tv:0, tb:1, tr:1, tu:1, tm:0 },  // yay island
    's': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1, tm:0 },  // sky
    'R': { ta:1, ti:1, tl:1, tp:1, tv:0, tb:1, tr:1, tu:1, tm:0 },  // rainbow
    't': { ta:1, ti:1, tl:0, tp:1, tv:0, tb:1, tr:2, tu:1, tm:0 },  // taiga
    'r': { ta:1, ti:1, tl:0, tp:1, tv:0, tb:1, tr:1, tu:0, tm:0 },  // mountain
    'z': { ta:1, ti:1, tl:0, tp:1, tv:0, tb:1, tr:1, tu:0, tm:0 },  // green mountain
    'i': { ta:0, ti:1, tl:0, tp:0, tv:0, tb:1, tr:2, tu:0, tm:0 },  // ice mountain
    'T': { ta:0, ti:1, tl:0, tp:0, tv:0, tb:1, tr:2, tu:0, tm:0 },  // snowy taiga
    'g': { ta:0, ti:1, tl:0, tp:0, tv:0, tb:1, tr:1, tu:0, tm:0 },  // glacier
    'd': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0, tm:1 },  // desert
    'O': { ta:0, ti:0, tl:1, tp:0, tv:1, tb:1, tr:0, tu:0, tm:1 },  // oasis
    'D': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0, tm:1 },  // dunes
    'C': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0, tm:0 },  // tentacle isl
    'o': { ta:0, ti:0, tl:1, tp:0, tv:0, tb:1, tr:0, tu:0, tm:0 },  // ocean
    'u': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0, tm:0 },  // underground
    'c': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0, tm:0 },  // cave
    'I': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0, tm:0 },  // ice cave
    'W': { ta:0, ti:0, tl:0, tp:0, tv:0, tb:1, tr:0, tu:0, tm:0 },  // warp

}

const CORAL_LOGIC = {
    'X0': {minwater: 4, mincoral:2, maxcoral:10, bfslimit:20},
    'X1': {minwater: 6, mincoral:0, maxcoral:6, bfslimit:20},
    'X2': {minwater: 6, mincoral:0, maxcoral:5, bfslimit:20},
    'X3': {minwater: 9, mincoral:2, maxcoral:3, bfslimit:8},
    'X4': {minwater: 6, mincoral:3, maxcoral:10, bfslimit:20},
    'X5': {minwater: 10, mincoral:0, maxcoral:6, bfslimit:20},
    'X6': {minwater: 4, mincoral:2, maxcoral:10, bfslimit:20},
    'X7': {minwater: 1, mincoral:7, maxcoral:30, bfslimit:-1},
    'X8': {minwater: 10, mincoral:0, maxcoral:6, bfslimit:-1},
}

const RUBY_ANIMATION = [
    {dx:+1, dy:0, duration:4},
    {dx:0, dy:+1, duration:4},
    {dx:-1, dy:0, duration:4},
    {dx:0, dy:-1, duration:4},
    {dx:+2, dy:0, duration:2},
    {dx:0, dy:+2, duration:2},
    {dx:-2, dy:0, duration:2},
    {dx:0, dy:-2, duration:2},
    {dx:+1, dy:+1, duration:2},
    {dx:-1, dy:+1, duration:2},
    {dx:-1, dy:-1, duration:2},
    {dx:+1, dy:-1, duration:2},
];

class Plant{

    constructor(intervalManager, map, playerData, subscribe, syncher, water, props) {
        this.map = map;
        this.subscribe = subscribe;
        this.syncher = syncher;
        this.ghosts = new Ghosts(map, playerData, subscribe, syncher, props);
        this.monsters = new Monsters(map, playerData, syncher, props);
        this.detectors = new Detectors(map, playerData);
        this.tree = new Tree(map, playerData, syncher);
        this.water = water;
        this.props = props;
        this.intervalManager = intervalManager;
        this.registerWithMultiplier(1);
        this.intervalManager.register('11monster', 1000, () => this.monsters.monsterTick());
    }

    registerWithMultiplier(multiplier) {
        this.intervalManager.unregister('10plant');
        this.intervalManager.register('10plant', 2000/multiplier,
            () => this.doAllPlantTicks2000Adjustable());
    }

    unregisterInterval() {
        this.intervalManager.unregister('10plant');
    }

    doAllPlantTicks2000Adjustable() {
        // I apologize to the reader, this interval
        // is confusing because it is so optimized
        for (let i = 0; i < 200; i++) {
            // for speed, only generate one pair of subchunk coordinates
            // and use this in all chunks.
            // it's really not important enough for randomizing in each separately
            // and this needs to be super fast!
            const subchunkx = Math.floor(Math.random()*256);
            const subchunky = Math.floor(Math.random()*256);
            for(const chunky in this.map.chunks) {
                for(const chunkx in this.map.chunks[chunky]) {
                    // For increased speed, not using map.getBlock here
                    const chunk = this.map.chunks[chunky][chunkx];
                    const blockHere = chunk.terrain[subchunky][subchunkx];
                    // For increased speed, not using map.getBiome here
                    const biomeHere = chunk.biomes[
                        64*(subchunky>>3)+(subchunkx>>2)];
                    const x = 256*parseInt(chunkx) + subchunkx;
                    const y = 256*parseInt(chunky) + subchunky;
                    // For increased speed, reuse x and y in multiple functions
                    this.plantUpdate10(x, y, blockHere, biomeHere);
                    if (i < 1) {
                        this.plantUpdate2000(x, y, blockHere, biomeHere);
                    }
                    if (i >= 1 && i < 3) {
                        this.plantUpdate200(x, y, blockHere, biomeHere, true, true);
                    }
                    if (i >= 3 && i < 6) {
                        this.plantUpdate200(x, y, blockHere, biomeHere, true, false);
                    }
                    if (i >= 6 && i < 11) {
                        this.plantUpdate200(x, y, blockHere, biomeHere, false, false);
                    }
                }
            }
        }
        this.globalPlantUpdate2000();
        this.tickcounter++;
    }

    plantUpdate10(x, y, blockHere, biomeHere) {

        if (blockHere[0] === 't') {
            // tiny tree must be on dirt
            // (or on stone, if it is type tb)
            // (or on sand, if it is type tm)
            const blockBelow = this.map.getBlockWithoutLoading(
                x, y-1, 'unloaded');
            if (blockBelow !== 'd'
                && !(blockHere[1] === 'b' && blockBelow === 'B')
                && !(blockHere[1] === 'm' && blockBelow === 'S'))
                return;
            if (blockHere[2] === '0') {
                // always goes from stage 0 to stage 1
                this.syncher.serverChangeBlock(x, y,
                    blockHere.substring(0,2)+'1');
            }
            else if (blockHere[2] === '1') {
                // before stage 2, check biome
                if (treeProbabilities[biomeHere]
                    ['t'+blockHere[1]] > 0) {
                    // success!
                    this.syncher.serverChangeBlock(x, y,
                        blockHere.substring(0,2)+'2');
                }
                else {
                    // DEATH
                    this.syncher.serverChangeBlock(x, y, 'Wt');
                }
            }
            else if (blockHere[2] === '2') {
                // okay tree attempt
                const type = Tree.letterToTreeType(blockHere[1]);
                if (type === null) {
                    console.error('Invalid tree: ' + blockHere);
                } else {
                    this.tree.treeAttempt(x, y, type);
                }
            }
        }

        else if (blockHere === 'WB') {
            if (this.props.spread_corruption !== true) return;
            // found a corruptston
            // limited by number of surrounding WB
            let numWB = 0;
            if (this.map.getBlockWithoutLoading(x+1, y, 'WB') === 'WB') numWB++;
            if (this.map.getBlockWithoutLoading(x, y+1, 'WB') === 'WB') numWB++;
            if (this.map.getBlockWithoutLoading(x-1, y, 'WB') === 'WB') numWB++;
            if (this.map.getBlockWithoutLoading(x, y-1, 'WB') === 'WB') numWB++;
            if (this.map.getBlockWithoutLoading(x+1, y+1, 'WB') === 'WB') numWB++;
            if (this.map.getBlockWithoutLoading(x-1, y-1, 'WB') === 'WB') numWB++;
            if (numWB > 2) return;
            for (let i = 0; i < 5; i++) {
                const dx = Math.round(Math.random()*3-1.5);
                const dy = Math.round(Math.random()*3-1.5);
                const inbetweenBlock = this.map.getBlockWithoutLoading(
                    x+dx, y);
                if (inbetweenBlock !== 'B' && inbetweenBlock !== 'WB') {
                    // not directly connected.
                    return;
                }
                const secondBlock = this.map.getBlockWithoutLoading(
                    x+dx, y+dy);
                if (secondBlock === 'B') {
                    // found a nearby B
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'WB');
                }
            }
        }

        else if (blockHere === 'Wt') {
            // dead tree disappears or turns to dirt
            if (Math.random() < 0.5) {
                this.syncher.serverChangeBlock(x, y, 'd');
            }
            else {
                this.syncher.serverChangeBlock(x, y, ' ');
            }
        }

        else if (blockHere === 'WA') {
            // must be on sand
            const blockBelow = this.map.getBlockWithoutLoading(
                x, y-1, 'unloaded');
            if (blockBelow === 'd') {
                // carrotatle growth attempt
                // must have space above
                if (this.map.getBlockWithoutLoading(x, y+1, 'B') !== ' ') return;
                // for simplicity, carrotatles grow tiny
                this.syncher.serverChangeBlock(x, y, 'WR');
                this.syncher.serverChangeBlock(x, y+1, 'WR');
                return;
            }
            if (blockBelow !== 'S') return;
            // must be in correct biome
            if (biomeHere !== 'C'
                && biomeHere !== 'W') return;
            // attempt tentacle growth
            this.tentacleAttempt(x, y);
        }

        else if (blockHere[0] === '#') {
            // ghost trail (smoke)
            // despawn it
            this.syncher.serverChangeBlock(x, y, ' ');
        }

        else if (blockHere[0] === 'W') {
            if (blockHere[1] === 'a') {
                // decaying leaf
                this.despawnLeaf(x, y, blockHere);
            }
        }

        else if (blockHere[0] === 'X' && blockHere !== 'Xx') {
            // coral death
            if (this.checkCoralDeath(x, y)) {
                this.syncher.serverChangeBlock(x, y, 'Xx');
            }
            else if (blockHere === 'X6'
                || blockHere === 'X7'
                || blockHere === 'X8') {
                // these three coral colors grow much faster
                const dir = dirs8[Math.floor(Math.random()*8)];
                if (this.checkCoralGrowth(
                    x+dir.dx, y+dir.dy, blockHere)) {
                    this.syncher.serverChangeBlock(
                        x+dir.dx, y+dir.dy, blockHere);
                }
                // also gets a chance of spontaneous death, hehe
                else if (Math.random() < 0.01) {
                    if (this.checkCoralSpontaneousDeath(x, y)) {
                        this.syncher.serverChangeBlock(x, y, 'Xx')
                    }
                }
            }
        }

        else if (blockHere === 'Xx') {
            if (!this.checkCoralStoning(x, y)) return;
            // possibly spread into sand
            for (let i = 0; i < 2; i++) {
                const dx = Math.floor(Math.random()*3-1);
                const dy = Math.floor(Math.random()*3-1);
                const b = this.map.getBlockWithoutLoading(x+dx, y+dy, 'B');
                if (b === 'S') {
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'Xx')
                }
            }
            this.syncher.serverChangeBlock(x, y,
                Math.random() < 0.2 ? 'S' : 'B');
        }

        else if (blockHere === 'w') {
            // this water might freeze into ice
            // (actually ice forms on water and not inside it)
            // cancel if wrong biome
            if (biomeHere !== 'i'
                && biomeHere !== 'I'
                && biomeHere !== 'g'
                && biomeHere !== 't'
                && biomeHere !== 'T') return;
            const blockAbove = this.map.getBlockWithoutLoading(
                x, y+1, 'B');
            // cancel if not air above
            if (blockAbove !== ' ') return;
            const blockLeft = this.map.getBlockWithoutLoading(
                x-1, y, ' ');
            const blockRight = this.map.getBlockWithoutLoading(
                x+1, y, ' ');
            const blockUpLeft = this.map.getBlockWithoutLoading(
                x-1, y+1, ' ');
            const blockUpRight = this.map.getBlockWithoutLoading(
                x+1, y+1, ' ');
            // must be adjacent to solid block or ice
            if (!BP.isSolidBlock(blockLeft)
                && !BP.isSolidBlock(blockRight)
                && blockUpLeft !== 'Wi'
                && blockUpRight !== 'Wi'
            ) return;
            // okay freeze
            this.syncher.serverChangeBlock(x, y+1, 'Wi');
            this.water.enqueue(x, y)
        }

        else if (blockHere === 'sn') {
            // this snow might spread
            // (to fill all the accessible corners)
            const dx = Math.floor(Math.random()*5-2);
            const dy = Math.floor(Math.random()*5-2);
            const blockThere = this.map.getBlockWithoutLoading(
                x+dx, y+dy, 'B');
            // only if it is air there
            if (blockThere !== ' ') return;
            // this air might become snow
            const blockUnderThere = this.map.getBlockWithoutLoading(
                x+dx, y+dy-1, ' ');
            // cancel if air below
            if (blockUnderThere === ' ') return;
            // cancel if wrong biome
            const biomeThere = this.map.getBiome(x+dx, y+dy);
            if (biomeThere !== 'i'
                && biomeThere !== 'I'
                && biomeThere !== 'g'
                && biomeThere !== 'T') return;
            // cancel if not on solid block
            if (!BP.isSolidBlock(blockUnderThere)) return;
            // cancel if on stalagmite
            if (blockUnderThere === 'WI') return;
            // okay become snow
            this.syncher.serverChangeBlock(x+dx, y+dy, 'sn');
        }

    }

    despawnLeaf(x, y, type) {
        // check if wood is nearby
        const wood = 'H' + type[2];
        for (let dx = -5; dx <= 5; dx++) {
            for (let dy = -2; dy <= 0; dy++) {
                if (this.map.getBlockWithoutLoading(
                    x+dx, y+dy, wood) === wood) {
                    // found wood. abort
                    return;
                }
            }
        }
        // there was no wood
        // despawn leaf
        this.syncher.serverChangeBlock(x, y, ' ');
        // despawn snow on top
        if (this.map.getBlockWithoutLoading(x, y+1) === 'sn') {
            this.syncher.serverChangeBlock(x, y+1, ' ');
        }
    }


    checkGrowVineDown(x, y) {
        // above the vine, there needs to be
        // a row of existing vines
        // and then a solid block
        for (let i = 1; i < 6; i++) {
            const blockThere = this.map.getBlockWithoutLoading(x, y+i);
            if (BP.isSolidBlock(blockThere)) {
                // found a solid block on top
                return true;
            }
            // otherwise, it has to be a vine
            if (blockThere !== '-a') {
                // well it's not
                return false;
            }
        }
        // can't grow because it is too long.
        return false;
    }

    checkGrowStalagmite(x, y) {
        // below the stalactite, there needs to be a sequence of air
        // and then stone (B)
        for (let i = 1; i < 32; i++) {
            const blockThere = this.map.getBlockWithoutLoading(x, y-i);
            if (blockThere === 'B') {
                // found bottom
                return i-1;
            }
            if (blockThere !== ' ' && blockThere !== 'sn') {
                // found ungrowable spot
                return -1
            }
        }
        // can't grow because it is too far.
        return -1;
    }

    checkGrowVineUp(x, y) {
        // below the vine, there needs to be
        // a row of existing vines
        // and then a solid block
        for (let i = 1; i < 6; i++) {
            const blockThere = this.map.getBlockWithoutLoading(x, y-i);
            // on top of coral?
            if (blockThere[0] === 'X' && i > 1) {
                // abort, because here, growth limit is much lower.
                return false;
            }
            if (BP.isSolidBlock(blockThere)) {
                // found a solid block on bottom
                return true;
            }
            // otherwise, it has to be a vine
            if (blockThere !== '-a') {
                // well it's not
                return false;
            }
        }
        // can't grow because it is too long.
        return false;
    }

    checkCoralDeath(x, y) {
        let adjacentDead = false;
        for (const dir of dirs8) {
            const b = this.map.getBlockWithoutLoading(x+dir.dx, y+dir.dy, 'w');
            if (b === 'w') {
                // can't die with adjacent water
                return false;
            }
            else if (b === 'Xx') {
                adjacentDead = true;
            }
        }
        if (adjacentDead) {
            // die
            return true;
        }
        for (const dir of dirs4) {
            const b = this.map.getBlockWithoutLoading(x+dir.dx, y+dir.dy, 'w');
            if (b === ' ') {
                // die in this case, too
                // because air
                return true;
            }
        }
        return false;
    }

    recognizeTentacleStage(x, y) {
        // we require the surrounding area to be loaded
        if(!this.map.isBlockLoaded(x-2, y)) return -1;
        if(!this.map.isBlockLoaded(x+2, y)) return -1;
        if(!this.map.isBlockLoaded(x-2, y+4)) return -1;
        if(!this.map.isBlockLoaded(x+2, y+4)) return -1;
        // check if stage 0
        if (
            this.map.getBlock(x, y+1) === ' '
            && this.map.getBlock(x+1, y) === ' '
            && this.map.getBlock(x-1, y) === ' '
        ) {
            return 0;
        }
        // check if stage 1
        if (
            this.map.getBlock(x, y+1) === 'WA'
            && this.map.getBlock(x+1, y) === ' '
            && this.map.getBlock(x-1, y) === ' '
            && this.map.getBlock(x+1, y+1) === ' '
            && this.map.getBlock(x-1, y+1) === ' '
            && this.map.getBlock(x, y+2) === ' '
        ) {
            return 1;
        }
        // check if stage 2
        if (
            this.map.getBlock(x, y+2) === ' '
            && this.map.getBlock(x, y+3) === ' '
            && this.map.getBlock(x+1, y+1) === ' '
            && this.map.getBlock(x-1, y+1) === ' '
            && this.map.getBlock(x+1, y+2) === ' '
            && this.map.getBlock(x-1, y+2) === ' '
            && this.map.getBlock(x+1, y+3) === ' '
            && this.map.getBlock(x-1, y+3) === ' '
            && this.map.getBlock(x, y+1) === 'WA'
            && this.map.getBlock(x+1, y) === 'WA'
            && this.map.getBlock(x-1, y) === 'WA'
            && this.map.getBlock(x+2, y) !== 'WA'
            && this.map.getBlock(x-2, y) !== 'WA'
            && this.map.getBlock(x+2, y+1) !== 'WA'
            && this.map.getBlock(x-2, y+1) !== 'WA'
        ) {
            return 2;
        }
        // check if stage 3
        if (
            this.map.getBlock(x, y+3) === ' '
            && this.map.getBlock(x+1, y+2) === ' '
            && this.map.getBlock(x-1, y+2) === ' '
            && this.map.getBlock(x, y+1) === 'WA'
            && this.map.getBlock(x, y+2) === 'WA'
            && this.map.getBlock(x+1, y) === 'WA'
            && this.map.getBlock(x-1, y) === 'WA'
            && this.map.getBlock(x+1, y+1) === 'WA'
            && this.map.getBlock(x-1, y+1) === 'WA'
            && this.map.getBlock(x+2, y) !== 'WA'
            && this.map.getBlock(x-2, y) !== 'WA'
            && this.map.getBlock(x+2, y+1) !== 'WA'
            && this.map.getBlock(x-2, y+1) !== 'WA'
            && this.map.getBlock(x+2, y+2) !== 'WA'
            && this.map.getBlock(x-2, y+2) !== 'WA'
            && this.map.getBlock(x+2, y+3) === ' '
            && this.map.getBlock(x-2, y+3) === ' '
            && this.map.getBlock(x+2, y+4) === ' '
            && this.map.getBlock(x-2, y+4) === ' '
        ) {
            // might be one of the stage 2 tentacles
            if (this.map.getBlock(x-1, y+3) === ' '
                && this.map.getBlock(x+1, y+3) === 'WA') {
                return 3;
            }
            if (this.map.getBlock(x-1, y+3) === 'WA'
                && this.map.getBlock(x+1, y+3) === ' ') {
                return 3;
            }
        }
        // it is not a valid stage.
        return -1;
    }

    tentacleAttempt(x, y) {

        const stage = this.recognizeTentacleStage(x, y);
        if (stage === -1) return;

        if (stage === 0) {
            this.syncher.serverChangeBlock(x, y+1, 'WA');
            return;
        }

        if (stage === 1) {
            this.syncher.serverChangeBlock(x+1, y, 'WA');
            this.syncher.serverChangeBlock(x-1, y, 'WA');
            return;
        }

        if (stage === 2) {
            this.syncher.serverChangeBlock(x+1, y+1, 'WA');
            this.syncher.serverChangeBlock(x-1, y+1, 'WA');
            this.syncher.serverChangeBlock(x, y+2, 'WA');
            if (Math.random() < 0.5) {
                this.syncher.serverChangeBlock(x-1, y+3, 'WA');
            }
            else {
                this.syncher.serverChangeBlock(x+1, y+3, 'WA');
            }
            return;
        }

        if (stage === 3) {
            // becomes proper tentacle
            // (using the terrain generator)
            this.tree.treeAttempt(x, y, 'tentacle');
        }

    }

    tryGrowFruit(x, y, type) {
        // we require the surrounding area to be loaded
        if(!this.map.isBlockLoaded(x-3, y+2)) return false;
        if(!this.map.isBlockLoaded(x+3, y+2)) return false;
        // block above must be part of tree too
        const blockAbove = this.map.getBlock(x, y+1);
        if (blockAbove !== 'a'+type
            && blockAbove !== 'A'+type
            && blockAbove !== 'Wa'+type
            && blockAbove !== 'H'+type) return false;
        if(!this.map.isBlockLoaded(x-3, y-2)) return false;
        if(!this.map.isBlockLoaded(x+3, y-2)) return false;
        for (let dy = -2; dy <= 2; dy++) {
            for (let dx = -3; dx <= 3; dx++) {
                const blockThere = this.map.getBlock(x+dx, y+dy);
                if (blockThere === 'f'+type
                    || blockThere === 'Wf'+type) {
                    // this other fruit is too close
                    return false;
                }
            }
        }
        this.syncher.serverChangeBlock(x, y, 'f'+type);
        return true;
    }


    tryGrowBerry(x, y, type) {
        // we require the surrounding area to be loaded
        if(!this.map.isBlockLoaded(x-2, y+1)) return false;
        if(!this.map.isBlockLoaded(x+2, y+1)) return false;
        if(!this.map.isBlockLoaded(x-2, y-1)) return false;
        if(!this.map.isBlockLoaded(x+2, y-1)) return false;
        for (let dy = -1; dy <= 1; dy++) {
            for (let dx = -2; dx <= 2; dx++) {
                const blockThere = this.map.getBlock(x+dx, y+dy);
                if (blockThere === 'f'+type
                    || blockThere === 'Wf'+type) {
                    // this other berry is too close
                    return false;
                }
            }
        }
        this.syncher.serverChangeBlock(x, y, 'f'+type);
        return true;
    }


    plantUpdate200(x, y, blockHere, biomeHere, isActuallyPlantUpdate400, isActuallyPlantUpdate1000) {

        if (blockHere === 'DD') {
            // found a diamond!
            // try to find nearby DD or B n times
            for (let i = 0; i < 6; i++) {
                // choose a nearby spot
                const dx = Math.floor(Math.random()*5)-2;
                const dy = Math.floor(Math.random()*5)-2;
                if (dx === 0 && dy === 0) continue;
                const secondBlock = this.map.getBlockWithoutLoading(
                    x+dx, y+dy);
                if (secondBlock === 'DD') {
                    // found another diamond nearby!
                    // and it's not the same one twice.
                    // 2 diamonds in the same area means
                    // we can convert nearby
                    // ruby stone (pink B) to ruby!
                    // unless there is already ruby in this area
                    let isAlreadyRuby = false;
                    for (let rdx = -7; rdx <= 7; rdx++) {
                        for (let rdy = -7; rdy <= 7; rdy++) {
                            if (this.map.getBlockWithoutLoading(
                                x+rdx, y+rdy, 'R1') === 'R1') {
                                isAlreadyRuby = true;
                                break;
                            }
                        }
                        if (isAlreadyRuby) break;
                    }
                    if (isAlreadyRuby) return;
                    // this loops many times to compensate
                    // for the fact that we had to randomly
                    // find the two diamonds (which is rare)
                    for (let i = 0; i < 10; i++) {
                        const dx2 = Math.floor(Math.random()*5)-2;
                        const dy2 = Math.floor(Math.random()*5)-2;
                        if (this.map.getBlockWithoutLoading(
                            x+dx2, y+dy2) !== 'R0') continue;
                        this.syncher.serverChangeBlock(x+dx2, y+dy2, 'R1');
                        this.syncher.serverBroadcastAnimation(
                            {x:x+dx2,y:y+dy2,b:'R0',a:4});
                        for (const rani of RUBY_ANIMATION) {
                            const aniwx = x+dx2+rani.dx;
                            const aniwy = y+dy2+rani.dy;
                            if (this.map.getBlockWithoutLoading(
                                aniwx, aniwy) !== 'B') continue;
                            this.syncher.serverBroadcastAnimation(
                                {x:aniwx,y:aniwy,b:'R0',a:rani.duration});
                        }
                    }
                }
                else if (secondBlock === 'B' && i < 3) {
                    // convert this stone to ruby stone (pink B)
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'R0');
                }
            }
        }

        else if (blockHere === 'R0') {
            // ruby stone devolves back to stone.  This
            // prevents diamond from getting surrounded.
            this.syncher.serverChangeBlock(x, y, 'B');
        }

        else if (blockHere === 'R1') {
            // possibly turns into ruby apple
            const blockAbove = this.map.getBlockWithoutLoading(x, y+1, ' ');
            if (blockAbove !== 'Aa' && blockAbove !== 'aa' && blockAbove !== 'Waa') return;
            const blockLeft = this.map.getBlockWithoutLoading(x-1, y, ' ');
            if (blockLeft !== 'Aa' && blockLeft !== 'aa' && blockLeft !== 'Waa') return;
            const blockRight = this.map.getBlockWithoutLoading(x+1, y, ' ');
            if (blockRight !== 'Aa' && blockRight !== 'aa' && blockRight !== 'Waa') return;
            this.syncher.serverChangeBlock(x, y, 'fR');
        }

        else if (blockHere === 'WW') {
            // found a warp!
            // check 1 surrounding block
            {
                const dx = Math.round(Math.random()*6)-3;
                const dy = Math.round(Math.random()*6)-3;
                const secondBlock = this.map.getBlockWithoutLoading(
                    x+dx, y+dy, 'WB');
                if (secondBlock === 'WB') {
                    return;
                }
            }
            {
                const dx = Math.round(Math.random()*6)-3;
                const dy = Math.round(Math.random()*6)-3;
                const secondBlock = this.map.getBlockWithoutLoading(
                    x+dx, y+dy);
                if (secondBlock === 'B') {
                    // found a nearby B
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'WB');
                }
            }
        }

        else if (blockHere === 'Aa'
            || blockHere === 'Waa'
            || blockHere === 'Al'
            || blockHere === 'Wal'
            || blockHere === 'Ap'
            || blockHere === 'Wap'
            || blockHere === 'Av'
            || blockHere === 'Wav'
            || blockHere === 'ar'
            || blockHere === 'au'
        ) {
            const type = blockHere[blockHere.length-1];
            const probability = treeProbabilities[
                biomeHere]['t'+type]
            if (probability < 1) {
                // Can not grow in this biome
                return;
            }
            if (type === 'u' || type === 'r') {
                if (probability < 2) {
                    // Berry grows more slowly in this biome
                    if (!isActuallyPlantUpdate400) return;
                }
                this.tryGrowBerry(x, y, type);
            } else {
                // Non-berry fruit only grows on update400
                if (!isActuallyPlantUpdate400) return;
                if (probability < 2) {
                    // Fruit grows especially slowly in this biome
                    if (!isActuallyPlantUpdate1000) return;
                }
                this.tryGrowFruit(x, y, type);
            }
        }


        else if (blockHere === '-a') {
            // vine growing down
            if (this.map.getBlockWithoutLoading(
                x, y-1, 'H') === ' ') {
                if (this.checkGrowVineDown(x, y)) {
                    this.syncher.serverChangeBlock(x, y-1, '-2');
                }
            }
            // vine growing up
            if (this.map.getBlockWithoutLoading(
                x, y+1, 'H') === 'w') {
                if (this.checkGrowVineUp(x, y)) {
                    this.syncher.serverChangeBlock(x, y+1, '-8');
                }
            }
        }

        else if (blockHere === '-2') {
            // vine growing down
            if (this.checkGrowVineDown(x, y)) {
                this.syncher.serverChangeBlock(x, y, '-a');
            }
        }

        else if (blockHere === '-8') {
            // vine growing up
            if (this.map.getBlockWithoutLoading(
                x, y+1, 'H') === 'w') {
                if (this.checkGrowVineUp(x, y)) {
                    this.syncher.serverChangeBlock(x, y, '-a');
                }
            }
        }

        else if (blockHere[0] === 'X' && blockHere !== 'Xx') {
            // coral, but not dead coral
            const dir = dirs8[Math.floor(Math.random()*8)];
            if (this.checkCoralGrowth(
                x+dir.dx, y+dir.dy, blockHere)) {
                this.syncher.serverChangeBlock(
                    x+dir.dx, y+dir.dy, blockHere);
            }
            // also gets a chance of spontaneous death, hehe
            else if (Math.random() < 0.01) {
                if (this.checkCoralSpontaneousDeath(x, y)) {
                    this.syncher.serverChangeBlock(x, y, 'Xx')
                }
            }
        }

        else if (blockHere === ' ') {
            // this air might become snow
            const blockBelow = this.map.getBlockWithoutLoading(
                x, y-1, ' ');
            // cancel if air below (this usually happens)
            if (blockBelow === ' ') return;
            // cancel if wrong biome
            if (biomeHere !== 'i'
                && biomeHere !== 'I'
                && biomeHere !== 'g'
                && biomeHere !== 'T') return;
            const blockAbove = this.map.getBlockWithoutLoading(
                x, y+1, 'B');
            // cancel if not on solid block
            if (!BP.isSolidBlock(blockBelow)) return;
            // check some blocks up
            const ny = biomeHere === 'I' ? 5 : 10;
            let yay = true;  // TODO: this code can be organized better
            for (let dy = 1; dy <= ny; dy++) {
                const someBlockAbove = this.map.getBlockWithoutLoading(
                    x, y+dy, 'B');
                // cancel if some block above is bad
                if (someBlockAbove !== ' '
                    && someBlockAbove !== 'sn'
                    && someBlockAbove[0] !== 'A'
                    && someBlockAbove[0] !== 'a'
                    && !(someBlockAbove[0] === 'W'
                        && someBlockAbove[1] === 'A')) {
                    yay = false;
                    break;
                };
            }
            if (!yay) return;
            // okay become snow
            this.syncher.serverChangeBlock(x, y, 'sn');
        }

    }

    plantUpdate2000(x, y, blockHere, biomeHere) {

        if (blockHere === 'WT') {
            // might spawn stalagmite
            if (Math.random() > 0.5) return;
            // ok try
            let distance = this.checkGrowStalagmite(x, y);
            if (distance <= 2) return;
            this.syncher.serverChangeBlock(x, y-distance, 'WI')
        }

        else if (blockHere === 'Wl') {
            // icice might grow
            // cancel if wrong biome
            if (biomeHere !== 'i'
                && biomeHere !== 'I'
                && biomeHere !== 'g'
                && biomeHere !== 'T') return;
            // cancel if no space for next icicle
            if (this.map.getBlockWithoutLoading(x, y-1, '..')
                !== ' ') return;
            // cancel if already enough icicles
            if (this.map.getBlockWithoutLoading(x, y+2, 'Wl')
                === 'Wl') return;
            // cancel if already enough icicles
            if (this.map.getBlockWithoutLoading(x, y+1, 'Wl')
                === 'Wl') return;
            this.syncher.serverChangeBlock(x, y-1, 'Wl')
        }

    }

    globalPlantUpdate2000() {
        if (this.props.spawn_monsters === true) {
            this.ghosts.ghostTick();
        }
        this.detectors.detectorTick();
    }

    checkCoralGrowth(x, y, type) {
        const blockHere = this.map.getBlockWithoutLoading(x, y);
        if (blockHere !== 'w' && blockHere[0] !== '-') {
            // not water here. Nothing to grow.
            return;
        }
        // count nearby waters,
        // count nearby corals
        let waters = 0;
        let corals = 0;
        for (let dx = -2; dx <= 2; dx++) {
            for (let dy = -2; dy <= 2; dy++) {
                const b = this.map.getBlockWithoutLoading(x+dx, y+dy, 'X?');
                if (b === 'w' || b[0] === '-') {
                    waters++;
                }
                else if (b[0] === 'X' && b !== 'Xx') {
                    corals++;
                }
            }
        }
        // lower limit for number of nearby waters
        if (waters < CORAL_LOGIC[type].minwater) return false;
        // lower limit for number of nearby corals
        if (corals < CORAL_LOGIC[type].mincoral) return false;
        // upper limit for number of nearby corals
        if (corals > CORAL_LOGIC[type].maxcoral) return false;
        // bfs
        const limit = CORAL_LOGIC[type].bfslimit;
        if (limit < 0) return true; // special case, skip bfs
        let numfound = -1;
        const visited = [];
        for (let i = 0; i < 41; i++) {
            visited.push(Array(41).fill(false));
        }
        const queue = [{dx:20, dy:20}];
        visited[20][20] = true;
        while (queue.length > 0) {
            // next in queue
            const v = queue.shift();
            numfound++;
            if (numfound > limit) return false;
            const dx = v.dx;
            const dy = v.dy;
            // consider enqueuing each neighbor
            for (let ddy = -1; ddy <= 1; ddy++) {
                for (let ddx = -1; ddx <= 1; ddx++) {
                    // not current block, only its neighbors
                    if (ddx === 0 && ddy === 0) continue;
                    // if outside of bounds, just abort this
                    // because we can't know what's out there.
                    if (dx+ddx<0 || dy+ddy<0
                        || dx+ddx>=visited[0].length
                        || dy+ddy>=visited.length) return false;
                    // not blocks that were already visited
                    if (visited[dy+ddy][dx+ddx]) continue;
                    // not blocks that are not matching
                    if (this.map.getBlockWithoutLoading(
                        x+dx-20+ddx,y+dy-20+ddy) !== type) continue;
                    // ok let's enqueue this
                    visited[dy+ddy][dx+ddx] = true;
                    queue.push({dx:dx+ddx,dy:dy+ddy});
                }
            }
        }
        // got to the end with no problems!
        // accept it
        return true
    }

    checkCoralSpontaneousDeath(x, y) {
        for (let dx = -2; dx <= 2; dx++) {
            for (let dy = -2; dy <= 2; dy++) {
                const b = this.map.getBlockWithoutLoading(x+dx, y+dy, 'w');
                if (b === 'w' || b === '-a') {
                    // can't die near water
                    return false;
                }
            }
        }
        return true;
    }

    checkCoralStoning(x, y) {
        for (const dir of dirs4) {
            const b = this.map.getBlockWithoutLoading(x+dir.dx, y+dir.dy, 'w');
            if (b !== 'B' && b !== 'Xx' && b !== 'S') {
                // needs only dead coral and stone adjacent
                return false;
            }
        }
        return true;
    }

    loadFromJSON(object){
        this.ghosts.loadFromJSON(object.ghosts);
        this.monsters.loadFromJSON(object.monsters);
        this.detectors.loadFromJSON(object.detectors);
    }

    saveToJSON(){
        return {
            ghosts: this.ghosts.saveToJSON(),
            monsters: this.monsters.saveToJSON(),
            detectors: this.detectors.saveToJSON(),
        };
    }

}
exports.Plant = Plant;
