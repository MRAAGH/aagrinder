// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Just a collection of data to keep about a known player
 * (a known player is a player who has logged in to this world at least once)
 */

"use strict";

const PLAYER_REACH = 7;
const PLAYER_MAXJUMP = 2;
const PLAYER_MAXFOOD = 1000;

const Inventory = require('../shared/inventory').Inventory;

class Player {
    constructor(x, y, name, socket, color, gamemode = 0, food, reach = PLAYER_REACH, maxjump = PLAYER_MAXJUMP){
        this.x = x; // world pos
        this.y = y; // world pos
        this.homex = undefined;
        this.homey = undefined;
        this.reach = reach;
        this.jump = maxjump;
        this.maxjump = maxjump;
        this.food = typeof food === 'undefined' ? PLAYER_MAXFOOD : food;
        this.digx = 0;
        this.digy = 0;
        this.digtime = 0;
        this.digduration = 0;
        this.gamemode = gamemode;
        this.color = color;
        this.currentlySetMatrixColor = null;
        this.name = name; // username
        this.socket = socket; // websocket of the current session
        this.subscriptions = []; // the chunks which are having updates sent to this client
        this.unsubscriptions = []; // the chunks that are no longer being updated and can be removed
        this.resubscribePosition = {x, y};
        this.chunkUpdates = []; // the chunks which will be sent to this client as a whole
        this.changeObj = {}; // the terrain updates that the client actually needs
        this.positionListUpdates = [];
        this.lastEventId = '0';
        this.skipNextTimerUpdate = false;
        this.detectorBlinkSpeed = 0;
        this.followBlinkSpeed = 0;
        this.changedx = false;
        this.changedy = false;
        this.changedFood = false;
        this.changedReach = false;
        this.changedMaxjump = false;
        this.changedGamemode = false;
        this.changedColor = false;
        this.changedColorLoud = false;
        this.changedFriends = false;
        this.changedInventory = false;
        this.changedDetectors = false;
        this.changedGhosts = false;
        this.changedMonsters = false;
        this.changedDetectorBlink = false;
        this.changedFollowBlink = false;
        this.inventory = new Inventory(Inventory.PLAYER_INVENTORY_SLOT_COUNT, true);
        this.animation = false;
        this.blockanimations = [];
        this.notifs = [];
        this.tileEntityCreations = [];
        this.tileEntityDeletions = [];
        this.chestSubscription = false;
        this.changedChest = false;
        this.announcement = "";
        this.visiblePlayers = [];
        this.visibleDetectors = [];
        this.visibleGhosts = [];
        this.visibleMonsters = [];
        this.debugLog = [];
        this.spawnCommandCooldown = false;
        this.hugRequestedFrom = '';
    }

    // TODO: this.jump should probably be saved too
    saveToJSON(){
        return {
            name: this.name,
            color: this.color,
            x: this.x,
            y: this.y,
            food: this.food,
            homex: this.homex,
            homey: this.homey,
            reach: this.reach,
            maxjump: this.maxjump,
            gamemode: this.gamemode,
            inventory: this.inventory.slots,
        }
    }

    sendChat(formatted) {
        this.socket.emit('chat', {formatted: formatted});
    }

    sendTimestampedChat(formatted, notify=false, raw, sender, color) {
        this.socket.emit('chat', {
            formatted: formatted,
            raw: raw,
            sender: sender,
            timestamp: true,
            notify: notify,
            color: color,
        });
    }

    sendError(message) {
        this.sendChat('\\[#944\\]'+message);
    }

    sendTyping(name, type) {
        this.socket.emit('typing', {name: name, type: type});
    }

    sendTypingContent(name, type, content) {
        this.socket.emit('typing', {name: name, type: type, content: content});
    }

    gainItemWithNotification(item_code, count, data) {
        this.inventory.gainItemByCode(item_code, count, data);
        const item_name = Inventory.item_names[item_code];
        if (count > 0) {
            this.notifs.push({p:'+', a:count, s:' '+item_name});
        } else {
            this.notifs.push({p:'-', a:-count, s:' '+item_name});
        }
    }

    gainItemWithInvertedNotification(item_code, count, data) {
        this.inventory.gainItemByCode(item_code, count, data);
        const item_name = Inventory.item_names[item_code];
        if (count > 0) {
            this.notifs.push({p:'-', a:-count, s:' '+item_name});
        } else {
            this.notifs.push({p:'+', a:count, s:' '+item_name});
        }
    }

}

exports.Player = Player;
