// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */


/*
 * Synching clients and server.
 * All terrain-changing, player position changing and inventory changing things
 * must go through here.
 * 
 * Synching on the server side is quite trivial: just keep track of indices.
 * Events happen in a determined order on the server.
 * Each event has an id, which was set by the creator of the event.
 * Clients are notified of each event visible to them (except for the one
 * client who triggered the event, as that one already knows about it).
 * 
 * Along with each event notification, we send its id and the id of the
 * previous event (ignoring those events irrelevant to the client).
 * We expect the client to correct the event order on its end
 * in case of desynchronization by undoing and redoing its own events.
 * If the client sends an invalid action (because its world was not
 * properly updated when the action was taken), we ignore the action.
 * We expect the client to do the same as soon as it finds out what
 * really happened.
 */

"use strict";

class Syncher{
    constructor(intervalManager, map, blockUpdate, playerData, wires, water, ghosts, monsters, vortex, detectors, props, hopper, nonAdminCommands, allCommands){
        this.map = map;
        this.blockUpdate = blockUpdate;
        this.playerData = playerData;
        this.wires = wires;
        this.water = water;
        this.ghosts = ghosts;
        this.monsters = monsters;
        this.vortex = vortex;
        this.detectors = detectors;
        this.props = props;
        this.hopper = hopper;
        this.nonAdminCommands = nonAdminCommands;
        this.allCommands = allCommands;
        intervalManager.register('80syncher', 100,
            () => this.doSynchronizationTick100());
    }

    doSynchronizationTick100() {
        // send updates to all clients every 0.1 seconds
        for (const player of this.playerData.onlinePlayers){
            // except those that should be skipped
            if (player.skipNextTimerUpdate) {
                // but only this time
                player.skipNextTimerUpdate = false;
                continue;
            }
            this.sendUpdatesToClient(player);
        };
        this.vortex.changed = false;
    }

    createView(player){
        return new View(this, player);
    }

    getBlock(x, y){
        return this.map.getBlock(x,y);
    }

    addClaim(accountName, x, y, x2, y2) {
        const chunkCoordList = this.map.addClaim(accountName, x, y, x2, y2);
        for (const {x, y} of chunkCoordList) {
            const chunk = this.map.getChunk(x, y);
            for (const subscriber of chunk.subscribers) {
                // it is ok to update whole chunks at this rare event
                subscriber.chunkUpdates.push({x, y});
            }
        }
    }

    removeClaimAt(x, y) {
        const chunkCoordList = this.map.removeClaimAt(x, y);
        for (const {x, y} of chunkCoordList) {
            const chunk = this.map.getChunk(x, y);
            for (const subscriber of chunk.subscribers) {
                // it is ok to update whole chunks at this rare event
                subscriber.chunkUpdates.push({x, y});
            }
        }
    }

    // TODO: remove this function because change lists are a bad idea
    serverChangeBlocks(changeList){
        for (const c of changeList) {
            this.serverChangeBlock(c.x, c.y, c.block);
        }
    }
    serverChangeBlock(x, y, block){
        this.map.setBlock(x, y, block);
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.map.getChunk(chunkx, chunky);
        for (const subscriber of chunk.subscribers) {
            if(!subscriber.changeObj[y]){
                subscriber.changeObj[y] = {};
            }
            subscriber.changeObj[y][x] = block;
        }
    }

    fabricateBlockChangeForClient(player, x, y, block) {
        if(!player.changeObj[y]){
            player.changeObj[y] = {};
        }
        player.changeObj[y][x] = block;
    }

    serverApplyBlockUpdates() {
        this.serverChangeBlocks(this.blockUpdate.blockList);
        this.blockUpdate.blockList = [];
    }

    movePlayer(player, x, y){
        player.x = x;
        player.y = y;
        player.changedx = true;
        player.changedy = true;
        // all other players need to be notified of this change
        this.updateMyAndOtherPositionLists(player);
        // different detectors and ghosts might be visible now
        this.detectors.recalculateVisibleDetectors(player);
        this.ghosts.recalculateVisibleGhosts(player);
        this.monsters.recalculateVisibleMonsters(player);
    }

    updateMyAndOtherPositionLists(player) {
        for (const otherPlayer of this.playerData.onlinePlayers) {
            // skip current player
            if (otherPlayer.name === player.name) continue;
            // check if players should see each other
            if ((otherPlayer.x < player.x+640
                && otherPlayer.x > player.x-640
                && otherPlayer.y < player.y+512
                && otherPlayer.y > player.y-512)
                || ACCOUNTS.accounts[player.name]
                    .friends.includes(otherPlayer.name)) {
                // they are nearby or they are friends
                // so they should see each other
                otherPlayer.positionListUpdates.push({
                    n: player.name,
                    x: player.x,
                    y: player.y,
                    c: player.color,
                });
                otherPlayer.visiblePlayers[player.name] = true;
                // for my player, there are fewer updates.
                // We only need to update the position if the
                // other player just got loaded right now.
                if (!player.visiblePlayers[otherPlayer.name]) {
                    player.positionListUpdates.push({
                        n: otherPlayer.name,
                        x: otherPlayer.x,
                        y: otherPlayer.y,
                        c: otherPlayer.color,
                    });
                    player.visiblePlayers[otherPlayer.name] = true;
                }
            } else {
                // too far away. Unload each other (if needed)
                if (otherPlayer.visiblePlayers[player.name]) {
                    otherPlayer.positionListUpdates.push({
                        n: player.name,
                    });
                    otherPlayer.visiblePlayers[player.name] = false;
                }
                if (player.visiblePlayers[otherPlayer.name]) {
                    player.positionListUpdates.push({
                        n: otherPlayer.name,
                    });
                    player.visiblePlayers[otherPlayer.name] = false;
                }
            }
        }
    }

    deletionFromPositionLists(player) {
        for (const otherPlayer of this.playerData.onlinePlayers) {
            // skip current player
            if (otherPlayer.name !== player.name) {
                otherPlayer.positionListUpdates.push({
                    n: player.name,
                });
            }
        }
    }

    movePlayerAt(x, y, x2, y2){
        const player = this.playerData.onlinePlayerByXY(x, y);
        this.movePlayer(player, x2, y2);
    }

    getPlayerAt(x, y){
        return this.playerData.onlinePlayerByXY(x, y);
    }

    // TODO: remove this function because change lists are a bad idea
    playerChangeBlocks(player, changeList){
        for(let i = 0; i < changeList.length; i++){
            this.playerChangeBlock(player, changeList[i].x, changeList[i].y, changeList[i].block);
        }
    }

    playerChangeBlock(player, x, y, block){
        this.map.setBlock(x, y, block);
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.map.getChunk(chunkx, chunky);
        for (const subscriber of chunk.subscribers) {
            if(player.name === subscriber.name){
                // skip the player who is doing this
                // (this player doesn't need the update)
                continue;
            }
            if(!subscriber.changeObj[y]){
                subscriber.changeObj[y] = {};
            }
            subscriber.changeObj[y][x] = block;
        }
    }

    chestSubscribe(player, tile) {
        this.chestUnsubscribe(player);
        tile.subscribers.push(player);
        player.chestSubscription = tile;
        player.changedChest = true;
        player.triggerChest = true;
    }

    chestUnsubscribe(player) {
        const tile = player.chestSubscription;
        if (tile !== false) {
            // there is a chest subscription.
            // remove this player from list of sub players
            tile.subscribers = tile.subscribers.filter(
                a=>a.name!==player.name);
            // remove the subscription in the player too
            player.chestSubscription = false;
            player.changedChest = true;
        }
    }

    chestUnsubscribeAll(chest) {
        for (const p of chest.subscribers) {
            p.chestSubscription = false;
            p.changedChest = true;
        }
        chest.subscribers = [];
    }

    notifyAllEntityDeletion(x, y) {
        // Send update to all chunk subscribers
        const chunkx = x >> 8;
        const chunky = y >> 8;
        const chunk = this.map.getChunk(chunkx, chunky);
        for (const subscriber of chunk.subscribers) {
            subscriber.tileEntityDeletions.push({x:x, y:y});
        }
    }

    notifyAllEntityCreation(tile) {
        // Send update to all chunk subscribers
        const chunkx = tile.x >> 8;
        const chunky = tile.y >> 8;
        const chunk = this.map.getChunk(chunkx, chunky);
        for (const subscriber of chunk.subscribers) {
            subscriber.tileEntityCreations.push(tile);
        }
    }

    updateTileEntity(tile, exceptForPlayerName = undefined) {
        this.map.setDirtyAtXY(tile.x, tile.y);
        if (tile.t === 'chest') {
            // Chest subscribers will receive update
            for (const p of tile.subscribers) {
                if (p.name !== exceptForPlayerName) {
                    p.changedChest = true;
                }
            }
            // Activate any chest relationships
            for (const rel of this.hopper.chestRelations) {
                if (tile.x === rel.x1 && tile.y === rel.y1) {
                    rel.active = true;
                }
            }
        } else if (tile.t === 'forg') {
            // Forg will not be sent to the client.
        } else {
            // Non-chest tile entity.
            // Exists on client.
            // Delete and re-create it on client
            this.notifyAllEntityDeletion(tile.x, tile.y);
            this.notifyAllEntityCreation(tile);
        }
    }

    moveTileEntityFromTo(x, y, xnew, ynew) {
        const tile = this.map.moveTileEntityFromTo(x, y, xnew, ynew);
        if (!tile) return;
        if (tile.t === 'chest') {
            // Probably should give this info to players who are currently looking at this chest that was moved
        } else {
            this.notifyAllEntityDeletion(x, y);
            this.notifyAllEntityCreation(tile);
        }
    }

    serverSwapTileEntities(x1, y1, x2, y2) {
        const tiles = this.map.swapTileEntities(x1, y1, x2, y2);
        this.notifyAllEntityDeletion(x1, y1);
        this.notifyAllEntityDeletion(x2, y2);
        for (const tile of tiles) {
            if (tile.t === 'chest') {
                // Probably should give this info to players who are currently looking at this chest that was moved
            } else {
                this.notifyAllEntityCreation(tile);
            }
        }
    }

    createTileEntity(data) {
        this.map.createTileEntity(data);
    }

    playerCreateTileEntity(player, data){
        const chunk = this.map.createTileEntity(data);
        for (const subscriber of chunk.subscribers) {
            if(player.name === subscriber.name){
                // skip the player who is doing this
                // (this player doesn't need the update)
                continue;
            }
            subscriber.tileEntityCreations.push(data);
        }
    }

    deleteTileEntityAt(x, y) {
        const tile = this.map.getTileEntityAt(x, y)[0];
        if (tile === undefined) {
            console.log('unable to delete tile entity at '+x+', '+y);
            return;
        }
        if (tile.t === 'chest') {
            this.chestUnsubscribeAll(tile);
        } else {
            this.notifyAllEntityDeletion(x, y);
        }
        this.map.deleteTileEntityAt(x, y);
    }

    playerDeleteTileEntityAt(player, x, y){
        const chunk = this.map.deleteTileEntityAt(x, y);
        for (const subscriber of chunk.subscribers) {
            if(player.name === subscriber.name){
                // skip the player who is doing this
                // (this player doesn't need the update)
                continue;
            }
            subscriber.tileEntityDeletions.push({x:x,y:y});
        }
    }

    playerBroadcastAnimation(player, animation){
        // animation : {x, y, b : temporaryblock, a : animationduration}
        const chunkx = animation.x >> 8;
        const chunky = animation.y >> 8;
        const chunk = this.map.getChunk(chunkx, chunky);
        for (const subscriber of chunk.subscribers) {
            if(player.name === subscriber.name){
                // skip the player who is doing this
                // (this player doesn't need the update)
                continue;
            }
            subscriber.blockanimations.push(animation);
        }
    }

    serverBroadcastAnimation(animation){
        // animation : {x, y, b : temporaryblock, a : animationduration}
        const chunkx = animation.x >> 8;
        const chunky = animation.y >> 8;
        const chunk = this.map.getChunk(chunkx, chunky);
        for (const subscriber of chunk.subscribers) {
            subscriber.blockanimations.push(animation);
        }
    }

    sendUpdatesToClient(player){
        const message = {};
        // block updates (if there are any)
        if (Object.keys(player.changeObj).length > 0){
            message.b = player.changeObj;
        }

        // tile entity updates (if there are any)
        // (there is only deletion and creation)
        // (modification is implemented as a combination
        // of deletion and creation)
        if (player.tileEntityCreations.length > 0) {
            message.ce = player.tileEntityCreations;
        }
        if (player.tileEntityDeletions.length > 0) {
            message.de = player.tileEntityDeletions;
        }

        // collect chunk updates
        const chunkUpdates = [];
        for (const update of player.chunkUpdates) {
            const chunkx = update.x;
            const chunky = update.y;
            const chunk = this.map.getChunk(chunkx, chunky);
            chunkUpdates.push({
                x: chunkx,
                y: chunky,
                // send whole chunk of terrain
                t: chunk.getCompressed(),
                // and the biome information
                b: chunk.biomes,
                // also send tile entities
                // however, chests stay hidden from the client
                e: chunk.tileEntities.filter(
                    e => e.t !== 'chest' && e.t !== 'forg'),
                // also send claim information
                c: chunk.claims,
            });
        }

        // chunk updates (if there are any)
        if(chunkUpdates.length > 0){
            message.c = chunkUpdates;
        }

        if (player.unsubscriptions.length > 0) {
            message.u = player.unsubscriptions;
        }

        // player position updates (if there are changes)
        if(player.changedx){
            message.px = player.x;
        }
        if(player.changedy){
            message.py = player.y;
        }

        // TODO: player jump state should be passed to client too

        // player food update (if applicable)
        if(player.changedFood){
            message.food = player.food;
        }

        // player reach update (if applicable)
        if(player.changedReach){
            message.reach = player.reach;
        }

        if(player.justLoggedIn){
            message.playername = player.name;
            message.entertochat = true;
            message.commands = this.playerData.isPlayerAdminByName(player.name)
            ? this.allCommands : this.nonAdminCommands;
        }

        // player gamemode update (if applicable)
        if(player.changedGamemode){
            message.gamemode = player.gamemode;
        }

        // player maxjump update (if applicable)
        if(player.changedMaxjump){
            message.maxjump = player.maxjump;
        }

        // player color update (if applicable)
        if(player.changedColor){
            message.color = player.color;
        }

        // player color update (if applicable)
        if (player.changedColorLoud) {
            message.pequalsyou = true;
        }

        // friends update (if applicable)
        if(player.changedFriends){
            message.friends = {
                friends: ACCOUNTS.accounts[player.name].friends.map(
                    friend => ({
                        name: friend,
                        color: ACCOUNTS.accounts[friend].color,
                        seen: ACCOUNTS.accounts[friend].seen,
                    })),
                pending: ACCOUNTS.accounts[player.name].pending,
                requests: ACCOUNTS.accounts[player.name].requests,
            }
        }

        // inventory update (if applicable)
        if(player.changedInventory){
            message.inv = player.inventory.slots;
        }

        // updates of other player positions (if applicable)
        if(player.positionListUpdates.length > 0){
            message.p = player.positionListUpdates;
        }

        // ghosts
        if (player.changedGhosts) {
            message.g = player.visibleGhosts;
        }

        // monsters
        if (player.changedMonsters) {
            message.monsters = player.visibleMonsters;
        }

        // vortex update is sent in 3 situations:
        // if vortex existence changed (despawned),
        // if vortex exists,
        // or if player just logged in.
        if (this.vortex.existencechanged || this.vortex.exists() || player.justLoggedIn) {
            message.vortex = {
                exists: this.vortex.exists(),
                x: this.vortex.x,
                y: this.vortex.y,
                face: this.vortex.face,
                target: this.vortex.target,
                hints: this.vortex.hints,
            };
        }

        // inventory detector blink
        if (player.changedDetectorBlink) {
            message.dbs = player.detectorBlinkSpeed;
        }
        if (player.changedSeatectorBlink) {
            message.sbs = player.seatectorBlinkSpeed;
        }

        // inventory follow blink
        if (player.changedFollowBlink) {
            message.fbs = player.followBlinkSpeed;
        }

        // reveal detectors
        if (player.changedDetectors){
            message.detectors = player.visibleDetectors;
        }

        if (player.animation) {
            message.animation = player.animation;
        }

        if (player.blockanimations.length > 0) {
            message.ba = player.blockanimations;
        }

        if (player.notifs.length > 0) {
            message.notifs = player.notifs;
        }

        if (player.changedChest) {
            const tile = player.chestSubscription;
            if (tile === false) {
                // means chest was destroyed
                message.ch = false;
            }
            else {
                message.ch = {
                    x: tile.x,
                    y: tile.y,
                    d: tile.inventory.saveToJSON(),
                }
                if (player.triggerChest) {
                    message.ch.T = true;
                }
            }
        }

        if (player.announcement !== '') {
            message.an = player.announcement;
        }

        // clear the lists of things to be sent
        player.changeObj = {};
        player.tileEntityCreations = [];
        player.tileEntityDeletions = [];
        player.chunkUpdates = [];
        player.unsubscriptions = [];
        player.changedx = false;
        player.changedy = false;
        player.changedFood = false;
        player.changedReach = false;
        player.changedMaxjump = false;
        player.changedGamemode = false;
        player.changedColor = false;
        player.changedColorLoud = false;
        player.changedFriends = false;
        player.changedInventory = false;
        player.changedDetectors = false;
        player.changedGhosts = false;
        player.changedMonsters = false;
        player.changedDetectorBlink = false;
        player.changedSeatectorBlink = false;
        player.changedFollowBlink = false;
        player.positionListUpdates = [];
        player.animation = false;
        player.blockanimations = [];
        player.notifs = [];
        player.changedChest = false;
        player.triggerChest = false;
        player.announcement = '';
        player.justLoggedIn = false;


        // abort if there is nothing to send
        if(Object.keys(message).length === 0) return false;

        message.l = player.lastEventId;
        if(!player.changedx && !player.changedy){
            // add player x and y so they can be validated on the client
            message.vx = player.x;
            message.vy = player.y;
        }
        if (!player.changedFood) {
            // add player hunger so it can be validated on the client
            message.vf = player.food;
        }
        player.lastEventId = undefined; // undefined means it was a server action
        player.socket.emit('t', message);
        if (this.props.enable_debug_log === true) {
            // Log server event but filter out giant chunk updates
            player.debugLog.push(this.getDebugState(player));
            player.debugLog.push('server['+message.l+'] '+JSON.stringify(message,
                (key, val) => (typeof val === 'string' && val.length > 1000)
                ? '<data>' : val));
        }
        return true;
    }

    getDebugState(player) {
        // get the current player state as a string
        // (for debug log)
        return '  state: '+JSON.stringify({
            px: player.x,
            py: player.y,
            pj: player.j,
            inv: player.inventory.slots
        });
    }

    sendDebugEventToPlayer(player) {
        // make sure the client knows about the latest server events before validation
        this.sendUpdatesToClient(player);
        const message = {
            vx: player.x,
            vy: player.y,
            vf: player.food,
            vinv: player.inventory.slots,
            l: player.lastEventId,
        }
        player.lastEventId = undefined; // undefined means it was a server action
        player.socket.emit('t', message);
        if (this.props.enable_debug_log === true) {
            // Log server event but filter out giant chunk updates
            player.debugLog.push(this.getDebugState(player));
            player.debugLog.push('server['+message.l+'] '+JSON.stringify(message,
                (key, val) => (typeof val === 'string' && val.length > 1000)
                ? '<data>' : val));
        }
    }
}

// View is a layer of abstraction to allow the same
// game logic code to be used both on server and client.
//
// This is the server-side View implementation.
// Unlike the client-side Syncher, the server-side never
// rolls back events (no time travel).
// However, state changes still need to be marked
// so they can later be propagated to other clients
// by the server-side Syncher.

class View{

    constructor(syncher, player){
        this.syncher = syncher;
        this.player = player;
        this.surprise = false;
        // keep track of previous player position
        // so we know whether it needs to be broadcast
        this.px = player.x;
        this.py = player.y;
        this.isServer = true;
    }

    setBlock(x, y, b, blockUpdate=false){
        this.syncher.playerChangeBlock(this.player, x, y, b);
        if (blockUpdate) {
            this.syncher.blockUpdate.updateAt(x, y, true);
        }
    }

    addAnimation(x, y, animationduration, b) {
        // The client triggered an animation.
        // Need to broadcast this animation to other clients.
        if (typeof b === 'undefined') {
            b = this.getBlock(x,y);
        }
        this.syncher.playerBroadcastAnimation(this.player,
            {x:x,y:y,b:b,a:animationduration});
    }

    getBlock(x, y){
        return this.syncher.map.getBlock(x,y);
    }

    isPlayer(x, y){
        for (const p of this.syncher.playerData.onlinePlayers) {
            if (p.x === x && p.y === y) {
                return true;
            }
        }
        return false;
    }

    playerPosChanged() {
        // different detectors and ghosts might be visible now
        this.syncher.detectors.recalculateVisibleDetectors(this.player);
        this.syncher.ghosts.recalculateVisibleGhosts(this.player);
    }

    getClaim(x, y){
        return this.syncher.map.getClaim(x, y);
    }

    getChestX() {
        return this.player.chestSubscription.x;
    }

    getChestY() {
        return this.player.chestSubscription.y;
    }

    getMyOpenedChestAt(x, y) {  // arguments are just for validation
        if (this.player.chestSubscription.x !== x) return null;
        if (this.player.chestSubscription.y !== y) return null;
        const tiles = this.getTileEntityAt(x, y);
        if (tiles.length < 1) {
            // no tile entities
            return null;
        }
        if (tiles[0].t !== 'chest') {
            // not a chest
            return null;
        }
        return tiles[0];
    }

    checkIfFriend(otherAccountName) {
        return ACCOUNTS.accounts[this.player.name]
            .friends.includes(otherAccountName);
    }

    gainItemByCode(item_code, count, data) {
        this.player.inventory.gainItemByCode(item_code, count, data);
    }

    gainItemAtIndex(item_code, index, count, data) {
        this.player.inventory.gainItemAtIndex(item_code, index, count, data);
    }

    surpriseGainItem(item_code, count, data) {
        this.player.gainItemWithNotification(item_code, count, data);
        this.surpriseUpdateInventory();
    }

    surpriseGainItemInverted(item_code, count, data) {
        this.player.gainItemWithInvertedNotification(item_code, count, data);
        this.surpriseUpdateInventory();
    }

    surpriseUpdateInventory() {
        this.player.changedInventory = true;
        this.surprise = true;
    }

    enqueueWater(x, y) {
        this.syncher.water.enqueue(x, y);
    }

    enqueueWire(x, y) {
        this.syncher.wires.enqueue(x, y);
    }

    processHopper(x, y, block) {
        this.syncher.hopper.processHopper(x, y, block);
    }

    hopperInvalidate(x, y) {
        this.syncher.hopper.hopperInvalidate(x, y);
    }

    isHopperTrailTooLong(x, y, block) {
        // Pass in block

        // The comparisons are 21 and 22 because of double counting

        let distanceRight = 0;
        let distanceLeft = 0;
        
        let dx;
        let dy;

        if (block === 'pv') {
            dx = 0;
            dy = 1;
        }
        else {
            dx = 1;
            dy = 0;
        }

        let block_here = block;

        // Check positive offset
        while (true) {
            block_here = this.syncher.map.getBlock(x + distanceRight * dx, y + distanceRight * dy);

            if (block_here !== block) break;

            distanceRight++;

            if (distanceRight > 21) return true;
        }

        block_here = block;

        // Check negative offset
        while (true) {
            block_here = this.syncher.map.getBlock(x - distanceLeft * dx, y - distanceLeft * dy);

            if (block_here !== block) break;

            distanceLeft++;

            if (distanceRight + distanceLeft > 22) return true;
        }

        return false;
    }

    getTileEntityAt(x, y) {
        return this.syncher.map.getTileEntityAt(x, y);
    }

    createTileEntity(data) {
        this.syncher.createTileEntity(data);
    }

    playerCreateTileEntity(data) {
        this.syncher.playerCreateTileEntity(this.player, data);
    }

    deleteTileEntityAt(x, y) {
        this.syncher.deleteTileEntityAt(x, y);
    }

    playerDeleteTileEntityAt(x, y) {
        this.syncher.playerDeleteTileEntityAt(this.player, x, y);
    }

    serverSwapTileEntities(x1, y1, x2, y2) {
        this.syncher.serverSwapTileEntities(x1, y1, x2, y2);
    }

    updateTileEntity(tile) {
        this.syncher.updateTileEntity(tile);
    }

    attemptCreateCustomWarp(x, y, code, scope) {
        // abort if already a custom warp with this code
        if (this.syncher.map.getCustomWarpByCode(code) !== null) {
            return false;
        }
        this.syncher.map.createCustomWarp(x, y, code, this.player.name, scope);
        return true;
    }

    subscribeChest(tile) {
        this.syncher.chestSubscribe(this.player, tile);
    }

    surpriseSetBlock(x, y, b, blockUpdate=false) {
        this.setBlock(x, y, b, blockUpdate);
        this.syncher.fabricateBlockChangeForClient(this.player, x, y, b);
        this.surprise = true;
    }

    apply(eventId){
        if (this.surprise) {
            // there's some surprise content that gets sent back
            this.player.lastEventId = eventId;
        }

        if (this.player.x !== this.px || this.player.y !== this.py) {
            this.syncher.updateMyAndOtherPositionLists(this.player);
            const chestTile = this.player.chestSubscription;
            if (chestTile !== false) {
                if (chestTile.x > this.player.x + 10
                    || chestTile.x < this.player.x - 10
                    || chestTile.y > this.player.y + 10
                    || chestTile.y < this.player.y - 10) {
                    this.syncher.chestUnsubscribe(this.player);
                }
            }
        }

        this.syncher.blockUpdate.cascadeUpdates();

        this.syncher.serverApplyBlockUpdates();

        return true;
    }

    notifyPrint(message, color) {
        // nothing to do on server, there is no UI
    }

    notifySend(text) {
        // nothing to do on server, there is no UI
    }

    notifySendAmount(prefix, amount, suffix) {
        // nothing to do on server, there is no UI
    }

}

exports.Syncher = Syncher;
