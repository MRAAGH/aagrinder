// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * subclass sandbox for server commands.
 * Gives access to various components.
 * Still, map should not be modified directly, but rather through the syncher.
 * This way, there's no need to worry about synchronization and order of events.
 *
 */

"use strict";

class Command {
    name = null;
    aliases = [];
    signatures = [];
    isAdmin = true;  // restricted by default, to avoid security issues
    help = 'No help provided for this command.';

    static KNOWN_BIOMES = [ // constant
        ['a', 'apple', 'apple'],
        ['A', 'apleforst', 'apple forest'],
        ['j', 'aplejungl', 'apple jungle'],
        ['p', 'apricot', 'apricot'],
        ['P', 'apriforst', 'apricot forest'],
        ['J', 'jungle', 'jungle'],
        ['t', 'taiga', 'taiga'],
        ['T', 'snowtaiga', 'snowy taiga'],
        ['d', 'desert', 'desert'],
        ['O', 'oasis', 'oasis'],
        ['D', 'dunes', 'dunes'],
        ['r', 'mountain', 'mountain'],
        ['z', 'greenmntn', 'green mountain'],
        ['i', 'icemntn', 'ice mountain'],
        ['g', 'glacier', 'glacier'],
        ['m', 'mixforest', 'mixed forest'],
        ['C', 'tentislnd', 'tentacle island'],
        ['y', 'yayisland', 'yay island'],
        ['f', 'flwrislnd', 'flower island'],
        ['u', 'undergrnd', 'underground'],
        ['c', 'spookcave', 'cave'],
        ['I', 'ice cave', 'ice cave'],
        ['o', 'ocean', 'ocean'],
        ['s', 'sky', 'sky'],
        ['R', 'rainbow', 'rainbow'],
        ['W', 'warp', 'warp'],
    ];

    static KNOWN_STRUCTURES = [ // constant
        ['forg', 'fort'],
        ['warp'],
        ['treasure', 'goldenchest'],
        ['badtreasure', 'cavechest'],
    ];

    constructor(server) {
        this.server = server;
        this.map = server.map;
        this.syncher = server.syncher;
        this.subscribe = server.subscribe;
        this.spawn = server.spawn;
        this.playerData = server.playerData;
        this.plant = server.plant;
        this.vortex = server.vortex;
        this.props = server.props;
        this.aagrinderMatrixBot = server.aagrinderMatrixBot;
        this.aagrinderMatrixAppservice = server.aagrinderMatrixAppservice;
    }

    parseRelative(playerCoord, arg) {
        if (arg.length < 1) return false;
        const isRelative = arg[0] === '~';
        if (isRelative) {
            arg = arg.substring(1);
            if (arg === '') {
                return playerCoord;
            }
        }
        if (isNaN(arg)) {
            return false;
        }
        let intArg = parseInt(arg);
        if (isRelative) intArg += playerCoord;
        return intArg;
    }

    // TODO: this is copypasted from clientcommands.js
    static makeUsageString(name, signatures, usageExtraLine) {
        const start = 'usage: ' + name;
        if (signatures.length < 1
            || signatures[0][0].name === '') return start;
        let usage = signatures.map(signature => start + ' '
            + signature.map(s => (s.mandatory ? '<' : '[') + s.name + (s.mandatory ? '>' : ']'))
            .join(' ')).join('\\[n\\]');
        if (usageExtraLine) {
            usage += '\\[n\\]'+usageExtraLine;
        }
        return usage;
    }

    sendUsageString(player) {
        player.sendError(Command.makeUsageString(this.name,
            this.signatures, this.usageExtraLine));
    }

    // I didn't have a better place to put biome searching stuff
    static ZIGZAGS = [  // constant
        // right edge, going up. ccw from bottom left
        [{dx:0, dy:0}, {dx:1, dy:0}, {dx:1, dy:1}, {dx:0, dy:1}],
        // top edge, going left. ccw from bottom right
        [{dx:1, dy:0}, {dx:1, dy:1}, {dx:0, dy:1}, {dx:0, dy:0}],
        // left edge, going down. ccw from top right
        [{dx:1, dy:1}, {dx:0, dy:1}, {dx:0, dy:0}, {dx:1, dy:0}],
        // bottom edge, going right. ccw from top left
        [{dx:0, dy:1}, {dx:0, dy:0}, {dx:1, dy:0}, {dx:1, dy:1}],
    ];

    static nthChunkToCheck(n) {
        if (n < 4) {
            // layer 0 is hardcoded
            const dxdy = Command.ZIGZAGS[2][n];
            return {x: dxdy.dx, y: dxdy.dy};
        }
        const nn = Math.floor(n/4);
        // find smallest layer which contains element
        let layer = 1;
        while (true) {
            // example for layer=2
            // (this layer is 5 by 5)
            // (and has 16 elements on perimiter)
            // .......
            // .76543.
            // .8...2.
            // .9...1.
            // .A...0.
            // .BCDEF.
            // .......
            const layerDiameter = 2 * layer + 1;
            const layerBoundingBoxSize = layerDiameter * layerDiameter;
            // found layer which contains searched element?
            if (nn < layerBoundingBoxSize) break;
            layer++;
        }
        // searched element is on the border of this layer
        const prevLayerDiameter = 2 * layer - 1;
        const prevLayerBoundingBoxSize = prevLayerDiameter * prevLayerDiameter;
        // find index along the entire border
        const indexOnBorder = nn - prevLayerBoundingBoxSize;
        // which edge is it on? (there are 4 edges on a square)
        const edgeLength = 2*layer;
        const whichEdge = Math.floor(indexOnBorder / edgeLength);
        // where on this edge is it?
        const indexOnEdge = indexOnBorder - edgeLength * whichEdge;
        // determine coordinates based on layer, which edge, and index
        let xx, yy;
        switch (whichEdge) {
            case 0:  // right edge, going up
                xx = layer;
                yy = -layer+1+indexOnEdge;
                // simple zigzag similar to Hilbert curve, going up
                break;
            case 1:  // top edge, going left
                xx = layer-1-indexOnEdge;
                yy = layer;
                // simple zigzag similar to Hilbert curve, going up
                break;
            case 2:  // left edge, going down
                xx = -layer;
                yy = layer-1-indexOnEdge;
                // simple zigzag similar to Hilbert curve, going up
                break;
            case 3:  // bottom edge, going right
                xx = -layer+1+indexOnEdge;
                yy = -layer;
                // simple zigzag similar to Hilbert curve, going up
        }
        // determine exact coordinates - zigzag goes in different directions
        const indexInZigzag = n - 4*nn;
        const dxdy = Command.ZIGZAGS[whichEdge][indexInZigzag];
        return {x: 2*xx+dxdy.dx, y: 2*yy+dxdy.dy};
    }

    async findBiome(originx, originy, biome, multithread, MAX_SEARCH_CHUNKS) {
        if (multithread < 1) multithread = 1;
        const originchunkx = originx >> 8;
        const originchunky = originy >> 8;
        let n = 0;
        while(n < MAX_SEARCH_CHUNKS) {
            const globalchunkxys = [];
            let dxdys = [];
            for (let m = 0; m < multithread && n < MAX_SEARCH_CHUNKS; m++) {
                // Get chunks in a smart order,
                // similar to a spiral Hilbert curve,
                // because if we generate adjacent chunks it's a bit faster.
                const chunkxy = Command.nthChunkToCheck(n);
                globalchunkxys.push({
                    x: originchunkx + chunkxy.x,
                    y: originchunky + chunkxy.y,
                });
                // Add all locations in this chunk to the list
                for (let dx = 0; dx < 256; dx += 4) {
                    for (let dy = 0; dy < 256; dy += 8) {
                        dxdys.push({
                            dx: 256*chunkxy.x + dx,
                            dy: 256*chunkxy.y + dy,
                        });
                    }
                }
                n++;
            }
            // load many chunks at once.
            // This way, terrain generator can use optimizations.
            // TODO: IMPORTANT: THIS IS AN ASYNC CALL TO PREVENT BLOCKING THE MAIN THREAD, WHICH WOULD CAUSE USERS TO LOSE CONNECTION. BUT ASYNC TERRAIN GENERATION IS UNSUPPORTED IN AAGRINDER AND CAN CAUSE DATA CORRUPTION
            await this.map.loadManyChunksAsync(globalchunkxys);
            // Determine the search order within these chunks:
            // sort locations by euclidean distance from the player
            // TODO: this is currently wrong, becaues these coordinates are relative to the corner of player's chunk, not the actual player.
            // I do not think this is a bottleneck, because we are
            // sorting only 32*multithread entries (for example 384)
            dxdys = dxdys.sort((a,b) =>
                a.dx*a.dx+a.dy*a.dy-b.dx*b.dx-b.dy*b.dy);
            // Check biome in this order
            for (const dxdy of dxdys) {
                const x = 256*originchunkx + dxdy.dx;
                const y = 256*originchunky + dxdy.dy;
                // (chunk is guaranteed to be loaded)
                const b = this.map.getBiome(x, y);
                if (b !== biome) continue;
                return {x: x, y: y};
            }
        }
        return null;
    }

    async findStructure(chunkx, chunky, structure, multithread) {
        if (multithread < 1) multithread = 1;
        // IMPORTANT: ASYNC CALL IS ALLOWED HERE BECAUSE THIS IS INDEPENDENT OF SERVER TERRAIN GENERATION.
        let infos;
        try {
            infos = await this.map.getChunkInfosAsync(
                [{x: chunkx, y: chunky}]);
        } catch (e) {
            throw 'generator does not support locate';
        }
        infos = infos.filter(i => i.chunkx === chunkx && i.chunky === chunky);
        if (infos.length < 1) throw 'generator returned mismatching chunks';
        let info = infos[0];
        if (info[structure] !== undefined) {
            // structure located.
            return info[structure];
        }
        if (info[structure+'_chunk'] === undefined) {
            // structure not located, and no other coordinates suggested
            throw 'failed to locate structure';
        }
        // aagrinder-terrain suggested other coordinates
        const betterchunkx = info[structure+'_chunk'].x;
        const betterchunky = info[structure+'_chunk'].y;
        try {
            infos = await this.map.getChunkInfosAsync(
                [{x: betterchunkx, y: betterchunky}]);
        } catch (e) {
            throw 'generator does not support locate';
        }
        infos = infos.filter(i => i.chunkx === betterchunkx && i.chunky === betterchunky);
        if (infos.length < 1) throw 'generator returned mismatching chunks';
        info = infos[0];
        if (info[structure] !== undefined) {
            // structure located.
            return info[structure];
        }
        throw 'failed to locate structure';
    }

}

module.exports = Command;
