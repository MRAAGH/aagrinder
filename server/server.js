// Copyright (C) 2018-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * This used to all be merged with index.js but now it's here.
 * Just turned the server into a class.
 * It is a collection of all the server components,
 * and also links them to sockets.
 */

"use strict";

const SERVER_VERSION = '1.0.0';

const fs = require('fs');
const { promisify } = require('util');
const existsAsync = promisify(fs.exists);
const writeFileAsync = promisify(fs.writeFile);
const readFileAsync = promisify(fs.readFile);

const { AASTRING_RAW } = require('../shared/aastring');

const { Player } = require('./player');
const { Map } = require('./map');
const { BlockUpdate } = require('./blockupdate');
const { PlayerData } = require('./playerdata');
const { Spawn } = require('./spawn');
const { Syncher } = require('./syncher');
const { Subscribe } = require('./subscribe');
const { IntervalManager } = require('./intervalmanager');
const { Actions } = require('./actions');
const { Commands } = require('./commands');
const { Plant } = require('./plant');
const { Vortex } = require('./vortex');
const { Wires } = require('./wires');
const { Water } = require('./water');
const { Hopper } = require('./hopper');
const { AagrinderMatrixBot } = require('./matrixbot');
const { AagrinderMatrixAppservice } = require('./matrixappservice');

class Server{

    constructor(props){
        this.props = props;
        this.map = new Map(props, (x,y)=>this.water.enqueue(x,y));
        this.playerData = new PlayerData(props);
        this.blockUpdate = new BlockUpdate(this.map);
        this.connectedSockets = [];
        this.intervalManager = new IntervalManager();
        this.syncher = new Syncher(
            this.intervalManager,
            this.map,
            this.blockUpdate,
            this.playerData,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            undefined,
            props,
            undefined,
            [],
            [],
        );
        this.spawn = new Spawn(this.map, this.syncher, props);
        this.subscribe = new Subscribe(this.map, props);
        this.water = new Water(
            this.intervalManager,
            this.map,
            this.playerData,
            this.subscribe,
            this.syncher,
        );
        this.plant = new Plant(
            this.intervalManager,
            this.map,
            this.playerData,
            this.subscribe,
            this.syncher,
            this.water,
            props,
        );
        this.vortex = new Vortex(
            this.intervalManager,
            this.map,
            this.playerData,
            this.subscribe,
            this.syncher,
        );
        this.plant.detectors.vortex = this.vortex;  // add missing reference
        this.hopper = new Hopper(
            this.intervalManager,
            this.map,
            this.syncher,
        );
        this.wires = new Wires(
            this.intervalManager,
            this.map,
            this.syncher,
            this.playerData,
            this.water,
            this.hopper,
        );
        // now fix the missing references in syncher:
        this.syncher.wires = this.wires;
        this.syncher.hopper = this.hopper;
        this.syncher.water = this.water;
        this.syncher.detectors = this.plant.detectors;
        this.syncher.ghosts = this.plant.ghosts;
        this.syncher.monsters = this.plant.monsters;
        this.syncher.vortex = this.vortex;
        this.actions = new Actions(
            this.map,
            this.syncher,
            this.subscribe,
            this.spawn,
            this.playerData,
            props,
        );
        this.aagrinderMatrixBot = new AagrinderMatrixBot(props, this.playerData);
        this.aagrinderMatrixAppservice = new AagrinderMatrixAppservice(props, this.playerData);

        // commands gets reference to whole server.
        // commands have access to everything.
        this.commands = new Commands(this);

        this.syncher.nonAdminCommands = this.commands.listCommands(false);
        this.syncher.allCommands = this.commands.listCommands(true);

        this.intervalManager.register('spawnCommandCooldown', 1000,
            () => {
                for (const player of this.playerData.onlinePlayers) {
                    player.spawnCommandCooldown = false;
                }
            });

        if(!fs.existsSync('./aagrinder-terrain')){
            console.error('You need the terrain generator! Get it here: https://gitlab.com/MRAAGH/aagrinder-terrain and put it in your aagrinder folder.');
            process.exit(1);
        }
        if(!fs.existsSync('./aagrinder-terrain/aagrinderterrain')){
            console.error('You need to compile the terrain generator! Go to the aagrinder-terrain folder and run this command: make');
            process.exit(1);
        }

    }

    async command(c){
        switch(c){
            case 'save':
                await this.saveToFile();
                console.log('Saved map.');
                break;
            case 'stop':
                await this.saveToFile();
                console.log('Saved map.');
                this.stopServer();
                break;
            case 'kill':
                this.stopServer();
                break;
            case 'admin':
                this.playerData.reloadAdmins();
                break;
            case 'forcereload':
                LOG.log('force reloading all clients!');
                for (const socket of this.connectedSockets) {
                    socket.emit('FORCERELOAD', { FORCE: 'RELOAD' });
                }
                break;
            case 'players':
                console.log(this.playerData.onlinePlayers.map(p=>p.name).join(', '));
                break;
            case 'chat':
                console.log('Usage: chat <message>');
                break;
            case 'kick':
                console.log('Usage: kick <player>');
                break;
            case 'log':
                console.log('Usage: log <player>');
                break;
            case 'passwd':
                console.log('Usage: passwd <account> <password>');
                break;
            case 'clearlog': case 'logclear':
                for (const player of this.playerData.onlinePlayers) {
                    player.debugLog = [];
                }
                console.log('Cleared all debug logs.');
                break;
            case 'help':
                console.log('available commands: save, stop, kill, players, kick, admin, forcereload, log, clearlog, help, chat, passwd');
                break;
            case '"help"':
                console.log('Without the quotes, you silly.');
                break;
            default:

                if (c.length > 4 && c.substring(0,5) === 'chat ') {
                    const message = '\\[#fff\\][SERVER]\\[/\\]: ' + c.substring(5);
                    LOG.log(message);

                    for (const sendPlayer of this.playerData.onlinePlayers) {
                        sendPlayer.sendTimestampedChat(message, true);
                    }
                    break;
                }

                if (c.length > 3 && c.substring(0,4) === 'log ') {
                    if (this.props.enable_debug_log !== true) {
                        console.log('debug log not enabled in config.json');
                        break;
                    }
                    const name = c.substring(4);
                    const player = this.playerData.onlinePlayerByName(name);
                    if (player === null) {
                        console.log('No log for player: '+name);
                        break;
                    }
                    const log = player.debugLog.join('\n');
                    const fname = './'+name+'.debuglog';
                    writeFileAsync(fname, log);
                    console.log('Log saved to '+fname);
                    break;
                }

                if (c.length > 7 && c.substring(0,7) === 'passwd ') {
                    let args = c.split(/ +(?=(?:(?:[^"]*"){2})*[^"]*$)/g);
                    args = args.map(a=>a.replace(/"/g,''));
                    if (args.length !== 3) {
                        console.log('Usage: passwd <account> <password>');
                        break;
                    }
                    const name = args[1];
                    const newpassword = args[2];
                    if (!(name in ACCOUNTS.accounts)) {
                        console.log('No such account: ' + name);
                        break;
                    }
                    const account = ACCOUNTS.accounts[name];
                    if (newpassword === '') {
                        account.hash = '';
                        console.log('Password cleared for ' + name);
                    } else if (/^.{1,72}$/.test(newpassword)) {
                        const bcrypt = require('bcryptjs');
                        const SALT_WORK_FACTOR = 10;
                        const salt = bcrypt.genSaltSync(SALT_WORK_FACTOR);
                        const hash = bcrypt.hashSync(newpassword, salt);
                        account.hash = hash;
                        console.log('Password changed for ' + name);
                    } else {
                        console.log('Password invalid ^.{1,72}$');
                        break;
                    }
                    // Immediately update accounts on disk
                    ACCOUNTS.save();
                    break;
                }

                if (c.length > 4 && c.substring(0,5) === 'kick ') {
                    const name = c.substring(5);
                    const player = this.playerData.onlinePlayerByName(name);
                    if (player === null) {
                        console.log('No online player by name: '+name);
                        break;
                    }
                    player.sendChat('Kicked by admin.');
                    player.socket.disconnect();
                    this.onClientDisconnect(null, player.socket);
                    console.log('Kicked ' + name);
                    break;
                }

                console.log('unknown command: '+c+' (type "help" for help)');
        }
    }

    async saveToFile() {
        await this.map.createSaveDirs();
        const savePath = './saves/' + this.props.level_name;
        const levelFilePath = savePath + '/level.json';
        const savingObjectMap = this.map.saveToJSON();
        const savingObjectPlayerData = this.playerData.saveToJSON();
        const savingObjectPlantInfo = this.plant.saveToJSON();
        const savingObjectWaterUpdates = this.water.saveToJSON();
        const savingObjectChestRelations = this.hopper.saveToJSON();
        const savingObjectVortex = this.vortex.saveToJSON();
        const savingObjectWires = this.wires.saveToJSON();
        const savingObject = {
            playerData: savingObjectPlayerData,
            plantInfo: savingObjectPlantInfo,
            waterUpdates: savingObjectWaterUpdates,
            map: savingObjectMap,
            chestRelations: savingObjectChestRelations,
            vortex: savingObjectVortex,
            wires: savingObjectWires,
        };
        const savingString = JSON.stringify(savingObject, null, 2);
        // it is ok to do disk operations async here,
        // because the order of operations is correct:
        // we create all strings with no async calls between
        // (see above code, and the start of flushChunks method)
        // so nothing can happen that would invalidate this data.
        // And then we can slowly save it to files.
        await Promise.all([
            this.map.flushChunks(),
            writeFileAsync(levelFilePath, savingString),
        ]);
    }

    stopServer() {
        // update everyone's last seen time
        const currentTime = Math.floor((new Date()).getTime() / 1000);
        for (const player of this.playerData.onlinePlayers) {
            ACCOUNTS.accounts[player.name].seen = currentTime;
        }
        ACCOUNTS.save();
        console.log('STOPPING SERVER');
        process.exit();
    }

    async loadFromFileOrConvert() {
        //Check if dir exists
        if (await existsAsync('./saves/'+this.props.level_name)) {
            await this.loadFromFile();
        }
        // Checkif old format exists
        else if (await existsAsync('./saves/' + this.props.level_name + '.txt')) {
            // save exists in old format!!!
            await this.loadOldFormatAndConvert();
            await this.loadFromFile();
        }
    }

    async loadFromFile() {
        const loadedString = await readFileAsync(
            './saves/' + this.props.level_name + '/level.json');
        const loadedObject = JSON.parse(loadedString.toString('utf8'));
        this.playerData.loadFromJSON(loadedObject.playerData);
        this.plant.loadFromJSON(loadedObject.plantInfo);
        this.water.loadFromJSON(loadedObject.waterUpdates);
        this.map.loadFromJSON(loadedObject.map);
        this.hopper.loadFromJSON(loadedObject.chestRelations);
        this.vortex.loadFromJSON(loadedObject.vortex);
        this.wires.loadFromJSON(loadedObject.wires);
    }

    async loadOldFormatAndConvert() {
        LOG.log('CONVERTING FROM OLD FORMAT');
        const loadedString = await readFileAsync(
            './saves/' + this.props.level_name + '.txt');
        const loadedObject = JSON.parse(loadedString.toString('utf8'));
        await this.map.createSaveDirs();
        const savingObject = {
            playerData: loadedObject.playerData,
            plantInfo: loadedObject.plantInfo,
            waterUpdates: loadedObject.waterUpdates,
            map: {
                seed: loadedObject.map.seed,
                spawn: loadedObject.map.spawn,
            },
        };
        const savingString = JSON.stringify(savingObject, null, 2);
        await writeFileAsync('./saves/' + this.props.level_name + '/level.json', savingString);
        const promises = [];
        for (const chunk of loadedObject.map.chunks) {
            const filename = this.map.chunkDir + 'chunk_'+chunk.x+'_'+chunk.y;
            console.log('create',filename);
            const p = writeFileAsync(filename, chunk.terrain);
            promises.push(p);
        }
        await Promise.all(promises);
        LOG.log('FINISHED CONVERTING');
    }

    async prepareSpawnArea() {
        console.log('Preparing spawn area');
        await this.spawn.prepareSpawnArea();
    }

    startGameLoops() {
        this.intervalManager.startGameLoops();
    }

    onClientConnect(socket) {
        console.log('Client connected: ' + socket.id);
        this.connectedSockets.push(socket);
        if (this.props.enable_autologin === true) {
            const result = this.onLogin({
                username: this.props.autologin_name,
                password: '',
            }, socket);
            if (!result) {
                console.log('Autologin failed. Ensure the password hash for '
                    +this.props.autologin_name+' is empty.');
            }
        }
    }

    onLogin(data, socket) {
        //Username sent?
        if (
            typeof(data.username) !== 'string'
            || typeof(data.password) !== 'string'
        ){
            socket.emit('loginerror', {message: 'incomplete data'});
            return false;
        }
        const result = this.playerData.login(data.username, data.password, socket);
        if (!result.success) {
            socket.emit('loginerror', {message: result.message});
            return false;
        }
        this.actions.login(result.player);
        socket.emit('loginsuccess', {});
        return true;
    }

    onClientDisconnect(data, socket) {
        console.log('Client disconnected: ' + socket.id);
        const player = this.playerData.logout(socket);
        // could be a player disconnecting
        // or just someone who closed the login screen.
        // check here:
        if(player !== null){
            // oki it's a logged in player
            this.actions.logout(player);
        }
        this.connectedSockets = this.connectedSockets.filter(
            s => s.id !== socket.id);
    }

    onChat(data, socket) {
        // needs to be a message
        if (typeof(data.message) !== 'string') return;
        // Chat is completely independent from the rest of the game.
        // You just need to be logged in.

        // who speaks?
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        const typed = data.message.substr(0,10000);
        this.handleChat(player, typed);
    }

    handleChat(player, typed) {

        // can't be empty message
        if (typed === '') return;

        const iscommand = /^\//.test(typed);

        if(iscommand){
            // is in fact a server-side command
            // this is where chat does affect the game

            this.commands.executeCommand(player, typed);
        } else {
            // it is not a command, only chat
            let formatted;
            let raw;
            let color;
            // make yellow if local
            if (typed.length > 2 && typed.substr(0,2) === 'L ') {
                color = '#cccc00';
                raw = typed.substr(2);
                formatted = '\\[#cc0\\]' + raw;
            } else {
                color = '#aaaaaa';
                raw = typed;
                formatted = raw;
            }
            // prefix is player name
            formatted = '\\[#'+player.color+'\\]'+player.name +
                '\\[/\\]: '+formatted;
            LOG.log(AASTRING_RAW(formatted));

            for (const sendPlayer of this.playerData.onlinePlayers) {
                // TODO: this API could be better. Maybe too many arguments right now
                sendPlayer.sendTimestampedChat(
                    formatted,
                    sendPlayer.name !== player.name,
                    raw,
                    player.name,
                    color,
                );
            }

            this.aagrinderMatrixBot.handleLocalChat(player, typed);
            this.aagrinderMatrixAppservice.handleLocalChat(player, typed);
        }
    }

    onTyping(data, socket) {
        // needs to have a type
        if (typeof(data.type) !== 'string') return;
        // if it's whisper, needs to have a recipient
        if (data.type === 'whisper' && typeof(data.recipient) !== 'string') return;
        // Chat is completely independent from the rest of the game.
        // You just need to be logged in.

        // who types?
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.handleTyping(player, data.type, data.recipient, data.content);
    }

    handleTyping(player, type, recipient, content) {
        switch (type) {
            case 'stop': case 'global': case 'local': case 'rainbow':
                for (const sendPlayer of this.playerData.onlinePlayers) {
                    if (sendPlayer.name === player.name) continue;
                    if (type !== 'stop'
                        && typeof content === 'string'
                        && content.length > 0
                        && content.length < 1000) {
                        sendPlayer.sendTypingContent(player.name, type, content);
                    } else {
                        sendPlayer.sendTyping(player.name, type);
                    }
                }
                return;
            case 'whisper':
                for (const sendPlayer of this.playerData.onlinePlayers) {
                    if (sendPlayer.name === player.name) continue;
                    if (sendPlayer.name.toLowerCase()
                        !== recipient.toLowerCase()) continue;
                    if (type !== 'stop'
                        && typeof content === 'string'
                        && content.length > 0
                        && content.length < 1000) {
                        sendPlayer.sendTypingContent(player.name, type, content);
                    } else {
                        sendPlayer.sendTyping(player.name, type);
                    }
                }
        }
    }

    onTick(data, socket) {
        if (typeof(data.t) !== 'object') return;
        if (typeof(data.i) !== 'number') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.actions.tick(player, data.t, data.i);
    }

    onAAtype(data, socket) {
        if (typeof(data.x) !== 'number') return;
        if (typeof(data.y) !== 'number') return;
        if (typeof(data.text) !== 'string') return;
        if (data.text.length < 1) return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        if (Math.abs(data.x - player.x) > 100) return;
        if (Math.abs(data.y - player.y) > 100) return;
        this.wires.onAAtype(data.x, data.y, data.text);
    }

    onIClickedDetector(data, socket) {
        if (typeof(data.x) !== 'number') return;
        if (typeof(data.y) !== 'number') return;
        if (typeof(data.follow) !== 'boolean') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.plant.detectors.iClickedDetector(player, data.x, data.y, data.follow)
    }

    onIClickedSspooky(data, socket) {
        if (typeof(data.x) !== 'number') return;
        if (typeof(data.y) !== 'number') return;
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return;
        this.plant.ghosts.iClickedSspooky(player, data.x, data.y)
    }

    onForgTest(data, socket) {
        if (typeof(data.x) !== 'number') return 'nanx';
        if (typeof(data.y) !== 'number') return 'nany';
        if (typeof(data.word) !== 'string') return 'word';
        if (!/^[a-z][a-z][a-z][a-z]$/.test(data.word)) return 'char';
        const player = this.playerData.onlinePlayerBySocket(socket);
        if (player === null) return 'null';
        const fail = this.forgTest(player, data.x, data.y, data.word);
        if (fail === '') {
            player.socket.emit('forgrepl', {
                x: data.x,
                y: data.y,
                word: data.word,
                free: true,
            });
        } else {
            player.socket.emit('forgrepl', {
                x: data.x,
                y: data.y,
                word: data.word,
                free: false,
                fail: fail,
            });
        }
    }

    forgTest(player, x, y, word) {
        // only test, without changing any state
        if (Math.abs(x - player.x) > 10) return 'farx';
        if (Math.abs(y - player.y) > 10) return 'fary';
        const entity = this.map.getTileEntityAt(
            parseInt(x), parseInt(y));
        if (entity.length !== 1) return 'cant forg';
        if (entity[0].t !== 'forg') return 'cant forg';
        if (entity[0].d.know.indexOf(word.substr(1,3)) > -1) return 'know';
        return '';
    }

    listen(http, io, port){
        io.on('connection', socket=>{
            this.onClientConnect(socket);
            socket.on('login', data=>this.onLogin(data, socket));
            socket.on('disconnect', data=>this.onClientDisconnect(data, socket));
            socket.on('chat', data=>this.onChat(data, socket));
            socket.on('typing', data=>this.onTyping(data, socket));
            socket.on('t', data=>this.onTick(data, socket));
            socket.on('aatype', data=>this.onAAtype(data, socket));
            socket.on('iclickeddetector', data=>this.onIClickedDetector(data, socket));
            socket.on('iclickedsspooky', data=>this.onIClickedSspooky(data, socket));
            socket.on('forgtest', data=>this.onForgTest(data, socket));
        });
        if (this.props.restrict_to_localhost) {
            http.listen(port, 'localhost');
            console.log('NOTE: access is restricted to localhost!');
        }
        else {
            http.listen(port);
        }
    }
}

exports.Server = Server;
