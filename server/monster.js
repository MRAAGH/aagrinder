// Copyright (C) 2025 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const Inventory = require('../shared/inventory').Inventory;

const MAX_MONSTERS = 4;
const MONSTER_ATTACK_REACH = 14;
const SAFE_AREA_RADIUS = 30;
const DESPAWN_RADIUS = 200;
const SPAWN_RADIUS = 200;
const VISIBLE_RADIUS = 40;  // How close does the player need to be
const NUM_MOSTER_TYPES = 4;

class Monsters {

    constructor(map, playerData, syncher, props) {
        this.map = map;
        this.playerData = playerData;
        this.syncher = syncher;
        this.monsters = [];
        this.props = props;
    }

    loadFromJSON(object) {
        if (!object) return;
        this.monsters = object.map(o=>{
            const m = new Monster(
                this.positions,
                this.map,
                this.playerData,
                this.syncher
            );
            m.x = o.x;
            m.y = o.y;
            m.state = o.state;
            m.type = o.type;
            return m;
        });
    }

    saveToJSON() {
        return this.monsters.map(m=>({
            x: m.x,
            y: m.y,
            state: m.state,
            type: m.type,
        }));
    }

    monsterTick() {
        if (this.playerData.onlinePlayers.length === 0) {
            // monsters are paused when there are no players
            return;
        }

        // tick the monsters
        for (const monster of this.monsters) {
            monster.tick();
        }

        // filter the monsters
        this.monsters = this.monsters.filter(m=>m.state!=='completelygone');

        // spawning a monster?
        if (this.props.spawn_monsters === true && this.monsters.length < MAX_MONSTERS
            && this.monsters.length < Math.floor(this.playerData.onlinePlayers.length/2)+2) {
            // not too many monsters already
            const m = new Monster(
                this.positions,
                this.map,
                this.playerData,
                this.syncher,
            );
            if (m.spawnSomewhere()) {
                // it worked!
                this.monsters.push(m);
            }
        }

        for (const player of this.playerData.onlinePlayers) {
            this.recalculateVisibleMonsters(player);
        }
        this.recalculateDetectorBlocks();
    }

    spawn(x, y, force = false) {
        const m = new Monster(
            this.positions,
            this.map,
            this.playerData,
            this.syncher
        );
        if (m.spawn(x, y, force)) {
            // it worked!
            this.monsters.push(m);
        }
    }

    recalculateVisibleMonsters(player) {
        // hide previous monsters
        if (player.visibleMonsters.length > 0) {
            player.visibleMonsters = [];
            player.changedMonsters = true;
        }
        let mindist = 10000;
        for (const monster of this.monsters) {
            const dx = player.x - monster.x;
            const dy = player.y - monster.y;
            const sqdist = dx*dx + dy*dy;
            if (sqdist < VISIBLE_RADIUS*VISIBLE_RADIUS) {
                // monster is within visible range. Show
                player.visibleMonsters.push({
                    x: monster.x,
                    y: monster.y,
                    facing: monster.facing,
                    eye: monster.getEye(),
                    type: monster.type,
                    id: monster.id,
                });
                player.changedMonsters = true;
            }
            if (sqdist < mindist) {
                mindist = sqdist;
            }
        }
        let detectorBlinkSpeed = 'o';
        if (mindist < 100*100) detectorBlinkSpeed = 'a';
        if (mindist < 70*70) detectorBlinkSpeed = 'b';
        if (mindist < 40*40) detectorBlinkSpeed = 'c';
        if (mindist < 20*20) detectorBlinkSpeed = 'd';
        if (player.seatectorBlinkSpeed !== detectorBlinkSpeed) {
            player.seatectorBlinkSpeed = detectorBlinkSpeed;
            player.changedSeatectorBlink = true;
        }
    }

    recalculateDetectorBlocks() {
        // iterate all loaded chunks
        for(const chunky in this.map.chunks){
            for(const chunkx in this.map.chunks[chunky]){
                const chunk = this.map.chunks[chunky][chunkx];
                const tileEntities = chunk.tileEntities;
                // iterate all tile entities per loaded chunk
                for (const tile of tileEntities) {
                    if (tile.t !== 'detector') continue;
                    const blockHere = this.map.getBlock(tile.x, tile.y);
                    // skip if not monster detector
                    if (blockHere !== 'Eo'
                        && blockHere !== 'Ea'
                        && blockHere !== 'Eb'
                        && blockHere !== 'Ec'
                        && blockHere !== 'Ed') continue;
                    let mindist = 10000;
                    // find nearest monster to this detector
                    for (const monster of this.monsters) {
                        const dx = tile.x - monster.x;
                        const dy = tile.y - monster.y;
                        const sqdist = dx*dx + 4*dy*dy;
                        if (sqdist < mindist) {
                            mindist = sqdist;
                        }
                    }
                    let detectorBlinkSpeed = 'o';
                    if (mindist < 100*100) detectorBlinkSpeed = 'a';
                    if (mindist < 70*70) detectorBlinkSpeed = 'b';
                    if (mindist < 40*40) detectorBlinkSpeed = 'c';
                    if (mindist < 20*20) detectorBlinkSpeed = 'd';
                    const newBlock = 'E'+detectorBlinkSpeed;
                    if (blockHere !== newBlock) {
                        this.syncher.serverChangeBlock(tile.x, tile.y, newBlock);
                    }
                }
            }
        }
    }


    killAllMonsters() {
        for (const monster of this.monsters) {
            monster.state = 'completelygone';
        }
        return this.monsters.length;
    }

}

class Monster {

    static isStealable(block) {
        if (typeof block !== 'string') return false;
        if (block[0] === 't') return false;
        if (block[0] === '*') return false;
        return true;
    }

    static nextID = 0;

    constructor(positions, map, playerData, syncher){
        this.positions = positions;
        this.map = map;
        this.playerData = playerData;
        this.syncher = syncher;
        this.x = 0;
        this.y = 0;
        this.state = 'sleeping';
        this.type = 0;
        this.facing = 'r';
        this.id = Monster.nextID;
        Monster.nextID++;
    }

    getEye() {
        switch (this.state) {
            case 'sleeping': case 'happysleepingsecret': return '-';
            case 'happysleeping': return 'v';
            case 'active': return 'o';
            case 'happy': return '^';
            case 'completelygone': return 'x';
        }
        return '?';
    }

    tick() {

        // check if area is loaded
        if (!this.map.isBlockLoaded(this.x-5, this.y-5)
            || !this.map.isBlockLoaded(this.x+5, this.y+5)
            || !this.map.isBlockLoaded(this.x+5, this.y-5)
            || !this.map.isBlockLoaded(this.x-5, this.y+5)) {
            // something is not loaded
            // time for vanish
            this.state = 'completelygone';
            return;
        }

        // check if any players nearby
        let yesThereIsNearbyPlayer = false;
        for (const anyplayer of this.playerData.onlinePlayers) {
            const dx = anyplayer.x - this.x;
            const dy = anyplayer.y - this.y;
            const sqdist = dx*dx + dy*dy;
            if (sqdist < DESPAWN_RADIUS*DESPAWN_RADIUS) {
                yesThereIsNearbyPlayer = true;
                break;
            }
        }
        if (!yesThereIsNearbyPlayer) {
            this.state = 'completelygone';
            return;
        }

        // whole monster tick
        switch (this.state) {
            case 'active':
                if (this.tryAttack()) {
                    // no change in type. Stays active
                    break;
                }
                // wander (guarding)
                this.move(true);
                if (Math.random() < 0.01) this.state = 'sleeping';
                break;
            case 'happy':
                if (this.tryFollow()) {
                    break;
                }
                // wander (yay)
                this.move(true);
                if (Math.random() < 0.01) this.state = 'happysleeping';
                break;
            case 'sleeping':
                if (this.wakeUpMaybe()) {
                    this.state = 'active';
                    break;
                }
                // wander (drifting)
                this.move(false);
                break;
            case 'happysleeping': case 'happysleepingsecret':
                if (this.wakeUpMaybe()) {
                    this.state = 'happy';
                    break;
                }
                // wander (drifting)
                this.move(false);
                break;
        }
    }

    wakeUpMaybe() {
        for (const player of this.playerData.onlinePlayers) {
            const dx = player.x - this.x;
            const dy = player.y - this.y;
            if (dx*dx + dy*dy*2 <= MONSTER_ATTACK_REACH*MONSTER_ATTACK_REACH) return true;
        }
        return false;
    }

    move(updateFacing = false) {
        // choose spot to move to
        let dx = Math.round(Math.random()*2) - 1;
        let dy = Math.round(Math.random()*2) - 1;
        const targetx = this.x + dx;
        const targety = this.y + dy;
        // check if valid space
        for (let ddx = -1; ddx <= 1; ddx++) {
            for (let ddy = -1; ddy <= 1; ddy++) {
                const blockThere = this.map.getBlockWithoutLoading(
                    targetx+ddx, targety+ddy, 'B');
                if (blockThere !== 'w' && blockThere[0] !== '>' && blockThere[0] !== '-')
                    return false;
            }
        }
        // execute the jump
        if (updateFacing && targetx > this.x) this.facing = 'r';
        if (updateFacing && targetx < this.x) this.facing = 'l';
        this.x = targetx;
        this.y = targety;
        return true;
    }

    spawnSomewhere() {
        if (this.playerData.onlinePlayers.length === 0) {
            // abort
            return false;
        }
        // who?
        const victim = this.playerData.onlinePlayers[
            Math.floor(Math.random()*this.playerData.onlinePlayers.length)];
        // where?
        const dx = Math.floor(Math.random()*2*SPAWN_RADIUS)-SPAWN_RADIUS;
        const dy = Math.floor(Math.random()*2*SPAWN_RADIUS)-SPAWN_RADIUS;
        return this.spawn(victim.x+dx, victim.y+dy);
    }

    spawn(x, y, force = false) {

        const biome = this.map.getBiome(x, y);
        if (!force) {
            // check this spot for okay-ness
            if (biome !== 'o') {
                return false;
            }
            // monster needs 5 by 5 water
            for (let dy = -2; dy <= 2; dy++) {
                for (let dx = -2; dx <= 2; dx++) {
                    if (this.map.getBlockWithoutLoading(
                        x+dx, y+dy, 'unloaded') !== 'w') {
                        return false;
                    }
                }
            }
            // also check for nearby players
            for (const anyplayer of this.playerData.onlinePlayers) {
                const dx = anyplayer.x - x;
                const dy = anyplayer.y - y;
                const sqdist = dx*dx + dy*dy;
                if (sqdist < SAFE_AREA_RADIUS*SAFE_AREA_RADIUS) {
                    // abort because too close to this player
                    return false;
                }
            }
        }

        // spawn the monster
        this.x = x;
        this.y = y;
        this.type = Math.floor(Math.random() * NUM_MOSTER_TYPES);
        this.state = Math.random() < 0.1 ? 'happysleepingsecret' : 'sleeping';
        return true;
    }

    tryAttack() {
        const attackablePlayers = this.playerData.onlinePlayers.filter(player => {
            // no attack if too far
            const dx = player.x - this.x;
            const dy = player.y - this.y;
            if (dx*dx + dy*dy*2 > MONSTER_ATTACK_REACH*MONSTER_ATTACK_REACH) return false;
            const blockThere = this.map.getBlockWithoutLoading(
                player.x, player.y, 'B');
            if (blockThere !== 'w' && blockThere[0] !== '>' && blockThere[0] !== '-') return false;
            return true;
        });
        if (attackablePlayers.length < 1) return false;
        // there is at least one attackable player
        const victim = attackablePlayers[Math.floor(Math.random()*attackablePlayers.length)];
        if (victim.x > this.x) this.facing = 'r';
        if (victim.x < this.x) this.facing = 'l';
        for (let distance = 1.6; distance > 0.8; distance -= 0.2) {
            const targetx = Math.round(this.x + distance * (victim.x - this.x));
            const targety = Math.round(this.y + distance * (victim.y - this.y));
            let numWaterBlocks = 0;
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    const blockThere = this.map.getBlockWithoutLoading(
                        targetx+dx, targety+dy, 'B');
                    if (blockThere === 'w'
                        || blockThere[0] === '>'
                        || blockThere[0] === '-') numWaterBlocks++;
                }
            }
            if (numWaterBlocks < 3) {
                // unable to jump there
                continue;
            }
            // jump there!
            this.x = targetx;
            this.y = targety;
            // steal a block
            const dropx = victim.x + Math.round(Math.random()*2-1);
            const dropy = victim.y + Math.round(Math.random()*2-1);
            const blockThere = this.map.getBlockWithoutLoading(
                dropx, dropy, 'B');
            // check if drop spot is occupied
            if (blockThere !== 'w'
                && blockThere[0] !== '>'
                && blockThere[0] !== '-'
                && blockThere !== ' ') return false;
            const slot_index = Math.floor(Math.random()
                * (victim.inventory.getNumExistingSlots()-3));
            const item = victim.inventory.getSlotAt(slot_index);
            if (item !== null && Monster.isStealable(item.code)) {
                victim.changedInventory = true;
                const item_code = item.code;
                // hehehehe
                if (Inventory.isDataItem(item_code)) {
                    // data item
                    victim.inventory.gainItemAtIndex(item_code, slot_index, -1);
                    const tile = {
                        t: 'grinder',
                        x: dropx,
                        y: dropy,
                        d: item.data,
                    };
                    this.syncher.createTileEntity(tile);
                } else {
                    // non-data item
                    victim.inventory.gainItemAtIndex(item_code, slot_index, -1);
                }
                const item_name = Inventory.item_names[item_code];
                victim.notifs.push({p:'-', a:1, s:' '+item_name});
                // Fake relativeCheck function,
                // just to be able to use item2block
                const relativeCheck = (rx,ry)=>' ';
                const block = Inventory.item2block(item_code, relativeCheck);
                this.syncher.serverChangeBlock(dropx, dropy, block);
            }
            return true;
        }
        return false;
    }

    tryFollow() {
        const followablePlayers = this.playerData.onlinePlayers.filter(player => {
            // no attack if too far
            const dx = player.x - this.x;
            const dy = player.y - this.y;
            return !(dx*dx + dy*dy*2 > MONSTER_ATTACK_REACH*MONSTER_ATTACK_REACH);
        });
        if (followablePlayers.length < 1) return false;
        // there is at least one followable player
        const victim = followablePlayers[Math.floor(Math.random()*followablePlayers.length)];

        for (let distance = 0.8; distance > 0.2; distance -= 0.2) {
            const targetx = Math.round(this.x + distance * (victim.x - this.x));
            const targety = Math.round(this.y + distance * (victim.y - this.y));
            let numWaterBlocks = 0;
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    const blockThere = this.map.getBlockWithoutLoading(
                        targetx+dx, targety+dy, 'B');
                    if (blockThere === 'w'
                        || blockThere[0] === '>'
                        || blockThere[0] === '-') numWaterBlocks++;
                }
            }
            if (numWaterBlocks < 3) {
                // unable to jump there
                continue;
            }
            // jump there!
            this.x = targetx;
            this.y = targety;
            return true;
        }
        return false;
    }

}

exports.Monsters = Monsters;
exports.Monster = Monster;
