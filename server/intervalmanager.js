// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

class IntervalManager {
    constructor() {
        this.registered = [];
    }

    startGameLoops() {
        this.interval = setInterval(()=>this.tick100(), 100);
    }

    register(name, period, callback) {
        if (this.isRegistered(name)) {
            console.error('Attempted re-registration of '+name);
            return;
        }
        if (period < 1) period = 1;
        this.registered.push({
            name: name,
            period: period,
            cooldown: period,
            callback: callback,
        });
        this.sortAlphabetically();
    }

    sortAlphabetically() {
        this.registered = this.registered.sort(
            (r1, r2) => r1.name < r2.name);
    }

    unregister(name) {
        this.registered = this.registered.filter(r=>r.name!==name);
    }

    isRegistered(name) {
        return this.registered.filter(r=>r.name===name).length > 0;
    }

    tick100() {
        for (const r of this.registered) {
            r.cooldown -= 100;
            while (r.cooldown < 0) {
                r.callback();
                r.cooldown += r.period;
            }
        }
    }

}

exports.IntervalManager = IntervalManager;
