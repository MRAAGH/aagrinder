// Copyright (C) 2021-2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const { promisify } = require('util');
const fs = require('fs');
const { ElementColors } = require('./elementcolors');

function generateRegistration(props) {
    console.log('Generating registration file...');
    if (fs.existsSync('mx-aagrinder-registration.yaml')) {
        console.log('Registration file already exists!');
        throw new Error('Registration file already exists!');
    }
    try {
        const { v4: uuid } = require('uuid');
        const reg = {
            'as_token': uuid(),
            'hs_token': uuid(),
            'id': props.matrix_appservice_id,
            'namespaces': {
                users: [{
                    exclusive: true,
                    regex: `@_${props.matrix_appservice_id}_.*`,
                }],
                rooms: [],
                aliases: [],
            },
            'protocols': [ ],
            'rate_limited': false,
            'sender_localpart': `_${props.matrix_appservice_id}_bot`,
            'url': props.matrix_appservice_url,
        };
        try{
            const yaml = require('js-yaml');
            fs.writeFileSync('mx-aagrinder-registration.yaml', yaml.dump(reg));
        }
        catch (err) {
            console.log(err);
            console.log('Unable to save registration file (please install js-yaml)');
            return;
        }
    }
    catch (err) {
        console.log(err);
        console.log('Unable to generate registration file (please install uuid)');
        return;
    }
}

class AagrinderMatrixAppservice {
    constructor(props, playerData) {

        this.props = props;
        this.playerData = playerData;
        this.matrixAppserviceRunning = false;
        if (this.props.matrix_appservice_enabled !== true) return;

        try {
            this.register();
        }
        catch (err) {
            LOG.logonly(err);
            LOG.log('No Matrix support (please install matrix-bot-sdk)');
            process.exit(-1);
        }

        this.start();
    }

    register() {
        const {
            Appservice,
            IAppserviceRegistration,
            SimpleRetryJoinStrategy,
            AutojoinRoomsMixin,
            MemoryStorageProvider,
        } = require('matrix-bot-sdk');

        const homeserverUrl = this.props.matrix_homeserver_url;

        let registration = null;
        try {
            const yaml = require('js-yaml');
            registration = yaml.load(fs.readFileSync('mx-aagrinder-registration.yaml', 'utf8'));
        } catch (err) {
            LOG.log('Failed to load registration file', err);
            LOG.log('(make sure that js-yaml is installed!)');
            process.exit(-1);
        }
        if (!registration) {
            LOG.log('Registration file seems blank');
            process.exit(-1);
        }

        const storage = new MemoryStorageProvider();

        this.appservice = new Appservice({
            bindAddress: this.props.matrix_appservice_bindaddress,
            homeserverName: this.props.matrix_appservice_domain,
            homeserverUrl: this.props.matrix_homeserver_url,
            port: this.props.matrix_appservice_port,
            storage: storage,
            registration: registration,
            joinStrategy: new SimpleRetryJoinStrategy(),
        });

        AutojoinRoomsMixin.setupOnAppservice(this.appservice);
    }


    async start() {
        LOG.log('Starting application service....');

        this.appservice.on('room.message', (roomId, event) => {
            this.handleRemoteChat(roomId, event);
        });

        await this.appservice.begin();
        LOG.log('Application service started!');
        this.matrixAppserviceRunning = true;

    }

    async handleLocalChat(player, typed) {
        if (this.matrixAppserviceRunning) {
            // do not bridge private message
            if (typed.length > 2 && typed.substr(0,2) === 'L ') return;
            const message = typed;
            const suffix = player.name.toLowerCase().replace(/[^a-z0-9]/g, '_');
            const intent = this.appservice.getIntentForSuffix(suffix)
            await intent.ensureRegistered();
            await this.ensureProfile(player, intent.underlyingClient);

            intent.sendText(this.props.matrix_room_id, message);
        }
    }

    async handleRemoteChat(roomId, event) {
        if (!event['content']) return;
        // ignore other rooms
        if (roomId !== this.props.matrix_room_id) return;
        const sender = event['sender'];
        // ignore my own accounts
        const reg = new RegExp(
            '^@_'+this.props.matrix_appservice_id+'_.*:'
            +this.props.matrix_appservice_domain+'$')
        if (reg.test(sender)) return;
        const body = event['content']['body'];
        let displayName;
        let nameColor;
        try {
            const profile = await this.appservice.botIntent
                .underlyingClient.getUserProfile(sender);
            displayName = profile.displayname;
            nameColor = ElementColors.getUserNameColor(sender);
        } catch (err) {
            LOG.log(err);
            displayName = '[MX UNKNOWN]';
            nameColor = '#ffffff';
        }
        const message = '\\['+nameColor+'\\]'+displayName + '\\[/\\]: ' + body;
        // broadcast this prepared message
        for (const sendPlayer of this.playerData.onlinePlayers) {
            sendPlayer.sendTimestampedChat(message, true);
        }
    }

    async ensureProfile(player, matrixClient) {
        // need to update matrix profile?
        if (player.color !== player.currentlySetMatrixColor) {
            player.currentlySetMatrixColor = player.color;
            // update profile.
            await matrixClient.setDisplayName(player.name);
            try {
                const sharp = require('sharp');
                const svg = AagrinderMatrixAppservice.makeSVG(player.color);
                const svgBuffer = Buffer.from(svg);
                // convert to png using sharp
                const pngBuffer = await sharp(svgBuffer).png().toBuffer();
                const mxc = await matrixClient.uploadContent(
                    pngBuffer,
                    'image/png',
                    `P_${player.color}.png`
                );
                await matrixClient.setAvatarUrl(mxc);
            } catch (err) {
                LOG.logonly(err);
                LOG.log('No profile picture support (please install sharp)');
            }
        }
    }

    static makeSVG(color) {
        // make a simple vector image. The letter P in dejavu-sans
        // in a specified color on a black background.
        return `<svg
          width="20mm"
          height="20mm"
          viewBox="0 0 50 50"
          version="1.1"
          xmlns:svg="http://www.w3.org/2000/svg">
          <rect
            style="fill:#000000"
            width="50" height="50" x="0" y="0" />
          <g
            transform="matrix(0.26458333,0,0,0.26458333,-0.8322787,6.3210284)"
            style="fill:#${color}">
            <path d="m 80.736328,36.454704 v 35.0625 h 15.875 q 8.812502,0 13.625002,-4.5625 4.8125,-4.5625 4.8125,-13 0,-8.375 -4.8125,-12.9375 -4.8125,-4.5625 -13.625002,-4.5625 z m -12.625,-10.375 h 28.5 q 15.687502,0 23.687502,7.125 8.0625,7.0625 8.0625,20.75 0,13.8125 -8.0625,20.875 -8,7.0625 -23.687502,7.0625 h -15.875 V 119.3922 h -12.625 z" />
          </g>
        </svg>`;
    }

}

exports.AagrinderMatrixAppservice = AagrinderMatrixAppservice;
exports.generateRegistration = generateRegistration;
