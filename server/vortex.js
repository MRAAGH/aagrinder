// Copyright (C) 2024 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

"use strict";

const Inventory = require('../shared/inventory').Inventory;
const BP = require('../shared/blockprops').BlockProps;

const VORTEX_TELEPORT_DISTANCE = 300;
const ATTACK_RADIUS = 5;
const AGRO_RADIUS = 45;
const DEAGRO_RADIUS = 68;
const DESPAWN_TIME = 1800;  // 120 = 1 minute
const MAX_HINT_DIST = 800;
const CAREFUL_RADIUS = 50;
const DISINTEGRATE_RADIUS = 3;

class Vortex {

    constructor(intervalManager, map, playerData, subscribe, syncher) {
        this.map = map;
        this.playerData = playerData;
        this.subscribe = subscribe;
        this.syncher = syncher;
        this.existencechanged = false;
        this.state = 'completelygone';
        this.x = 0;
        this.y = 0;
        this.prevx = 0;
        this.prevy = 0;
        this.face = '';
        this.target = '';
        this.hints = [];
        this.hintCache = [];
        this.speed = 0;
        this.despawnTimer = 0;
        this.age = 0;
        intervalManager.register('49vortexhint', 30000,
            () => this.updateHintCache());
        intervalManager. register('50vortex', 500,
            () => this.doVortexTick500());
    }

    exists() {
        return this.state !== 'completelygone';
    }

    doVortexTick500() {
        if (this.checkDespawn()) {
            this.despawn();
            return;
        }
        if (this.playerData.onlinePlayers.length === 0) {
            // vortex movement is paused when there are no players
            return;
        }
        this.updateTarget();
        this.tryAttack();
        this.move();
        this.updateHints();
        this.careful();
        this.face = this.chooseFace();
        this.age++;
        if (this.state === 'spawning' && this.age === 44) this.state = 'hunting';
    }

    updateTarget() {
        if (this.state === 'completelygone') return;

        // check if already has target player
        if (this.target !== '') {
            const player = this.playerData.onlinePlayerByName(this.target);
            if (player === null) {
                this.target = '';
            } else {
                const dx = player.x - this.x;
                const dy = player.y - this.y;
                const sqdist = dx*dx + 4*dy*dy;
                if (sqdist > DEAGRO_RADIUS*DEAGRO_RADIUS) {
                    this.target = '';
                }
            }
        }

        if (this.target === '') {
            // find a new target
            for (const anyplayer of this.playerData.onlinePlayers) {
                const dx = anyplayer.x - this.x;
                const dy = anyplayer.y - this.y;
                const sqdist = dx*dx + 4*dy*dy;
                // check if within agro radius
                if (sqdist < AGRO_RADIUS*AGRO_RADIUS) {
                    this.target = anyplayer.name;
                    this.speed = 0;
                    break;
                }
            }
        }
    }

    checkDespawn() {
        // can only despawn while hunting
        if (this.state !== 'hunting') return false;
        // has despawn condition?
        const yesPlayerOnline = this.playerData.onlinePlayers.length > 0;
        if (yesPlayerOnline) {
            this.despawnTimer = 0;
            return false;
        }
        // despawn timer
        this.despawnTimer++;
        if (this.despawnTimer < DESPAWN_TIME) return false;
            return true;
    }

    despawn() {
        this.state = 'completelygone';
        this.existencechanged = true;
        const x = parseInt(this.x);
        const y = parseInt(this.y);
        for (let dx = -2; dx <= 2; dx++) {
            for (let dy = -2; dy <= 2; dy++) {
                if (Math.random() > 0.3) continue;
                const b = this.map.getBlock(x+dx, y+dy);
                if (b === 'd') {
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'WS');
                } else if (b === 'B') {
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'WB');
                } else if (b === ' ' || b === 'w') {
                    this.syncher.serverChangeBlock(x+dx, y+dy, 'Ws');
                    this.syncher.createTileEntity({
                        t: 'sspooky',
                        x: x+dx,
                        y: y+dy,
                    });
                }
            }
        }
    }

    tryAttack() {
        if (this.state === 'completelygone') return;
        if (this.state === 'spawning') return;

        for (const player of this.playerData.onlinePlayers) {
            const dx = player.x - this.x;
            const dy = player.y - this.y;
            const sqdist = dx*dx + 2*dy*dy;
            if (sqdist < ATTACK_RADIUS*ATTACK_RADIUS) {
                this.executeAttack(player);
            }
        }
    }

    executeAttack(player) {
        let targetx;
        let targety;
        let i = 0;
        while (i < 100) {
            // choose a target position
            const distance = Math.random()*100 + VORTEX_TELEPORT_DISTANCE-50;
            const angle = Math.random()*Math.PI*2;
            targetx = player.x + Math.floor(Math.cos(angle)*distance);
            targety = player.y + Math.floor(Math.sin(angle)*distance);
            // check if spot is clear
            const blockThere = this.map.getBlock(targetx, targety);
            if (BP.isEnterable(blockThere)) break;
            i++;
        }
        if (i >= 100) {
            // you got lucky this time
            console.log('vortex failed to attack');
            return false;
        }
        this.syncher.movePlayer(player, targetx, targety);
        this.subscribe.resubscribe(player, true);
        player.animation = 'ghost';
        const message = '\\[#'+player.color+'\\]'+player.name
            +' \\[#aa3\\]crossed the event horizon';
        LOG.log(message);
        for (const otherplayer of this.playerData.onlinePlayers) {
            otherplayer.sendChat(message);
        }
    }

    move() {
        if (this.state === 'completelygone') return;
        if (this.state === 'spawning') return;
        if (this.target === '') {
            // wander
            this.x += Math.random()-0.5;
            this.y += Math.random()-0.5;
            this.speed = 0;
            if (this.hints.length > 0) {
                const hint = this.hints[0];
                const dx = hint.x - this.x;
                const dy = hint.y - this.y;
                const sqdist = dx*dx + dy*dy;
                if (sqdist < MAX_HINT_DIST * MAX_HINT_DIST) return;
                const normdx = dx / Math.sqrt(sqdist);
                const normdy = dy / Math.sqrt(sqdist);
                this.x += normdx*0.3;
                this.y += normdy*0.3;
            }
            return;
        }
        const player = this.playerData.onlinePlayerByName(this.target);
        if (player === null) {
            this.speed = 0;
            return;
        }

        const dx = player.x - this.prevx;
        const dy = player.y - this.prevy;
        const sqdist = dx*dx + dy*dy;
        const dist = Math.sqrt(sqdist);

        // while agro, increase speed
        this.speed += 0.2;
        if (this.speed > 15) {
            this.speed = 15;
        }

        this.prevx = this.x;
        this.prevy = this.y;
        this.x += dx/dist*this.speed;
        this.y += dy/dist*this.speed;

        if (this.speed > dist) {
            // Immediately execute attack on player
            this.executeAttack(player);
        }

    }

    addToHintCache(x, y) {
        for (const xy of this.hintCache) {
            if (xy.x === x && xy.y === y) return;
        }
        this.hintCache.push({x:x, y:y});
    }

    updateHintCache() {
        if (!this.exists()) return;
        // find all loaded warp tile entities
        for (const chunky in this.map.chunks) {
            for (const chunkx in this.map.chunks[chunky]) {
                const chunk = this.map.chunks[chunky][chunkx];
                const tileEntities = chunk.tileEntities;
                // iterate all tile entities per loaded chunk
                for (const tile of tileEntities) {
                    if (tile.t !== 'warp') continue;
                    this.addToHintCache(tile.x, tile.y);
                }
            }
        }
        // at this point, nearby hint is not guaranteed yet.
        // So we ask aagrinder-terrain for a warp in this region,
        // and add it to the cache:
        const chunkx = this.x>>8;
        const chunky = this.y>>8;
        this.map.getChunkInfosAsync([{x: chunkx, y: chunky}]).then(infos => {
            if (infos.length < 1 || infos[0].warp === undefined) {
                console.log('warp lookup via aagrinder-terrain failed!');
                return;
            }
            this.addToHintCache(infos[0].warp.x, infos[0].warp.y);
        }, err => {console.log('warp lookup not possible! You need to update aagrinder-terrain!');});
    }

    updateHints() {
        this.hints = [];
        if (this.state === 'spawning') return;
        if (this.hintCache.length < 1) return;
        let minHintX = this.hintCache[0].x;
        let minHintY = this.hintCache[0].y;
        for (const hint of this.hintCache) {
            const dx = hint.x - this.x;
            const dy = hint.y - this.y;
            const sqdist = dx*dx + dy*dy;
            const mindx = minHintX - this.x;
            const mindy = minHintY - this.y;
            const minsqdist = mindx*mindx + mindy*mindy;
            if (sqdist < minsqdist) {
                minHintX = hint.x;
                minHintY = hint.y;
            }
        }
        this.hints = [{x: minHintX, y: minHintY}];
    }

    careful() {
        if (this.state === 'completelygone') return;
        if (this.state === 'spawning') return;
        let minHintX = 0;
        let minHintY = 0;
        let minHintDist = 9999;
        for (const hint of this.hints) {
            const dx = hint.x - this.x;
            const dy = hint.y - this.y;
            const sqdist = dx*dx + 4*dy*dy;
            const dist = Math.sqrt(sqdist);
            if (dist < minHintDist) {
                minHintDist = dist;
                minHintX = hint.x;
                minHintY = hint.y;
            }
        }
        if (minHintDist > CAREFUL_RADIUS) {
            // no reason to be careful. Back to hunting
            this.state = 'hunting';
            return;
        }
        // Be careful of this hint
        this.state = 'careful';
        this.carefulAnimationAt(minHintX, minHintY);
        // speed limit
        if (this.speed > 0.6) {
            this.speed *= 0.7;
        }
        // gets pulled towards hint
        const force = 1+5/(minHintDist/CAREFUL_RADIUS+1);
        const dx = minHintX - this.x;
        const dy = minHintY - this.y;
        this.x += dx/minHintDist*force;
        this.y += dy/minHintDist*force;
        if (minHintDist > DISINTEGRATE_RADIUS) {
            // No reason to disintegrate
            return;
        }
        // disintegrate.
        this.state = 'completelygone';
        this.existencechanged = true;
        for (let i = 0; i < 8; i++) {
            this.spawnWarpCoreNear(minHintX, minHintY);
        }
        this.disintegrateAnimationAt(minHintX, minHintY+1)
    }

    spawnWarpCoreNear(x, y) {
        for (let i = 0; i < 500; i++) {
            const dx = Math.floor(Math.random()*14-7);
            const dy = Math.floor(Math.random()*5-2);
            const blockThere = this.map.getBlock(x+dx, y+dy);
            if (blockThere !== ' '
                && blockThere !== 'w'
                && blockThere[0] !== '>'
                && blockThere[0] !== 'a'
                && blockThere[0] !== 'A'
                && blockThere[0] !== 'f'
                && !(blockThere[0] === 'W' && blockThere[1] === 'a')
                && !(blockThere[0] === 'W' && blockThere[1] === 'f')) continue;
            this.syncher.serverChangeBlock(x+dx, y+dy, 'WC');
            // make warp core invisible until animation finished
            this.syncher.serverBroadcastAnimation({
                x: x+dx,
                y: y+dy,
                b: blockThere,
                a: 8,
                d: 0,
            });
            return;
        }
    }

    chooseFace() {
        if (this.state === 'completelygone') return '';
        if (this.state === 'careful') return 'afraid';
        if (this.state === 'hunting') {
            const biomeHere = this.map.getBiome(parseInt(this.x), parseInt(this.y));
            if (biomeHere === 'y') return 'uwu';
            // hunting, either with terget, or without
            if (this.target === '') return 'awake';
            const player = this.playerData.onlinePlayerByName(this.target);
            if (player === null) return 'awake';
            return 'angryplayer';
        }
        if (this.state === 'spawning') {
            if (this.age < 10) return '';
            if (this.age < 20) return 'sleep';
            if (this.age < 30) return 'awake';
            if (this.age < 38) return 'lookplayer';
            if (this.age < 40) return 'angrysleep';
            return 'angryplayer';
        }
        return '';
    }

    spawnAt(x, y) {
        this.existencechanged = true;
        this.state = 'spawning';
        this.x = x;
        this.y = y;
        this.prevx = x;
        this.prevy = y;
        this.face = '';
        this.target = '';
        this.hints = [];
        this.speed = 0;
        this.despawnTimer = 0;
        this.age = 0;
    }

    carefulAnimationAt(x, y) {
        for (let i = 0; i < 4; i++) {
            const anix = this.x + (Math.random()-0.5)*6;
            const aniy = this.y + (Math.random()-0.5)*4;
            for (let j = 1; j <= 5; j++) {
                this.syncher.serverBroadcastAnimation({
                    x: Math.round(x+(anix-x)*j/5),
                    y: Math.round(y+(aniy-y)*j/5),
                    b: 'W#',
                    a: 5-j + 2,
                    d: 5-j,
                });
            }
        }
    }

    disintegrateAnimationAt(x, y) {
        // prepare shape
        const _ = -1;
        const ANIMATION = [
           //0 1 2 3 4 5 6 7 x
            [_,7,7,6,_,_,_,_],//y 0
            [_,_,6,5,5,5,_,_],//y 1
            [_,_,_,5,5,4,4,_],//y 2
            [_,_,_,_,4,4,4,3],//y 3
            [_,_,_,_,3,3,3,3],//y 4
            [_,_,_,_,2,2,3,3],//y 5
            [_,_,_,1,1,2,2,2],//y 6
            [_,_,0,1,1,1,2,_],//y 7
            [0,0,0,0,_,_,_,_],//y 8
        ]
        for (let i = 0; i < ANIMATION.length; i++) {
            for (let j = 0; j < ANIMATION[0].length; j++) {
                const ani = ANIMATION[i][j];
                if (ani === _) continue;
                this.syncher.serverBroadcastAnimation({
                    x: x + j,
                    y: y + i,
                    b: 'W#',
                    a: ani + 3,
                    d: ani,
                });
                this.syncher.serverBroadcastAnimation({
                    x: x - j,
                    y: y - i,
                    b: 'W#',
                    a: ani + 3,
                    d: ani,
                });
            }
        }
    }

    saveToJSON() {
        return {
            state: this.state,
            x: this.x,
            y: this.y,
        };
    }

    loadFromJSON(object) {
        if (typeof object === 'undefined') return;
        this.state = object.state;
        this.x = object.x;
        this.y = object.y;
    }

}

exports.Vortex = Vortex;
