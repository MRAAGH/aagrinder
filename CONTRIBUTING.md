# Code style

Line length should be kept at most 79 characters, if possible.
Bonus points for line length 71 and lower.

Use empty lines to separate long blocks into sensible paragraphs.

Always use `===` and `!==`, not `==` and `!=`.

Use `delete` instead of `= undefined`.

use `const` where possible, otherwise `let`. Never `var`.

Always use `;` after every statement.

Correct use of braces and spaces:
```
if (condition) {
    something;
} else if (condition) {
    something;
} else {
    something;
}
```

Prefer negative checks over positive:
```
while (meow) {
    if (condition) {  // AVOID
        if (condition2) {  // AVOID
            something;
        }
    }
}

while (meow) {
    if (!condition) continue;  // CORRECT
    if (!condition2) continue;  // CORRECT
    something;
}

function meow() {
    if (condition) {  // AVOID
        if (condition2) {  // AVOID
            something;
        }
    }
}

function meow() {
    if (!condition) return;  // CORRECT
    if (!condition2) return;  // CORRECT
    something;
}
```

# Coding guidelines

Even though this is Javascript, I am trying to keep the code reasonably optimized. Avoid unnecessary iteration and data structures, especially in code that is often executed (more than once per second during normal gameplay).

As a general rule, design new game mechanics in a way where they can be implemented efficiently. When coding safety checks, be careful that your safety checks do not execute much longer than the rest of the relevant code.
