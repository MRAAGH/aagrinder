// Copyright (C) 2018-2023 MRAAGH

/* This file is part of AAGRINDER.
 * 
 * AAGRINDER is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * AAGRINDER is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with AAGRINDER.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * Main server file.
 * I try to put code somewhere else.
 */

const SERVER_VERSION = '1.0.0';

const { promisify } = require('util');
const execAsync = promisify(require('child_process').exec);
execAsync('git rev-parse HEAD').then(a =>
    SERVER_GIT_COMMIT = a.stdout.trim());

const express = require('express');
const app = express();
// @ts-ignore
const http = require('http').Server(app);
const io = require('socket.io')(http, {cookie: false});
const bodyParser = require('body-parser');

const loadServerProperties = require('./server/properties').load;
Logger = require('./server/logger').Logger;

LOG = new Logger();  // global logger

app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true, parameterLimit:500}));

const Accounts = require('./server/accounts').Accounts;
const Server = require('./server/server').Server;

process.stdin.resume();
process.stdin.setEncoding('utf8');

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/shared'));

console.log('AAGRINDER  Copyright (C) 2018-2024  MRAAGH and contributors');
console.log('This program comes with ABSOLUTELY NO WARRANTY');
console.log('This is free software, and you are welcome to redistribute it');
console.log('under certain conditions; for details see:');
console.log('https://www.gnu.org/licenses/agpl-3.0-standalone.html');
console.log('The source code for this program can be found at:');
console.log('https://gitlab.com/MRAAGH/aagrinder');
console.log('https://gitlab.com/MRAAGH/aagrinder-terrain');
console.log('');

const props = loadServerProperties();

LOG.initialize(props.enable_server_log);

LOG.log('Starting AAGRINDER server');

if(props.everyone_is_admin === true){
    LOG.log('WARNING: ALL PLAYERS CAN RUN ADMIN COMMANDS!');
}

if(props.enable_debug_log === true){
    LOG.log('WARNING: DEBUG LOG IS ENABLED AND USES UNLIMITED RAM!');
}



// set globals
SERVER_ALLOW_REGISTRATION = props.allow_registration;
alreadyReceivedSIGINT = false;
// accounts are global because we want to access them from the API too
ACCOUNTS = new Accounts(props);

// expose api
app.use('/api/users', require('./server/api/user'));

async function launchServer(){
    const hrstart_server_load = process.hrtime();
    const server = new Server(props);
    process.stdin.on('data', text => {
        server.command(text.trim());
    });
    console.log('Preparing level "' + props.level_name + '"');
    await server.loadFromFileOrConvert();
    await server.prepareSpawnArea();
    if (props.enable_autosave) {
        // autosave is enabled
        // Immediately save the freshly generated world
        await server.saveToFile();
        // Periodically save the world
        setInterval(() => server.saveToFile(),
            props.autosave_minutes*60000);
        // Save the world on keyboard interrupt
        process.on('SIGINT', async () => {
            if (alreadyReceivedSIGINT) return;
            alreadyReceivedSIGINT = true;
            console.log('KeyboardInterrupt');
            await server.saveToFile()
            server.stopServer()
        });
    }
    server.startGameLoops();
    server.listen(http, io, props.server_port);
    const hrend_server_load = process.hrtime(hrstart_server_load);
    const server_load_ms = Math.floor(hrend_server_load[1] / 1000000);
    const server_load_s = hrend_server_load[0] + '.' + server_load_ms;
    console.log('Done (' + server_load_s + 's)!');
    console.log('AAGRINDER server listening on *:' + props.server_port);
    console.log('http://127.0.0.1:' + props.server_port);
    console.log('type "help" for a list of commands!');
}

launchServer();
